# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/focal64"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  config.vm.synced_folder ".", "/vagrant"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    vb.memory = 4096
    vb.cpus = 2
  end

  config.vm.provision "shell", inline: <<-SHELL
    echo "deb [trusted=yes] http://hop.inria.fr/linux/Ubuntu focal hop" > /etc/apt/sources.list.d/hop.list
    apt-get -qq update
    apt-get install -y dh-make
    apt-get install -y libssl1.0.2
    apt-get install -y libssl-dev
    apt-get install -y libsqlite3-0
    apt-get install -y libsqlite3-dev
    apt-get install -y libasound2
    apt-get install -y libasound2-dev
    apt-get install -y libflac8
    apt-get install -y libflac-dev
    apt-get install -y libmpg123-0
    apt-get install -y libmpg123-dev
    apt-get install -y libavahi-core7
    apt-get install -y libavahi-core-dev
    apt-get install -y libavahi-common-dev
    apt-get install -y libavahi-common3
    apt-get install -y libavahi-client3
    apt-get install -y libavahi-client-dev
    apt-get install -y libunistring0
    apt-get install -y libunistring-dev
    apt-get install -y libpulse-dev
    apt-get install -y libpulse0
    apt-get install -y automake
    apt-get install -y libtool
    apt-get install -y libgmp-dev
    apt-get install -y libgmp3-dev
    apt-get install -y libgmp10

    apt-get install -y hop
  SHELL

  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    echo "export SJS_PATH=/vagrant" >> /home/vagrant/.bashrc
    echo "export PATH=$PATH:/vagrant/bin" >> /home/vagrant/.bashrc
    echo "export HOPC_PATH=/opt/hop/bin/hopc" >> /home/vagrant/.bashrc
  SHELL
end
