#!/bin/bash

if [[ "$SJS_PATH " == " " ]]; then
	SJS_PATH=~/Works/JavaScript/sjs/sjs-compiler
fi
if [[ "$HOPC_PATH " == " " ]]; then
	HOPC_PATH=~/Works/JavaScript/hop/bin/hopc
	HOPC_PATH=hopc
fi

SJS_COMPILER=$SJS_PATH/src/sjscomp.hop
SJS_REWRITE=$SJS_PATH/src/reorder.hop
SJS_MALICIOUS=$SJS_PATH/src/malicious.js
SJS_BOOTSTRAP=$SJS_PATH/src/bootstrap.js
SJS_BOOTSTRAP_JERRY=$SJS_PATH/src/bootstrap.jerry.js
JSE_BOOTSTRAP=$SJS_PATH/src/bootstrap-jsexp.js

color_code=(
	"\033[1;35m" # text_s
       	"\033[37m"   # text
	"\033[1;32m" # info_s
	"\033[0;32m" # info
	"\033[1;31m" # warn_s
	"\033[0;31m" # warn
	)

msg() {
	if [ -n "$NO_ANSI" ];then
		shift
		echo -e "$@"
	else
		c=$1
		shift
		case $c in
			text_s) echo -e "${color_code[0]}$@\033[m";;
			text) echo -e "${color_code[1]}$@\033[m";;
			info_s) echo -e "${color_code[2]}$@\033[m";;
			info) echo -e "${color_code[3]}$@\033[m";;
			warn_s) echo -e "${color_code[4]}$@\033[m";;
			warn) echo -e "${color_code[5]}$@\033[m";;
			*) echo -e "$@";;
		esac
	fi	
}

sjsenv () {
	export SJS_PATH
	export SJS_COMPILER
	export SJS_BOOTSTRAP
	echo "SJS_PATH: $SJS_PATH"
	echo "SJS_COMPILER: $SJS_COMPILER"
	echo "SJS_BOOTSTRAP: $SJS_BOOTSTRAP"
	export PATH=$PATH:$SJS_PATH/bin
}

usage_sjscompile () {
	cat << EOF
Usage: `basename $0` [options]
	-a: includes bootstrapping code.
	-m: to add /src/malicious.js
	-r: does not apply secure compilation. only parsing/sourcemap/symbol/letfusion/pp
	-o: use hop test bootstrap.
	-j: use jerryscript bootstrap.
	-k: use jerryscript test bootstrap.
	-p: use "jsexp mode"
	-v: verbose
EOF
	exit
}

sjscompile () {
	local OPTIND OPT all es6 old malicious new raw steps debug_mode hoptest res jerry jerrytest nodejs_opt v2
	debug_mode=false
	jsexp_mode=false
	opt_mode=false
	stat_mode=false
	v2=""
	while getopts "phaonmrdjkov6st" OPT;do
		case "$OPT" in
			a) all=true;;
			m) malicious=true;;
			r) raw=true;;
			o) hoptest=true;;
			k) jerry=true;jerrytest=true;;
			j) jerry=true;;
			h) usage_sjscompile;;
			p) jsexp_mode=true;;
			d) debug_mode=true;;
			t) opt_mode=true;all=true;;
			v) HOPTRACE="j2s:stage"; v2="-v2";;
		esac
	done
	shift $((OPTIND-1))
	[[ $all = true ]] && cat $SJS_BOOTSTRAP
	[[ $jsexp_mode = true ]] && cat $JSE_BOOTSTRAP
	if [[ $jerry = true ]];then
		sed 's/instrumented_test = false/instrumented_test = true/g' $SJS_BOOTSTRAP_JERRY 
	fi
	[[ $malicious = true ]] && cat $SJS_MALICIOUS
	if [[ $raw ]];then
		steps="syntax,sourcemap,symbol,letfusion,javascript"
	else
		steps="syntax,$SJS_REWRITE,sourcemap,symbol,use,$SJS_COMPILER,letfusion,javascript"
	fi

	[[ $hoptest = true ]] && echo "var _\$_secret_env_\$_ = require('./bootstrap.hop.js');"
	[[ $hoptest = true ]] && echo "#:tprint('Start');"
	[[ $jerrytest = true ]] && echo "print(':Start');"

	$HOPC_PATH --js-es5 $v2 \
		   --js-option djs-test $debug_mode \
		   --js-option commonjs-export false \
		   --js-option jsexp-mode $jsexp_mode \
		   --js-option opt-mode $opt_mode \
		   --js-driver "$steps" $1 -j
	res=$?

	[[ $hoptest = true ]] && echo "#:tprint('End');"
	[[ $jerrytest = true ]] && echo "print(':End');"
	return $res
}

djsadd () {
	case $1 in
		todo) tf="TODOLIST";;
		bl) tf="BLACKLIST";;
		*) echo "unknown list."; exit;;
	esac
	filename=$(basename -- "$2")
	shift 2

	b=$(grep -o "\"$filename\"" $tf)
	if [[ "$b" == "" ]]; then
		if [ "$1 " != " " ]; then
			echo "# $@" >> $tf
		fi
		echo "\"$filename\"" >> $tf
		echo "$filename is added to $tf with \"$@\""
	else
		echo "$filename is already in $tf"
	fi
}

cmd=`basename $0`

case $cmd in
	"sjsadd" | "sjscompile" | "sjsenv" )
		$cmd $@;;
	*) exit;;
esac

