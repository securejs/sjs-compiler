# JerryScript engine patch for the instrumentation.

## Requirement
- JerryScript engine [link]()
- This patch has been tested on the following commit:
	- 89342ea503e64db7430276d6f1e528d06cbd6c2c

## How to apply
```bash
$ git clone https://github.com/jerryscript-project/jerryscript.git
$ cd jerryscript
$ git reset --hard 89342ea503e64db7430276d6f1e528d06cbd6c2c
$ git am ../0001-yk-added-traps-for-a-prototype-chain-lookup.patch
$ python tools/build.py --line-info=ON
```

