(function() { var log, warn, info; try 
{ (function(_global_) {
    "use strict";

    // will be replaced by script.sh
    const instrumented_test = false;
    let msg = () => {}
    if (instrumented_test) {
	msg = print;
    }
    const native_map = WeakMap;
    const native_mapset = WeakMap.prototype.set;
    const native_mapget = WeakMap.prototype.get;
    const native_maphas = WeakMap.prototype.has;
    const native_setPrototypeOf = Object.setPrototypeOf;
    const Reflect_setPrototypeOf = Reflect.setPrototypeOf;
    const native_apply = Reflect.apply, arg0 = [], arg1 = [null], arg2 = [null, null];
    const Reflect_defineProperty = Reflect.defineProperty;

    function print_wrapper(f) {
	var o = f;
	return function() {
	    var args = [];
	    for (var i=0; i<arguments.length; i++) {
		arguments[i] = ToStringForError(arguments[i]);
	    }
	    return native_apply(o, null, arguments);
	};
    }
    // for test in Node.js
    if (typeof _global_.console === 'object') {
	if (typeof _global_.console.log === 'function') {
	    log = print_wrapper(_global_.console.log);
	} else {
	    log = function() {};
	}
	if (typeof _global_.console.warn === 'function') {
	    warn = print_wrapper(_global_.console.warn);
	} else {
	    warn = function() {};
	}
	if (typeof _global_.console.info === 'function') {
	    info = print_wrapper(_global_.console.info);
	} else {
	    info = function() {};
	}
    } else if (typeof _global_["print"] === "function") {
	log = print_wrapper(_global_.print);
	warn = print_wrapper(_global_.print);
	info = print_wrapper(_global_.print);
    } else {
        log = function() {};
        warn = function() {};
        info = function() {};
    }

    var descTTT = Object.create(null);
    descTTT.configurable =  descTTT.enumerable = descTTT.writable = true;
    var descFFF = Object.create(null);
    descFFF.configurable = descFFF.enumerable = descFFF.writable = false;
    var descValue = Object.create(null);

    var native = {};
    native.global = _global_;
    native.Object = Object;
    native.Function = Function;
    native.Array = Array;
    native.Math = Math;
    native.Date = Date;
    native.Boolean = Boolean;
    native.Number = Number;
    native.String = String;
    native.RegExp = RegExp;
    native.JSON = JSON;
    native.Error = Error;
    native.TypeError = TypeError;
    native.RangeError = RangeError;
    native.SyntaxError = SyntaxError;
    native.ReferenceError = ReferenceError;
    native.URIError = URIError;
    native.EvalError = EvalError;
    native.Reflect = _global_.Reflect;
    const Reflect_getOwnPropertyDescriptor = native.Reflect.getOwnPropertyDescriptor;
    const Reflect_getPrototypeOf = native.Reflect.getPrototypeOf;
    const Object_create = native.Object.create;
    const Object_prototype_hasOwnProperty = native.Object.prototype.hasOwnProperty;
    const Object_getOwnPropertyNames = Object.getOwnPropertyNames;

	// Reflect_defineProperty(new_proto, native.s_symbol_match, {value: native.symbol_match, writable: true, enumerable: false, configurable: true});
	// Reflect_defineProperty(new_proto, native.s_symbol_match_all, {value: native.symbol_match_all, writable: true, enumerable: false, configurable: true});
	// Reflect_defineProperty(new_proto, native.s_symbol_replace, {value: native.symbol_replace, writable: true, enumerable: false, configurable: true});
	// Reflect_defineProperty(new_proto, native.s_symbol_search, {value: native.symbol_search, writable: true, enumerable: false, configurable: true});
	// Reflect_defineProperty(new_proto, native.s_symbol_split, {value: native.symbol_split, writable: true, enumerable: false, configurable: true});

    // String.prototype.replace
    native.s_symbol_replace = Symbol.replace;
    // String.prototype.search
    native.s_symbol_search = Symbol.search;
    // String.prototype.split
    native.s_symbol_split = Symbol.split;
    // String.prototype.match
    native.s_symbol_match = Symbol.match;
    native.s_symbol_match_all = Symbol.matchAll;
    native.s_symbol_toStringTag = Symbol.toStringTag;
    {
	let tmp;
	tmp = Reflect_getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_replace);
	{
	    let nat = tmp.value;
	    native.symbol_replace = function(string, replacedValue) {
		if (load_check_i(this)) {
		    string = ToString(string);
		    if (!IsCallable(replacedValue)) replacedValue = ToString(replacedValue);
		    try {
			msg('secure start');
			return native_apply(nat, this, (arg2[0] = string, arg2[1] = replacedValue, arg2));
		    } finally {
			msg('secure end');
		    }
		} else {
		    throw "TODO";
		}
	    };
	}
	tmp = Reflect_getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_search);
	{
	    let nat = tmp.value;
	    native.symbol_search = function(string) {
		if (load_check_i(this)) {
		    try {
			msg('secure start');
			return native_apply(nat, this, (arg1[0] = string, arg1));
		    } finally {
			msg('secure end');
		    }
		} else {
		    throw "TODO";
		}
	    };
	}
	tmp = Reflect_getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_split);
	{
	    let nat = tmp.value;
	    native.symbol_split = function(string, limit) {
		if (load_check_i(this)) {
		    try {
			msg('secure start');
			return native_apply(nat, this, (arg2[0] = string, arg2[1] = limit, arg2));
		    } finally {
			msg('secure end');
		    }
		} else {
		    throw "TODO";
		}
	    };
	}
	tmp = Reflect_getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_match);
	native.symbol_match = tmp.value;
	tmp = Reflect_getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_match_all);
	if (tmp !== undefined) native.symbol_match_all = tmp.value;
    }
    // // Invariant conditions for the secure programming environment.
    // descFFF.value = undefined;
    // Reflect_defineProperty(String.prototype, native.s_symbol_replace, descFFF);
    // Reflect_defineProperty(String.prototype, native.s_symbol_split, descFFF);

    const internalPrefix = '@';
    const makeI = function(s) { return internalPrefix + s; };
    const isInternal = function(s) {
	return (typeof s === 'string' && s.length > 0 && (s[0] === internalPrefix[0]));
    }
    // monkey patches for internal fields
    native.Object.getOwnPropertyNames = (function() {
	var Object_getOwnPropertyNames = native.Object.getOwnPropertyNames;
	return function (O) {
	    var list = Object_getOwnPropertyNames(O);
	    var nlist = [];
	    var i;
	    for (i=0; i<list.length; i++) {
		if (!isInternal(list[i]))
		    nlist[nlist.length] = list[i];
	    }
	    return nlist;
	}
    })();
	
    native.Object_prototype_toString_native = native.Object.prototype.toString;
    native.Object_defineProperty = native.Object.defineProperty;
    native.Object_keys = native.Object.keys;
    native.Object_freeze = native.Object.freeze;
    native.Object_prototype = native.Object.prototype;
    native.Object_isExtensible = native.Object.isExtensible;
    native.call = native.Function.prototype.call;
    native.apply = native.Function.prototype.apply;
    native.indexOf = native.Array.prototype.indexOf;
    native.String_indexOf = native.String.prototype.indexOf;
    native.String_prototype_slice = String.prototype.slice;
    native.String_prototype_substring = String.prototype.substring;
    native.Boolean_prototype_valueOf = Boolean.prototype.valueOf;
    native.Array_prototype_join = native.Array.prototype.join;
    native.Function_prototype_bind = native.Function.prototype.bind;
    native.Date_prototype_getTime = native.Date.prototype.getTime;
    native.Math_min = native.Math.min;
    native.Math_max = native.Math.max;
    native.Math_floor = native.Math.floor;
    native.Math_abs = native.Math.abs;
    native.isNaN = native.Number.isNaN;
    native.isFinite = isFinite;
    native.Array_isArray = native.Array.isArray;
    native.RegExp_prototype_compile = native.RegExp.prototype.compile;
    native.RegExp_prototype_exec = native.RegExp.prototype.exec;
    native.RegExp_prototype = native.RegExp.prototype;

    function install(f, o) {
	Reflect_defineProperty(f, internalPrefix + o, {value: native[o], enumerable: false, configurable: false, writable: false});
    };
    function install_call(f) {
	install(f, 'call');
    };
    var icall = makeI('call');
    install_call(native_mapset);
    install_call(native.call);

    var sDesc = function (desc) {
	var vdesc = Object_create(null);
	var arr = ["get", "set", "value", "writable", "enumerable", "configurable"];
	var i, name;
	for (i=0; i<arr.length; i++) {
	    name = arr[i];
	    if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = name, arg1)))
		vdesc[name] = desc[name];
	}
	return vdesc;
    };
    function preaccess(o, arr) {
	var i, k;
	var no = Object_create(o);
	for (i=0; i<arr.length; i++) {
	    k = arr[i];
	    descTTT.value = Get(o, k);
	    Reflect_defineProperty(no, k, descTTT);
	}
	return no;
    };

    {
	const nullDesc = sDesc({value: null, writable: true, configurable: true, enumerable: true});
	const nativeTOS = native.Object.prototype.toString;
	const getNative = function(o) {
	    var oldd = Reflect_getOwnPropertyDescriptor(o, native.s_symbol_toStringTag);
	    if (oldd !== undefined && oldd.configurable === false) {
		// TODO
		return "[object Object]";
	    }
	    try {
		Reflect_defineProperty(o, native.s_symbol_toStringTag, nullDesc);
	    } catch(e) {
		// TODO
		return "[object Object]";
	    }
	    var rtn = native_apply(nativeTOS, o, arg0);
	    if (oldd === undefined) delete o[native.s_symbol_toStringTag];
	    else Reflect_defineProperty(o, native.s_symbol_toStringTag, oldd);
	    return rtn;
	}
	native.Object_prototype_toString = function () {
	    if (this === undefined) return "[object Undefined]";
	    else if (this === null) return "[object Null]";
	    var no = ToObject(this);
	    var s = Get(no, native.s_symbol_toStringTag);
	    if (typeof s === "string") return "[object "+s+"]";
	    return getNative(no);
	    // return tag;
	};
    }

    const test = native.global.hasOwnProperty("runTestCase")?true:false;

    function virtual_Object(v) { return native_apply(native.Object, this, arguments); };
    var virtual;
    // for testsuite 15.1.{2.2,2.3,2.4,2.5,3.1,3.2,3.3,3.4}.
    if (test) virtual = Object.create(virtual_Object.prototype);
    else virtual = Object.create(null);
    
    // Note: the property 'name' must exist.
    var m_virt_org = new native_map();
    var m_virt_proto = {key: [], value: [], name: makeI("proto")};
    var m_org_virt = new native_map();
    var m_tos = {key: [], value: [], name: makeI("tos")};
    var set_no_construct = {key: [], name: makeI("noconstruct")};
    install(m_virt_proto.key, 'indexOf')
    install(m_tos.key, 'indexOf')
    install(set_no_construct.key, 'indexOf')

    var thisCheck;
    thisCheck = function (o) {
	    if (o === native.global) return virtual;
	    return o;
    };
    var this_opt = function(o) { }
    function Class(o) {
	return native_apply(native.String_prototype_slice, ToStringNative(o), (arg2[0] = 8, arg2[1] = -1, arg2));
    };
    function ToStringForError(s) {
	if (isObject(s)) {
	    return native_apply(native.Object_prototype_toString, s, arg0);
	}
	return ""+s;
    }
    function ToStringNative(s) {
	var res = native_apply(native.Object_prototype_toString, s, arg0);
	return res;
    }
    function initAccessors(o, name, get, set) {
	var desc = Reflect_getOwnPropertyDescriptor(o, name);
	var vdesc = Object_create(null);
	if (desc === undefined) {
	    vdesc.enumerable = true;
	    vdesc.configurable = true;
	} else {
	    vdesc.enumerable = desc.enumerable;
	    vdesc.configurable = desc.configurable;
	}
	vdesc.set = set;
	vdesc.get = get;
	Reflect_defineProperty(o, name, vdesc);
	return o;
    }
    function newfn(fn, name) {
	return fn;
    }
    function GetMethod(V, P) {
	// 1. Assert: IsPropertyKey(P) is true.
	// 2. Let func be ? GetV(V, P).
	let func = Get(V, P);
	// 3. If func is either undefined or null, return undefined.
	if (func == null) return undefined;
	// 4. If IsCallable(func) is false, throw a TypeError exception.
	if (IsCallable(func) === false) 
	    throw new native.TypeError();
	// 5. Return func.
	return func;
    }
    function Call(fn, ths, args) {
	if (args === undefined) args = arg0;
	return native_apply(fn, ths, args);
    }
    function DefineOwnProperty(O, P, Attributes, Throw) {
	if (P === 'length' && Class(O) === 'Array') {
	    if (!native_apply(Object_prototype_hasOwnProperty, Attributes, (arg1[0] = 'value', arg1))) {
		try {
		    native.Object_defineProperty(O, P, Attributes);
		} catch (e) {
		    if (Throw) throw e;
		}
		return true;
	    }
	    var v = Attributes.value;
	    // 15.4.5.1. Step 3.d.
	    var newLen = ToUint32(v);
	    if (newLen !== ToNumber(v)) throw new native.RangeError("Invalid array length");
	    // TODO: Other properties.
	    try {
		Attributes.value = newLen;
		native.Object_defineProperty(O, P, Attributes);
//	        O[P] = newLen;
	    	return true;
	    } catch (e) {
		// Since the above code is executed in 'strict mode', it throws an exception when it should be.
		// On the other hand, since the compiled code may not be executed in 'strict mode',
		// we filter out not-necessary-exceptions from here.
		if (Throw) throw e;
	    }
	} else {
	    // 8.12.9 [[DefineOwnProperty]](P, Desc, Throw)
	    // The following operator does not throw an exception in the following pre-condition:
	    //   IsDataDescriptor = true /\ [[writable]] = true
	    try {
		native.Object_defineProperty(O, P, Attributes);
	    } catch (e) {
		if (Throw) throw e;
	    }
	    return true;
	}
    };
    var isafe = makeI('safe');
    var descSAFE = Object.create(null);
    descSAFE.configurable = false;
    descSAFE.enumerable = false;
    descSAFE.writable = false;
    descSAFE.value = null;
    
    const load_check_i = function(o) {
	if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = isafe, arg1)))
	    return true;
	var proto = Reflect_getPrototypeOf(o);
	if (proto === null) return true;

	var vproto = native_apply(native_mapget, m_org_virt, (arg1[0] = proto, arg1));
	if (vproto === undefined) {
	    vproto = proto;
	} else {
	    try { native_setPrototypeOf(o, vproto); } catch (_) { return false; }
	}
	if (load_check_i(vproto)) {
	    try { Reflect_defineProperty(o, isafe, descSAFE); } catch(_) {}
	    return true;
	}
	return false;
    }

    const load_check = function (o, s) {
	if (typeof o === 'object' || typeof o === 'function') {
	    if (typeof s !== 'string' && typeof s !== 'number') return false;
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1)))
		return true;
	    if (load_check_i(o)) return true;
	    o = Reflect_getPrototypeOf(o);
	    if (native_apply(native_maphas, m_org_virt, (arg1[0] = o, arg1))) return false;
	    var oo = o;
	    while (o !== null) {
		if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1)))
		    return true;

		// a specialized version of the virtual "getPrototypeOf".
		o = Reflect_getPrototypeOf(o);
		if (native_apply(native_maphas, m_org_virt, (arg1[0] = o, arg1))) return false;
	    }
	    return !(s in oo);
	} else {
	    return false;
	}
    }

    var this_opt = function(o) {
	if (o && (typeof o === 'object' || typeof o === 'function' )) {
	    var proto = Reflect_getPrototypeOf(o);
	    if (proto === null) return true;

	    var vproto = native_apply(native_maphas, m_org_virt, (arg1[0] = proto, arg1));
	    if (!vproto && native_apply(Object_prototype_hasOwnProperty, proto, (arg1[0] = isafe, arg1)))
		return true;

	    return load_check_i(o);
	}
	return false;
    }

    const store_check = function (o, s) {
	if (o && (typeof o === 'object' || typeof o === 'function')) {
	    if (typeof s !== 'string' && typeof s !== 'number') return false;
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1)))
		return true;
	    if (load_check_i(o)) return true;
	    o = Reflect_getPrototypeOf(o);
	    if (native_apply(native_maphas, m_org_virt, (arg1[0] = o, arg1))) return false;
	    var oo = o;
	    while (o !== null) {
		if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1)))
		    return true;
		// a specialized version of the virtual "getPrototypeOf".
		o = Reflect_getPrototypeOf(o);
		if (native_apply(native_maphas, m_org_virt, (arg1[0] = o, arg1))) return false;
	    }
	    return !(s in oo);
	} else {
	    return false;
	}
    }

    const mload_check = function (o, s) {
	if (o && (typeof o === 'object' || typeof o === 'function')) {
	    if (typeof s !== 'string' && typeof s !== 'number' && isObject(s)) return false;
	    if (load_check_i(o)) return true;

	    o = Reflect_getPrototypeOf(o);
	    if (native_apply(native_maphas, m_org_virt, (arg1[0] = o, arg1))) {
		return false;
	    }
	    while (o !== null) {
		if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1)))
		    return true;
		
		// a specialized version of the virtual "getPrototypeOf".
		o = Reflect_getPrototypeOf(o);
		if (native_apply(native_maphas, m_org_virt, (arg1[0] = o, arg1))) {
		    return false;
		}
	    }
	}
	// exception
	return false;
    }

    function GetValue(o, s) {
	// return o[s];
	var no;
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    no = ToObject(o);
	} else {
	    no = o;
	}
	s = ToString(s);
	return load_opt(no, s, no, o);
    };
    function GetValuec(o, s) {
    	// return o[s];
	var no;
    	if (o == null) {
    	    s = ToStringForError(s);
    	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
    	} else if (typeof o !== "object" && typeof o !== "function") {
    	    no = ToObject(o);
    	} else {
	    no = o;
	}
    	return load_opt(no, s, no, o);
    }
    function mGetValue(o, s) {
	// return o[s];
	var no;
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    no = ToObject(o);
	} else {
	    no = o;
	}
	s = ToString(s);
	return load_opt(no, s, no, o);
    };
    function mGetValuec(o, s) {
	var no;
	// return o[s];
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    no = ToObject(o);
	} else {
	    no = o;
	}
	return load_opt(no, s, no, o);
    }
    function GetProperty(o, s) {
	while (true) {
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1))) {
		return Reflect_getOwnPropertyDescriptor(o, s);
	    }
	    o = virtual_Object_getPrototypeOf(o);
	    if (o === null) return undefined;
	}
    };
    function Get(o, s) {
	var no;
	if (!isObject(o)) no = ToObject(o);
	else no = o;
	return load_opt(no, s, no, o);
    };
    function nGet(o, s) {
	var no;
	if (!isObject(o)) no = ToObject(o);
	else no = o;
	var desc = GetProperty(no, s);
	if (desc === undefined) return undefined;
	if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = 'value', arg1))) return desc.value;
	if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = 'get', arg1))) {
	    if (desc.get === undefined) return undefined;
	    return Call(desc.get, o);
	}
    };
    const load_opt = function load_opt(o, s, oo, no) {
	var insecure = false;
	while (o !== null) {
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1))) {
		if (!insecure) {
		    msg('secure start');
		    const rtn = oo[s];
		    msg('secure end');
		    return rtn;
		}
		var desc = Reflect_getOwnPropertyDescriptor(o, s);
		if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = 'value', arg1)))
		    return desc.value;
		if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = 'get', arg1))) {
		    if (desc.get === undefined) return undefined;
		    return Call(desc.get, no);
		}
	    } 
	    // a specialized version of the virtual "getPrototypeOf".
	    var o2 = o;
	    o = Reflect_getPrototypeOf(o);
	    var t = native_apply(native_mapget, m_org_virt, (arg1[0] = o, arg1));
	    // var t = search(m_org_virt, o, false);
	    if (t !== undefined) {
		// if (t !== null) {
		o = t;
		insecure = true;
	    }
	}
	return undefined;
    };

    function oGet(o, s) {
	var no;
	if (!isObject(o)) no = ToObject(o);
	else no = o;
	return load_opt(no, s, no, o);
    };
    function Geto(o, s) {
	return load_opt(o, s, o, o);
    };
    const store_i = function store_i(o, s, v, oo, th, prim, insecure) {
	while (o !== null) {
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1))) {
		// optimized case.
		if (!insecure) {
		    try {
			msg('secure start');
			return oo[s] = v;
		    } catch(e) {
			if (th) throw e;
		    } finally {
			msg('secure end');
		    }
		}
		var desc = Reflect_getOwnPropertyDescriptor(o, s);
		if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = 'value', arg1))) {
		    if (desc.writable) {
			// 15.4.5.1 [[DefineOwnProperty]](P, Desc, Throw)
			descValue.value = v;
			descValue.writable = true;
			descValue.enumerable = desc.enumerable;
			descValue.configurable = desc.configurable;
			DefineOwnProperty(oo, s, descValue, th);
			return v;
		    } else if (th) {
			throw new native.TypeError("Cannot assign to read only property '"+s+"' of object '"+ToStringForError(o)+"'");
		    }
		} else {
		    if (desc.set !== undefined) {
			Call(desc.set, oo, (arg1[0] = v, arg1));
			return v;
		    } else if (th) {
			throw new native.TypeError("Cannot set property '"+s+"' of '"+ToStringForError(o)+"' which has only a getter");
		    }
		}
		// 8.12.5. Step 1.b.
		return v;
	    } else {
		// a specialized version of the virtual "getPrototypeOf".
		var o2 = o = Reflect_getPrototypeOf(o);
		var t = native_apply(native_mapget, m_org_virt, (arg1[0] = o, arg1));
		// var t = search(m_org_virt, o, false);
		// if (t !== null) {
		if (t !== undefined) {
		    o = t;
		    insecure = true;
		}
	    }
	}
	// Note: the following semantics are changed in the recent version of JavaScript.
	// Thus, we ignore the semantics defined in "8.7.2 [[Put]] 7.a".
	// if (prim)
	// 	throw new native.TypeError("Cannot create property '"+s+"' on primitive value");

	// 8.12.5. Step 6.
	// not extensible can throw an exception, but it should be ignored.
	if (th || native.Object_isExtensible(oo)) {
	    descTTT.value = v;
	    DefineOwnProperty(oo, s, descTTT, th);
	}
	return v;
    };

    function Put(o, s, v, th) {
	if (isObject(o)) {
	    return store_i(o, s, v, o, th, false);
	}
	o = ToObject(o);
	return store_i(o, s, v, o, th, true);
    };
    function Puto(o, s, v, th) {
	return store_i(o, s, v, o, th, true);
    }
    var CheckObjectCoercible = function(o, name) {
	if (o == null) {
	    throw new native.TypeError(name + " called on null or undefined")
	}
    };
    function store(o, s, v, th) {
	// CheckObjectCoercible
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot set property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    o = ToObject(o);
	}
	s = ToString(s);
	return store_i(o, s, v, o, th, true);
    };
    function storec(o, s, v, th) {
	// return o[s] = v;
	// CheckObjectCoercible
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot set property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    o = ToObject(o);
	}
	return store_i(o, s, v, o, th, true);
    };
    function loadvar(x, r) { 
	var rtn;
	if (native_apply(Object_prototype_hasOwnProperty, virtual, (arg1[0] = x, arg1))) {
	    msg('secure start');
	    rtn = virtual[x];
	    msg('secure end');
	}
	else if (r) throw new native.ReferenceError(x+" is not defined.");
	else rtn = undefined;
	return rtn;
    };
    function storevar(x, v, th) {
	if (th) {
	    if (!native_apply(Object_prototype_hasOwnProperty, virtual, (arg1[0] = x, arg1)))
		throw native.ReferenceError(ToStringForError(x)+" is not defined");
	    msg('secure start');
	    virtual[x] = v;
	    msg('secure end');
	} else {
	    msg('secure start');
	    try { virtual[x] = v; } catch(_) {};
	    msg('secure end');
	    // descTTT.value = v;
	    // try { native.Object_defineProperty(virtual, x, descTTT) } catch(_) {};
	}
	return v;
    };
    function deletevar(x, th) {
	try {
	    msg('secure start');
	    return delete virtual[x];
	} catch(e) {
	    if (th) throw e;
	    return false;
	} finally {
	    msg('secure end');
	}
    };
    function initIterator(o, a, v) {
	if (a === undefined) a = [];
	if (o == null) return a;

	o = ToObject(o);
	if (v === undefined) v = Object_create(null);
	var m, n = native.Object_keys(o);
	var vv = Object_getOwnPropertyNames(o);
	var i;
	for (i=0; i<n.length; i++) {
	    m = n[i];
	    if (!(m in v)) {
		descFFF.value = m;
		Reflect_defineProperty(a, a.length, descFFF);
	    }
	}
	for (i=0; i<vv.length; i++) v[vv[i]] = true;
	return initIterator(virtual_Object_getPrototypeOf(o), a, v);
    };
    
    function unary(o, op) {
	if (op === "+")
	    return +ToNumber(o);
	else if (op === "-")
	    return -ToNumber(o);
	else if (op === "~")
	    return ~ToNumber(o);
	throw new native.TypeError("unknown unary operator: "+op);
    };
    function uplusv(o) {
	return +ToNumber(o);
    };
    var uminusv = function(o) {
	return -ToNumber(o);
    };
    var unotv = function(o) {
	return ~ToNumber(o);
    };
    function ToPrimitive_plus(o) {
	var to = typeof o;
	if (isNotObject(o)) return o;
	if (virtual_Object_getPrototypeOf(o) === virtual.Date.prototype) return ToPrimitive_S(o);

	return ToPrimitive_N(o);
    };
    // 11.6. Additive Operators
    function plusvv(lhs, rhs) {
	lhs = ToPrimitive_plus(lhs);
	rhs = ToPrimitive_plus(rhs);
	if (typeof lhs === "string" || typeof rhs === "string")
	    return ToString(lhs) + ToString(rhs);
	return lhs + rhs;
    }
    var plusvn = function(lhs, rhs) {
	lhs = ToPrimitive_plus(lhs);
	if (typeof lhs === "string")
	    return ToString(lhs) + ("" + rhs);
	return lhs + rhs;
    }
    var plusnv = function(lhs, rhs) {
	rhs = ToPrimitive_plus(rhs);
	if (rhs === "string")
	    return ("" + lhs) + ToString(rhs);
	return lhs + rhs;
    }
    var minusvv = function(lhs, rhs) {
	return ToNumber(lhs) - ToNumber(rhs);
    }
    var minusvn = function(lhs, rhs) {
	return ToNumber(lhs) - rhs;
    }
    var minusnv = function(lhs, rhs) {
	return lhs - ToNumber(rhs);
    }
    // 11.5. Multiplicative Operators
    var multvv = function(lhs, rhs) {
	return ToNumber(lhs) * ToNumber(rhs);
    }
    var multvn = function(lhs, rhs) {
	return ToNumber(lhs) * rhs;
    }
    var multnv = function(lhs, rhs) {
	return lhs * ToNumber(rhs);
    }
    var divvv = function(lhs, rhs) {
	return ToNumber(lhs) / ToNumber(rhs);
    }
    var divvn = function(lhs, rhs) {
	return ToNumber(lhs) / rhs;
    }
    var divnv = function(lhs, rhs) {
	return lhs / ToNumber(rhs);
    }
    var modvv = function(lhs, rhs) {
	return ToNumber(lhs) % ToNumber(rhs);
    }
    var modvn = function(lhs, rhs) {
	return ToNumber(lhs) % rhs;
    }
    var modnv = function(lhs, rhs) {
	return lhs % ToNumber(rhs);
    }
    // 11.10. Binary Bitwise Operators
    var bandvv = function(lhs, rhs) {
	return ToNumber(lhs) & ToNumber(rhs);
    }
    var bandvn = function(lhs, rhs) {
	return ToNumber(lhs) & rhs;
    }
    var bandnv = function(lhs, rhs) {
	return lhs & ToNumber(rhs);
    }
    var borvv = function(lhs, rhs) {
	return ToNumber(lhs) | ToNumber(rhs);
    }
    var borvn = function(lhs, rhs) {
	return ToNumber(lhs) | rhs;
    }
    var bornv = function(lhs, rhs) {
	return lhs | ToNumber(rhs);
    }
    var bpowvv = function(lhs, rhs) {
	return ToNumber(lhs) ^ ToNumber(rhs);
    }
    var bpowvn = function(lhs, rhs) {
	return ToNumber(lhs) ^ rhs;
    }
    var bpownv = function(lhs, rhs) {
	return lhs ^ ToNumber(rhs);
    }
    var ltvv = function(lhs, rhs) {
	return ToPrimitive_N(lhs) < ToPrimitive_N(rhs);
    }
    var ltvn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) < rhs;
    }
    var ltnv = function(lhs, rhs) {
	return lhs < ToPrimitive_N(rhs);
    }
    var gtvv = function(lhs, rhs) {
	return ToPrimitive_N(lhs) > ToPrimitive_N(rhs);
    }
    var gtvn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) > rhs;
    }
    var gtnv = function(lhs, rhs) {
	return lhs > ToPrimitive_N(rhs);
    }
    var ltevv = function(lhs, rhs) {
	if (typeof lhs === 'number' && typeof rhs === 'number')
	    return lhs <= rhs;
	return ToPrimitive_N(lhs) <= ToPrimitive_N(rhs);
    }
    var ltevn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) <= rhs;
    }
    var ltenv = function(lhs, rhs) {
	return lhs <= ToPrimitive_N(rhs);
    }
    var gtevv = function(lhs, rhs) {
	return ToPrimitive_N(lhs) >= ToPrimitive_N(rhs);
    }
    var gtevn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) >= rhs;
    }
    var gtenv = function(lhs, rhs) {
	return lhs >= ToPrimitive_N(rhs);
    }
    // 11.7. Bitwise Shift Operators
    var rshiftvv = function(lhs, rhs) {
	return ToNumber(lhs) >> ToNumber(rhs);
    }
    var rshiftvn = function(lhs, rhs) {
	return ToNumber(lhs) >> rhs;
    }
    var rshiftnv = function(lhs, rhs) {
	return lhs >> ToNumber(rhs);
    }
    var rushiftvv = function(lhs, rhs) {
	return ToNumber(lhs) >>> ToNumber(rhs);
    }
    var rushiftvn = function(lhs, rhs) {
	return ToNumber(lhs) >>> rhs;
    }
    var rushiftnv = function(lhs, rhs) {
	return lhs >>> ToNumber(rhs);
    }
    var lshiftvv = function(lhs, rhs) {
	return ToNumber(lhs) << ToNumber(rhs);
    }
    var lshiftvn = function(lhs, rhs) {
	return ToNumber(lhs) << rhs;
    }
    var lshiftnv = function(lhs, rhs) {
	return lhs << ToNumber(rhs);
    }
    // 11.9.1 The Equals Operator
    var equalvv = function(lhs, rhs) {
	var t1, t2;
	t1 = typeof lhs;
	t2 = typeof rhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs == rhs
    }
    var equalvn = function(lhs, rhs) {
	var t2;
	t2 = typeof rhs;
	if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs == rhs
    }
    var equalnv = function(lhs, rhs) {
	var t1;
	t1 = typeof lhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	return lhs == rhs
    }
    // 11.9.2. The Does-not-equals Operator
    var nequalvv = function(lhs, rhs) {
	var t1, t2;
	t1 = typeof lhs;
	t2 = typeof rhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs != rhs
    }
    var nequalvn = function(lhs, rhs) {
	var t2;
	t2 = typeof rhs;
	if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs != rhs
    }
    var nequalnv = function(lhs, rhs) {
	var t1;
	t1 = typeof lhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	return lhs != rhs;
    }
    var instanceofvv = function (lhs, rhs) {
	if (isNotObject(rhs))
	    throw new native.TypeError("Right-hand side of 'instanceof' is not an object");
	if (typeof rhs !== "function")
	    throw new native.TypeError("Right-hand side of 'instanceof' is not callable");
	// [[HasInstance]]
	var F = rhs;
	var V = lhs;
	if (isNotObject(V))
	    return false;
	var O = Geto(F, "prototype")
	if (isNotObject(O))
	    throw new native.TypeError();
	do {
	    V = virtual_Object_getPrototypeOf(V);
	    if (O === V) return true;
	} while (V !== null)
	return false;
    };
    var invv = function (lhs, rhs) {
	if (isNotObject(rhs)) throw new native.TypeError("Cannot use 'in' operator to search for "+ToStringForError(lhs)+" in "+ToStringForError(rhs));
	lhs = ToString(lhs);
	return HasProperty(lhs, rhs)
    };
    var innv = function (lhs, rhs) {
	if (isNotObject(rhs)) throw new native.TypeError("Cannot use 'in' operator to search for "+ToStringForError(lhs)+" in "+ToStringForError(rhs));
	return HasProperty(lhs, rhs)
    };

    var newcall = function (fn, a) {
	// only for built-in functions: Boolean, Number, Date. No other exceptions.
	var i, nfn;
	nfn = native_apply(native_mapget, m_virt_org, (arg1[0] = fn, arg1));
	if (nfn === undefined) nfn = fn;

	// Exceptional cases that does not have [[Construct]] internal field.
	if (map_contains(set_no_construct, nfn) === true) {
	    throw new native.TypeError(ToStringForError(nfn)+" is not a constructor");
	}
	return nfn;
    };
    var preop = function (x, op) {
	if (op === "++") {
	    return storevar(x, ToNumber(loadvar(x, true)) + 1)
	} else if (op === "--") {
	    return storevar(x, ToNumber(loadvar(x, true)) - 1)
	}
    };
    var postop = function (x, op) {
	var t;
	if (op === "++") {
	    t = ToNumber(loadvar(x, true));
	    storevar(x, t + 1);
	    return t;
	} else if (op === "--") {
	    t = ToNumber(loadvar(x, true));
	    storevar(x, t - 1);
	    return t;
	}
    };
    var mpre_plus = function (o, x, th) {
	return Put(o, x, ToNumber(GetValue(o, x)) + 1, th);
    };
    var mpre_minus = function (o, x, th) {
	return Put(o, x, ToNumber(GetValue(o, x)) - 1, th);
    };
    var mpost_plus = function (o, x, th) {
	var t;
	t = ToNumber(GetValue(o, x));
	Put(o, x, t + 1, th);
	return t;
    };
    var mpost_minus = function (o, x, th) {
	var t;
	t = ToNumber(GetValue(o, x));
	Put(o, x, t - 1, th);
	return t;
    };
    function ToBoolean(o) {
	return Boolean(o);
    };
    function ToNumber(o) {
	if (typeof o === 'object' || typeof o === 'function')
	    o = ToPrimitive_N(o);
	return +o;
    };
    function ToString(o) {
	if (typeof o === 'object' || typeof o === 'function')
	    o = ToPrimitive_S(o);
	return ""+o;
    };
    var Export = function (o) {
	var g = native.global;
	if (isNotObject(o)) return;
	var v, i, names = native.Object_keys(o);
	for (i=0; i<names.length; i++) {
	    v = names[i];
	    if (isObject(o[v])) {
		Reflect_defineProperty(g, v, (descTTT.value = H2L(o[v]), descTTT));
	    }
	}
    };
    function isNotObject(o) {
	return o === null || (typeof o !== 'object' && typeof o !== 'function');
    }
    function isObject(o) {
	return o && (typeof o === 'object' || typeof o === 'function');
    }

    function HasProperty(x, o) {
	while (o !== null) {
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = x, arg1))) {
		return true;
	    }
	    o = virtual_Object_getPrototypeOf(o);
	}
	return false;
    }
    var ToInteger = function(number) {
	var sign, abs;
	number = ToNumber(number);
	if (native.isNaN(number)) return 0;
	if (!native.isFinite(number)) return number;
	// sign(number) x floor(abs(number))
	abs = native.Math_abs(number);
	sign = (number === abs)? sign = 1 : -1;
	return sign * native.Math_floor(abs);
    }

    // The original semantics
    var ToUint32 = function(o) {
	return ToNumber(o) >>> 0;
    }
    var ToInt32 = function(o) {
	return ToNumber(o) >> 0;
    }

    function IsCallable(f) { return typeof f === "function"; };
    function ToObject(v, name) {
	if (v == null) {
	    var exc;
	    if (typeof name === 'string') exc = name + " called on null or undefined"
	    else exc = 'Cannot convert undefined or null to object';
	    throw new native.TypeError(exc);
	}
	if (typeof v === "boolean") return new native.Boolean(v);
	else if (typeof v === "number") return new native.Number(v);
	else if (typeof v === "string") return new native.String(v);
	else {
	    return v;
	}
    };
    // ToPrimitive with hint String
    function ToPrimitive_S(o) {
	var f, r;
	if (isNotObject(o)) return o;
	f = Geto(o, "toString");
	if (IsCallable(f)) {
            r = Call(f, o);
	    if (isNotObject(r)) return r;
	}
	f = Geto(o, "valueOf");
	if (IsCallable(f)) {
            r = Call(f, o);
	    if (isNotObject(r)) return r;
	}
	throw new native.TypeError ("Cannot convert object to primitive value");
    };
    // ToPrimitive with hint Number
    var ToPrimitive_N = function (o) {
	var f, r;
	if (isNotObject(o)) return o;
	f = Geto(o, "valueOf");
	if (IsCallable(f)) {
            r = Call(f, o);
	    if (isNotObject(r)) return r;
	}
	f = Geto(o, "toString");
	if (IsCallable(f)) {
            r = Call(f, o);
	    if (isNotObject(r)) return r;
	}
	throw new native.TypeError ("Cannot convert object to primitive value");
    };
    var iindexOf = makeI('indexOf');

    var register = function(a, n, t, t2) {
	var searchKey = a.name;
	var i;
	i = a.key[iindexOf](n)

	if (i < 0) {
	    i = a.key.length;
	    Reflect_defineProperty(a.key, i, (descTTT.value = n, descTTT));
	    if (native_apply(Object_prototype_hasOwnProperty, a, (arg1[0] = 'value', arg1))) {
		Reflect_defineProperty(a.value, i, (descTTT.value = t, descTTT));
	    }
	    // just for optimization
	    if (typeof searchKey === "string") {
	    	Reflect_defineProperty(n, searchKey, (descFFF.value = i, descFFF));
	    }
	}
    };
    var registerOPT = function(a, n, t, t2) {
	var searchKey = a.name;
	var i = a.key.length;
	Reflect_defineProperty(a.key, i, (descTTT.value = n, descTTT));
	if (native_apply(Object_prototype_hasOwnProperty, a, (arg1[0] = 'value', arg1))) {
	    Reflect_defineProperty(a.value, i, (descTTT.value = t, descTTT));
	}
	// just for optimization
	if (typeof searchKey === "string") {
	    Reflect_defineProperty(n, searchKey, (descFFF.value = i, descFFF));
	}
    };
    var registerOPT2 = function(a, n, t) {
	var searchKey = a.name;
	var i = a.key.length;
	Reflect_defineProperty(a.key, i, (descTTT.value = n, descTTT));
	Reflect_defineProperty(a.value, i, (descTTT.value = t, descTTT));

	// just for optimization
	Reflect_defineProperty(n, searchKey, (descFFF.value = i, descFFF));
    };
					  
    function search_org(n) {
	if (n == null) return null;
	var t = native_apply(native_mapget, m_org_virt, (arg1[0] = n, arg1));
	if (t === undefined) return n;
	else return t;
    }

    function search(a, n, b) {
	if (n == null) return null;
	var to = typeof n;
	if (to !== "object" && to !== "function") {
	    if (b) return n;
	    else return null;
	}
	var j, i, searchKey = a.name;
	j = Reflect_getOwnPropertyDescriptor(n, searchKey);
	if (j !== undefined && native_apply(Object_prototype_hasOwnProperty, j, (arg1[0] = 'value', arg1)) &&
	    typeof j.value === 'number' && j.value < a.key.length) {
	    i = j.value;
	    if (a.key[i] !== n) i = -1; // i = a.key[iindexOf](n);
	} else i = -1;// else i = a.key[iindexOf](n);

	if (i >= 0) return a['value'][i];
	else if (b) return n;
	else return null;
    };
    function map_contains(a, n) {
	if (n == null) return null;
	var to = typeof n;
	if (to !== "object" && to !== "function") return false;
	var j, i, searchKey = a.name;
	j = Reflect_getOwnPropertyDescriptor(n, searchKey);
	if (j !== undefined && native_apply(Object_prototype_hasOwnProperty, j, (arg1[0] = 'value', arg1)) &&
	    typeof j.value === 'number' && j.value < a.key.length) {
	    i = j.value;
	    if (a.key[i] !== n) i = -1;
	} else return false;

	return i >= 0;
    };
    function register_function(F, name, noConstruct, i, user, danger) {
	var n;
	if (i === undefined) i = 0;

	if (name === undefined) name = " "; else name = " "+name;
	newfn(F, name);
	Reflect_defineProperty(F, "length", (descFFF.value = i, descFFF));
	if (noConstruct) {
	    register(set_no_construct, F);
	    // should be removed. Can we exploit 'Function.prototype.bind' to create a function without prototype field?
	    F.prototype = undefined;
	}

	return F;
    };
    var copy_desc = function(dest, org, name) {
	var desc = Reflect_getOwnPropertyDescriptor(org, name);
	if (desc === undefined) {
	    warn("* Warning: the field '"+name+"' is missing.");
	    return false;
	}
	if (Object_prototype_hasOwnProperty.call(desc, "set") && desc.set !== undefined)
	    desc.set = register_function(desc.set, name+" set", true, 0);
	if (Object_prototype_hasOwnProperty.call(desc, "get") && desc.get !== undefined)
	    desc.get = register_function(desc.get, name+" get", true, 0);
	if (Reflect_defineProperty(dest, name, sDesc(desc))) {
	    return true;
	} else {
	    warn("fails to update "+name);
	}
	return false;
    };
    
    var simpleFn = function(lhs, rhs, name, alen, noConstruct) {
	let fn;
	{
	    const o = rhs[name];
	    let f;
	    if (alen === 0 || alen === undefined) {
		f = function() {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 1) {
		f = function(v1) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 2) {
		f = function(v1, v2) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 3) {
		f = function(v1, v2, v3) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 4) {
		f = function(v1, v2, v3, v4) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 5) {
		f = function(v1, v2, v3, v4, v5) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 6) {
		f = function(v1, v2, v3, v4, v5, v6) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 7) {
		f = function(v1, v2, v3, v4, v5, v6, v7) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 8) {
		f = function(v1, v2, v3, v4, v5, v6, v7, v8) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    }
	    fn = register_function(f, name, noConstruct, alen);
	}
	var t = Reflect_getOwnPropertyDescriptor(rhs, name);
	if (t === undefined) {
	    warn(name+' is missing.');
	} else {
	    Reflect.defineProperty(lhs, name, sDesc((t.value = fn, t)));
	}
    };
    var nsimpleFn = function(lhs, rhs, name, alen, noConstruct) {
	var fn = (function() {
	    var o = rhs[name];
	    var f;
	    if (alen === 0 || alen === undefined) {
		f = function() {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 1) {
		f = function(x) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 2) {
		f = function(x,y) {
		    var r = native_apply(o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else {
		throw "an error of our bootstrapping code";
	    }
	    return register_function(f, name, noConstruct, alen);
	})();
	var w, e, c;
	var t = Reflect_getOwnPropertyDescriptor(rhs, name);
	if (t === undefined) {
	    warn(name+' is missing.');
	} else {
	    w = t.writable;
	    e = t.enumberable;
	    c = t.configurable;
	    Reflect_defineProperty(lhs, name, sDesc((t.value = fn, t)));
	}
    };
    var eFn = function(o, name, w, e, c, fn) {
	Reflect_defineProperty(o, name, sDesc({value: fn, writable: w, enumerable: e, configurable: c}));
    };

    // Overriding the semantics of native built-in functions.
    var native_Function_prototype_toString = native.Function.prototype.toString;
    (function() {
	Function.prototype.toString = (function() {
	    var F = function() {
		if (typeof this !== 'function')
		    throw new native.TypeError("Function.prototype.toString requires that 'this' be a Function");
		return "function () { [protected code] }";
		// // var name = search(trusted_fn, this, false);
		// // if (name !== null) return "function"+name+"() { [defensive code] }"; else
		// if (this === F) return "function toString() { [native code] }";
		// else
		//     return native_apply(native_Function_prototype_toString, this, arguments);
	    };
	    return F;
	})();
    })();
    // non-standard object 'console'
    virtual.console = {};
    virtual.console.log = register_function(log);
    virtual.console.warn = register_function(warn);
    
    // Virtual environment.
    // Auxiliary functions
    var ToStringWrapper1 = function(o, name, fn) {
	var f = function (s) {
	    s = ToString(s);
	    return native_apply(fn, this, (arg1[0] = s, arg1));
	};
	f = register_function(f, name, true, 1);
	eFn(o, name, true, false, true, f);
    };
    var ToNumberWrapper1 = function(o, name, fn) {
	var f = function (s) {
	    s = ToNumber(s);
	    return native_apply(fn, this, (arg1[0] = s, arg1));

	};
	f = register_function(f, name, true, 1);
	eFn(o, name, true, false, true, f);
    };
    var ToNumberWrapperN = function(o, name, fn, n1, n2) {
	var f;
	if (n2 === 0 || n2 === undefined) {
	    f  = function () {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native_apply(fn, this, arguments);
	    };
	} else if (n2 === 1) {
	    f  = function (v1) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native_apply(fn, this, arguments);
	    };
	} else if (n2 === 2) {
	    f  = function (v1, v2) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native_apply(fn, this, arguments);
	    };
	} else if (n2 === 3) {
	    f  = function (v1, v2, v3) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native_apply(fn, this, arguments);
	    };
	} else if (n2 === 4) {
	    f  = function (v1, v2, v3, v4) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native_apply(fn, this, arguments);
	    };
	} else throw "TODO1"
	f = register_function(f, name, true, n2);
	eFn(o, name, true, false, true, f);
    };
    var MathWrapper = function(o, name, fn) {
	var f = function (v1) {
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToNumber(arguments[i]);
	    }
	    return native_apply(fn, this, arguments);
	};
	f = register_function(f, name, true, fn.length);
	eFn(o, name, true, false, true, f);
    };
    var MathWrapper2 = function(o, name, fn) {
	var f = function (v1, v2) {
	    for (let i=0; i<arguments.length; i++) {
		arguments[i] = ToNumber(arguments[i]);
	    }
	    return native_apply(fn, this, arguments);
	};
	f = register_function(f, name, true, fn.length);
	eFn(o, name, true, false, true, f);
    };
    var ToObjectWrapper0 = function(o, name, fn) {
	var f = function () {
	    const this_ = ToObject(this);
	    return native_apply(fn, this_, arg0);
	};
	f = register_function(f, name, true, 0);
	eFn(o, name, true, false, true, f);
    };
    var ToPropertyDescriptor = function(desc) {
	var ndesc;
	if (isNotObject(desc)) ndesc = desc;
	else {
    	    var i, name, arr = ["value", "get", "set", "writable", "enumerable", "configurable"];
	    ndesc = Object_create(null);
    	    for (i=0; i<arr.length; i++) {
    		name = arr[i];
    		if (HasProperty(name, desc)) {
    		    ndesc[name] = Geto(desc, name);
    		}
    	    }
	}
	return ndesc;
    };
    var GetCompatibleRegExp = function(o, name) {
	if (Class(o) !==  'RegExp') throw new native.TypeError("Method "+name+" called on incompatible receiver "+ToStringForError(o));
	return o;
    }
    var GetPropertyDescriptor = function GetPropertyDescriptor(o, p) {
	if (o === null) return undefined;
	var desc = Reflect_getOwnPropertyDescriptor(o, p);
	if (desc !== undefined)
	    return desc;
	return GetPropertyDescriptor(virtual_Object_getPrototypeOf(o), p);
    };
    var CreateRegExp = function (o, name, gets, bReplace, bSearch, bSplit) {
	o = GetCompatibleRegExp(o, name);
	// TODO need to revised.
	Reflect_defineProperty(o, native.s_symbol_match, Reflect_getOwnPropertyDescriptor(native.RegExp_prototype, native.s_symbol_match));

	var desc, i, P, n = new native.RegExp(o);
	Reflect_setPrototypeOf(o, virtual_RegExp_prototype);
	Put(n, 'lastIndex', ToNumber(Geto(o, 'lastIndex')));
	for (i=0; i<gets.length; i++) {
	    P = gets[i];
	    desc = GetPropertyDescriptor(o, P);
	    if (desc !== undefined) {
		Reflect_defineProperty(n, P, desc);
	    }
	}
	if (bReplace) {
	    descTTT.value = native.symbol_replace;
	    DefineOwnProperty(n, Symbol.replace, descTTT, false);
	}
	if (bSearch) {
	    descTTT.value = native.symbol_search;
	    DefineOwnProperty(n, Symbol.search, descTTT, false);
	}
	if (bSplit) {
	    descTTT.value = native.symbol_split;
	    DefineOwnProperty(n, Symbol.split, descTTT, false);
	}
	return n;
    };
    var extend = function (A, B, fields) {
	var nat = native.global[A];
    	var fn = register_function(function() { return native_apply(nat, this, arguments); }, A, false, 1);
    	Reflect_defineProperty(fn, 'prototype', {value: Object.create(virtual[B].prototype), enumerable: false, writable: false, configurable: false});
    	Reflect_defineProperty(fn.prototype, 'constructor', {value: fn, enumerable: false, writable: true, configurable: true});
	var bnat = native.Object.getPrototypeOf(nat.prototype);
	(function() {
	    var i, n
	    for (i in fields) {
		n = fields[i];
		// for hopjs
		if (!copy_desc(fn.prototype, nat.prototype, n)) {
		    copy_desc(fn.prototype, bnat, n);
		}
	    }
	})();
    	Reflect_defineProperty(virtual, A, {value: fn, writable: true, configurable: true, enumerable: false});
    };

    // 15.1 The Global Object
    // 15.1.1.1 NaN
    copy_desc(virtual, native.global, "NaN");
    // 15.1.1.2 Infinity
    copy_desc(virtual, native.global, "Infinity");
    // 15.1.1.3 undefined
    copy_desc(virtual, native.global, "undefined");
    // Object.defineProperty(virtual, "undefined", sDesc({value: undefined, writable: false, enumerable: false, configurable: false}));

    // 15.1.2.1 eval(x)
    // Note: the defensive mode does not provide eval.
    virtual.eval = (function() {
    	var f = function(x) { throw new native.Error("'eval' is not allowed in defensive mode.") };
    	return register_function(f, "eval", true, 1);
    })();

    // 15.1.2.2 parseInt(string, radix)
    eFn(virtual, "parseInt", true, false, true, (function () {
	var o = parseInt;
	var f = function parseInt(string, radix) {
	    string = ToString(string);
	    radix = ToInt32(radix);
	    return native_apply(o, this, (arg2[0] = string, arg2[1] = radix, arg2));
	};
	return register_function(f, "parseInt", true, 2);
    })());
    // 15.1.2.3 parseFloat(string)
    ToStringWrapper1(virtual, "parseFloat", parseFloat);
    // 15.1.2.4 isNaN(number)
    ToNumberWrapper1(virtual, "isNaN", isNaN);
    // 15.1.2.5 isFinite(number)
    ToNumberWrapper1(virtual, "isFinite", isFinite);
    // 15.1.3.1 decodeURI(encodedURI)
    ToStringWrapper1(virtual, "decodeURI", decodeURI);
    // 15.1.3.2 decodeURIComponent(encodedURIComponent)
    ToStringWrapper1(virtual, "decodeURIComponent", decodeURIComponent);
    // 15.1.3.3 encodeURI(uri)
    ToStringWrapper1(virtual, "encodeURI", encodeURI);
    // 15.1.3.4 encodeURIComponent(uriComponent)
    ToStringWrapper1(virtual, "encodeURIComponent", encodeURIComponent);
    // B.2.1 escape(string)
    ToStringWrapper1(virtual, "escape", escape);
    // B.2.2 unescape(string)
    ToStringWrapper1(virtual, "unescape", unescape);

    // for testcases
    virtual.assert = register_function(function(o, msg) { if (!o) throw new native.Error(msg)}, "assert", true, 1);

    // 15.2 Object Objects
    eFn(virtual, "Object", true, false, true, register_function(virtual_Object, "Object", false, 1));
    // 15.2.3.1
    (function() {
    	var new_proto = Object_create(null);
    	Reflect_defineProperty(virtual.Object, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
    	Reflect_defineProperty(new_proto, "constructor", {value: virtual.Object, writable: true, enumerable: false, configurable: true});
    })();
    var securePrototypeOf = (function() {
        return function(a) {
	    var t = Reflect_getPrototypeOf(a);
	    if (native_apply(native_maphas, m_org_virt, (arg1[0] = t, arg1))) return false;
	    return t;
	};
    })();
    // 15.2.3.2
    eFn(virtual.Object, "getPrototypeOf", true, false, true, (function() {
	m_virt_proto; // TODO JerryScript bug.
    	return register_function(a => {
	    a = ToObject(a);
	    return search_org(Reflect_getPrototypeOf(a), true);
	}, "getPrototypeOf", true, 1);
    })());
    // 15.2.3.3 Object.getOwnPropertyDescriptor(O, P)
    eFn(virtual.Object, "getOwnPropertyDescriptor", true, false, true, (function () {
	var o = native.Object.getOwnPropertyDescriptor;
	var f = function (O, P) {
	    O = ToObject(O);
	    P = ToString(P);
	    return native_apply(o, this, (arg2[0] = O, arg2[1] = P, arg2));
	};
	return register_function(f, "getOwnPropertyDescriptor", true, 2);
    })());
    // 15.2.3.4 Object.getOwnPropertyNames(O)
    eFn(virtual.Object, "getOwnPropertyNames", true, false, true, (() => {
	const o = native.Object.getOwnPropertyNames;
	const f = function(O) {
	    O = ToObject(O);
	    return o(O);
	};
	return register_function(f, "getOwnPropertyNames", true, 1);
    })());
    // 15.2.3.5 Object.create(O[, Properties])
    eFn(virtual.Object, "create", true, false, true, (function() {
    	var f = function (O, Properties) {
	    // 1. If Type(O) is not Object or Null throw a TypeError exception.
	    if (typeof O !== 'object' && typeof O !== 'function' && typeof O !== null)
		throw new native.TypeError("Object.create called on non-object");
	    // 2. Let obj be the result of creating a new object as if by the expression new Object() where Object is the standard built-in constructor with that name
	    // 3. Set the [[Prototype]] internal property of obj to O.
	    var obj = native_apply(Object_create, this, (arg1[0] = O, arg1));
	    // 4. If the argument Properties is present and not undefined, add own properties to obj as if by calling the standard built-in function Object.defineProperties with arguments obj and Properties.
	    if (Properties !== undefined) {
		virtual_Object_defineProperties(obj, Properties);
	    }
	    // 5. Return obj.
	    return obj;
    	};
    	return register_function(f, "create", true, 2);
    })());
    // 15.2.3.6 Object.defineProperty(O, P, Attributes)
    eFn(virtual.Object, "defineProperty", true, false, true, (function() {
    	var f = function (O, P, Attributes) {
	    // 1. If Type(O) is not Object throw a TypeError exception.
	    if (isNotObject(O))
		throw new native.TypeError("Object.defineProperty called on non-object");
	    // 2. Let name be ToString(P).
	    var name = ToString(P);
	    // 3. Let desc be the result of calling ToPropertyDescriptor with Attributes as the argument.
	    var desc = ToPropertyDescriptor(Attributes);
	    // 4. Call the [[DefineOwnProperty]] internal method of O with arguments name, desc, and true.
	    DefineOwnProperty(O, name, desc, true);
	    // 5. Return O.
	    return O;
    	};
    	return register_function(f, "defineProperty", true, 3);
    })());
    // 15.2.3.7 Object.defineProperties(O,Properties)
    eFn(virtual.Object, "defineProperties", true, false, true, (function() {
	// Original semantics
    	var f = function (O,Properties) {
	    // 1. If Type(O) is not Object throw a TypeError exception.
	    if (isNotObject(O)) throw new native.TypeError("Object.defineProperties called on non-object");
	    // 2. Let props be ToObject(Properties).
	    var props = ToObject(Properties);
	    // 3. Let names be an internal list containing the names of each enumerable own property of props.
	    var names = native.Object_keys(props);
	    // 4. Let descriptors be an empty internal List.
	    var descriptors = [];
	    // 5. For each element P of names in list order,
	    var i, P, descObj, desc;
	    for (i=0; i<names.length; i++) {
		P = names[i];
		// a. Let descObj be the result of calling the [[Get]] internal method of props with P as the argument.
		descObj = Geto(props, P);
		// b. Let desc be the result of calling ToPropertyDescriptor with descObj as the argument.
		desc = ToPropertyDescriptor(descObj);
		// c. Append the pair (a two element List) consisting of P and desc to the end of descriptors.
    		native.Object_defineProperty(descriptors, i, (descTTT.value = {P:P, desc:desc}, descTTT));
	    }
	    // 6. For each pair from descriptors in list order,
	    for (i=0; i < descriptors.length; i++) {
		// a. Let P be the first element of pair.
		P = descriptors[i].P;
		// b. Let desc be the second element of pair.
		desc = descriptors[i].desc;
		// c. Call the [[DefineOwnProperty]] internal method of O with arguments P, desc, and true.
		DefineOwnProperty(O, P, desc, true);
	    }
	    // 7. Return O.
	    return O;
    	};
    	return register_function(f, "defineProperties", true, 2);
    })());
    // 15.2.3.8 Object.seal(O)
    nsimpleFn(virtual.Object, native.Object, "seal", 1, true);
    // 15.2.3.9 Object.freeze(O)
    nsimpleFn(virtual.Object, native.Object, "freeze", 1, true);
    // 15.2.3.10 Object.preventExtensions(O)
    nsimpleFn(virtual.Object, native.Object, "preventExtensions", 1, true);
    // 15.2.3.11 Object.isSealed(O)
    nsimpleFn(virtual.Object, native.Object, "isSealed", 1, true);
    // 15.2.3.12 Object.isFrozen(O)
    nsimpleFn(virtual.Object, native.Object, "isFrozen", 1, true);
    // 15.2.3.13 Object.isExtensible(O)
    nsimpleFn(virtual.Object, native.Object, "isExtensible", 1, true);
    // 15.2.3.14 Object.keys(O)
    eFn(virtual.Object, "keys", true, false, true, (() => {
	const o = native.Object.keys;
	const f = function(O) {
	    O = ToObject(O);
	    return o(O);
	};
	return register_function(f, "keys", true, 1);
    })());
    
    // 15.2.4 Properties of the Object Prototype Object
    // 15.2.4.1 Object.prototype.constructor
    Reflect_defineProperty(virtual.Object.prototype, "constructor", sDesc({value: virtual.Object, writable: true, enumerable: false, configurable: true}));
    // 15.2.4.2 Object.prototype.toString()
    eFn(virtual.Object.prototype, "toString", true, false, true, (function() {
    	var f = function () {
    	    var cls = search(m_tos, this, false); 
    	    if (cls !== null) return "[object "+cls+"]";
	    else {
		return native_apply(native.Object_prototype_toString, this, arg0);
	    }
    	};
    	return register_function(f, "toString", true, 0);
    })());
    // 15.2.4.3 Object.prototype.toLocaleString
    eFn(virtual.Object.prototype, "toLocaleString", true, false, true, (function() {
    	var o = native.Object.prototype.toLocaleString;
    	var f = function () {
	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
	    var O = ToObject(this);
	    // 2. Let toString be the result of calling the [[Get]] internal method of O passing "toString" as the argument.
	    var toString = Geto(O, "toString");
	    // 3. If IsCallable(toString) is false, throw a TypeError exception.
	    if (IsCallable(toString) === false)
		throw new native.TypeError(ToStringForError(toString)+" is not a function");
	    // 4. Return the result of calling the [[Call]] internal method of toString passing O as the this value and no arguments.
	    return Call(toString, O);
    	};
    	return register_function(f, "toLocaleString", true, 0);
    })());
    // 15.2.4.4 Object.prototype.valueOf()
    ToObjectWrapper0(virtual.Object.prototype, "valueOf", native.Object.prototype.valueOf);
    // 15.2.4.5 Object.prototype.hasOwnProperty(V)
    eFn(virtual.Object.prototype, "hasOwnProperty", true, false, true, (function() {
    	var o = native.Object.prototype.hasOwnProperty;
    	var f = function (V) {
	    V = ToString(V);
	    var O = ToObject(this);
	    return native_apply(o, O, (arg1[0] = V, arg1));
    	};
    	return register_function(f, "hasOwnProperty", true, 1);
    })());
    // 15.2.4.6 Object.prototype.isPrototypeOf(V)
    eFn(virtual.Object.prototype, "isPrototypeOf", true, false, true, (function() {
    	var f = function (V) {
	    // 1. If V is not an object, return false.
    	    if (isNotObject(V)) return false;
	    // 2. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = search_org(ToObject(this), true);
	    // 3. Repeat
    	    while (true) {
		// a. Let V be the value of the [[Prototype]] internal property of V.
    		V = virtual_Object_getPrototypeOf(V);
		// b. if V is null, return false
    		if (V === null) return false;
		// c. If O and V refer to the same object, return true.
    		if (O === V) return true;
    	    }
    	};
    	return register_function(f, "isPrototypeOf", true, 1);
    })());
    // 15.2.4.7 Object.prototype.propertyIsEnumerable(V)
    eFn(virtual.Object.prototype, "propertyIsEnumerable", true, false, true, (function() {
	var o = native.Object.prototype.propertyIsEnumerable;
    	var f = function (V) {
	    // 1. Let P be ToString(V)
	    var P = ToString(V);
	    var O = ToObject(this);
	    return native_apply(o, O, (arg1[0] = P, arg1));
    	};
    	return register_function(f, "propertyIsEnumerable", true, 1);
    })());
    // 15.3 Function Objects
    eFn(virtual, "Function", true, false, true, register_function(function(v) {
    	throw new native.Error("'eval' is not allowed in defensive mode.")
    }, "Function", false, 1, false, true));
    (function() {
	// 15.3.3.1 Function.prototype
    	var new_proto = register_function(native_apply(native.Function_prototype_bind, new Function(), arg0), "", true, 0);
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
	var notAllowed = function() {
	    throw new native.TypeError("'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them");
	}
	var desc = {get: notAllowed, set: notAllowed, configurable: true, enumerable: false};
	try {
	    native.Object.defineProperty(new_proto, 'caller', desc);
	    native.Object.defineProperty(new_proto, 'arguments', desc);
	    native.Object.defineProperty(new_proto, 'callee', desc);
	} catch(_) {
	    throw "TODO: caller_check";
	}

    	Reflect_defineProperty(virtual.Function, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.3.4 Properties of the Function Prototype Object
	// 15.3.4.1. Function.prototype.constructor
    	Reflect_defineProperty(new_proto, "constructor", {value: virtual.Function, writable: true, enumerable: false, configurable: true});
    })();
    // 15.3.4.2 Function.prototype.toString()
    eFn(virtual.Function.prototype, "toString", true, false, true, (function () {
	var f = function() {
	    if (typeof this !== 'function')
		throw new native.TypeError("Function.prototype.toString requires that 'this' be a Function");
	    return "function () { [protected code] }";
	};
	return register_function(f, "toString", true, 0);
    })());
    // 15.3.4.3 Function.prototype.apply(thisArg, argArray)
    eFn(virtual.Function.prototype, "apply", true, false, true, (function () {
    	var f = function(thisArg, argArray) {
	    var func = this;
	    // 1. If IsCallable(func) is false, then throw a TypeError exception.
    	    if (IsCallable(func) === false) {
    		throw new native.TypeError(typeof func + " is not a function")
	    }
	    // 2. If argArray is null or undefined, then
	    if (argArray == undefined) {
		// a. Return the result of calling the [[Call]] internal method of func, providing thisArg as the this value and an empty list of arguments.
    	    	return native_apply(func, thisArg, arg0);
	    }
	    // 3. If Type(argArray) is not Object, then throw a TypeError exception.
	    if (isNotObject(argArray))
    		throw new native.TypeError()
	    // 4. Let len be the result of calling the [[Get]] internal method of argArray with argument "length".
	    var len = Geto(argArray, "length");
	    // 5. Let n be ToUint32(len).
	    var n = ToUint32(len);
	    var index;

	    // optimized version. Checks whether 'argArray' is sparse array or not.
	    // TODO need to confirm.
	    var opt = true;
	    for (index = 0; index < n; index++) {
		if (!native_apply(Object_prototype_hasOwnProperty, argArray, (arg1[0] = index, arg1))) {
		    opt = false;
		    break;
		}
	    }
	    if (opt)
		return native_apply(func, thisArg, argArray);

	    // 6. Let argList be an empty List.
	    var argList = [];
	    // 7. Let index be 0.
	    index = 0;
	    // 8. Repeat while index < n
	    while (index < n) {
		// a. Let indexName be ToString(index).
		var indexName = ToString(index);
		// b. Let nextArg be the result of calling the [[Get]] internal method of argArray with indexName as the argument.
		var nextArg = Geto(argArray, indexName);
		// c. Append nextArg as the last element of argList.
		descTTT.value = nextArg;
    		Reflect_defineProperty(argList, indexName, descTTT);
		// d. Set index to index + 1.
		index += 1;
	    }
	    return native_apply(func, thisArg, argList);
    	};
    	return register_function(f, "apply", true, 2);
    })());
    // 15.3.4.4 Function.prototype.call(thisArg [, arg2  [, arg2, ...]])
    eFn(virtual.Function.prototype, "call", true, false, true, (function () {
    	var nat = native.Function.prototype.call;
	var f = function(x) {
	    return native_apply(nat, this, arguments);
	};
	return register_function(f, "call", true, 1);
    })());
    // 15.3.4.5 Function.prototype.bind(thisArg [, arg2 [, arg2, ...]])
    eFn(virtual.Function.prototype, "bind", true, false, true, (function () {
    	var nat = native.Function.prototype.bind;
    	var f = function(i) {
	    if (!IsCallable(this)) 
    		throw new native.TypeError()
	    if (!Reflect_getOwnPropertyDescriptor(this, "name")) {
		const name = Geto(this, "name");
    		Reflect_defineProperty(this, "name", (descTTT.value = name, descTTT));
	    }
    	    return native_apply(nat, this, arguments);
    	};
    	return register_function(f, "bind", true, 1);
    })());

    // TODO Uint32Array
    // if ("Uint32Array" in _global_) {
    // 	native.Uint32Array = Uint32Array;
    // 	eFn(virtual, "Uint32Array", true, false, true, register_function(function uint32(v) {
    // 	    if (this instanceof uint32) {
    // 		if (arguments.length === 0)
    // 		    return new native.Uint32Array();
    // 		else if (arguments.length === 1)
    // 		    return new native.Uint32Array(arguments[0]);
    // 		else throw new native.TypeError("TODO");
    // 	    } else {
    // 		return native_apply(native.Uint32Array, this, arguments);
    // 	    }
    // 	}, "Uint32Array", false, 1));
    // 	(function() {
    // 	    var new_proto = new native.Uint32Array();
    // 	    native.Object.defineProperty(virtual.Uint32Array, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
    // 	    // 15.4.4.1 Array.prototype.constructor
    // 	    native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Uint32Array, writable: true, enumerable: false, configurable: true}));
    // 	})();
    // }

    // 15.4 Array Objects
    eFn(virtual, "Array", true, false, true, register_function(function(v) { return native_apply(native.Array, this, arguments); }, "Array", false, 1));
    (function() {
	// 15.4.3.1 Array.prototype
    	var new_proto = new native.Array();
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	Reflect_defineProperty(virtual.Array, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.4.4.1 Array.prototype.constructor
    	Reflect_defineProperty(new_proto, "constructor", sDesc({value: virtual.Array, writable: true, enumerable: false, configurable: true}));
    })();
    // 15.4.3.2 Array.isArray(arg)
    nsimpleFn(virtual.Array, native.Array, "isArray", 1, true);
    // // ES6 Array.from()
    // nsimpleFn(virtual.Array, native.Array, "from", 1, true);

    // 15.4.4 Properties of the Array Prototype Object
    copy_desc(virtual.Array.prototype, native.Array.prototype, "length");
    // 15.4.4.2 Array.prototype.toString()
    eFn(virtual.Array.prototype, "toString", true, false, true, (function() {
	const tos = virtual.Object.prototype.toString;
    	var f = function () {
    	    // 1. Let array be the result of calling ToObject on the this value.
    	    var array = ToObject(this);
    	    // 2. Let func be the result of calling the [[Get]] internal method of array with argument "join".
    	    var func = Geto(array, "join");
    	    // 3. If IsCallable(func) is false, then let func be the standard built-in method Object.prototype.toString (15.2.4.2).
    	    if (!IsCallable(func)) func = tos;
    	    // 4. Return the result of calling the [[Call]] internal method of func providing array as the this value and an empty arguments list.
    	    return Call(func, array);
    	};
    	return register_function(f, "toString", true, 0);
    })());
    // 15.4.4.3 Array.prototype.toLocaleString()
    eFn(virtual.Array.prototype, "toLocaleString", true, false, true, (function() {
    	var f = function () {
    	    // 1. Let array be the result of calling ToObject passing the this value as the argument.
    	    var array = ToObject(this);
    	    // 2. Let arrayLen be the result of calling the [[Get]] internal method of array with argument "length".
    	    var arrayLen = Geto(array, "length");
    	    // 3. Let len be ToUint32(arrayLen).
    	    var len = ToUint32(arrayLen)
    	    // 4. Let separator be the String value for the list-separator String appropriate for the host environment’s current locale (this is derived in an implementation-defined way).
    	    var separator = ","
    	    // 5. If len is zero, return the empty String.
    	    if (len === 0) return "";
    	    // 6. Let firstElement be the result of calling the [[Get]] internal method of array with argument "0".
    	    var firstElement = Geto(array, "0")
    	    // 7. If firstElement is undefined or null, then
    	    var R, elementObj;
    	    if (firstElement == null) {
    		// a. Let R be the empty String.
    		R = "";
    	    } // 8. Else
    	    else {
    		// a. Let elementObj be ToObject(firstElement).
    		elementObj = ToObject(firstElement);
    		// b. Let func be the result of calling the [[Get]] internal method of elementObj with argument "toLocaleString".
    		var func = Geto(elementObj, "toLocaleString")
    		// c. If IsCallable(func) is false, throw a TypeError exception.
    		if (IsCallable(func) === false) {
    		    throw new native.TypeError(typeof func + " is not a function")
    		}
    		// d. Let R be the result of calling the [[Call]] internal method of func providing elementObj as the this value and an empty arguments list.
    		R = ToString(Call(func, elementObj));
    	    }
    	    // 9. Let k be 1.
    	    var k = 1;
    	    // 10. Repeat, while k < len
    	    while (k < len) {
    		// a. Let S be a String value produced by concatenating R and separator.
    		var S = R + separator;
    		// b. Let nextElement be the result of calling the [[Get]] internal method of array with argument ToString(k).
    		var nextElement = Geto(array, ToString(k));
    		// c. If nextElement is undefined or null, then
    		if (nextElement == null) {
    		    // i. Let R be the empty String.
    		    R = "";
    		} // d. Else
    		else {
    		    // i. Let elementObj be ToObject(nextElement).
    		    elementObj = ToObject(nextElement);
    		    // ii. Let func be the result of calling the [[Get]] internal method of elementObj with argument "toLocaleString".
    		    var func = Geto(elementObj, "toLocaleString");
    		    // iii. If IsCallable(func) is false, throw a TypeError exception.
    		    if (IsCallable(func) === false) {
    			throw new native.TypeError(typeof func + " is not a function");
    		    }
    		    // iv. Let R be the result of calling the [[Call]] internal method of func providing elementObj as the this value and an empty arguments list.
    		    R = ToString(Call(func, elementObj));
    		}
    		// e. Let R be a String value produced by concatenating S and R.
    		R = S + R;
    		// f. Increase k by 1.
    		k++;
    	    }
    	    // 11. Return R.
    	    return R;
    	};

    	return register_function(f, "toLocaleString", true, 0);
    })());
    // 15.4.4.4 Array.prototype.concat( [item1 [, item2 [, ...]]] )
    eFn(virtual.Array.prototype, "concat", true, false, true, (function() {
    	var f =  function (item) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.concat');
    	    // 2. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
    	    var A = new native.Array();
    	    // 3. Let n be 0.
    	    var n = 0;
    	    // 4. Let items be an internal List whose first element is O and whose subsequent elements are, in left to right order, the arguments that were passed to this function invocation.
    	    var items = [O];
    	    for (var i = 0; i< arguments.length; i++) {
		descTTT.value = arguments[i];
    		native.Object_defineProperty(items, ToString(i + 1), descTTT);
    	    }
	    
    	    // 5. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of the element.
    		var E = items[i];
    		// b. If the value of the [[Class]] internal property of E is "Array", then
    		if(typeof E === 'object' && Class(E) === "Array") {
    		    // i. Let k be 0.
    		    var k = 0;
    		    // ii. Let len be the result of calling the [[Get]] internal method of E with argument "length".
    		    var len = Geto(E, "length");
    		    // iii. Repeat, while k < len
    		    while(k < len){
    			// 1. Let P be ToString(k).
    			var P = ToString(k);
    			// 2. Let exists be the result of calling the [[HasProperty]] internal method of E with P.
    			var exists = HasProperty(P, E);
    			// 3. If exists is true, then
    			if (exists) {
    			    // a Let subElement be the result of calling the [[Get]] internal method of E with argument P.
    			    var subElement = Geto(E, P);
    			    // b Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), Property Descriptor {[[Value]]: subElement, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
			    descTTT.value = subElement;
    			    DefineOwnProperty(A, ToString(n), descTTT, false);
    			}
    			// 4. Increase n by 1.
    			n++;
    			// 5. Increase k by 1.
    			k++;
    		    }
    		}
    		// c. Else, E is not an Array
    		else {
    		    // i. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), Property Descriptor {[[Value]]: E, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
		    descTTT.value = E;
    		    DefineOwnProperty(A, ToString(n), descTTT, false);
    		    // ii. Increase n by 1.
    		    n++;
    		}
    	    }
    	    A.length = n;
    	    // 6. Return A.
    	    return A;
    	};
    	return register_function(f, "concat", true, 1);
    })());
    // 15.4.4.5 Array.prototype.join (separator)
    eFn(virtual.Array.prototype, "join", true, false, true, (function() {
	var nat = native.Array.prototype.join;
    	var f = function (separator) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);

	    // optimized path.
	    // TODO need to confirm.
	    // var desc = Reflect_getOwnPropertyDescriptor(O, "length");
	    // if (desc !== undefined) {
	    // 	var opt = true;
	    // 	if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = "value", arg1)) &&
	    // 	    typeof desc.value === "number") {
	    // 	    for (var i=0; i<O.length; i++) {
	    // 		if (!native_apply(Object_prototype_hasOwnProperty, O, (arg1[0] = i, arg1))) {
	    // 		    opt = false;
	    // 		    break;
	    // 		}
	    // 	    }
	    // 	}
	    // 	if (opt) return native_apply(nat, O, (arg1[0] = separator, arg1));
	    // }

    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. If separator is undefined, let separator be the single-character String ",".
    	    if (separator === undefined) {
    		separator = ",";
    	    }
    	    // 5. Let sep be ToString(separator).
    	    var sep = ToString(separator);
    	    // 6. If len is zero, return the empty String.
    	    if (len === 0){
    		return "";
    	    }
    	    // 7. Let element0 be the result of calling the [[Get]] internal method of O with argument "0".
    	    var element0 = Geto(O, "0");
    	    // 8. If element0 is undefined or null, let R be the empty String; otherwise, Let R be ToString(element0).
    	    if (element0 == null) {
    		var R = "";
    	    }else {
    		var R = ToString(element0);
    	    }
    	    // 9. Let k be 1.
    	    var k = 1;
    	    // 10. Repeat, while k < len
    	    while (k < len) {
    		// a. Let S be the String value produced by concatenating R and sep.
    		var S = R + sep;
    		// b. Let element be the result of calling the [[Get]] internal method of O with argument ToString(k).
    		var element = Geto(O, ToString(k));
    		// c. If element is undefined or null, Let next be the empty String; otherwise, let next be ToString(element).
    		if (element == null) {
    		    var next = "";
    		}else {
    		    var next = ToString(element);
    		}
    		// d. Let R be a String value produced by concatenating S and next.
    		var R = S + next;
    		// e. Increase k by 1.
    		k++;
    	    }
    	    //11. Return R.
    	    return R;
    	};
    	return register_function(f, "join", true, 1);
    })());
    // 15.4.4.6 Array.prototype.pop()
    eFn(virtual.Array.prototype, "pop", true, false, true, (function() {
	var nat = native.Array.prototype.pop;
    	var f = function () {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. If len is zero, then
    	    if (len === 0) {
    		// a. Call the [[Put]] internal method of O with arguments "length", 0, and true.
    		Put(O, "length",  0);
    		// b. Return undefined.
    		return undefined;
    	    } else { // 5. Else,
		// a. Assert: len > 0.
		// b. Let newLen be len - 1.
		let newLen = len - 1;

		// optimization
		// if (native_apply(Object_prototype_hasOwnProperty, O, (arg1[0] = oindx, arg1)) &&
		//     native_apply(Object_prototype_hasOwnProperty, O, (arg1[0] = "length", arg1)))
		//     return native_apply(nat, O, arg0);

		// c. Let index be ! ToString(newLen).
		let index = "" + newLen;
		// d. Let element be ? Get(O, index).
		let element = Geto(O, index);
		// e. Perform ? DeletePropertyOrThrow(O, index).
		delete O[index];
		// f. Perform ? Set(O, "length", newLen, true).
		Put(O, "length", newLen);
		// g. Return element.
		return element;
    	    }
    	};
    	return register_function(f, "pop", true, 0);
    })());
    // 15.4.4.7 Array.prototype.push ( [item1 [, item2 [, ... ]]] )
    eFn(virtual.Array.prototype, "push", true, false, true, (function() {
	var nat = native.Array.prototype.push;
    	var f = function (v) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);

	    // optimized path.
	    // if (arguments.length === 1) {
	    // 	var desc = Reflect_getOwnPropertyDescriptor(O, "length");
	    // 	if (desc !== undefined) {
	    // 	    if (native_apply(Object_prototype_hasOwnProperty, desc, (arg1[0] = "value", arg1)) &&
	    // 		typeof desc.value === "number") {
	    // 		// TODO need to confirm.
	    // 		if (!native_apply(Object_prototype_hasOwnProperty, O, (arg1[0] = desc.value, arg1))) {
	    // 		    return native_apply(nat, O, (arg1[0] = v, arg1));
	    // 		}
	    // 	    }
	    // 	}
	    // }

    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let n be ToUint32(lenVal).
    	    var n = ToUint32(lenVal);
    	    // 4. Let items be an internal List whose elements are, in left to right order, the arguments that were passed to this function invocation.
    	    var items = arguments;
    	    // 5. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of the element.
    		var E = items[i];
    		// b. Call the [[Put]] internal method of O with arguments ToString(n), E, and true.
    		Put(O, ToString(n), E, true);
    		// c. Increase n by 1.
    		n++;
    	    }
    	    // 6. Call the [[Put]] internal method of O with arguments "length", n, and true.
    	    Put(O, "length", n, true);
    	    // 7. Return n.
    	    return n;
    	}
    	return register_function(f, "push", true, 1);
    })());
    // 15.4.4.8 Array.prototype.reverse()
    eFn(virtual.Array.prototype, "reverse", true, false, true, (function() {
    	var f = function () {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. Let middle be floor(len/2).
    	    var middle = native.Math_floor(len/2);
    	    // 5. Let lower be 0.
    	    var lower = 0;
    	    // 6. Repeat, while lower !=  middle
    	    while (lower != middle){
    		// a. Let upper be len-lower-1.
    		var upper = len-lower-1;
    		// b. Let upperP be ToString(upper).
    		var upperP = ToString(upper);
    		// c. Let lowerP be ToString(lower).
    		var lowerP = ToString(lower);
    		// d. Let lowerValue be the result of calling the [[Get]] internal method of O with argument lowerP.
    		var lowerValue = Geto(O, lowerP);
    		// e. Let upperValue be the result of calling the [[Get]] internal method of O with argument upperP .
    		var upperValue = Geto(O, upperP);
    		// f. Let lowerExists be the result of calling the [[HasProperty]] internal method of O with argument lowerP.
    		var lowerExists = HasProperty(lowerP, O);
    		// g. Let upperExists be the result of calling the [[HasProperty]] internal method of O with argument upperP.
    		var upperExists = HasProperty(upperP, O);
    		// h. If lowerExists is true and upperExists is true, then
    		if(lowerExists && upperExists){
    		    // i. Call the [[Put]] internal method of O with arguments lowerP, upperValue, and true .
    		    Put(O, lowerP, upperValue);
    		    // ii. Call the [[Put]] internal method of O with arguments upperP, lowerValue, and true .
    		    Put(O, upperP, lowerValue);
    		}
    		// i. Else if lowerExists is false and upperExists is true, then
    		else if(!lowerExists && upperExists){
    		    // i. Call the [[Put]] internal method of O with arguments lowerP, upperValue, and true .
    		    Put(O, lowerP, upperValue);
    		    // ii. Call the [[Delete]] internal method of O, with arguments upperP and true.
    		    delete O[upperP];
    		}
    		// j. Else if lowerExists is true and upperExists is false, then
    		else if (lowerExists && !upperExists){
    		    // i. Call the [[Delete]] internal method of O, with arguments lowerP and true .
    		    delete O[lowerP];
    		    // ii. Call the [[Put]] internal method of O with arguments upperP, lowerValue, and true .
    		    Put(O, upperP, lowerValue);
    		}
    		// k. Else both lowerExists and upperExists are false
		else {
    		    // i. No action is required.
		}
    		// l. Increase lower by 1.
    		lower++;
    	    }
    	    // 7. Return O .
    	    return O;
    	};
    	return register_function(f, "reverse", true, 0);
    })());
    // 15.4.4.9 Array.prototype.shift()
    eFn(virtual.Array.prototype, "shift", true, false, true, (function() {
    	var f = function() {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. If len is zero, then
    	    if(len === 0){
    		// a. Call the [[Put]] internal method of O with arguments "length", 0, and true.
    		Put(O, "length", 0);
    		// b. Return undefined.
    		return undefined;
    	    }
    	    // 5. Let first be the result of calling the [[Get]] internal method of O with argument "0".
    	    var first = Geto(O, "0");
    	    // 6. Let k be 1.
    	    var k = 1;
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let from be ToString(k).
    		var from = ToString(k);
    		// b. Let to be ToString(k–1).
    		var to = ToString(k-1);
    		// c. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		var fromPresent = HasProperty(from, O);
    		// d. If fromPresent is true, then
    		if(fromPresent){
    		    // i. Let fromVal be the result of calling the [[Get]] internal method of O with argument from.
    		    var fromVal = Geto(O, from);
    		    // ii. Call the [[Put]] internal method of O with arguments to, fromVal, and true.
    		    Put(O, to, fromVal);
    		}
    		// e. Else, fromPresent is false
    		else{
    		    // i. Call the [[Delete]] internal method of O with arguments to and true.
    		    delete O[to];
    		}
    		// f. Increase k by 1.
    		k++;
    	    }
    	    // 8. Call the [[Delete]] internal method of O with arguments ToString(len–1) and true.
    	    delete O[ToString(len-1)];
    	    // 9. Call the [[Put]] internal method of O with arguments "length", (len–1) , and true.
    	    Put(O, "length", len-1);
    	    // 10. Return first.
    	    return first;
    	};
    	return register_function(f, "shift", true, 0);
    })());
    // 15.4.4.10 Array.prototype.slice(start, end)
    eFn(virtual.Array.prototype, "slice", true, false, true, (function() {
    	var f = function (start, end) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
    	    var A = new native.Array();
    	    // 3. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 4. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 5. Let relativeStart be ToInteger(start).
    	    var relativeStart = ToInteger(start);
    	    // 6. If relativeStart is negative, let k be max((len + relativeStart),0); else let k be min(relativeStart, len).
    	    if (relativeStart < 0){
    		var k = native.Math_max(len+relativeStart,0);
    	    } else {
    		var k = native.Math_min(relativeStart,len);
    	    }
    	    // 7. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    	    if (end === undefined){
    		var relativeEnd = len;
    	    } else {
    		var relativeEnd = ToInteger(end);
    	    }
    	    // 8. If relativeEnd is negative, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    	    if (relativeEnd < 0){
    		var final = native.Math_max(len+relativeEnd,0);
    	    } else {
    		var final = native.Math_min(relativeEnd,len);
    	    }
    	    // 9. Let n be 0.
    	    var n = 0;
    	    // 10. Repeat, while k < final
    	    while (k < final){
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), Property Descriptor {[[Value]]: kValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]:    true}, and false.
		    descTTT.value = kValue;
    		    DefineOwnProperty(A, ToString(n), descTTT, false);
    		}
    		// d. Increase k by 1.
    		k++;
    		// e. Increase n by 1.
    		n++;
    	    }
    	    A.length = n;
    	    // 11. Return A.
    	    return A;
    	};
    	return register_function(f, "slice", true, 2);
    })());
    // 15.4.4.11 Array.prototype.sort(comparefn)
    simpleFn(virtual.Array.prototype, native.Array.prototype, "sort", 1, true);
    eFn(virtual.Array.prototype, "sort", true, false, true, (function() {
    	var f = function (comparefn) {
	    // Let obj be the result of calling ToObject passing the this value as the argument.
    	    var obj = ToObject(this);
	    // Let len be the result of applying Uint32 to the result of calling the [[Get]] internal method of obj with argument "length".
    	    var len = ToUint32(Geto(obj, "length"));
	    // SortCompare abstract operation with two arguments j and k:
    	    var SortCompare = function(j, k) {
    		// 1. Let jString be ToString(j).
    		var jString = ToString(j);
    		// 2. Let kString be ToString(k).
    		var kString = ToString(k);
    		// 3. Let hasj be the result of calling the [[HasProperty]] internal method of obj with argument jString.
    		var hasj = HasProperty(jString, obj);
    		// 4. Let hask be the result of calling the [[HasProperty]] internal method of obj with argument kString.
    		var hask = HasProperty(kString, obj);
    		// 5. If hasj and hask are both false, then return +0.
    		if (hasj === false && hask === false) return +0;
    		// 6. If hasj is false, then return 1.
    		if (hasj === false) return 1;
    		// 7. If hask is false, then return –1.
    		if (hask === false) return -1;
    		// 8. Let x be the result of calling the [[Get]] internal method of obj with argument jString.
    		var x = Geto(obj, jString);
    		// 9. Let y be the result of calling the [[Get]] internal method of obj with argument kString.
    		var y = Geto(obj, kString);
    		// 10. If x and y are both undefined, return +0.
    		if (x === undefined && y === undefined) return +0;
    		// 11. If x is undefined, return 1.
    		if (x === undefined) return 1;
    		// 12. If y is undefined, return −1.
    		if (y === undefined) return -1;
    		// 13. If the argument comparefn is not undefined, then
    		if (comparefn !== undefined) {
    		    // a. If IsCallable(comparefn) is false, throw a TypeError exception.
    		    if (IsCallable(comparefn) === false) {
    			throw new native.TypeError(typeof comparefn + " is not a function");
    		    }
    		    // b. Return the result of calling the [[Call]] internal method of comparefn passing undefined as the this value and with arguments x and y.
    		    return Call(comparefn, undefined, [x, y]);
    		}
    		// 14. Let xString be ToString(x).
    		var xString = ToString(x);
    		// 15. Let yString be ToString(y).
    		var yString = ToString(y);
    		// 16. If xString < yString, return −1.
    		if (xString < yString) return -1;
    		// 17. If xString > yString, return 1.
    		if (xString > yString) return 1;
    		// 18. Return +0.
    		return +0;
    	    };

	    // an implementation dependent sorting algorithm.
    	    var b, tmp;
    	    do {
    		b = false;
    		for (var i = 1; i < len; i++) {
		    var ii = i-1, iis = "" + ii, is = "" + i;
    		    var k = SortCompare(ii, i);
    		    if (SortCompare(ii, i) > 0) {
			// tmp = obj[i - 1];
			tmp = Geto(obj, iis);
    			// obj[i-1] = obj[i];
			Put(obj, iis, Geto(obj, is), false);
    			// obj[i] = tmp;
			Put(obj, is, tmp, false);
    			b = true;
    		    }
    		}
    	    } while (b);
    	    return obj;
    	};
    	return register_function(f, "sort", true, 1);
    })());
    // 15.4.4.12 Array.prototype.splice(start, deleteCount [, item1 [, item2 [, ...]]] )
    eFn(virtual.Array.prototype, "splice", true, false, true, (function() {
    	var f = function splice (start, deleteCount) {
	    var actualStart;
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let A be a new array created as if by the expression new Array() where Array is the standard  built-in constructor with that name.
    	    var A = new native.Array();
    	    // 3. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 4. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 5. Let relativeStart be ToInteger(start).
    	    var relativeStart = ToInteger(start);
    	    // 6. If relativeStart is negative, let actualStart be max((len + relativeStart),0); else let actualStart be min(relativeStart, len).
    	    if (relativeStart < 0) {
    		actualStart = native.Math_max(len + relativeStart, 0);
    	    } else {
    		actualStart = native.Math_min(relativeStart, len);
    	    }
    	    // 7. Let actualDeleteCount be min(max(ToInteger(deleteCount),0), len – actualStart).
    	    var actualDeleteCount = native.Math_min(native.Math_max(ToInteger(deleteCount), 0), len - actualStart);
    	    // 8. Let k be 0.
    	    var k = 0;
    	    // 9. Repeat, while k < actualDeleteCount
    	    while (k < actualDeleteCount) {
    		// a. Let from be ToString(actualStart+k).
    		var from = ToString(actualStart + k);
    		// b. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		var fromPresent = HasProperty(from, O);
    		// c. If fromPresent is true, then
    		if (fromPresent) {
    		    // i. Let fromValue be the result of calling the [[Get]] internal method of O with argument from.
    		    var fromValue = Geto(O, from);
    		    // ii. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(k), Property Descriptor {[[Value]]: fromValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
		    descTTT.value = fromValue;
    		    DefineOwnProperty(A, ToString(k), descTTT, false);
    		}
    		// d. Increment k by 1.
    		k += 1;
    	    }
    	    // 10. Let items be an internal List whose elements are, in left to right order, the portion of the actual argument list starting with item1. The list will be empty if no such items are present.
    	    var items = new native.Array();
    	    for (var i = 0;i < arguments.length - 2;i++) {
		descTTT.value = arguments[i+2];
    		native.Object_defineProperty(items, ToString(i), descTTT);
    	    }
    	    // 11. Let itemCount be the number of elements in items.
    	    var itemCount = items.length;
    	    // 12. If itemCount < actualDeleteCount, then
    	    if (itemCount < actualDeleteCount) {
    		// a. Let k be actualStart.
    		var k = actualStart;
    		// b. Repeat, while k < (len – actualDeleteCount)
    		while ( k < (len - actualDeleteCount)) {
    		    // i. Let from be ToString(k+actualDeleteCount).
    		    var from = ToString(k + actualDeleteCount);
    		    // ii. Let to be ToString(k+itemCount).
    		    var to = ToString(k + itemCount);
    		    // iii. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		    var fromPresent = HasProperty(from, O);
    		    // iv If fromPresent is true, then
    		    if (fromPresent) {
    			// 1. Let fromValue be the result of calling the [[Get]] internal method of O with argument  from.
    			var fromValue = Geto(O, from);
    			// 2. Call the [[Put]] internal method of O with arguments to, fromValue, and true.
    			Put(O, to, fromValue);
    		    }
    		    // v. Else, fromPresent is false
    		    else {
    			// 1. Call the [[Delete]] internal method of O with arguments to and true.
    			delete O[to];
    		    }
    		    // vi. Increase k by 1.
    		    k++;
    		}
    		//c. Let k be len.
    		var k = len;
    		// d. Repeat, while k > (len – actualDeleteCount + itemCount)
    		while (k > (len - actualDeleteCount + itemCount)) {
    		    // i. Call the [[Delete]] internal method of O with arguments ToString(k–1) and true.
    		    delete O[ToString(k-1)];
    		    // ii. Decrease k by 1.
    		    k -= 1;
    		}
    	    }
    	    // 13. Else if itemCount > actualDeleteCount, then
    	    else {
    		// a. Let k be (len – actualDeleteCount).
    		var k = len - actualDeleteCount;
    		// b. Repeat, while k > actualStart
    		while (k > actualStart) {
    		    // i. Let from be ToString(k + actualDeleteCount – 1).
    		    var from = ToString(k + actualDeleteCount - 1);
    		    // ii. Let to be ToString(k + itemCount – 1)
    		    var to = ToString(k + itemCount - 1);
    		    // iii. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		    var fromPresent = HasProperty(from, O);
    		    // iv. If fromPresent is true, then
    		    if (fromPresent) {
    			// 1. Let fromValue be the result of calling the [[Get]] internal method of O with argument  from.
    			var fromValue = Geto(O, from);
    			// 2. Call the [[Put]] internal method of O with arguments to, fromValue, and true.
    			Put(O, to, fromValue);
    		    } else { // v. Else, fromPresent is false
    			// 1. Call the [[Delete]] internal method of O with argument to and true.
    			delete O[to];
    		    }
    		    // vi. Decrease k by 1.
    		    k -= 1;
    		}
    	    }
    	    // 14. Let k be actualStart.
    	    var k = actualStart;
    	    // 15. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of that element.
    		var E = items[i];
    		// b. Call the [[Put]] internal method of O with arguments ToString(k), E, and true.
    		Put(O, ToString(k), E);
    		// c. Increase k by 1.
    		k++;
    	    }
    	    // 16. Call the [[Put]] internal method of O with arguments "length", (len – actualDeleteCount +   itemCount), and true.
    	    Puto(O, "length", len - actualDeleteCount + itemCount);
    	    A.length = actualDeleteCount;
    	    // 17. Return A.
    	    return A;
    	};
    	return register_function(f, "splice", true, 2);
    })());
    // 15.4.4.13 Array.prototype.unshift([item1 [, item2 [, ...]]])
    eFn(virtual.Array.prototype, "unshift", true, false, true, (function() {
    	var f = function (V) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. Let argCount be the number of actual arguments.
    	    var argCount = arguments.length;
    	    // 5. Let k be len.
    	    var k = len;
    	    // 6. Repeat, while k > 0,
    	    while (k > 0) {
    		// a. Let from be ToString(k–1).
    		var from = ToString(k-1);
    		// b. Let to be ToString(k+argCount –1).
    		var to = ToString(k + argCount - 1);
    		// c. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		var fromPresent = HasProperty(from, O);
    		// d. If fromPresent is true, then
    		if (fromPresent) {
    		    // i. Let fromValue be the result of calling the [[Get]] internal method of O with argument from.
    		    var fromValue = Geto(O, from);
    		    // ii. Call the [[Put]] internal method of O with arguments to, fromValue, and true.
    		    Put(O, to, fromValue);
    		}
    		// e. Else, fromPresent is false
    		else {
    		    // i. Call the [[Delete]] internal method of O with arguments to, and true.
    		    delete O[to];
    		}
    		// f. Decrease k by 1.
    		k -= 1;
    	    }
    	    // 7. Let j be 0.
    	    var j = 0;
    	    // 8. Let items be an internal List whose elements are, in left to right order, the arguments that were passed to this function invocation.
    	    var items = arguments;
    	    // 9. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of that element.
    		var E = items[i];
    		// b. Call the [[Put]] internal method of O with arguments ToString(j), E, and true.
    		Put(O, ToString(j), E);
    		// c. Increase j by 1.
    		j++;
    	    }
    	    // 10. Call the [[Put]] internal method of O with arguments "length", len+argCount, and true.
    	    Put(O, "length", len + argCount);
    	    // 11. Return len+argCount.
    	    return len + argCount;
    	};
    	return register_function(f, "unshift", true, 1);
    })());
    // 15.4.4.14 Array.prototype.indexOf(searchElement [, fromIndex])
    eFn(virtual.Array.prototype, "indexOf", true, false, true, (function() {
    	var f = function indexOf (searchElement /* ,fromIndex */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.indexOf');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If len is 0, return -1.
    	    if (len === 0){
    		return -1;
    	    }
    	    // 5. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be 0.
    	    if (arguments.length > 1){
    		var n = ToInteger(arguments["1"]);
    	    } else {
    		var n = 0;
    	    }
    	    // 6. If n ≥ len, return -1.
    	    if (n>=len){
    		return -1;
    	    }
    	    // 7. If n ≥ 0, then
    	    if (n>=0){
    		// a. Let k be n.
    		var k = n;
    	    }
    	    // 8. Else, n<0
    	    else {
    		// a. Let k be len - abs(n).
    		var k = len - native.Math_abs(n);
    		// b. If k is less than 0,then let k be 0.
    		if (k<0) {
    		    var k = 0;
    		}
    	    }
    	    // 9. Repeat, while k<len
    	    while (k < len) {
    		// a. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument ToString(k).
    		var kPresent = HasProperty(ToString(k), O);
    		// b. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let elementK be the result of calling the [[Get]] internal method of O with the argument ToString(k).
    		    var elementK = Geto(O, ToString(k));
    		    // ii. Let same be the result of applying the Strict Equality Comparison Algorithm to searchElement and elementK.
    		    var same = searchElement === elementK;
    		    // iii. If same is true, return k.
    		    if (same) {
    			return k;
    		    }
    		}
    		// c. Increase k by 1.
    		k++;
    	    }
    	    // 10. Return -1.
    	    return -1;
    	};
    	return register_function(f, "indexOf", true, 1);
    })());
    // 15.4.4.15 Array.prototype.lastIndexOf(searchElement [, fromIndex])
    eFn(virtual.Array.prototype, "lastIndexOf", true, false, true, (function() {
    	var f = function lastIndexOf (searchElement /* ,fromIndex */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.lastIndexOf');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If len is 0, return -1.
    	    if (len === 0) {
    		return -1;
    	    }
    	    // 5. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be len-1.
    	    if(arguments.length > 1){
    		var n = ToInteger(arguments["1"]);
    	    }else {
    		var n = len-1;
    	    }
    	    // 6. If n ≥ 0, then let k be min(n,len–1).
    	    if (n>=0){
    		var k = native.Math_min(n,len-1);
    	    }
    	    // 7. Else, n < 0
    	    else {
    		// a. Let k be len - abs(n).
    		var k = len - native.Math_abs(n);
    	    }
    	    // 8. Repeat, while k≥ 0
    	    while (k>=0) {
    		// a. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument ToString(k).
    		var kPresent = HasProperty(ToString(k), O);
    		// b. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let elementK be the result of calling the [[Get]] internal method of O with the argument ToString(k).
    		    var elementK = Geto(O, ToString(k));
    		    // ii. Let same be the result of applying the Strict Equality Comparison Algorithm to searchElement and elementK.
    		    var same = searchElement === elementK;
    		    // iii. If same is true, return k.
    		    if (same) {
    			return k;
    		    }
    		}
    		// c. Decrease k by 1.
    		k -= 1;
    	    }
    	    // 9. Return -1.
    	    return -1;
    	};
    	return register_function(f, "lastIndexOf", true, 1);
    })());
    // 15.4.4.16 Array.prototype.every(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "every", true, false, true, (function() {
    	var f = function every (callbackfn /*, thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length > 1) {
    		var T = arguments["1"];
    	    }else {
    		var T = undefined;
    	    }
    	    // 6. Let k be 0.
    	    var k = 0;
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let testResult be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var testResult = Call(callbackfn,T,[kValue,k,O]);
    		    // iii. If ToBoolean(testResult) is false, return false.
    		    if (ToBoolean(testResult) === false) {
    			return false;
    		    }
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    //8. Return true.
    	    return true;
    	};
    	return register_function(f, "every", true, 1);
    })());
    // 15.4.4.17 Array.prototype.some(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "some", true, false, true, (function() {
    	var f = function some (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.some');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length > 1) {
    		var T = arguments["1"];
    	    } else {
    		var T = undefined;
    	    }
    	    // 6. Let k be 0.
    	    var k = 0; 
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let testResult be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var testResult = Call(callbackfn,T,[kValue,k,O]);
    		    // iii. If ToBoolean(testResult) is true, return true.
    		    if (ToBoolean(testResult) === true) {
    			return true;
    		    }
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 8. Return false.
    	    return false;
    	};
    	return register_function(f, "some", true, 1);
    })());
    // 15.4.4.18 Array.prototype.forEach(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "forEach", true, false, true, (function() {
    	var f = function (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError();
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length > 1) {
    		var T = arguments["1"];
    	    } else {
    		var T = undefined;
    	    }
    	    // 6. Let k be 0.
    	    var k = 0;
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Call the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    Call(callbackfn, T, [kValue, k, O]);
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 8. Return undefined.
    	    return undefined;
    	};
    	return register_function(f, "forEach", true, 1);
    })());
    // 15.4.4.19 Array.prototype.map(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "map", true, false, true, (function() {
    	var f = function (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    var T;
    	    if (arguments.length > 1){
    		T = arguments["1"];
    	    } else {
    		T = undefined;
    	    }
    	    // 6. Let A be a new array created as if by the expression new Array(len) where Array is the standard built-in constructor with that name and len is the value of len.
    	    var A = new native.Array(len);
    	    // 7. Let k be 0.
    	    var k = 0;
    	    // 8. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O)
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let mappedValue be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var m = Call(callbackfn, T, [kValue, k, O]);
    		    // iii. Call the [[DefineOwnProperty]] internal method of A with arguments Pk, Property Descriptor {[[Value]]: mappedValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
		    descTTT.value = m;
    		    DefineOwnProperty(A, Pk, descTTT, false);
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    //9. Return A.
    	    return A;
    	};
    	return register_function(f, "map", true, 1);
    })());
    // 15.4.4.20 Array.prototype.filter(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "filter", true, false, true, (function() {
    	var f = function filter (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length >1) {
    		var T = arguments["1"];
    	    }else {
    		var T = undefined;
    	    }
    	    // 6. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
    	    var A = new native.Array();
    	    // 7. Letk be 0.
    	    var k = 0;
    	    // 8. Let to be 0.
    	    var to = 0;
    	    // 9. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let selected be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var selected = Call(callbackfn,T,[kValue,k,O]);
    		    // iii. If ToBoolean(selected) is true, then
    		    if (ToBoolean(selected) === true) {
    			// 1. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(to), Property Descriptor {[[Value]]: kValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
			descTTT.value = kValue;
    			DefineOwnProperty(A, ToString(to), descTTT, false);
    			// 2. Increase to by 1.
    			to += 1;
    		    }
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 10. Return A.
    	    return A;
    	};
    	return register_function(f, "filter", true, 1);
    })());
    // 15.4.4.21 Array.prototype.reduce(callbackfn [, initialVAlue])
    eFn(virtual.Array.prototype, "reduce", true, false, true, (function() {
    	var f = function (callbackfn /*, initialValue */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.reduce');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function")
    	    }
    	    // 5. If len is 0 and initialValue is not present, throw a TypeError exception.
    	    if (len === 0 && arguments.length < 2) {
    		throw new native.TypeError();
    	    }
    	    // 6. Let k be 0.
    	    var k = 0;
    	    // 7. If initialValue is present, then
    	    if (arguments.length > 1) {
    		// a. Set accumulator to initialValue.
    		var accumulator = arguments["1"];
    	    }
    	    // 8. Else, initialValue is not present
    	    else {
    		// a. Let kPresent be false.
    		var kPresent = false;
    		// b. Repeat, while kPresent is false and k < len
    		while (kPresent === false && k < len) {
    		    // i. Let Pk be ToString(k).
    		    var Pk = ToString(k);
    		    //ii. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		    var kPresent = HasProperty(Pk, O);
    		    // iii. If kPresent is true, then
    		    if (kPresent) {
    			// 1. Let accumulator be the result of calling the [[Get]] internal method of O with argument Pk.
    			var accumulator = Geto(O, Pk);
    		    }
    		    // iv. Increase k by 1.
    		    k++;
    		}
    		// c. If kPresent is false, throw a TypeError exception.
    		if (!kPresent) {
    		    throw new TypeError();
    		}
    	    }
    	    // 9. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let accumulator be the result of calling the [[Call]] internal method of callbackfn with undefined as the this value and argument list containing accumulator, kValue, k, and O.
    		    var accumulator = Call(callbackfn, undefined, [accumulator, kValue, k, O]);
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 10. Return accumulator.
    	    return accumulator;
    	};
    	return register_function(f, "reduce", true, 1);
    })());
    // 15.4.4.22 Array.prototype.reduceRight(callbackfn [, initialValue])
    eFn(virtual.Array.prototype, "reduceRight", true, false, true, (function() {
    	var f = function (callbackfn /*, initialValue */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If len is 0 and initialValue is not present, throw a TypeError exception.
    	    if (len === 0 && arguments.length < 2) {
    		throw new native.TypeError("Reduce of empty array with no initial value");
    	    }
    	    // 6. Let k be len-1.
    	    var k = len - 1;
    	    // 7. If initialValue is present, then
    	    if (arguments.length > 1) {
    		// a. Set accumulator to initialValue.
    		var accumulator = arguments["1"];
    	    }
    	    // 8. Else, initialValue is not present
    	    else {
    		// a. Let kPresent be false.
    		var kPresent = false;
    		// b. Repeat, while kPresent is false and k ≥ 0
    		while (kPresent === false && k >= 0) {
    		    // i. Let Pk be ToString(k).
    		    var Pk = ToString(k);
    		    // ii. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		    var kPresent = HasProperty(Pk, O);
    		    // iii. If kPresent is true, then
    		    if (kPresent) {
    			// 1. Let accumulator be the result of calling the [[Get]] internal method of O with argument Pk.
    			accumulator = Geto(O, Pk)
    		    }
    		    // iv. Decrease k by 1.
    		    k -= 1;
    		}
    		// c. If kPresent is false, throw a TypeError exception.
    		if (!kPresent) {
    		    throw new native.TypeError("Reduce of empty array with no initial value");
    		}
    	    }
    	    // 9. Repeat, while k ≥ 0
    	    while (k >= 0) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    //i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let accumulator be the result of calling the [[Call]] internal method of callbackfn with undefined as the this value and argument list containing accumulator, kValue, k, and O.
    		    var accumulator = Call(callbackfn, undefined, [accumulator, kValue, k, O]);
    		}
    		// d. Decrease k by 1.
    		k -= 1;
    	    }
    	    // 10. Return accumulator.
    	    return accumulator;
    	}
    	return register_function(f, "reduceRight", true, 1);
    })());

    // TODO need to confirm the semantics.
    eFn(virtual.Array.prototype, "fill", true, false, true, (function() {
    	var f = function (value) {
	    var start = arguments[1];
	    var end = arguments[2];
	    // 1. Let O be ? ToObject(this value).
	    var O = ToObject(this);
	    // 2. Let len be ? ToLength(? Get(O, "length")).
    	    var lenValue = Geto(O, "length");
    	    var len = ToUint32(lenValue);
	    // 3. Let relativeStart be ? ToInteger(start).
	    var relativeStart = ToInteger(start);
	    // 4. If relativeStart < 0, let k be max((len + relativeStart), 0); else let k be min(relativeStart, len)
	    var k = relativeStart < 0 ? native.Math_max((len + relativeStart), 0) : native.Math_min(relativeStart, len);
	    // 5. If end is undefined, let relativeEnd be len; else let relativeEnd be ? ToInteger(end).
	    var relativeEnd = end === undefined ? len : ToInteger(end);
	    // 6. If relativeEnd < 0, let final be max((len + relativeEnd), 0); else let final be min(relativeEnd, len).
	    var final = relativeEnd < 0 ? native.Math_max((len + relativeEnd), 0) : native.Math_min(relativeEnd, len);
	    // 7. Repeat, while k < final
	    while (k < final) {
		// a. Let Pk be ! ToString(k).
		var Pk = ToString(k);
		// b. Perform ? Set(O, Pk, value, true).
    		Puto(O, Pk, value, true);
		// c. Increase k by 1.
		k = k + 1;
	    }
	    // 8. Return O.
	    return O;
    	};
    	return register_function(f, "fill", true, 1);
    })());

    // 15.5 String Objects
    eFn(virtual, "String", true, false, true, (function () {
    	const o = String;
    	var f = function String(value) {
	    if (new.target === undefined) {
    		if (arguments.length === 0) return "";
    		else return ToString(value);	
	    } else {	
    	    	if (arguments.length === 0) return new o();
    	    	else return new o(ToString(value));
    	    }
    	};
    	return register_function(f, "String", false, 1);
    })());
    // Related to:
    // - /test262/test/suite/ch15/15.5/15.5.2/S15.5.2.1_A1_T8.js
    // - /test262/test/suite/ch15/15.5/15.5.2/S15.5.2.1_A1_T7.js
    // eFn(virtual, "String", true, false, true, (function() {
    // 	var f = function() {
    // 	    if (arguments.length > 0)
    // 		arguments[0] = ToString(arguments[0]);
    // 	    return native.String["@apply"](this, arguments);
    // 	};
    // 	return register_function(f, "String", false, 1)
    // })());
    (function() {
	// 15.5.3.1 String.prototype
    	var new_proto = new native.String();
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
	
    	native.Object.defineProperty(virtual.String, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.5.4.1 String.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.String, writable: true, enumerable: false, configurable: true}));
    })();

    // 15.5.3.2 String.fromCharCode([char0 [, char1 [, ...]]])
    eFn(virtual.String, "fromCharCode", true, false, true, (function () {
	var o = native.String.fromCharCode;
	var f = function (v) {
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToNumber(arguments[i]);
	    }
	    return native_apply(o, this, arguments);
	};
	return register_function(f, "fromCharCode", true, 1);
    })());
    // 15.5.4. Properties of the String Prototype Object
    // 15.5.4.2 String.prototype.toString()
    simpleFn(virtual.String.prototype, native.String.prototype, "toString", 0, true);
    // 15.5.4.3 String.prototype.valueOf()
    simpleFn(virtual.String.prototype, native.String.prototype, "valueOf", 0, true);
    // 15.5.4.4 String.prototype.charAt(pos)
    eFn(virtual.String.prototype, "charAt", true, false, true, (function() {
	var nat = String.prototype.charAt;
	var f = function(pos) {
	    // 1. Call CheckObjectCoercible passing the this value as its argument.
	    CheckObjectCoercible(this, 'String.prototype.charAt');
	    return native_apply(nat, ToString(this), (arg1[0] = ToInteger(pos), arg1));
	};
	return register_function(f, "charAt", true, 1);
    })());
    // 15.5.4.5 String.prototype.charCodeAt(pos)
    eFn(virtual.String.prototype, "charCodeAt", true, false, true, (function() {
	var nat = String.prototype.charCodeAt;
	var f = function(pos) {
	    CheckObjectCoercible(this, 'String.prototype.charCodeAt');
	    return native_apply(nat, ToString(this), (arg1[0] = ToInteger(pos), arg1));
	};
	return register_function(f, "charCodeAt", true, 1);
    })());
    // 15.5.4.6 String.prototype.concat([string1 [, string2 [, ...]]])
    eFn(virtual.String.prototype, "concat", true, false, true, (function() {
	var nat = String.prototype.concat;
	var f = function(v) {
	    CheckObjectCoercible(this, 'String.prototype.concat');
	    var S = ToString(this);
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToString(arguments[i]);
	    }
	    return native_apply(nat, S, arguments);
	};
	return register_function(f, "concat", true, 1);
    })());
    // 15.5.4.7 String.prototype.indexOf(searchString, position)
    eFn(virtual.String.prototype, "indexOf", true, false, true, (function() {
	var nat = String.prototype.indexOf;
	var f = function(searchString) {
	    var position;
	    if (arguments.length > 1)
		position = arguments[1];
	    CheckObjectCoercible(this, 'String.prototype.indexOf');
	    var S = ToString(this);
	    var searchStr = ToString(searchString);
	    if (position === undefined) position = 0;
	    var pos = ToInteger(position);
	    return native_apply(nat, S, (arg2[0] = searchStr, arg2[1] = pos, arg2));
	};
	return register_function(f, "indexOf", true, 1);
    })());
    // 15.5.4.8 String.prototype.lastIndexOf(searchString, position)
    eFn(virtual.String.prototype, "lastIndexOf", true, false, true, (function() {
	var nat = String.prototype.lastIndexOf;
	var f = function(searchString) {
	    var position;
	    if (arguments.length > 1)
		position = arguments[1];
	    CheckObjectCoercible(this, 'String.prototype.lastIndexOf');
	    var S = ToString(this);
	    var searchStr = ToString(searchString);
	    var pos = ToNumber(position);
	    return native_apply(nat, S, (arg2[0] = searchStr, arg2[1] = pos, arg2));
	};
	return register_function(f, "lastIndexOf", true, 1);
    })());
    // 15.5.4.9 String.prototype.localeCompare(that)
    eFn(virtual.String.prototype, "localeCompare", true, false, true, (function() {
	var nat = String.prototype.localeCompare;
	var f = function(that) {
	    CheckObjectCoercible(this, 'String.prototype.localeCompare');
	    var S = ToString(this);
	    that = ToString(that);
	    return native_apply(nat, S, (arg1[0] = that, arg1));
	};
	return register_function(f, "localeCompare", true, 1);
    })());
    // 15.5.4.10 String.prototype.match(regexp)
    eFn(virtual.String.prototype, "match", true, false, true, (function() {
	// original semantics
	var f = function(regexp) {
	    // 1. Call CheckObjectCoercible passing the this value as its argument.
	    CheckObjectCoercible(this, 'String.prototype.match');
	    // 2. Let S be the result of calling ToString, giving it the this value as its argument.
	    var S = ToString(this);
	    // 3. If Type(regexp) is Object and the value of the [[Class]] internal property of regexp is "RegExp", then let rx be regexp;
	    if (Class(regexp) === 'RegExp') {
		var rx = regexp;
	    } else {
		// 4. Else, let rx be a new RegExp object created as if by the expression new RegExp(regexp) where RegExp is the standard built-in constructor with that name.
		rx = new virtual_RegExp(regexp);
	    }
	    // 5. Let global be the result of calling the [[Get]] internal method of rx with argument "global".
	    var global = Geto(rx, "global");
	    // 6. Let exec be the standard built-in function RegExp.prototype.exec (see 15.10.6.2)
	    var exec = virtual_RegExp_prototype_exec;
	    // 7. If global is not true, then
	    if (global !== true) {
		// a. Return the result of calling the [[Call]] internal method of exec with rx as the this value and argument list containing S.
		return Call(exec, rx, [S]);
	    }
	    // Else, global is true
	    else if (global === true) {
		// a. Call the [[Put]] internal method of rx with arguments "lastIndex" and 0.
		Put(rx, "lastIndex", 0);
		// b. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
		var A = new native.Array();
		// c. Let previousLastIndex be 0.
		var previousLastIndex = 0;
		// d. Let n be 0.
		var n = 0;
		// e. Let lastMatch be true.
		var lastMatch = true;
		// f. Repeat, while lastMatch is true
		while (lastMatch === true) {
		    // i. Let result be the result of calling the [[Call]] internal method of exec with rx as the this value and argument list containing S.
		    var result = Call(exec, rx, [S]);
		    // ii. If result is null, then set lastMatch to false.
		    if (result === null) lastMatch = false;
		    // iii. Else, result is not null
		    else if (result !== null) {
			// 1. Let thisIndex be the result of calling the [[Get]] internal method of rx with argument "lastIndex".
			var thisIndex = Geto(rx, "lastIndex");
			// 2. If thisIndex = previousLastIndex then
			if (thisIndex === previousLastIndex) {
			    // a. Call the [[Put]] internal method of rx with arguments "lastIndex" and thisIndex+1.
			    Put(rx, "lastIndex", thisIndex + 1);
			    // b. Set previousLastIndex to thisIndex+1.
			    previousLastIndex = thisIndex + 1;
			}
			// 3. Else, set previousLastIndex to thisIndex.
			else {
			    previousLastIndex = thisIndex;
			}
			// 4. Let matchStr be the result of calling the [[Get]] internal method of result with argument "0".
			var matchStr = Geto(result, "0");
			// 5. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), the Property Descriptor {[[Value]]: matchStr, [[Writable]]: true, [[Enumerable]]: true, [[configurable]]: true}, and false.
			descTTT.value = matchStr;
			DefineOwnProperty(A, ToString(n), descTTT, false);
			// 6. Increment n.
			n++;
		    }
		}
		// g. If n = 0, then return null.
		if (n === 0) return null;
		// h. Return A.
		return A;
	    }
	    // This is an undefined behavior. V8 engine does not terminate the execution in this case.
	    throw new native.Error("undefined behavior");
	};
	return register_function(f, "match", true, 1);
    })());
    // function GetSubstitution(matched, str, position, captures, namedCaptures, replacement) {
    // 	// 1. Assert: Type(matched) is String.
    // 	// 2. Let matchLength be the number of code units in matched.
    // 	let matchLength = matched;
    // 	// 3. Assert: Type(str) is String.
    // 	// 4. Let stringLength be the number of code units in str.
    // 	let stringLength = str;
    // 	// 5. Assert: ! IsNonNegativeInteger(position) is true.
    // 	// 6. Assert: position ≤ stringLength.
    // 	// 7. Assert: captures is a possibly empty List of Strings.
    // 	// 8. Assert: Type(replacement) is String.
    // 	// 9. Let tailPos be position + matchLength.
    // 	let tailPos = position + matchLength;
    // 	// 10. Let m be the number of elements in captures.
    // 	let m = captures.length;
    // 	// 11. Let result be the String value derived from replacement by copying code unit elements from replacement to result while performing replacements as specified in Table 52.
    // 	// These $ replacements are done left-to-right, and, once such a replacement is performed, the new replacement text is not subject to further replacements.
	
    // 	// 12. Return result.
    // }
    // 15.5.4.11 String.prototype.replace(searchValue, replaceValue)
    eFn(virtual.String.prototype, "replace", true, false, true, (function() {
	var nat = String.prototype.replace;
	var f = function(searchValue, replaceValue) {
	    // 1. Let O be ? RequireObjectCoercible(this value).
	    CheckObjectCoercible(this, 'String.prototype.replace');
	    let O = this;
	    // 2. If searchValue is neither undefined nor null, then
	    if (searchValue !== null && searchValue !== undefined) {
		// a. Let replacer be ? GetMethod(searchValue, @@replace).
		let replacer = GetMethod(searchValue, native.s_symbol_replace);
		// b. If replacer is not undefined, then
		if (replacer !== undefined) {
		    // i. Return ? Call(replacer, searchValue, « O, replaceValue »).
		    return Call(replacer, searchValue, [this, replaceValue]);
		}
	    }
	    // 3. Let string be ? ToString(O).
	    let string = ToString(O);
	    // 4. Let searchString be ? ToString(searchValue).
	    let searchString = ToString(searchValue);
	    // 5. Let functionalReplace be IsCallable(replaceValue).
	    let functionalReplace = IsCallable(replaceValue);
	    // 6. If functionalReplace is false, then
	    if (functionalReplace === false) {
		// a. Set replaceValue to ? ToString(replaceValue).
		replaceValue = ToString(replaceValue);
	    }
	    // 7. Search string for the first occurrence of searchString and
	    //    let pos be the index within string of the first code unit of the matched substring and
	    //    let matched be searchString.
	    //    If no occurrences of searchString were found, return string.
	    let pos = native_apply(native.String_indexOf, string, (arg1[0] = searchString, arg1));
	    if (pos < 0) return string;
	    let matched = searchString;
	    // 8. If functionalReplace is true, then
	    let replValue, replStr;
	    if (functionalReplace === true) {
		// a. Let replValue be ? Call(replaceValue, undefined, « matched, pos, string »).
		replValue = Call(replaceValue, undefined, [matched, pos, string]);
		replStr = ToString(replValue);
	    }
	    // 9. Else,
	    else {
		const mock = Object_create(null);
		mock.toString = function() { return searchValue; };
		return native_apply(nat, string, (arg2[0] = mock, arg2[1] = replaceValue, arg2));
	    }
	    // 10. Let tailPos be pos + the number of code units in matched.
	    const tailPos = pos + matched.length;
	    // 11. Let newString be the string-concatenation of the first pos code units of string, replStr, and the trailing substring of string starting at index tailPos. If pos is 0, the first element of the concatenation will be the empty String.
	    
	    let s1;
	    if (pos === 0) s1 = "";
	    else s1 = native_apply(native.String_prototype_substring, string, (arg2[0] = 0, arg2[1] = pos, arg2));
	    const s2 = native_apply(native.String_prototype_substring, string, (arg1[0] = tailPos, arg1));
	    const newString = s1 + replStr + s2;
	    // 12. Return newString
	    return newString;
	};
	return register_function(f, "replace", true, 2);
    })());
    
    function Invoke(V, P, args) {
	// Note: Not necessary here. Call will handle it.
	// if (args === undefined) args = arg0;
	let func = Get(V, P);
	return Call(func, V, args);
    }
    // 15.5.4.12 String.prototype.search(regexp)
    eFn(virtual.String.prototype, "search", true, false, true, (function() {
	var nat = String.prototype.search;
	var f = function(regexp) {
	    // 1. Let O be ? RequireObjectCoercible(this value).
	    CheckObjectCoercible(this, 'String.prototype.search');
	    let O = this;
	    // 2. If regexp is neither undefined nor null, then
	    if (regexp !== null && regexp !== undefined) {
		// a. Let searcher be ? GetMethod(regexp, @@search).
		let searcher = GetMethod(regexp, native.s_symbol_search);
		// b. If searcher is not undefined, then
		if (searcher !== undefined) {
		    // i. Return ? Call(searcher, regexp, « O »).
		    return Call(searcher, regexp, [O]);
		}
	    }
	    // 3. Let string be ? ToString(O).
	    let string = ToString(O);
	    // 4. Let rx be ? RegExpCreate(regexp, undefined).
	    let rx;
	    rx = new virtual_RegExp(regexp, "");
	    rx = CreateRegExp(rx, 'String.prototype.search', ['source', 'global', 'ignoreCase', 'multiline'], true);
	    return Invoke(rx, native.s_symbol_search, [string]);
	    // return native_apply(nat, string, (arg1[0] = rx, arg1));
	};
	return register_function(f, "search", true, 1);
    })());
    // 15.5.4.13 String.prototype.slice(start, end)
    eFn(virtual.String.prototype, "slice", true, false, true, (function() {
	var nat = String.prototype.slice;
	var f = function(start, end) {
	    CheckObjectCoercible(this, 'String.prototype.slice');
	    var S = ToString(this);
	    start = ToInteger(start);
	    if (end !== undefined)
		end = ToInteger(end);
	    return native_apply(nat, S, (arg2[0] = start, arg2[1] = end, arg2));
	};
	return register_function(f, "slice", true, 2);
    })());
    // 15.5.4.14 String.prototype.split(separator, limit)
    eFn(virtual.String.prototype, "split", true, false, true, (function() {
    	var nat = String.prototype.split;
    	var f = function(separator,limit) {
	    CheckObjectCoercible(this, 'String.prototype.split');
	    let O = this;
	    // 2. If separator is neither undefined nor null, then
	    if (separator !== null && separator !== undefined) {
		// a. Let splitter be ? GetMethod(separator, @@split).
		let splitter = GetMethod(separator, native.s_symbol_split);
		// b. If splitter is not undefined, then
		if (splitter !== undefined) {
		    // i. Return ? Call(splitter, separator, « O, limit »).
		    return Call(splitter, separator, [O, limit]);
		}
	    }
	    var R;
    	    var S = ToString(this);
	    if (limit !== undefined)
		limit = ToUint32(limit);
	    if (Class(separator) !== 'RegExp' && separator !== undefined) separator = ToString(separator);
	    if (Class(separator) === 'RegExp')
	    	R = CreateRegExp(separator, 'RegExp.prototype.split', ['source', 'global', 'ignoreCase', 'multiline'], false, false, true);
	    else {
		let t = separator;
		if (t == null) R = t;
		else {
		    R = Object_create(null);
		    R.toString = function() { return t; };
		}
	    }

	    return native_apply(nat, S, (arg2[0] = R, arg2[1] = limit, arg2));
    	};
    	return register_function(f, "split", true, 2);
    })());
    // 15.5.4.15 String.prototype.substring(start, end)
    eFn(virtual.String.prototype, "substring", true, false, true, (function() {
    	var nat = String.prototype.substring;
    	var f = function(start, end) {
	    CheckObjectCoercible(this, 'String.prototype.substring');
    	    var S = ToString(this);
	    start = ToInteger(start);
	    if (end !== undefined) end = ToInteger(end);
	    return native_apply(nat, S, (arg2[0] = start, arg2[1] = end, arg2));
    	};
    	return register_function(f, "substring", true, 2);
    })());
    // 15.5.4.16 String.prototype.toLowerCase() 
    eFn(virtual.String.prototype, "toLowerCase", true, false, true, (function() {
    	var nat = String.prototype.toLowerCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toLowerCase');
    	    var S = ToString(this);
	    return native_apply(nat, S, arg0);
    	};
    	return register_function(f, "toLowerCase", true, 0);
    })());
    // 15.5.4.17 String.prototype.toLocaleLowerCase()
    eFn(virtual.String.prototype, "toLocaleLowerCase", true, false, true, (function() {
    	var nat = String.prototype.toLocaleLowerCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toLocaleLowerCase');
    	    var S = ToString(this);
	    return native_apply(nat, S, arg0);
    	};
    	return register_function(f, "toLocaleLowerCase", true, 0);
    })());
    // 15.5.4.18 String.prototype.toUpperCase()
    eFn(virtual.String.prototype, "toUpperCase", true, false, true, (function() {
    	var nat = String.prototype.toUpperCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toUpperCase');
    	    var S = ToString(this);
	    return native_apply(nat, S, arg0);
    	};
    	return register_function(f, "toUpperCase", true, 0);
    })());
    // 15.5.4.19 String.prototype.toLocaleUpperCase()
    eFn(virtual.String.prototype, "toLocaleUpperCase", true, false, true, (function() {
    	var nat = String.prototype.toLocaleUpperCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toLocaleUpperCase');
    	    var S = ToString(this);
	    return native_apply(nat, S, arg0);
    	};
    	return register_function(f, "toLocaleUpperCase", true, 0);
    })());
    // 15.5.4.20 String.prototype.trim()
    eFn(virtual.String.prototype, "trim", true, false, true, (function() {
    	var nat = String.prototype.trim;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.trim');
    	    var S = ToString(this);
	    return native_apply(nat, S, arg0);
    	};
    	return register_function(f, "trim", true, 0);
    })());
    
    // B.2.3 String.prototype.substr(start, length)
    eFn(virtual.String.prototype, "substr", true, false, true, (function() {
    	var nat = String.prototype.substr;
    	var f = function(start, length) {
    	    var S = ToString(this);
	    start = ToInteger(start);
	    if (length !== undefined) length = ToInteger(length);
	    return native_apply(nat, S, (arg2[0] = start, arg2[1] = length, arg2));
    	};
    	return register_function(f, "substr", true, 2);
    })());
    simpleFn(virtual.String.prototype, String.prototype, "substr", 2, true);
    // copy_desc(virtual.String.prototype, String.prototype, "length");


    // ES6 String.endsWith(search, this_len)
    simpleFn(virtual.String.prototype, String.prototype, "endsWith", 2, true);
    simpleFn(virtual.String.prototype, String.prototype, "includes", 2, true);
    simpleFn(virtual.String.prototype, String.prototype, "startsWith", 2, true);

    // 15.6 Boolean Objects
    eFn(virtual, "Boolean", true, false, true, register_function(function(v) { return native_apply(native.Boolean, this, arguments); }, "Boolean", false, 1));
    (function() {
	// 15.6.3.1 Boolean.prototype
    	var new_proto = new Boolean(false);
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	Object.defineProperty(virtual.Boolean, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.6.4.1 Boolean.prototype.constructor
    	Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Boolean, writable: true, enumerable: false, configurable: true}));
    })();
    // 15.6.4 Properties of the Boolean Prototype Object
    // 15.6.4.2 Boolean.prototype.toString()
    simpleFn(virtual.Boolean.prototype, native.Boolean.prototype, "toString");
    // 15.6.4.3 Boolean.prototype.valueOf()
    simpleFn(virtual.Boolean.prototype, native.Boolean.prototype, "valueOf");

    // 15.7 Number Objects
    eFn(virtual, "Number", true, false, true, register_function(function(v) {
	if (arguments.length > 0) arguments[0] = ToNumber(arguments[0]);
	return native_apply(native.Number, this, arguments);
    }, "Number", false, 1));
    // 15.7.3 Properties of the Number Constructor
    (function() {
	// 15.7.3.1 Number.prototype
    	var new_proto = new native.Number();
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Number, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.7.4.1 Number.prototype.constructor
	native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Number, writable: true, enumerable: false, configurable: true}));
    })();
    // 15.7.3.2 Number.MAX_VALUE
    copy_desc(virtual.Number, native.Number, "MAX_VALUE");
    // 15.7.3.3 Number.MIN_VALUE
    copy_desc(virtual.Number, native.Number, "MIN_VALUE");
    // 15.7.3.4 Number.NaN
    copy_desc(virtual.Number, native.Number, "NaN");
    // 15.7.3.5 Number.NEGATIVE_INFINITY
    copy_desc(virtual.Number, native.Number, "NEGATIVE_INFINITY");
    // 15.7.3.6 Number.POSITIVE_INFINITY
    copy_desc(virtual.Number, native.Number, "POSITIVE_INFINITY");
    // 15.7.4 Properties of the Number Prototype Object
    // 15.7.4.2 Number.prototype.toString([radix])
    eFn(virtual.Number.prototype, "toString", true, false, true, (function() {
    	var nat = native.Number.prototype.toString;
    	var f = function(radix) {
	    if (radix !== undefined) radix = ToInteger(radix);
	    return native_apply(nat, this, (arg1[0] = radix, arg1));
    	};
    	return register_function(f, "toString", true, 1);
    })());
    // 15.7.4.3 Number.prototype.toLocaleString()
    simpleFn(virtual.Number.prototype, native.Number.prototype, "toLocaleString", 0, true);
    // 15.7.4.4 Number.prototype.valueOf()
    simpleFn(virtual.Number.prototype, native.Number.prototype, "valueOf", 0, true);
    // 15.7.4.5 Number.prototype.toFixed(fractionDigits)
    eFn(virtual.Number.prototype, "toFixed", true, false, true, (function() {
    	var nat = native.Number.prototype.toFixed;
    	var f = function(fractionDigits) {
	    if (fractionDigits !== undefined)
		fractionDigits = ToInteger(fractionDigits);
	    return native_apply(nat, this, (arg1[0] = fractionDigits, arg1));
    	};
    	return register_function(f, "toFixed", true, 1);
    })());
    // 15.7.4.6 Number.prototype.toExponential(fractionDigits)
    eFn(virtual.Number.prototype, "toExponential", true, false, true, (function() {
    	var nat = native.Number.prototype.toExponential;
    	var f = function(fractionDigits) {
	    if (fractionDigits !== undefined)
		fractionDigits = ToInteger(fractionDigits);
	    return native_apply(nat, this, (arg1[0] = fractionDigits, arg1));
    	};
    	return register_function(f, "toExponential", true, 1);
    })());
    // 15.7.4.7 Number.prototype.toPrecision(precision)
    eFn(virtual.Number.prototype, "toPrecision", true, false, true, (function() {
    	var nat = native.Number.prototype.toPrecision;
    	var f = function(precision) {
	    if (precision !== undefined)
		precision = ToInteger(precision);
	    return native_apply(nat, this, (arg1[0] = precision, arg1));
    	};
    	return register_function(f, "toPrecision", true, 1);
    })());
    // 15.8 The Math Object
    eFn(virtual, "Math", true, false, true, {});
    var virtual_Math = virtual.Math;
    native.Object_defineProperty(virtual.Math, native.s_symbol_toStringTag, sDesc({value: "Math", configurable: true, writable: false, enumerable: false}));

    // 15.8.1 Value Properties of the Math Object
    // 15.8.1.1 E
    copy_desc(virtual.Math, native.Math, "E");
    // 15.8.1.2 LN10
    copy_desc(virtual.Math, native.Math, "LN10");
    // 15.8.1.3 LN2
    copy_desc(virtual.Math, native.Math, "LN2");
    // 15.8.1.4 LOG2E
    copy_desc(virtual.Math, native.Math, "LOG2E");
    // 15.8.1.5 LOG10E
    copy_desc(virtual.Math, native.Math, "LOG10E");
    // 15.8.1.6 PI
    copy_desc(virtual.Math, native.Math, "PI");
    // 15.8.1.7 SQRT1_2
    copy_desc(virtual.Math, native.Math, "SQRT1_2");
    // 15.8.1.8 SQRT2
    copy_desc(virtual.Math, native.Math, "SQRT2");
    // 15.8.2 Function Properties of the Math Object
    // 15.8.2.1 abs(x)
    MathWrapper(virtual.Math, 'abs', native.Math.abs);
    // 15.8.2.2 acos(x)
    MathWrapper(virtual.Math, 'acos', native.Math.acos);
    // 15.8.2.3 asin(x)
    MathWrapper(virtual.Math, 'asin', native.Math.asin);
    // 15.8.2.4 atan(x)
    MathWrapper(virtual.Math, 'atan', native.Math.atan);
    // 15.8.2.5 atan2(y,x)
    MathWrapper(virtual.Math, 'atan2', native.Math.atan2);
    // 15.8.2.6 ceil(x)
    MathWrapper(virtual.Math, 'ceil', native.Math.ceil);
    // 15.8.2.7 cos(x)
    MathWrapper(virtual.Math, 'cos', native.Math.cos);
    // 15.8.2.8 exp(x)
    MathWrapper(virtual.Math, 'exp', native.Math.exp);
    // 15.8.2.9 floor(x)
    MathWrapper(virtual.Math, 'floor', native.Math.floor);
    // 15.8.2.10 log(x)
    MathWrapper(virtual.Math, 'log', native.Math.log);
    // 15.8.2.11 max([value1 [, value2 [, ...]]])
    MathWrapper2(virtual.Math, 'max', native.Math.max);
    // 15.8.2.12 min([value1 [, value2 [, ...]]])
    MathWrapper2(virtual.Math, 'min', native.Math.min);
    // 15.8.2.13 pow(x,y)
    MathWrapper(virtual.Math, 'pow', native.Math.pow);
    // 15.8.2.14 random()
    simpleFn(virtual.Math, native.Math, "random");
    // 15.8.2.15 round(x)
    MathWrapper(virtual.Math, 'round', native.Math.round);
    // 15.8.2.16 sin(x)
    MathWrapper(virtual.Math, 'sin', native.Math.sin);
    // 15.8.2.17 sqrt(x)
    MathWrapper(virtual.Math, 'sqrt', native.Math.sqrt);
    // 15.8.2.18 tan(x)
    MathWrapper(virtual.Math, 'tan', native.Math.tan);
    // 15.9 Date Objects
    eFn(virtual, "Date", true, false, true, (function () {
    	var o = Date;
    	var f = function Date(a, b, c, d, e, f, g) {
    	    // In this version, Date.bind() will return a function that does not provide standard semantics of JavaScript.
	    if (new.target === undefined) {
    		return native_apply(o, this, arguments);
    	    } else {
    	    	if (arguments.length === 0) return new o();
    	    	else if (arguments.length === 1) return new o(ToPrimitive_N(a));
    	    	else if (arguments.length === 2) return new o(ToNumber(a), ToNumber(b));
    	    	else if (arguments.length === 3) return new o(ToNumber(a), ToNumber(b), ToNumber(c));
    	    	else if (arguments.length === 4) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d));
    	    	else if (arguments.length === 5) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d), ToNumber(e));
    	    	else if (arguments.length === 6) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d), ToNumber(e), ToNumber(f));
    	    	else if (arguments.length === 7) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d), ToNumber(e), ToNumber(f), ToNumber(g));
    	    }
    	};
    	return register_function(f, "Date", false, 7);
    })());

    // 15.9.4 Properties of the Date Constructor
    (function() {
	// 15.9.4.1 Date.prototype
    	var new_proto = new native.Date(NaN);
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Date, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.9.5.1 Date.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.Date, writable: true, enumerable: false, configurable: true});
    })();
    // 15.9.4.2 Date.parse(string)
    ToStringWrapper1(virtual.Date, "parse", native.Date.parse);
    // 15.9.4.3 Date.UTC(year, month [, hours [, minutes [, seconds [, ms]]]])
    eFn(virtual.Date, "UTC", true, false, true, (function () {
    	var o = native.Date.UTC;
    	var f = function UTC(a, b, c, d, e, f, g) {
	    var arg = [];
	    descTTT.value = ToNumber(a);
    	    native.Object_defineProperty(arg, 0, descTTT);
	    descTTT.value = ToNumber(b);
    	    native.Object_defineProperty(arg, 1, descTTT);
    	    if (arguments.length >= 3)
		native.Object_defineProperty(arg, 2, sDesc({value: ToNumber(c), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 4)
		native.Object_defineProperty(arg, 3, sDesc({value: ToNumber(d), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 5)
		native.Object_defineProperty(arg, 4, sDesc({value: ToNumber(e), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 6)
		native.Object_defineProperty(arg, 5, sDesc({value: ToNumber(f), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 7)
		native.Object_defineProperty(arg, 6, sDesc({value: ToNumber(g), writable: true, enumerable: true, configurable: true}));

    	    return native_apply(o, this, arg);
    	};
    	return register_function(f, "UTC", false, 7);
    })());
    // 15.9.4.4 Date.now()
    simpleFn(virtual.Date, native.Date, "now", 0, true);
    // 15.9.5 Properties of the Date Prototype Object
    // 15.9.5.2 Date.prototype.toString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toString", 0, true);
    // 15.9.5.3 Date.prototype.toDateString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toDateString", 0, true);
    // 15.9.5.4 Date.prototype.toTimeString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toTimeString", 0, true);
    // 15.9.5.5 Date.prototype.toLocaleString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toLocaleString", 0, true);
    // 15.9.5.6 Date.prototype.toLocaleDateString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toLocaleDateString", 0, true);
    // 15.9.5.7 Date.prototype.toLocaleTimeString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toLocaleTimeString", 0, true);
    // 15.9.5.8 Date.prototype.valueOf()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "valueOf", 0, true);
    // 15.9.5.9 Date.prototype.getTime()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getTime", 0, true);
    // 15.9.5.10 Date.prototype.getFullYear()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getFullYear", 0, true);
    // 15.9.5.11 Date.prototype.getUTCFullYear()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCFullYear", 0, true);
    // 15.9.5.12 Date.prototype.getMonth()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getMonth", 0, true);
    // 15.9.5.13 Date.prototype.getUTCMonth()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCMonth", 0, true);
    // 15.9.5.14 Date.prototype.getDate()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getDate", 0, true);
    // 15.9.5.15 Date.prototype.getUTCDate()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCDate", 0, true);
    // 15.9.5.16 Date.prototype.getDay()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getDay", 0, true);
    // 15.9.5.17 Date.prototype.getUTCDay()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCDay", 0, true);
    // 15.9.5.18 Date.prototype.getHours()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getHours", 0, true);
    // 15.9.5.19 Date.prototype.getUTCHours()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCHours", 0, true);
    // 15.9.5.20 Date.prototype.getMinutes()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getMinutes", 0, true);
    // 15.9.5.21 Date.prototype.getUTCMinutes()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCMinutes", 0, true);
    // 15.9.5.22 Date.prototype.getSeconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getSeconds", 0, true);
    // 15.9.5.23 Date.prototype.getUTCSeconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCSeconds", 0, true);
    // 15.9.5.24 Date.prototype.getMilliseconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getMilliseconds", 0, true);
    // 15.9.5.25 Date.prototype.getUTCMilliseconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCMilliseconds", 0, true);
    // 15.9.5.26 Date.prototype.getTimezoneOffset()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getTimezoneOffset", 0, true);
    // 15.9.5.27 Date.prototype.setTime(time)
    ToNumberWrapper1(virtual.Date.prototype, "setTime", native.Date.prototype.setTime);
    // 15.9.5.28 Date.prototype.setMilliseconds(ms)
    simpleFn(virtual.Date.prototype, native.Date.prototype, "setMilliseconds", 1, true);
    // 15.9.5.29 Date.prototype.setUTCMilliseconds(ms)
    simpleFn(virtual.Date.prototype, native.Date.prototype, "setUTCMilliseconds", 1, true);
    // 15.9.5.30 Date.prototype.setSeconds(sec [, ms])
    ToNumberWrapperN(virtual.Date.prototype, "setSeconds", native.Date.prototype.setSeconds, 1, 2, 2);
    // 15.9.5.31 Date.prototype.setUTCSeconds(sec [, ms])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCSeconds", native.Date.prototype.setUTCSeconds, 1, 2);
    // 15.9.5.32 Date.prototype.setMinutes(min [, sec [, ms]])
    ToNumberWrapperN(virtual.Date.prototype, "setMinutes", native.Date.prototype.setMinutes, 1, 3);
    // 15.9.5.33 Date.prototype.setUTCMinutes(min [, sec [, ms]])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCMinutes", native.Date.prototype.setUTCMinutes, 1, 3);
    // 15.9.5.34 Date.prototype.setHours(hour [, min [, sec [, ms]]])
    ToNumberWrapperN(virtual.Date.prototype, "setHours", native.Date.prototype.setHours, 1, 4);
    // 15.9.5.35 Date.prototype.setUTCHours(hour [, min [, sec [, ms]]])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCHours", native.Date.prototype.setUTCHours, 1, 4);
    // 15.9.5.36 Date.prototype.setDate(date)
    ToNumberWrapper1(virtual.Date.prototype, "setDate", native.Date.prototype.setDate);
    // 15.9.5.37 Date.prototype.setUTCDate(date)
    ToNumberWrapper1(virtual.Date.prototype, "setUTCDate", native.Date.prototype.setUTCDate);
    // 15.9.5.38 Date.prototype.setMonth(month [, date])
    ToNumberWrapperN(virtual.Date.prototype, "setMonth", native.Date.prototype.setMonth, 1, 2);
    // 15.9.5.39 Date.prototype.setUTCMonth(month [, date])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCMonth", native.Date.prototype.setUTCMonth, 1, 2);
    // 15.9.5.40 Date.prototype.setFullYear(year [, month [, date]])
    ToNumberWrapperN(virtual.Date.prototype, "setFullYear", native.Date.prototype.setFullYear, 1, 3);
    // 15.9.5.41 Date.prototype.setUTCFullYear(year [, month [, date]])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCFullYear", native.Date.prototype.setUTCFullYear, 1, 3);
    // 15.9.5.42 Date.prototype.toUTCString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toUTCString", 0, true);
    // 15.9.5.43 Date.prototype.toISOString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toISOString", 0, true);
    // 15.9.5.44 Date.prototype.toJSON(key)
    eFn(virtual.Date.prototype, "toJSON", true, false, true, (function() {
    	var f = function(key) {
	    // 1. Let O be the result of calling ToObject, giving it the this value as its argument.
	    var O = ToObject(this);
	    // 2. Let tv be ToPrimitive(O, hint Number).
	    var tv = ToPrimitive_N(O);
	    // 3. If tv is a Number and is not finite, return null.
	    if (typeof tv === 'number' && !native.isFinite(tv))	return null;
	    // 4. Let toISO be the result of calling the [[Get]] internal method of O with argument "toISOString".
	    var toISO = Geto(O, 'toISOString');
	    // 5. If IsCallable(toISO) is false, throw a TypeError exception.
	    if (IsCallable(toISO) === false) throw new native.TypeError("toISOString is not a function");
	    // 6. Return the result of calling the [[Call]] internal method of toISO with O as the this value and an empty argument list.
	    return Call(toISO, O);
    	};
    	return register_function(f, "toJSON", true, 1);
    })());
    // B.2.4 Date.prototype.getYear()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getYear", 0, true);
    // B.2.5 Date.prototype.setYear(year)
    ToNumberWrapper1(virtual.Date.prototype, "setYear", native.Date.prototype.setYear);
    // B.2.6 Date.prototype.toGMTString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toGMTString", 0, true);

    // 7.2.9 IsRegExp ( argument )
    // https://tc39.es/ecma262/#sec-isregexp
    function IsRegExp(argument) {
	// 1. If Type(argument) is not Object, return false.
	if (isNotObject(argument)) return false;
	// 2. Let matcher be ? Get(argument, @@match).
	let matcher = Get(argument, native.s_symbol_match);
	// 3. If matcher is not undefined, return ! ToBoolean(matcher).
	if (matcher !== undefined) {
	    return ToBoolean(matcher);
	}
	// 4. If argument has a [[RegExpMatcher]] internal slot, return true.
	if (Class(argument) === 'RegExp') return true;
	// 5. Return false.
	return false;
    }

    // 15.10 RegExp (Regular Expression) Objects
    eFn(virtual, "RegExp", true, false, true, register_function(function active(pattern, flags) {
	// 1. Let patternIsRegExp be ? IsRegExp(pattern).
	let patternIsRegExp = IsRegExp(pattern);
	// 2. If NewTarget is undefined, then
	let newTarget;
	if (new.target === undefined) {
	    // a. Let newTarget be the active function object.
	    newTarget = active;
	    // b. If patternIsRegExp is true and flags is undefined, then
	    if (patternIsRegExp === true && flags === undefined) {
		// i. Let patternConstructor be ? Get(pattern, "constructor").
		let patternConstructor = Get(pattern, "constructor");
		// ii. If SameValue(newTarget, patternConstructor) is true, return pattern.
		if (newTarget === patternConstructor)
		    return pattern;
	    }
	}
	// 3. Else, let newTarget be NewTarget.
	else {
	    newTarget = new.target;
	}
	let P, F;
	// 4. If Type(pattern) is Object and pattern has a [[RegExpMatcher]] internal slot, then
	if (isObject(pattern) && Class(pattern) === 'RegExp') {
	    if (flags !== undefined) flags = ToString(flags);
	    pattern = CreateRegExp(pattern, 'RegExp.prototype.exec', []);
	    let desc = Reflect_getOwnPropertyDescriptor(pattern, native.s_symbol_match);
	    if (desc === undefined) desc = (descTTT.value = undefined, descTTT);
	    Reflect_defineProperty(pattern, native.s_symbol_match, desc);
	    return new native.RegExp(pattern, flags);
	}	
	// 5. Else if patternIsRegExp is true, then
	if (patternIsRegExp === true) {
	    // a. Let P be ? Get(pattern, "source").
	    P = Get(pattern, "source");
	    // b. If flags is undefined, then
	    if (flags === undefined) {
		// i. Let F be ? Get(pattern, "flags").
		F = Get(pattern, "flags");
	    }
	    // c. Else, let F be flags.
	    else {
		F = flags;
	    }
	}
	// 6. Else,
	else {
	    // a. Let P be pattern.
	    P = pattern;
	    // b. Let F be flags.
	    F = flags;
	}
	if (P === undefined) P = "";
	P = ToString(P);
	if (F !== undefined) F = ToString(F);
	return new native.RegExp(P, F);
    }, "RegExp", false, 2));
    // 15.10.5 Properties of the RegExp Constructor
    (function() {
	// 15.10.5.1 RegExp.prototype
    	var new_proto = Object_create(virtual.Object.prototype); // new native.RegExp();
	Reflect_defineProperty(new_proto, native.s_symbol_match, {value: native.symbol_match, writable: true, enumerable: false, configurable: true});
	if (native.s_symbol_match_all !== undefined) 
	    Reflect_defineProperty(new_proto, native.s_symbol_match_all, {value: native.symbol_match_all, writable: true, enumerable: false, configurable: true});
	Reflect_defineProperty(new_proto, native.s_symbol_replace, {value: native.symbol_replace, writable: true, enumerable: false, configurable: true});
	Reflect_defineProperty(new_proto, native.s_symbol_search, {value: native.symbol_search, writable: true, enumerable: false, configurable: true});
	Reflect_defineProperty(new_proto, native.s_symbol_split, {value: native.symbol_split, writable: true, enumerable: false, configurable: true});
	Reflect_defineProperty(new_proto, 'flags', Reflect_getOwnPropertyDescriptor(native.RegExp.prototype, 'flags'));
	Reflect_defineProperty(new_proto, 'ignoreCase', Reflect_getOwnPropertyDescriptor(native.RegExp.prototype, 'ignoreCase'));
	Reflect_defineProperty(new_proto, 'global', Reflect_getOwnPropertyDescriptor(native.RegExp.prototype, 'global'));
	
	if ('dotAll' in native.RegExp.prototype)
	Reflect_defineProperty(new_proto, 'dotAll', Reflect_getOwnPropertyDescriptor(native.RegExp.prototype, 'dotAll'));
	if ('unicode' in native.RegExp.prototype)
	    Reflect_defineProperty(new_proto, 'unicode', Reflect_getOwnPropertyDescriptor(native.RegExp.prototype, 'unicode'));
	if ('sticky' in native.RegExp.prototype)
	    Reflect_defineProperty(new_proto, 'sticky', Reflect_getOwnPropertyDescriptor(native.RegExp.prototype, 'sticky'));

	native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.RegExp, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.10.6 Properties of the RegExp Prototype Object
	// 15.10.6.1 RegExp.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.RegExp, writable: true, enumerable: false, configurable: true});
    })();
    // 15.10.6.2 RegExp.prototype.exec(string)
    eFn(virtual.RegExp.prototype, "exec", true, false, true, (function() {
    	var nat = native.RegExp.prototype.exec;
    	var f = function(string) {
	    var R = CreateRegExp(this, 'RegExp.prototype.exec', ['global']);
	    var S = ToString(string);
	    var result = native_apply(nat, R, (arg1[0] = S, arg1));
	    Put(this, "lastIndex", R.lastIndex);
	    return result;
    	};
    	return register_function(f, "exec", true, 1);
    })());
    // 15.10.6.3 RegExp.prototype.test(string)
    eFn(virtual.RegExp.prototype, "test", true, false, true, (function() {
    	var f = function(string) {
	    var R = CreateRegExp(this, 'RegExp.prototype.test', ['global']);
	    var S = ToString(string);
	    // 1. Let match be the result of evaluating the RegExp.prototype.exec (15.10.6.2) algorithm upon this RegExp object using string as the argument.
	    var match = native_apply(virtual_RegExp_prototype_exec, R, (arg1[0] = S, arg1));
	    Put(this, "lastIndex", R.lastIndex);
	    // If match is not null, then return true; else return false.
	    if (match !== null) return true;
	    else return false;
    	};
    	return register_function(f, "test", true, 1);
    })());
    // 15.10.6.4 RegExp.prototype.toString()
    eFn(virtual.RegExp.prototype, "toString", true, false, true, (function() {
	var nat = native.RegExp.prototype.toString;
    	var f = function() {
	    var R = CreateRegExp(this, 'RegExp.prototype.toString', ['source', 'global', 'ignoreCase', 'multiline']);
	    try {
		msg('secure start');
		return native_apply(nat, R, arg0);
	    } finally {
		msg('secure end');
	    }
    	};
    	return register_function(f, "toString", true, 0);
    })());

    // B RegExp.prototype.compile Non-standard semantics. 
    simpleFn(virtual.RegExp.prototype, native.RegExp.prototype, "compile", 2, true);

    // 15.10.7 Properties of RegExp Instances
    // 15.10.7.1 source
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "source");
    // 15.10.7.2 global
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "global");
    // 15.10.7.3 ignoreCase
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "ignoreCase");
    // 15.10.7.4 multiline
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "multiline");
    // 15.10.7.5 lastIndex: automatically created.

    // 15.11 Error Objects
    eFn(virtual, "Error", true, false, true, register_function(function(e) { return native_apply(native.Error, this, arguments); }, "Error", false, 1));
    // 15.11.3 Properties of the Error Constructor
    (function() {
	// 15.11.3.1 Error.prototype
    	var new_proto = new native.Error();
	native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Error, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.11.4 Properties of the Error Prototype Object
	// 15.11.4.1 Error.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.Error, writable: true, enumerable: false, configurable: true});
    })();

    // 15.11.4 Properties of the Error Prototype Object
    // 15.11.4.2 Error.prototype.name
    copy_desc(virtual.Error.prototype, native.Error.prototype, "name");
    // 15.11.4.3 Error.prototype.message
    copy_desc(virtual.Error.prototype, native.Error.prototype, "message");
    // 15.11.4.4 Error.prototype.toString()
    simpleFn(virtual.Error.prototype, native.Error.prototype, "toString");
    eFn(virtual.Error.prototype, "toString", true, false, true, (function() {
    	var f = function() {
	    // 1. Let O be the this value.
	    var O = this;
	    // 2. If Type(O) is not Object, throw a TypeError exception.
	    if (isNotObject(O))
		throw new native.TypeError("Method Error.prototype.toString called on incompatible receiver "+ToStringForError(O));
	    // 3. Let name be the result of calling the [[Get]] internal method of O with argument "name".
	    var name = Geto(O, 'name');
	    // 4. If name is undefined, then let name be "Error"; else let name be ToString(name).
	    if (name === undefined) name = "Error";
	    else name = ToString(name);
	    // 5. Let msg be the result of calling the [[Get]] internal method of O with argument "message".
	    var msg = Geto(O, 'message');
	    // 6. If msg is undefined, then let msg be the empty String; else let msg be ToString(msg).
	    if (msg === undefined) msg = "";
	    else msg = ToString(msg);
	    // 7. [mistake] If msg is undefined, then let msg be the empty String; else let msg be ToString(msg).
	    // 8. If name is the empty String, return msg.
	    if (name === "") return msg;
	    // 9. If msg is the empty String, return name.
	    if (msg === "") return name;
	    // 10. Return the result of concatenating name, ":", a single space character, and msg.
	    return name + ": " + msg;
    	};
    	return register_function(f, "toString", true, 1);
    })());
    
    // 15.11.6.1 EvalError
    extend("EvalError", "Error", ["name", "message"]);
    // 15.11.6.2 RangeError
    extend("RangeError", "Error", ["name", "message"]);
    // 15.11.6.3 ReferenceError
    extend("ReferenceError", "Error", ["name", "message"]);
    // 15.11.6.4 SyntaxError
    extend("SyntaxError", "Error", ["name", "message"]);
    // 15.11.6.5 TypeError
    extend("TypeError", "Error", ["name", "message"]);
    // 15.11.6.6 URIError
    extend("URIError", "Error", ["name", "message"]);

    // 15.12 The JSON Object
    eFn(virtual, "JSON", true, false, true, {});
    // The value of [[Class]] internal property of the JSON object is "JSON".
    registerOPT(m_tos, virtual.JSON, "JSON");
    // 15.12.2 parse(text [, reviver])
    eFn(virtual.JSON, "parse", true, false, true, (function() {
	var nat = native.JSON.parse;
    	var f = function(text, reviver) {
	    if (arguments.length > 0)
		arguments[0] = ToString(text);
	    return native_apply(nat, this, arguments);
    	};
    	return register_function(f, "parse", true, 2);
    })());
    // 15.12.3 stringify(value [, replacer [, space]])
    eFn(virtual.JSON, "stringify", true, false, true, (function() {
	var nat = native.JSON.stringify;
	var Quote = nat;
	var JO = function JO(value, env) {
	    // 1. If stack contains value then throw a TypeError exception because the structure is cyclical.
	    if (elementOf(env.stack, value))
		throw new native.TypeError("Converting circular structure to JSON");
	    // 2. Append value to stack.
	    Append(env.stack, value);
	    // 3. Let stepback be indent.
	    var stepback = env.indent;
	    // 4. Let indent be the concatenation of indent and gap.
	    env.indent += env.gap;
	    // 5. If PropertyList is not undefined, then
	    var K;
	    if (env.PropertyList !== undefined) {
		// a. Let K be PropertyList.
		K = env.PropertyList;
	    }
	    // 6. Else
	    else {
		// a. Let K be an internal List of Strings consisting of the names of all the own properties of value whose [[Enumerable]] attribute is true. The ordering of the Strings should be the same as that used by the Object.keys standard built-in function.
		K = native.Object_keys(value);
	    }
	    // 7. Let partial be an empty List.
	    var partial = [];
	    var member;
	    // 8. For each element P of K.
	    for (var i=0; i<K.length; i++) {
		var P = K[i];
		// a. Let strP be the result of calling the abstract operation Str with arguments P and value.
		var strP = Str(P, value, env);
		// b. If strP is not undefined
		if (strP !== undefined) {
		    // i. Let member be the result of calling the abstract operation Quote with argument P.
		    member = Quote(P);
		    // ii. Let member be the concatenation of member and the colon character.
		    member += ":";
		    // iii. If gap is not the empty String
		    if (env.gap !== "") {
			// 1. Let member be the concatenation of member and the space character.
			member += " ";
		    }
		    // iv. Let member be the concatenation of member and strP.
		    member += strP;
		    // v. Append member to partial.
		    Append(partial, member);
		}
	    }
	    // 9. If partial is empty, then
	    var finale, separator, properties;
	    if (partial.length === 0) {
		// a. Let final be "{}".
		finale = "{}";
	    }
	    // 10. Else
	    else {
		// a. If gap is the empty String
		if (env.gap === "") {
		    // i. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with the comma character. A comma is not inserted either before the first String or after the last String.
		    properties = native_apply(native.Array_prototype_join, partial, (arg1[0] = ",", arg1));
		    // ii. Let final be the result of concatenating "{", properties, and "}".
		    finale = "{" + properties + "}";
		}
		// b. Else gap is not the empty String
		else if (env.gap !== "") {
		    // i. Let separator be the result of concatenating the comma character, the line feed character, and indent.
		    separator = ",\n" + env.indent;
		    // ii. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with separator. The separator String is not inserted either before the first String or after the last String.
		    properties = native_apply(native.Array_prototype_join, partial, (arg1[0] = separator, arg1));
		    // iii. Let final be the result of concatenating "{", the line feed character, indent, properties, the line feed character, stepback, and "}".
		    finale = "{\n" + env.indent + properties + "\n" + stepback + "}";
		}
	    }
	    // 11. Remove the last element of stack.
	    env.stack.length = env.stack.length - 1;
	    // 12. Let indent be stepback.
	    env.indent = stepback;
	    // 13. Return final.
	    return finale;
	};
	var JA = function(value, env) {
	    // 1. If stack contains value then throw a TypeError exception because the structure is cyclical.
	    if (elementOf(env.stack, value))
		throw new native.TypeError("Converting circular structure to JSON");
	    // 2. Append value to stack.
	    Append(env.stack, value);
	    // 3. Let stepback be indent.
	    var stepback = env.indent;
	    // 4. Let indent be the concatenation of indent and gap.
	    env.indent += env.gap;
	    // 5. Let partial be an empty List.
	    var partial = [];
	    // 6. Let len be the result of calling the [[Get]] internal method of value with argument "length".
	    // value.[[Class]] === "Array"
	    var len = value.length;
	    // 7. Let index be 0.
	    var index = 0;
	    // 8. Repeat while index < len
	    while (index < len) {
		// a. Let strP be the result of calling the abstract operation Str with arguments ToString(index) and value.
		var strP = Str(ToString(index), value, env);
		// b. If strP is undefined
		if (strP === undefined) {
		    // i. Append "null" to partial.
		    Append(partial, "null");
		}
		// c. Else
		else {
		    // i. Append strP to partial.
		    Append(partial, strP);
		}
		// d. Increment index by 1.
		index += 1;
	    }
	    // 9. If partial is empty ,then
	    var finale, properties;
	    if (partial.length === 0) {
		// a. Let final be "[]".
		finale = "[]";
	    }
	    // 10. Else
	    else {
		// a. If gap is the empty String
		if (env.gap === "") {
		    // i. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with the comma character. A comma is not inserted either before the first String or after the last String.
		    properties = native_apply(native.Array_prototype_join, partial, (arg1[0] = ",", arg1));
		    // ii. Let final be the result of concatenating "[", properties, and "]".
		    finale = "[" + properties + "]";
		}
		// b. Else
		else if (env.gap !== "") {
		    // i. Let separator be the result of concatenating the comma character, the line feed character, and indent.
		    var separator = ",\n" + env.indent;
		    // ii. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with separator. The separator String is not inserted either before the first String or after the last String.
		    properties = native_apply(native.Array_prototype_join, partial, (arg1[0] = separator, arg1));
		    // iii. Let final be the result of concatenating "[", the line feed character, indent, properties, the line feed character, stepback, and "]".		    
		    finale = "[\n" + env.indent + properties + "\n" + stepback + "]";
		}
	    }
	    // 11. Remove the last element of stack.
	    env.stack.length = env.stack.length - 1;
	    // 12. Let indent be stepback.
	    env.indent = stepback;
	    // 13. Return final.
	    return finale;
	};
	var Str = function Str(key, holder, env) {
	    // 1. Let value be the result of calling the [[Get]] internal method of holder with argument key.
	    var value = Get(holder, key);
	    // 2. If Type(value) is Object, then
	    if (isObject(value)) {
		// a. Let toJSON be the result of calling the [[Get]] internal method of value with argument "toJSON".
		var toJSON = Geto(value, "toJSON");
		// b. If IsCallable(toJSON) is true
		if (IsCallable(toJSON)) {
		    // i. Let value be the result of calling the [[Call]] internal method of toJSON passing value as the this value and with an argument list consisting of key.
		    value = Call(toJSON, value, [key]);
		}
	    }
	    // 3. If ReplacerFunction is not undefined, then
	    if (env.ReplacerFunction !== undefined) {
		// a. Let value be the result of calling the [[Call]] internal method of ReplacerFunction passing holder as the this value and with an argument list consisting of key and value.
		value = Call(env.ReplacerFunction, holder, [key, value]);
	    }
	    // 4. If Type(value) is Object then,
	    if (isObject(value)) {
		var cls = Class(value);
		// a. If the [[Class]] internal property of value is "Number" then,
		if (cls === 'Number') {
		    // i. Let value be ToNumber(value).
		    value = ToNumber(value);
		}
		// b. Else if the [[Class]] internal property of value is "String" then,
		else if (cls === 'String') {
		    // i. Let value be ToString(value).
		    value = ToString(value);
		}
		// c. Else if the [[Class]] internal property of value is "Boolean" then,
		else if (cls === 'Boolean') {
		    // i. Let value be the value of the [[PrimitiveValue]] internal property of value.
		    value = native_apply(native.Boolean_prototype_valueOf, value, arg0);
		}
	    }
	    // 5. If value is null then return "null".
	    if (value === null) return "null";
	    // 6. If value is true then return "true".
	    if (value === true) return "true";
	    // 7. If value is false then return "false".
	    if (value === false) return "false";
	    // 8. If Type(value) is String, then return the result of calling the abstract operation Quote with argument value.
	    if (typeof value === 'string') {
		return Quote(value);
	    }
	    // 9. If Type(value) is Number
	    if (typeof value === 'number') {
		// a. If value is finite then return ToString(value).
		if (native.isFinite(value)) return ToString(value);
		// b. Else, return "null".
		else return "null";
	    }
	    // 10. If Type(value) is Object, and IsCallable(value) is false
	    if (isObject(value) && !IsCallable(value)) {
		// a. If the [[Class]] internal property of value is "Array" then
		if (Class(value) === 'Array') {
		    // i. Return the result of calling the abstract operation JA with argument value.
		    return JA(value, env);
		}
		// b. Else, return the result of calling the abstract operation JO with argument value.
		else {
		    return JO(value, env);
		}
	    }
	    // 11. Return undefined.
	    return undefined;
	};
	var elementOf = function(list, item) {
	    return native_apply(native.indexOf, list, (arg1[0] = item, arg1)) >= 0;
	};
	var Append = function(list, item) {
	    descTTT.value = item;
    	    native.Object_defineProperty(list, list.length, descTTT);
	};
    	var f = function(value, replacer, space) {
	    var i;
	    // 1. Let stack be an empty List.
	    // 2. Let indent be the empty String.
	    // 3. Let PropertyList and ReplacerFunction be undefined.
	    var env = {stack: [], indent: "", gap: undefined, PropertyList: undefined, ReplacerFunction: undefined};
	    // 4. If Type(replacer) is Object, then
	    if (isObject(replacer)) {
		// a. If IsCallable(replacer) is true, then
		if (IsCallable(replacer)) {
		    // i. Let ReplacerFunction be replacer.
		    env.ReplacerFunction = replacer;
		}
		// b. Else if the [[Class]] internal property of replacer is "Array", then
		else if (Class(replacer) === 'Array') {
		    // i. Let PropertyList be an empty internal List
		    env.PropertyList = [];
		    // ii. For each value v of a property of replacer that has an array index property name. The properties are enumerated in the ascending array index order of their names.
		    var v;
		    for (i=0; i<replacer.length; i++) {
			v = Geto(replacer, "" + i);
			// 1. Let item be undefined.
			var item = undefined;
			// 2. If Type(v) is String then let item be v.
			if (typeof v === 'string') item = v;
			// 3. Else if Type(v) is Number then let item be ToString(v).
			else if (typeof v === 'number') item = ToString(v);
			// 4. Else if Type(v) is Object then,
			else if (isObject(v)) {
			    // a. If the [[Class]] internal property of v is "String" or "Number" then let item be ToString(v).
			    var cls = Class(v);
			    if (cls === 'String' || cls === 'Number') item = ToString(v);
			}
			// 5. If item is not undefined and item is not currently an element of PropertyList then,
			if (item !== undefined && !elementOf(env.PropertyList, item)) {
			    // a. Append item to the end of PropertyList.
			    Append(env.PropertyList, item);
			}
		    }
		}
	    }
	    // 5. If Type(space) is Object then,
	    if (isObject(space)) {
		var cls = Class(space);
		// a. If the [[Class]] internal property of space is "Number" then,
		//   i. Let space be ToNumber(space).
		if (cls === 'Number') space = ToNumber(space);
		// b. Else if the [[Class]] internal property of space is "String" then,
		//   i. Let space be ToString(space).
		else if (cls === 'String') space = ToString(space);
	    }
	    // 6. If Type(space) is Number
	    var gap = "";
	    if (typeof space === 'number') {
		// a. Let space be min(10, ToInteger(space)).
		space = native.Math_min(10, ToInteger(space));
		// b. Set gap to a String containing space space characters. This will be the empty String if space is less than 1.
		if (space >= 1)
		    for (i=0; i<space; i++) gap += " ";
	    }
	    // 7. Else if Type(space) is String
	    else if (typeof space === 'string') {
		// a. If the number of characters in space is 10 or less, set gap to space otherwise set gap to a String consisting of the first 10 characters of space.
		if (space.length <= 10) gap = space;
		else gap = native_apply(native.String_prototype_substring, space, (arg2[0] = 0, arg2[1] = 10, arg2));
	    }
	    // 8. Else
	    else {
		// a. Set gap to the empty String.
		gap = "";
	    }
	    env.gap = gap;
	    // 9. Let wrapper be a new object created as if by the expression new Object(), where Object is the standard built-in constructor with that name.
	    var wrapper = {};
	    // 10. Call the [[DefineOwnProperty]] internal method of wrapper with arguments the empty String, the Property Descriptor {[[Value]]: value, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
	    DefineOwnProperty(wrapper, "", sDesc({value: value, writable: true, enumerable: true, configurable: true}), false);
	    return Str("", wrapper, env);
    	};
    	return register_function(f, "stringify", true, 3);
    })());


    // [[Construct]] !== [[Function]]
    {
    	const list= ["Boolean", "Number"]; // , "Date"];
    	for (let v in list) {
    	    v = list[v];
	    native_apply(native_mapset, m_virt_org, (arg2[0] = virtual[v], arg2[1] = native.global[v], arg2));
    	}
    }
    {
    	const list= ["Object", "Array", "String", "Boolean", "RegExp", "Number", "Math", "Function", "TypeError", "Date", "Error", "RangeError", "SyntaxError", "ReferenceError", "URIError", "EvalError", "JSON"];
    	for (let v in list) {
	    icall; // TODO JerryScript bug.
    	    v = list[v];
	    native_apply(native_mapset, m_org_virt, (arg2[0] = native.global[v], arg2[1] = virtual[v], arg2));
    	}
    }
    // During the executions of JavaScript primitive constructs, the following native objects can be created.
    native_apply(native_mapset, m_org_virt, (arg2[0] = Object.prototype, arg2[1] = virtual.Object.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = Date.prototype, arg2[1] = virtual.Date.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = Array.prototype, arg2[1] = virtual.Array.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = String.prototype, arg2[1] = virtual.String.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = Boolean.prototype, arg2[1] = virtual.Boolean.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = Number.prototype, arg2[1] = virtual.Number.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = RegExp.prototype, arg2[1] = virtual.RegExp.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = Function.prototype, arg2[1] = virtual.Function.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = Error.prototype, arg2[1] = virtual.Error.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = TypeError.prototype, arg2[1] = virtual.TypeError.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = RangeError.prototype, arg2[1] = virtual.RangeError.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = SyntaxError.prototype, arg2[1] = virtual.SyntaxError.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = ReferenceError.prototype, arg2[1] = virtual.ReferenceError.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = URIError.prototype, arg2[1] = virtual.URIError.prototype, arg2));
    native_apply(native_mapset, m_org_virt, (arg2[0] = EvalError.prototype, arg2[1] = virtual.EvalError.prototype, arg2));

    // Secure Communication
    native.WeakMap = WeakMap;
    native.WeakSet = WeakSet;
    const native_Date_prototype_getTime = Date.prototype.getTime;
    const native_Object_prototype_hasOwnProperty = Object.prototype.hasOwnProperty;
    const native_Object_getPrototypeOf = Object.getPrototypeOf;
    const native_Reflect_getOwnPropertyDescriptor = Reflect.getOwnPropertyDescriptor;
    const native_Object_getOwnPropertySymbols = Object.getOwnPropertySymbols;
    const native_Reflect_get = Reflect.get;
    const native_Object_prototype_toString = Object.prototype.toString;
    const native_Object_defineProperty = Object.defineProperty;
    const native_symbol_match = Symbol.match;
    const native_Symbol = Symbol;

/*
    const empty_arr = [];
    function EqClass(o, name) {
	const k = `[object ${name}]`;
	return native_apply(native_Object_prototype_toString, o, empty_arr) === k;
    }
*/
    function isObject(o) {
	return o && (typeof o === 'object' || typeof o === 'function');
    }
    function isArguments(o) {
	return Class(o) === "Arguments";
    }
    const isArray = Array.isArray;
    function isDate(o) {
	try {
	    if (o && typeof o === 'object') {
		native_apply(native_Date_prototype_getTime, o, arg0);
		return true;
	    }
	    return false;
	} catch(_) { return false; }
    };
    function isRegExp(argument) { // eslint-disable-line no-unused-vars
	// 1. If Type(argument) is not Object, return false.
	if (typeof argument !== 'object') {
	    return false;
	}
	// 2. Let matcher be ? Get(argument, @@match).
	var matcher = 'match' in native_Symbol ? native_Reflect_get(argument, native_symbol_match) : undefined;
	// 3. If matcher is not undefined, return ToBoolean(matcher).
	if (matcher !== undefined) {
	    return Boolean(matcher);
	}
	// 4. If argument does not have lastIndex, return false.
	if (!native_apply(native_Object_prototype_hasOwnProperty, argument, (arg1[0] = 'lastIndex', arg1)))
	    return false;
	// 5. If argument has a [[RegExpMatcher]] internal slot, return true.
	try {
	    var lastIndex = argument.lastIndex;
	    argument.lastIndex = 0;
	    native_apply(native.RegExp_prototype_exec, argument, arg0);
	    return true;
	    // eslint-disable-next-line no-empty
	} catch (e) {} finally {
	    argument.lastIndex = lastIndex;
	}
	// 6. Return false.
	return false;
    }

    function isString(o) {
	try {
	    if (typeof o === 'object') {
		String.prototype.toString.apply(o);
		return true;
	    }
	} catch(_) {}
	return false;
    }
    function isNumber(o) {
	try {
	    if (typeof o === 'object') {
		Number.prototype.toString.apply(o);
		return true;
	    }
	} catch(_) {}
	return false;
    }
    function isBoolean(o) {
	try {
	    if (typeof o === 'object') {
		Boolean.prototype.toString.apply(o);
		return true;
	    }
	} catch(_) {}
	return false;
    }


    // realm
    const global_realm = Object_create(null);
    global_realm.type = 'jerryscript';
    global_realm.global = _global_;
    {
/*
	const AsyncFunctionInstance = async function(){};
	const AsyncGeneratorFunctionInstance = async function*(){};
	const AsyncFunction = AsyncFunctionInstance.constructor;
	const AsyncFunctionPrototype = AsyncFunction.prototype;
	const AsyncGeneratorFunction = AsyncGeneratorFunctionInstance.constructor;
	const AsyncGenerator = AsyncGeneratorFunction.prototype; // XXX: (JerryBug) Don't print this object.
	const AsyncGeneratorPrototype = AsyncGenerator.prototype;
	const AsyncIteratorPrototype = Reflect.getPrototypeOf(AsyncGeneratorPrototype);
*/
	// A list of intrinsic objects for the unification of the prototype chain of shadow objects.
	global_realm.intrinsics = {
	    Array: _global_.Array,
	    ArrayBuffer: _global_.ArrayBuffer,
	    ArrayBufferPrototype: _global_.ArrayBuffer.prototype,
	    ArrayIteratorPrototype: _global_.Object.getPrototypeOf([].keys()),
	    ArrayPrototype: _global_.Array.prototype,
	    ArrayProto_entries: _global_.Array.prototype.entries,
	    ArrayProto_forEach: _global_.Array.prototype.forEach,
	    ArrayProto_keys: _global_.Array.prototype.keys,
	    ArrayProto_values: _global_.Array.prototype.values,
	    AsyncFromSyncIteratorPrototype: null, // TODO
	    AsyncFunction: null, // AsyncFunction,
	    AsyncFunctionPrototype: null, // AsyncFunctionPrototype,
	    AsyncGenerator: null, // AsyncGenerator,
	    AsyncGeneratorFunction: null, // AsyncGeneratorFunction,
	    AsyncGeneratorPrototype: null, // AsyncGeneratorPrototype,
	    AsyncIteratorPrototype: null, // AsyncIteratorPrototype,
	    Atomics: null, // _global_.Atomics,
	    Boolean: _global_.Boolean,
	    BooleanPrototype: _global_.Boolean.prototype,
	    DataView: _global_.DataView,
	    DataViewPrototype: _global_.DataView.prototype,
	    Date: _global_.Date,
	    DatePrototype: _global_.Date.prototype,
	    decodeURI: _global_.decodeURI,
	    decodeURIComponent: _global_.decodeURIComponent,
	    encodeURI: _global_.encodeURI,
	    encodeURIComponent: _global_.encodeURIComponent,
	    Error: _global_.Error,
	    ErrorPrototype: _global_.Error.prototype,
	    eval: _global_.eval,
	    EvalError: _global_.EvalError,
	    EvalErrorPrototype: _global_.EvalError.prototype,
	    Float32Array,
	    Float32ArrayPrototype: _global_.Float32Array.prototype,
	    Float64Array,
	    Float64ArrayPrototype: _global_.Float64Array.prototype,
	    Function: _global_.Function,
	    FunctionPrototype: _global_.Function.prototype,
	    Generator: (function*(){})().__proto__,
	    GeneratorFunction: (function*(){}).constructor,
	    GeneratorPrototype: Object.getPrototypeOf((function*(){}).prototype),
	    Int8Array,
	    Int8ArrayPrototype: _global_.Int8Array.prototype,
	    Int16Array,
	    Int16ArrayPrototype: _global_.Int16Array.prototype,
	    Int32Array,
	    Int32ArrayPrototype: _global_.Int32Array.prototype,
	    isFinite: _global_.isFinite,
	    isNaN: _global_.isNaN,
	    IteratorPrototype: null,
	    JSON: _global_.JSON,
	    JSONParse: _global_.JSON.parse,
	    JSONStringify: _global_.JSON.stringify,
	    Map: _global_.Map,
	    MapIteratorPrototype: null,
	    MapPrototype: _global_.Map.prototype,
	    Math: _global_.Math,
	    Number: _global_.Number,
	    NumberPrototype: _global_.Number.prototype,
	    Object: _global_.Object,
	    ObjectPrototype: _global_.Object.prototype,
	    ObjProto_toString: _global_.Object.prototype.toString,
	    ObjProto_valueOf: _global_.Object.prototype.valueOf,
	    parseFloat: _global_.parseFloat,
	    parseInt: _global_.parseInt,
	    Promise: _global_.Promise,
	    PromisePrototype: _global_.Promise.prototype,
	    PromiseProto_then: _global_.Promise.prototype.then,
	    Promise_all: _global_.Promise.all,
	    Promise_reject: _global_.Promise.reject,
	    Promise_resolve : _global_.Promise.resolve,
	    Proxy : _global_.Proxy,
	    RangeError : _global_.RangeError,
	    RangeErrorPrototype : _global_.RangeError.prototype,
	    ReferenceError : _global_.ReferenceError,
	    ReferenceErrorPrototype : _global_.ReferenceError.prototype,
	    Reflect : _global_.Reflect,
	    RegExp : _global_.RegExp,
	    RegExpPrototype : _global_.RegExp.prototype,
	    Set : _global_.Set,
	    SetIteratorPrototype : null,
	    SetPrototype : _global_.Set.prototype,
	    SharedArrayBuffer : null, // _global_.SharedArrayBuffer,
	    SharedArrayBufferPrototype : null, // _global_.SharedArrayBuffer.prototype,
	    String : _global_.String,
	    StringIteratorPrototype : null,
	    StringPrototype : _global_.String.prototype,
	    Symbol : _global_.Symbol,
	    SymbolPrototype : _global_.Symbol.prototype,
	    SyntaxError : _global_.SyntaxError,
	    SyntaxErrorPrototype : _global_.SyntaxError.prototype,
	    ThrowTypeError : null,	// (function() { "use strict"; return Object.getOwnPropertyDescriptor(arguments, 'callee').get; })()
	    TypedArray : null, // Object.getPrototypeOf(g.Int8Array),
	    TypedArrayPrototype : null, // Object.getPrototypeOf(g.Int8Array).prototype,
	    TypeError : _global_.TypeError,
	    TypeErrorPrototype : _global_.TypeError.prototype,
	    Uint8Array : _global_.Uint8Array,
	    Uint8ArrayPrototype : _global_.Uint8Array.prototype,
	    Uint8ClampedArray : _global_.Uint8ClampedArray,
	    Uint8ClampedArrayPrototype : _global_.Uint8ClampedArray.prototype,
	    Uint16Array : _global_.Uint16Array,
	    Uint16ArrayPrototype : _global_.Uint16Array.prototype,
	    Uint32Array : _global_.Uint32Array,
	    Uint32ArrayPrototype : _global_.Uint32Array.prototype,
	    URIError : _global_.URIError,
	    URIErrorPrototype : _global_.URIError.prototype,
	    WeakMap : _global_.WeakMap,
	    WeakMapPrototype : _global_.WeakMap.prototype,
	    WeakSet : _global_.WeakSet,
	    WeakSetPrototype : _global_.WeakSet.prototype
	};
	global_realm.native = {
	    Array: Array,
	    Date: Date,
	    RegExp: RegExp,
	    String: String,
	    Boolean: Boolean,
	    Number: Number
	};
	global_realm.duplicate_native = function(o) {
	    if (isArray(o)) return [];
	    else if (isDate(o)) return new global_realm.native.Date(o);
	    else if (isRegExp(o)) return new global_realm.native.RegExp(o);
	    else if (isString(o)) return new global_realm.native.String(o);
	    else if (isBoolean(o)) return new global_realm.native.Boolean(o);
	    else if (isNumber(o)) return new global_realm.native.Number(o)
	    return null;
	};
    }
    //
    const compiled_realm = Object_create(null);
    compiled_realm.type = 'compiled';
    compiled_realm.global = virtual;
    compiled_realm.intrinsics = {
	Array: virtual.Array,
	ArrayBuffer: null, // virtual.ArrayBuffer,
	ArrayBufferPrototype: null, // g.ArrayBuffer.prototype,
	ArrayIteratorPrototype: null, // Object.getPrototypeOf([].keys()),
	ArrayPrototype: virtual.Array.prototype,
	ArrayProto_entries: null, // virtual.Array.prototype.entries,
	ArrayProto_forEach: null, // virtual.Array.prototype.forEach,
	ArrayProto_keys: null, // virtual.Array.prototype.keys,
	ArrayProto_values: null, // virtual.Array.prototype.values,
	AsyncFromSyncIteratorPrototype: null,
	AsyncFunction: null, // (async function(){}).constructor,
	AsyncFunctionPrototype: null,
	AsyncGenerator: null, // (async function*(){})().__proto__,
	AsyncGeneratorFunction: null, // (async function*(){}).constructor,
	AsyncGeneratorPrototype: null, // Object.getPrototypeOf((async function*(){}).prototype),
	AsyncIteratorPrototype: null,
	Atomics: null, // g.Atomics,
	Boolean: virtual.Boolean,
	BooleanPrototype: virtual.Boolean.prototype,
	DataView: null, // virtual.DataView,
	DataViewPrototype: null, // g.DataView.prototype,
	Date: virtual.Date,
	DatePrototype: virtual.Date.prototype,
	decodeURI: virtual.decodeURI,
	decodeURIComponent: virtual.decodeURIComponent,
	encodeURI: virtual.encodeURI,
	encodeURIComponent: virtual.encodeURIComponent,
	Error: virtual.Error,
	ErrorPrototype: virtual.Error.prototype,
	eval: null, //g.eval,
	EvalError: virtual.EvalError,
	EvalErrorPrototype: virtual.EvalError.prototype,
	Float32Array: null,
	Float32ArrayPrototype: null, // g.Float32Array.prototype,
	Float64Array: null,
	Float64ArrayPrototype: null, // g.Float64Array.prototype,
	Function: virtual.Function,
	FunctionPrototype: virtual.Function.prototype,
	Generator: null, // (function*(){})().__proto__,
	GeneratorFunction: null, // (function*(){}).constructor,
	GeneratorPrototype: null, // Object.getPrototypeOf((function*(){}).prototype),
	Int8Array: null,
	Int8ArrayPrototype: null, // g.Int8Array.prototype,
	Int16Array: null,
	Int16ArrayPrototype: null, // g.Int16Array.prototype,
	Int32Array: null,
	Int32ArrayPrototype: null, // g.Int32Array.prototype,
	isFinite: virtual.isFinite,
	isNaN: virtual.isNaN,
	IteratorPrototype: null,
	JSON: virtual.JSON,
	JSONParse: virtual.JSON.parse,
	JSONStringify: virtual.JSON.stringify,
	Map: null, // g.Map,
	MapIteratorPrototype: null,
	MapPrototype: null, // g.Map.prototype,
	Math: virtual.Math,
	Number: virtual.Number,
	NumberPrototype: virtual.Number.prototype,
	Object: virtual.Object,
	ObjectPrototype: virtual.Object.prototype,
	ObjProto_toString: virtual.Object.prototype.toString,
	ObjProto_valueOf: virtual.Object.prototype.valueOf,
	parseFloat: virtual.parseFloat,
	parseInt: virtual.parseInt,
	Promise: null, // g.Promise,
	PromisePrototype: null, // .Promise.prototype,
	PromiseProto_then: null, // g.Promise.prototype.then,
	Promise_all: null, // .Promise.all,
	Promise_reject: null, // .Promise.reject,
	Promise_resolve : null, // .Promise.resolve,
	Proxy : null, // .Proxy,
	RangeError : virtual.RangeError,
	RangeErrorPrototype : virtual.RangeError.prototype,
	ReferenceError : virtual.ReferenceError,
	ReferenceErrorPrototype : virtual.ReferenceError.prototype,
	Reflect : null, // g.Reflect,
	RegExp : virtual.RegExp,
	RegExpPrototype : virtual.RegExp.prototype,
	Set : null, // g.Set,
	SetIteratorPrototype : null,
	SetPrototype : null, // g.Set.prototype,
	SharedArrayBuffer : null, // g.SharedArrayBuffer,
	SharedArrayBufferPrototype : null, // g.SharedArrayBuffer.prototype,
	String : virtual.String,
	StringIteratorPrototype : null,
	StringPrototype : virtual.String.prototype,
	Symbol : null, // g.Symbol,
	SymbolPrototype : null, // g.Symbol.prototype,
	SyntaxError : virtual.SyntaxError,
	SyntaxErrorPrototype : virtual.SyntaxError.prototype,
	ThrowTypeError : null,	// (function() { "use strict"; return Object.getOwnPropertyDescriptor(arguments, 'callee').get; })()
	TypedArray : null, // Object.getPrototypeOf(g.Int8Array),
	TypedArrayPrototype : null, // Object.getPrototypeOf(g.Int8Array).prototype,
	TypeError : virtual.TypeError,
	TypeErrorPrototype : virtual.TypeError.prototype,
	Uint8Array : null, // g.Uint8Array,
	Uint8ArrayPrototype : null, // g.Uint8Array.prototype,
	Uint8ClampedArray : null, // g.Uint8ClampedArray,
	Uint8ClampedArrayPrototype : null, // g.Uint8ClampedArray.prototype,
	Uint16Array : null, // g.Uint16Array,
	Uint16ArrayPrototype : null, // g.Uint16Array.prototype,
	Uint32Array : null, // g.Uint32Array,
	Uint32ArrayPrototype : null, // g.Uint32Array.prototype,
	URIError : virtual.URIError,
	URIErrorPrototype : virtual.URIError.prototype,
	WeakMap : null, // g.WeakMap,
	WeakMapPrototype : null, // g.WeakMap.prototype,
	WeakSet : null, // g.WeakSet,
	WeakSetPrototype : null, // g.WeakSet.prototype
    };
    compiled_realm.native = {
	Array: virtual.Array,
	Date: virtual.Date,
	RegExp: virtual.RegExp,
	String: virtual.String,
	Boolean: virtual.Boolean,
	Number: virtual.Number
    };
    compiled_realm.duplicate_native = function(o) {
	if (isArray(o)) return [];
	else if (isDate(o)) return new compiled_realm.native.Date(o);
	else if (isRegExp(o)) return new compiled_realm.native.RegExp(o);
	else if (isString(o)) return new compiled_realm.native.String(o);
	else if (isBoolean(o)) return new compiled_realm.native.Boolean(o);
	else if (isNumber(o)) return new compiled_realm.native.Number(o)
	return null;
    };
    
    const high_realm = compiled_realm;
    const low_realm = global_realm;
    var safeException = function(e) {
	print(":End");
	return e;
    }

    const {H2L, L2H} = (function() {
	// Create a map from new_realm and the current realm.
	const m_H2L = new native.WeakMap();
	const m_L2H = new native.WeakMap();
	const highI = high_realm.intrinsics, lowI = low_realm.intrinsics;
	for (let it in highI) {
	    if (highI[it] !== null) {
		m_H2L.set(highI[it], lowI[it]);
	    }
	    if (lowI[it] !== null) {
		m_L2H.set(lowI[it], highI[it]);
	    }
	}

	// common routines.
	function wrap(o) {
	    // TODO
	    // no immutable field, no freeze, no seal.
	    // But, if we wrap the object by proxy, it will lose its object type identity.
	    // other solution: skip the identity unification.
	    return o;
	}
	function alloc(m, current_realm) {
	    return {
		weakmap: m,
		deepcopy: function deepcopy(o, visited, declassified) {
		    if (!isObject(o)) return o;
		    // if there is a shadow object, we just override its fields.
		    let proxy = this.map(o);
		    if (proxy === undefined) {
			proxy = this.duplicateObj(o, visited, declassified);
			this.reg(o, proxy);
		    }
		    if (declassified.has(o)) return proxy;
		    
		    const names = Object_getOwnPropertyNames(o);
		    for (let i=0; i<names.length; i++) {
			let name = names[i];
			let desc = native_Reflect_getOwnPropertyDescriptor(o, name);
			if (native_apply(native_Object_prototype_hasOwnProperty, desc, (arg1[0] = 'value', arg1))) {
			    desc.value = this.sanitize(desc.value, visited, declassified);
			} else {
			    if (native_apply(native_Object_prototype_hasOwnProperty, desc, (arg1[0] = 'get', arg1)) && typeof desc.get === 'function') {
				desc.get = this.sanitize(desc.get, visited, declassified);
			    }
			    if (native_apply(native_Object_prototype_hasOwnProperty, desc, (arg1[0] = 'set', arg1)) && typeof desc.set === 'function') {
				desc.set = this.sanitize(desc.set, visited, declassified);
			    }
			}
			Reflect_defineProperty(proxy, name, desc);
		    }
		    const syms = native_Object_getOwnPropertySymbols(o);
		    for (let i=0; i<syms.length; i++) {
			let sym = syms[i];
			let desc = native_Reflect_getOwnPropertyDescriptor(o, sym);
			if (native_apply(native_Object_prototype_hasOwnProperty, desc, (arg1[0] = 'value', arg1))) {
			    desc.value = this.sanitize(desc.value, visited, declassified);
			} else {
			    if (native_apply(native_Object_prototype_hasOwnProperty, desc, (arg1[0] = 'get', arg1)) && typeof desc.get === 'function') {
				desc.get = this.sanitize(desc.get, visited, declassified);
			    }
			    if (native_apply(native_Object_prototype_hasOwnProperty, desc, (arg1[0] = 'set', arg1)) && typeof desc.set === 'function') {
				desc.set = this.sanitize(desc.set, visited, declassified);
			    }
			}
			Reflect_defineProperty(proxy, sym, desc);
		    }
		    declassified.add(o);
		    return proxy;
		},
		sanitize: function sanitize(o, visited, declassified) {
		    const to = typeof o;
		    if (o === undefined) return o;
		    if (o === null) return o;
		    if (o === virtual) return null;
		    if (to === 'boolean') return o;
		    if (to === 'number') return o;
		    if (to === 'string') return o;
		    if (to !== 'object' && to !== 'function') {
			throw "TODO: unknown typed value: "+to;
		    }

		    if (visited.has(o)) return this.map(o);
		    visited.add(o);

		    if (typeof o === "object" && isArguments(o)) {
			const arg = Object_create(null);
			arg.length = o.length;
			for (let i=0; i<o.length; i++) {
			    arg[i] = this.sanitize(o[i], visited, declassified);
			}
			return wrap(arg);
		    }
		    return wrap(this.deepcopy(o, visited, declassified));
		},
		duplicateObj: function(o, visited, declassified) {
		    if (typeof o === 'function') {
			const self = this, f = o, nf = function() {
			    "use strict";
			    var visited = new native.WeakSet();
			    var declassified = new native.WeakSet();
			    try {
				if (new.target === undefined) {
				    var set = new native.WeakSet();
				    if (this && typeof this === 'object') set.add(this);
				    var _this_ = self.other.sanitize(this, visited, set);
				    var args = self.other.sanitize(arguments, visited, declassified);
				    const v = native_apply(f, _this_, args);
				    return self.sanitize(v, new native.WeakSet(), new native.WeakSet());
				} else {
				    var _this_ = self.other.sanitize(this, visited, declassified);
				    var args = self.other.sanitize(arguments, visited, declassified);
				    const v = native_apply(f, _this_, args);
				    return;
				}
			    } catch (e) {
				throw safeException(self.sanitize(e, new native.WeakSet(), new native.WeakSet()));
			    }
			};
			self.reg(o, nf);
			nf.prototype = self.sanitize(o.prototype, visited, declassified);
			Object.setPrototypeOf(nf, current_realm.intrinsics.FunctionPrototype);
			return nf;
		    }
		    const no = current_realm.duplicate_native(o);
		    if (no !== null) return no;
		    
		    var t = Reflect_getPrototypeOf(o);
		    // TODO WeakMap.prototype.get
		    var h = this.map(t);
		    if (h === undefined) {
			// Note: the value of [[Prototype]] is also declassified.
			// h = null;
			h = this.sanitize(t, visited, declassified);
		    }
		    return Object_create(h);
		},
		map: function(o) {
		    // TODO Weak.prototype.get
		    return this.weakmap.get(o);
		},
		reg: function(o1, o2) {
		    this.weakmap.set(o1, o2);
		    this.other.weakmap.set(o2, o1);
		}
	    };
	}
	const low = alloc(m_L2H, high_realm), high = alloc(m_H2L, low_realm);
	low.other = high;
	high.other = low;

	return { L2H: o => low.sanitize(o, new native.WeakSet(), new native.WeakSet()), H2L: o => high.sanitize(o, new native.WeakSet(), new native.WeakSet()) };
    })();

    var securePropertyc = function(o,s) {
	// TODO: for instrumented test cases.
	return false;
	var t = typeof o;
	if (o == null) return true;
	else if (t !== "object" && t !== "function") return false;
	while (o !== null) {
	    if (native_apply(Object_prototype_hasOwnProperty, o, (arg1[0] = s, arg1)))
		return true;
	    o = securePrototypeOf(o);
	    if (o === false) return false;
	}
	return true;
    };
    // creates an object of auxiliary functions to access a secure environment.
    var g = {
	newfn: newfn,
	load_check: load_check,
	store_check: store_check,
	mload_check: mload_check,
	load: GetValue,
	loadc: GetValuec,
	oloadc: Get,
	nloadc: nGet,
	mload: mGetValue,
	mloadc: mGetValuec,
	store: store,
	storec: storec,
	Put: Put,
	loadvar: loadvar,
	storevar: storevar,
	deletevar: deletevar,
	apply: native.apply,
	call: native.call,
	newcall: newcall,
	preop: preop,
	postop: postop,
	mpre_plus: mpre_plus,
	mpre_minus: mpre_minus,
	mpost_plus: mpost_plus,
	mpost_minus: mpost_minus,

	unary: unary,

	plusvv: plusvv,
	plusvn: plusvn,
	plusnv: plusnv,
	minusvv: minusvv,
	minusvn: minusvn,
	minusnv: minusnv,
	multvv: multvv,
	multvn: multvn,
	multnv: multnv,
	divvv: divvv,
	divvn: divvn,
	divnv: divnv,

	equalvv: equalvv,
	equalvn: equalvn,
	equalnv: equalnv,
	nequalvv: nequalvv,
	nequalvn: nequalvn,
	nequalnv: nequalnv,

	modvv: modvv,
	modvn: modvn,
	modnv: modnv,
	bandvv: bandvv,
	bandvn: bandvn,
	bandnv: bandnv,
	borvv: borvv,
	borvn: borvn,
	bornv: bornv,
	bpowvv: bpowvv,
	bpowvn: bpowvn,
	bpownv: bpownv,
	ltvv: ltvv,
	ltvn: ltvn,
	ltnv: ltnv,
	gtvv: gtvv,
	gtvn: gtvn,
	gtnv: gtnv,
	ltevv: ltevv,
	ltevn: ltevn,
	ltenv: ltenv,
	gtevv: gtevv,
	gtevn: gtevn,
	gtenv: gtenv,
	equalvv: equalvv,
	equalvn: equalvn,
	equalnv: equalnv,
	rshiftvv: rshiftvv,
	rshiftvn: rshiftvn,
	rshiftnv: rshiftnv,
	rushiftvv: rushiftvv,
	rushiftvn: rushiftvn,
	rushiftnv: rushiftnv,
	lshiftvv: lshiftvv,
	lshiftvn: lshiftvn,
	lshiftnv: lshiftnv,
	uplusv: uplusv,
	uminusv: uminusv,
	unotv: unotv,
	instanceofvv: instanceofvv,
	invv: invv,
	innv: innv,
	this: thisCheck,
	this_opt: this_opt,

	hasSecurePropertyc: securePropertyc,
	hasOwnProperty: Object_prototype_hasOwnProperty,
	Export: Export,
	ToNumber: ToNumber,
	ToString: ToString,
	initIterator: initIterator,
	accessors: initAccessors,
	getPrototypeOf: virtual.Object.getPrototypeOf,

	safeException: safeException
    };

    var profiling = "${bProfile}";
    var profile = {cnt: {}, time: {}};
    if (profiling == "true") {
//	for (var v in g) {
//	    var t = (function() {
//		var name = v;
//		var o = g[v];
//		return function() {
//		    if (profile.cnt[name] === undefined) {
//			profile.cnt[name] = 1n;
//			profile.time[name] = 0n;
//		    } else profile.cnt[name] += 1n;
//
//		    var hrstart = process.hrtime.bigint();
//		    var rtn = o.apply(this, arguments);
//		    var hrduration = process.hrtime.bigint() - hrstart;
//		    if (name !== "call" && name !== "apply") profile.time[name] += hrduration;
//
//		    return rtn;
//		};
//	    })();
//	    g[v] = t;
//	    install_call(t);
//	}
//	var show = function() {
//	    for (var v in profile.cnt) {
//       		console.info('%s %d ms called %d times %d', v, Number(profile.time[v]) / 1000, Number(profile.cnt[v]), Number(profile.time[v]) / Number(profile.cnt[v]));
//	    }
//	};
//	g.show = show;
    } else {
	native.Object_freeze(g);
    }

    // for internal use.
    var virtual_RegExp = virtual.RegExp;
    var virtual_RegExp_prototype = virtual.RegExp.prototype;
    var virtual_RegExp_prototype_exec = virtual.RegExp.prototype.exec;
    var virtual_Object_getPrototypeOf = virtual.Object.getPrototypeOf;
    var virtual_Object_defineProperties = virtual.Object.defineProperties;
    var virtual_Function_prototype = virtual.Function.prototype;
    
    var exposed = function (s) { return g; };
    // registerOPT(trusted_fn, exposed, "");
    descFFF.value = exposed;
    Reflect_defineProperty(native.global, "_$_secret_env_$_", descFFF);

    // for debug
    // g.virtual = virtual;
    // Object.defineProperty(native.global, "_", {value: g, enumerable: false, configurable: false, writable: false});
    // Test mode.
    if (test) {
	const testFns = ["ERROR", "$ERROR", "$FAIL", "$PRINT", "Test262Error", "fnGlobalObject", "runTestCase", "fnExists", "arrayContains", "dataPropertyAttributesAreCorrect", "accessorPropertyAttributesAreCorrect", "$quit", "isEqual", "getPrecision", "$INCLUDE", "compareArray"];
    	for (let n of testFns) {
    	    virtual[n] = register_function(native.global[n], n, false, 0);
    	}
    	virtual.fnGlobalObject = newfn(function() { return virtual; }, " ");
    	{
    	    const org = native.global["print"];
    	    virtual.print = register_function(function () {
    		org.apply(this, arguments);
    	    }, " ");
	}
    }
})(this);
} catch (e) { throw e; if (typeof e === 'object') warn(e.message); else warn(e); }
})();
