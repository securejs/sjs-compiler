(function() { var log, warn, info; try { (function(_global_) {
    "use strict";
    
    if (_global_.hasOwnProperty('_$_secret_env_$_')) return false;
    var node_mode = false;
    var DOM_mode = false;
    var proxy_mode = false;
    var setproto_mode = false;
    var map_mode = false;

    var native_map, native_mapset, native_mapget, native_maphas;
    if (_global_.WeakMap) {
	map_mode = true;
	native_map = WeakMap;
	native_mapset = WeakMap.prototype.set;
	native_mapget = WeakMap.prototype.get;
	native_maphas = WeakMap.prototype.has;
    }
    var native_setPrototypeOf = null;
    if (Object.setPrototypeOf) {
	setproto_mode = true;
	native_setPrototypeOf = Object.setPrototypeOf;
    }

    if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(search, this_len) {
	    if (this_len === undefined || this_len > this.length) {
		this_len = this.length;
	    }
	    return this.substring(this_len - search.length, this_len) === search;
	};
    }
    if (!String.prototype.includes) {
	String.prototype.includes = function(search, start) {
	    'use strict';

	    if (search instanceof RegExp) {
		throw TypeError('first argument must not be a RegExp');
	    } 
	    if (start === undefined) { start = 0; }
	    return this.indexOf(search, start) !== -1;
	};
    }
    if (!String.prototype.startsWith) {
	Object.defineProperty(String.prototype, 'startsWith', {
            value: function(search, rawPos) {
		var pos = rawPos > 0 ? rawPos|0 : 0;
		return this.substring(pos, pos + search.length) === search;
            }
	});
    }
    if (!Array.from) {
	Array.from = (function () {
	    var toStr = Object.prototype.toString;
	    var isCallable = function (fn) {
		return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
	    };
	    var toInteger = function (value) {
		var number = Number(value);
		if (isNaN(number)) { return 0; }
		if (number === 0 || !isFinite(number)) { return number; }
		return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
	    };
	    var maxSafeInteger = Math.pow(2, 53) - 1;
	    var toLength = function (value) {
		var len = toInteger(value);
		return Math.min(Math.max(len, 0), maxSafeInteger);
	    };

	    // The length property of the from method is 1.
	    return function from(arrayLike/*, mapFn, thisArg */) {
		// 1. Let C be the this value.
		var C = this;

		// 2. Let items be ToObject(arrayLike).
		var items = Object(arrayLike);

		// 3. ReturnIfAbrupt(items).
		if (arrayLike == null) {
		    throw new TypeError('Array.from requires an array-like object - not null or undefined');
		}

		// 4. If mapfn is undefined, then let mapping be false.
		var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
		var T;
		if (typeof mapFn !== 'undefined') {
		    // 5. else
		    // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
		    if (!isCallable(mapFn)) {
			throw new TypeError('Array.from: when provided, the second argument must be a function');
		    }

		    // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
		    if (arguments.length > 2) {
			T = arguments[2];
		    }
		}

		// 10. Let lenValue be Get(items, "length").
		// 11. Let len be ToLength(lenValue).
		var len = toLength(items.length);

		// 13. If IsConstructor(C) is true, then
		// 13. a. Let A be the result of calling the [[Construct]] internal method 
		// of C with an argument list containing the single item len.
		// 14. a. Else, Let A be ArrayCreate(len).
		var A = isCallable(C) ? Object(new C(len)) : new Array(len);

		// 16. Let k be 0.
		var k = 0;
		// 17. Repeat, while k < len… (also steps a - h)
		var kValue;
		while (k < len) {
		    kValue = items[k];
		    if (mapFn) {
			A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
		    } else {
			A[k] = kValue;
		    }
		    k += 1;
		}
		// 18. Let putStatus be Put(A, "length", len, true).
		A.length = len;
		// 20. Return A.
		return A;
	    };
	}());
    }
    // print wrapper
    function print_wrapper(f) {
	var o = f;
	return function() {
	    var args = [];
	    for (var i=0; i<arguments.length; i++) {
		arguments[i] = ToStringForError(arguments[i]);
	    }
	    return native.apply[icall](o, null, arguments);
	};
    }
    if (typeof _global_.console === 'object') {
	if (typeof _global_.console.log === 'function') {
	    log = print_wrapper(_global_.console.log);
	} else {
	    log = function() {};
	}
	if (typeof _global_.console.warn === 'function') {
	    warn = print_wrapper(_global_.console.warn);
	} else {
	    warn = function() {};
	}
	if (typeof _global_.console.info === 'function') {
	    info = print_wrapper(_global_.console.info);
	} else {
	    info = function() {};
	}
    } else if (typeof _global_["print"] === "function") {
	log = print_wrapper(_global_.print);
	warn = print_wrapper(_global_.print);
	info = print_wrapper(_global_.print);
    } else {
        log = function() {};
        warn = function() {};
        info = function() {};
    }

    if (typeof process === 'object' && process !== null &&
	typeof process.release === 'object' && process.release !== null &&
	process.release.name === 'node')
	node_mode = true;
    if (typeof _global_.window === 'object')
        DOM_mode = true;

    var caller_check = false;
    var descTTT = Object.create(null);
    descTTT.configurable = true;
    descTTT.enumerable = true;
    descTTT.writable = true;
    var descFFF = Object.create(null);
    descFFF.configurable = false;
    descFFF.enumerable = false;
    descFFF.writable = false;
    var descValue = Object.create(null);
    descTTT.configurable = true;
    descTTT.enumerable = true;
    descTTT.writable = true;

    var native = {};
    native.global = _global_;
    native.Object = Object;
    native.Function = Function;
    native.Array = Array;
    native.Math = Math;
    native.Date = Date;
    native.Boolean = Boolean;
    native.Number = Number;
    native.String = String;
    native.RegExp = RegExp;
    native.JSON = JSON;
    native.Error = Error;
    native.TypeError = TypeError;
    native.RangeError = RangeError;
    native.SyntaxError = SyntaxError;
    native.ReferenceError = ReferenceError;
    native.URIError = URIError;
    native.EvalError = EvalError;
    if (DOM_mode) {
	if (_global_.XMLHttpRequest)
	    native.XMLHttpRequest = _global_.XMLHttpRequest;
    }
    if ("Proxy" in _global_) {
	proxy_mode = true;
	native.Proxy = Proxy;
    }

    var use_symbol = false;
    if (_global_.hasOwnProperty("Symbol")) {
	// String.prototype.replace
	native.s_symbol_replace = Symbol.replace;
	// String.prototype.search
	native.s_symbol_search = Symbol.search;
	// String.prototype.split
	native.s_symbol_split = Symbol.split;
	// String.prototype.match
	native.s_symbol_match = Symbol.match;
	native.s_symbol_toStringTag = Symbol.toStringTag;
	var tmp;
	use_symbol = true;
	tmp = native.Object.getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_replace);
	if (tmp !== undefined) native.symbol_replace = tmp.value;
	tmp = native.Object.getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_search);
	if (tmp !== undefined) native.symbol_search = tmp.value;
	tmp = native.Object.getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_split);
	if (tmp !== undefined) native.symbol_split = tmp.value;
	tmp = native.Object.getOwnPropertyDescriptor(RegExp.prototype, native.s_symbol_match);
	if (tmp !== undefined) native.symbol_match = tmp.value;

	// Invariant conditions for the secure programming environment.
	descFFF.value = undefined;
	native.Object.defineProperty(String.prototype, native.s_symbol_replace, descFFF);
	native.Object.defineProperty(String.prototype, native.s_symbol_split, descFFF);
    }

    var internalPrefix = '@';
    var makeI = function(s) { return internalPrefix + s; };
    var isInternal = function(s) {
	return (s[0] === internalPrefix[0]);
    }
    // monkey patches for internal fields
    native.Object.getOwnPropertyNames = (function() {
	var native_Object_getOwnPropertyNames = native.Object.getOwnPropertyNames;
	return function (O) {
	    var list = native_Object_getOwnPropertyNames(O);
	    var nlist = [];
	    var i;
	    for (i=0; i<list.length; i++) {
		if (!isInternal(list[i]))
		    nlist[nlist.length] = list[i];
	    }
	    return nlist;
	}
    })();
	

    native.Object_prototype_toString_native = Object.prototype.toString;
    native.Object_prototype_hasOwnProperty = native.Object.prototype.hasOwnProperty;
    native.Object_getOwnPropertyDescriptor = native.Object.getOwnPropertyDescriptor;
    native.Object_getOwnPropertyNames = native.Object.getOwnPropertyNames;
    native.Object_getPrototypeOf = native.Object.getPrototypeOf;
    native.Object_defineProperty = native.Object.defineProperty;
    native.Object_create = native.Object.create;
    native.Object_keys = native.Object.keys;
    native.Object_freeze = native.Object.freeze;
    native.Object_prototype = native.Object.prototype;
    native.Object_isExtensible = native.Object.isExtensible;
    native.call = native.Function.prototype.call;
    native.apply = native.Function.prototype.apply;
    native.indexOf = native.Array.prototype.indexOf;
    native.String_prototype_slice = String.prototype.slice;
    native.String_prototype_substring = String.prototype.substring;
    native.Boolean_prototype_valueOf = Boolean.prototype.valueOf;
    native.Array_prototype_join = native.Array.prototype.join;
    native.Function_prototype_bind = native.Function.prototype.bind;
    native.Date_prototype_getTime = native.Date.prototype.getTime;
    native.Math_min = native.Math.min;
    native.Math_max = native.Math.max;
    native.Math_floor = native.Math.floor;
    native.Math_abs = native.Math.abs;
    native.isNaN = isNaN;
    native.isFinite = isFinite;
    native.Array_isArray = native.Array.isArray;
    native.RegExp_prototype_compile = native.RegExp.prototype.compile;

    function install(f, o) {
	native.Object.defineProperty(f, internalPrefix + o, {value: native[o], enumerable: false, configurable: false, writable: false});
    };
    function install_call(f) {
	install(f, 'call');
    };
    install_call(native.call);
    install_call(native.apply);
    install_call(native.Object_prototype_hasOwnProperty);
    install_call(native.String_prototype_slice);
    install_call(native_maphas);
    install_call(native_mapset);
    install_call(native_mapget);
    var icall = makeI('call');

    var sDesc = function (desc) {
	var vdesc = native.Object.create(null);
	var arr = ["get", "set", "value", "writable", "enumerable", "configurable"];
	var i, name;
	for (i=0; i<arr.length; i++) {
	    name = arr[i];
	    if (native.Object_prototype_hasOwnProperty[icall](desc, name))
		vdesc[name] = desc[name];
	}
	return vdesc;
    };
    function preaccess(o, arr) {
	var i, k;
	var no = native.Object_create(o);
	for (i=0; i<arr.length; i++) {
	    k = arr[i];
	    descTTT.value = Get(o, k);
	    native.Object_defineProperty(no, k, descTTT);
	}
	return no;
    };

    if (use_symbol) {
	// var hasRegExpMatcher = function(o) {
	//     var lastIndex = native.Object_getOwnPropertyDescriptor(o, "lastIndex")
	//     if (lastIndex === undefined) return false;
	//     try {
	// 	native.apply[icall](native.RegExp_prototype_compile, o, [o]);
	// 	o.lastIndex = lastIndex.value;
	//     } catch(_) {
	// 	return false;
	//     }
	//     return true;
	// };
	(function() {
	    var nullDesc = sDesc({value: null, writable: true, configurable: true, enumerable: true});
	    var nativeTOS = native.Object.prototype.toString;
	    var getNative = function(o) {
		var oldd = native.Object_getOwnPropertyDescriptor(o, native.s_symbol_toStringTag);
		if (oldd !== undefined && oldd.configurable === false) {
		    // TODO
		    return "[object Object]";
		}
		try {
		    native.Object_defineProperty(o, native.s_symbol_toStringTag, nullDesc);
		} catch(e) {
		    // TODO
		    return "[object Object]";
		}
		var rtn = native.call[icall](nativeTOS, o);
		if (oldd === undefined) delete o[native.s_symbol_toStringTag];
		else native.Object_defineProperty(o, native.s_symbol_toStringTag, oldd);
		return rtn;
	    }
	    native.Object_prototype_toString = function () {
		if (this === undefined) return "[object Undefined]";
		else if (this === null) return "[object Null]";
		var no = ToObject(this);
		var s = Get(no, native.s_symbol_toStringTag);
		if (typeof s === "string") return "[object "+s+"]";
		return getNative(no);
		// return tag;
	    };
	})();
    } else {
	native.Object_prototype_toString = native.Object.prototype.toString;
    }
    install_call(native.Object_prototype_toString);

    var test = false;
    if (native.global.hasOwnProperty("runTestCase")) {
    	test = true;
	node_mode = false;
    }

    function virtual_Object(v) { return native.apply[icall](native.Object, this, arguments); };
    var virtual;
    // for testsuite 15.1.{2.2,2.3,2.4,2.5,3.1,3.2,3.3,3.4}.
    if (test) virtual = Object.create(virtual_Object.prototype);
    else virtual = Object.create(null);
    
    // Note: the property 'name' must exist.
    var m_virt_org;
    if (map_mode) m_virt_org = new native_map();
    else m_virt_org = {key: [], value: [], name: makeI("virt")};
    var m_virt_proto = {key: [], value: [], name: makeI("proto")};
    var m_org_virt;
    if (map_mode) m_org_virt = new native_map();
    else m_org_virt = {key: [], value: [], name: makeI("org")};
    // var trusted_fn = {key: [], value: [], name: makeI("trusted_fn")};
    var m_tos = {key: [], value: [], name: makeI("tos")};
    var set_no_construct = {key: [], name: makeI("noconstruct")};
    var m_H2L = {key: [], value: [], name: makeI("H2L")};
    var m_L2H = {key: [], value: [], name: makeI("L2H")};
    if (!map_mode)
	install(m_virt_org.key, 'indexOf')
    install(m_virt_proto.key, 'indexOf')
    if (!map_mode)
	install(m_org_virt.key, 'indexOf')
    // install(trusted_fn.key, 'indexOf')
    install(m_tos.key, 'indexOf')
    install(set_no_construct.key, 'indexOf')
    install(m_H2L.key, 'indexOf')
    install(m_L2H.key, 'indexOf')

    var thisCheck;
    // if (test)
	thisCheck = function (o) {
	    if (o === native.global) return virtual;
	    return o;
	};

    // else {
    // 	var descDontTouch = (function() {
    // 	    var fnDontTouchThis = function() {
    // 		throw new native.Error("Accessing 'this' in the context of a function call is not allowed");
    // 	    };
    // 	    return sDesc({get: fnDontTouchThis, set: fnDontTouchThis});
    // 	})();
    // 	// In defensive mode, it does not allow to access the global object by 'this' keyword.
    // 	thisCheck = function (o) {
    // 	    if (o === native.global) {
    // 		var nThis = native.Object_create(null);
    // 		var keys = native.Object_keys(virtual);
    // 		for (var i=0; i<keys.length; i++) {
    // 		    native.Object_defineProperty(nThis, keys[i], descDontTouch);
    // 		}
    // 		return nThis;
    // 	    }
    // 	    return o;
    // 	};
    // }

    function Class(o) {
	return native.String_prototype_slice[icall](ToStringNative(o), 8, -1);
    };
    function ToStringForError(s) {
	if (isObject(s)) {
	    // implicit accesses to "Symbol.toStringTag". secure.
	    return native.Object_prototype_toString[icall](s);
	}
	return ""+s;
    }
    function ToStringNative(s) {
	// implicit accesses to "Symbol.toStringTag". secure.
	var res = native.Object_prototype_toString[icall](s);
	return res;
    }
    function initAccessors(o, name, get, set) {
	var desc = native.Object_getOwnPropertyDescriptor(o, name);
	var vdesc = native.Object.create(null);
	if (desc === undefined) {
	    vdesc.enumerable = true;
	    vdesc.configurable = true;
	} else {
	    vdesc.enumerable = desc.enumerable;
	    vdesc.configurable = desc.configurable;
	}
	vdesc.set = set;
	vdesc.get = get;
	native.Object_defineProperty(o, name, vdesc);
	return o;
    }
    function newfn(fn, name) {
	// registerOPT2(trusted_fn, fn, name);
	return fn;
    };
    function Call(fn, ths, args) {
	return native.apply[icall](fn, ths, args);
    }
    function DefineOwnProperty(O, P, Attributes, Throw) {
	if (P === 'length' && Class(O) === 'Array') {
	    if (!native.Object_prototype_hasOwnProperty[icall](Attributes, 'value')) {
		try {
		    native.Object_defineProperty(O, P, Attributes);
		} catch (e) {
		    if (Throw) throw e;
		}
		return true;
	    }
	    var v = Attributes.value;
	    // 15.4.5.1. Step 3.d.
	    var newLen = ToUint32(v);
	    if (newLen !== ToNumber(v)) throw new RangeError("Invalid array length");
	    // TODO: Other properties.
	    try {
		Attributes.value = newLen;
		native.Object_defineProperty(O, P, Attributes);
//	        O[P] = newLen;
	    	return true;
	    } catch (e) {
		// Since the above code is executed in 'strict mode', it throws an exception when it should be.
		// On the other hand, since the compiled code may not be executed in 'strict mode',
		// we filter out not-necessary-exceptions from here.
		if (Throw) throw e;
	    }
	} else {
	    // 8.12.9 [[DefineOwnProperty]](P, Desc, Throw)
	    // The following operator does not throw an exception in the following pre-condition:
	    //   IsDataDescriptor = true /\ [[writable]] = true
	    try {
		native.Object_defineProperty(O, P, Attributes);
	    } catch (e) {
		if (Throw) throw e;
	    }
	    return true;
	}
    };
    var load_check;
    if (setproto_mode && map_mode) {
	// load_check = function (o, s) {
	//     if (typeof o !== "object" && typeof o !== "function") return false;
	//     if (typeof s !== 'string' && typeof s !== 'number') return false;
	//     if (caller_check && (s === "arguments" || s === "caller")) return false;
	//     var oo = o;
	//     while (o !== null) {
	// 	if (native.Object_prototype_hasOwnProperty[icall](o, s))
	// 	    return true;

	// 	// a specialized version of the virtual "getPrototypeOf".
	// 	o = native.Object_getPrototypeOf(o);
	// 	if (native_maphas[icall](m_org_virt, o)) return false;
	//     }
	//     // TODO should return undefined.
	//     return !(s in oo);
	// }
	var isafe = makeI('safe');
	var descSAFE = Object.create(null);
	descSAFE.configurable = false;
	descSAFE.enumerable = false;
	descSAFE.writable = false;
	descSAFE.value = null;
	
	var load_check_i = function(o) {
	    if (native.Object_prototype_hasOwnProperty[icall](o, isafe))
		return true;
	    var proto = native.Object_getPrototypeOf(o);
	    if (proto === null) return true;

	    var vproto = native_mapget[icall](m_org_virt, proto);
	    if (vproto === undefined) {
		vproto = proto;
	    } else {
		try { native_setPrototypeOf(o, vproto); } catch (_) { return false; }
	    }
	    if (load_check_i(vproto)) {
		try { native.Object_defineProperty(o, isafe, descSAFE); } catch(_) {}
		return true;
	    }
	    return false;
	}

	load_check = function (o, s) {
	    if (typeof o === 'object' || typeof o === 'function') {
		if (typeof s !== 'string' && typeof s !== 'number') return false;
		// if (caller_check && (s === "arguments" || s === "caller")) return false;
		if (native.Object_prototype_hasOwnProperty[icall](o, s))
		    return true;
		if (load_check_i(o)) return true;
		o = native.Object_getPrototypeOf(o);
		if (native_maphas[icall](m_org_virt, o)) return false;
		var oo = o;
		while (o !== null) {
		    if (native.Object_prototype_hasOwnProperty[icall](o, s))
			return true;

		    // a specialized version of the virtual "getPrototypeOf".
		    o = native.Object_getPrototypeOf(o);
		    if (native_maphas[icall](m_org_virt, o)) return false;
		}
		return !(s in oo);
	    } else {
		return false;
	    }
	}
    } else {
	load_check = function (o, s) {
	    if (typeof o !== "object" && typeof o !== "function") return false;
	    if (isObject(s)) return false;
	    s = "" + s;
	    // if (caller_check && (s === "arguments" || s === "caller")) return false;
	    while (o !== null) {
		if (native.Object_prototype_hasOwnProperty[icall](o, s))
		    return true;

		// a specialized version of the virtual "getPrototypeOf".
		var t = search(m_virt_proto, o, false);
		if (t === null) {
		    o = native.Object_getPrototypeOf(o);
		    t = search(m_org_virt, o, false);
		    if (t !== null)
			return false;
		} else {
		    return false;
		}
	    }
	    // TODO should return undefined.
	    return false;
	}
    }
    var this_opt = function(o) {
	if (o && typeof (o === 'object' || typeof o === 'function' )) {
	    var proto = native.Object_getPrototypeOf(o);
	    if (proto === null) return true;

	    var vproto = native_maphas[icall](m_org_virt, proto);
	    if (!vproto && native.Object_prototype_hasOwnProperty[icall](proto, isafe))
		return true;

	    return load_check_i(o);
	}
	return false;
    }

    var store_check;
    if (setproto_mode && map_mode) {
	store_check = function (o, s) {
	    if (o && (typeof o === 'object' || typeof o === 'function')) {
		if (typeof s !== 'string' && typeof s !== 'number') return false;
		if (native.Object_prototype_hasOwnProperty[icall](o, s))
		    return true;
		if (load_check_i(o)) return true;
		o = native.Object_getPrototypeOf(o);
		if (native_maphas[icall](m_org_virt, o)) return false;
		var oo = o;
		while (o !== null) {
		    if (native.Object_prototype_hasOwnProperty[icall](o, s))
			return true;
		    // a specialized version of the virtual "getPrototypeOf".
		    o = native.Object_getPrototypeOf(o);
		    if (native_maphas[icall](m_org_virt, o)) return false;
		}
		return !(s in oo);
	    } else {
		return false;
	    }
	}
    } else {
	store_check = function (o, s) {
	    if (!isObject(o)) return false;
	    // if (caller_check && (s === "caller" || s === "callee" || s === "arguments")) return false;
	    while (o !== null) {
		if (native.Object_prototype_hasOwnProperty[icall](o, s))
		    return true;
		// a specialized version of the virtual "getPrototypeOf".
		var t = search(m_virt_proto, o, false);
		if (t === null) {
		    o = native.Object_getPrototypeOf(o);
		    t = search(m_org_virt, o, false);
		    if (t !== null)
			return false;
		} else {
		    return false;
		}
	    }
	    // TODO create field
	    return false;
	}
    }
	

    var mload_check;
    if (setproto_mode && map_mode) {
	mload_check = function (o, s) {
	    if (o && (typeof o === 'object' || typeof o === 'function')) {
		if (typeof s !== 'string' && typeof s !== 'number' && isObject(s)) return false;
		// if (caller_check && (s === "arguments" || s === "caller")) return false;
		if (load_check_i(o)) return true;

		o = native.Object_getPrototypeOf(o);
		if (native_maphas[icall](m_org_virt, o)) {
		    return false;
		}
		while (o !== null) {
		    if (native.Object_prototype_hasOwnProperty[icall](o, s))
			return true;
		    
		    // a specialized version of the virtual "getPrototypeOf".
		    o = native.Object_getPrototypeOf(o);
		    if (native_maphas[icall](m_org_virt, o)) {
			return false;
		    }
		}
	    }
	    // exception
	    return false;
	}
    } else {
	mload_check = function (o, s) {
	    if (typeof o !== "object" && typeof o !== "function") return false;
	    if (typeof s !== 'string' && typeof s !== 'number' && isObject(s)) return false;
	    // if (caller_check && (s === "arguments" || s === "caller")) return false;
	    while (o !== null) {
		if (native.Object_prototype_hasOwnProperty[icall](o, s))
		    return true;

		// a specialized version of the virtual "getPrototypeOf".
		var t = search(m_virt_proto, o, false);
		if (t === null) {
		    o = native.Object_getPrototypeOf(o);
		    t = search(m_org_virt, o, false);
		    if (t !== null)
			return false;
		} else {
		    return false;
		}
	    }
	    // exception
	    return false;
	}
    }

    function GetValue(o, s) {
	// return o[s];
	var no;
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    no = ToObject(o);
	} else {
	    no = o;
	}
	s = ToString(s);
	return load_opt(no, s, no, o);
    };
    function GetValuec(o, s) {
    	// return o[s];
	var no;
    	if (o == null) {
    	    s = ToStringForError(s);
    	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
    	} else if (typeof o !== "object" && typeof o !== "function") {
    	    no = ToObject(o);
    	} else {
	    no = o;
	}
    	return load_opt(no, s, no, o);
    }
    function mGetValue(o, s) {
	// return o[s];
	var no;
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    no = ToObject(o);
	} else {
	    no = o;
	}
	s = ToString(s);
	return load_opt(no, s, no, o);
    };
    function mGetValuec(o, s) {
	var no;
	// return o[s];
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot read property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    no = ToObject(o);
	} else {
	    no = o;
	}
	return load_opt(no, s, no, o);
    }
    function GetProperty(o, s) {
	while (true) {
	    // Make sure that a function object does not flow out to the untrusted env.
	    // if (caller_check && typeof o === 'function') {
	    // 	if (s === "arguments")
	    // 	    throw new native.TypeError("'arguments' property of a function is not able to accessed on defensive mode");
	    // 	if (s === "caller")
	    // 	    throw new native.TypeError("'caller' property of a function is not able to accessed on defensive mode");
	    // }
	    if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		return native.Object_getOwnPropertyDescriptor(o, s);
	    }
	    o = virtual_Object_getPrototypeOf(o);
	    if (o === null) return undefined;
	}
    };
    function Get(o, s) {
	var no;
	if (!isObject(o)) no = ToObject(o);
	else no = o;
	return load_opt(no, s, no, o);
    };
    function nGet(o, s) {
	var no;
	if (!isObject(o)) no = ToObject(o);
	else no = o;
	var desc = GetProperty(no, s);
	if (desc === undefined) return undefined;
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) return desc.value;
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'get')) {
	    if (desc.get === undefined) return undefined;
	    return Call(desc.get, o);
	}
    };
    var load_opt;
    if (setproto_mode && map_mode)
	load_opt = function load_opt(o, s, oo, no) {
	    var insecure = false;
	    // var res = caller_check && (s === "arguments" || s === "caller");
	    while (o !== null) {
		// if (res && typeof o === 'function') {
		//     if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		// 	if (s === "arguments")
		// 	    throw new native.TypeError("'arguments' property of a function is not able to accessed on defensive mode");
		// 	if (s === "caller")
		// 	    throw new native.TypeError("'caller' property of a function is not able to accessed on defensive mode");
		//     } else if (s === 'caller') {
		// 	// JerryScript does not create a caller property.
		// 	return undefined;
		//     }
		// }
		if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		    if (!insecure) {
			return oo[s];
		    }
		    var desc = native.Object_getOwnPropertyDescriptor(o, s);
		    if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) return desc.value;
		    if (native.Object_prototype_hasOwnProperty[icall](desc, 'get')) {
			if (desc.get === undefined) return undefined;
			return Call(desc.get, no);
		    }
		} 
		// a specialized version of the virtual "getPrototypeOf".
		var o2 = o;
		o = native.Object_getPrototypeOf(o);
		var t = native_mapget[icall](m_org_virt, o);
		// var t = search(m_org_virt, o, false);
		if (t !== undefined) {
		// if (t !== null) {
		    o = t;
		    insecure = true;
		}
	    }
	    return undefined;
	};
    else
	load_opt = function load_opt(o, s, oo, no) {
	    var insecure = false;
	    // var res = caller_check && (s === "arguments" || s === "caller");
	    while (o !== null) {
		// if (res && typeof o === 'function') {
		//     if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		// 	if (s === "arguments")
		// 	    throw new native.TypeError("'arguments' property of a function is not able to accessed on defensive mode");
		// 	if (s === "caller")
		// 	    throw new native.TypeError("'caller' property of a function is not able to accessed on defensive mode");
		//     } else if (s === 'caller') {
		// 	// JerryScript does not create a caller property.
		// 	return undefined;
		//     }
		// }
		if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		    if (!insecure) {
			return oo[s];
		    }
		    var desc = native.Object_getOwnPropertyDescriptor(o, s);
		    if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) return desc.value;
		    if (native.Object_prototype_hasOwnProperty[icall](desc, 'get')) {
			if (desc.get === undefined) return undefined;
			return Call(desc.get, no);
		    }
		} 
		// a specialized version of the virtual "getPrototypeOf".
		var t = search(m_virt_proto, o, false);
		var o2 = o;
		if (t === null) {
		    o = native.Object_getPrototypeOf(o);
		    t = search(m_org_virt, o, false);
		    if (t !== null) {
			o = t;
			insecure = true;
		    }
		} else {
		    o = t;
		    insecure = true;
		}
	    }
	    return undefined;
	};
	
    function oGet(o, s) {
	var no;
	if (!isObject(o)) no = ToObject(o);
	else no = o;
	return load_opt(no, s, no, o);
    };
    function Geto(o, s) {
	return load_opt(o, s, o, o);
	// var desc = GetProperty(o, s);
	// if (desc === undefined) return undefined;
	// if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) return desc.value;
	// if (native.Object_prototype_hasOwnProperty[icall](desc, 'get')) {
	//     if (desc.get === undefined) return undefined;
	//     return Call(desc.get, o);
	// }
    };
    var store_i;
    if (setproto_mode && map_mode) 
	store_i = function store_i(o, s, v, oo, th, prim, insecure) {
	    while (o !== null) {
		// if (caller_check) {
		//     if (o === virtual_Function_prototype && (s === "caller" || s === "callee" || s === "arguments")) {
		// 	throw new native.TypeError("'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them");
		//     }
		// }
		if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		    // optimized case.
		    if (!insecure) {
			try {
			    return oo[s] = v;
			} catch(e) {
			    if (th) throw e;
			}
		    }
		    var desc = native.Object_getOwnPropertyDescriptor(o, s);
		    if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) {
			if (desc.writable) {
			    // 15.4.5.1 [[DefineOwnProperty]](P, Desc, Throw)
			    descValue.value = v;
			    descValue.writable = true;
			    descValue.enumerable = desc.enumerable;
			    descValue.configurable = desc.configurable;
			    DefineOwnProperty(oo, s, descValue, th);
			    return v;
			} else if (th) {
			    throw new native.TypeError("Cannot assign to read only property '"+s+"' of object '"+ToStringForError(o)+"'");
			}
		    } else {
			if (desc.set !== undefined) {
			    Call(desc.set, oo, [v]);
			    return v;
			} else if (th) {
			    throw new native.TypeError("Cannot set property '"+s+"' of '"+ToStringForError(o)+"' which has only a getter");
			}
		    }
		    // 8.12.5. Step 1.b.
		    return v;
		} else {
		    // a specialized version of the virtual "getPrototypeOf".
		    var o2 = o = native.Object_getPrototypeOf(o);
		    var t = native_mapget[icall](m_org_virt, o);
		    // var t = search(m_org_virt, o, false);
		    // if (t !== null) {
		    if (t !== undefined) {
			o = t;
			insecure = true;
		    }
		}
	    }
	    // Note: the following semantics are changed in the recent version of JavaScript.
	    // Thus, we ignore the semantics defined in "8.7.2 [[Put]] 7.a".
	    // if (prim)
	    // 	throw new native.TypeError("Cannot create property '"+s+"' on primitive value");

	    // 8.12.5. Step 6.
	    // not extensible can throw an exception, but it should be ignored.
	    if (th || native.Object_isExtensible(oo)) {
		descTTT.value = v;
		DefineOwnProperty(oo, s, descTTT, th);
	    }
	    return v;
	};
    else
	store_i = function store_i(o, s, v, oo, th, prim, insecure) {
	    while (o !== null) {
		// if (caller_check) {
		//     if (o === virtual_Function_prototype && (s === "caller" || s === "callee" || s === "arguments")) {
		// 	throw new native.TypeError("'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them");
		//     }
		// }
		if (native.Object_prototype_hasOwnProperty[icall](o, s)) {
		    // optimized case.
		    if (!insecure) {
			try {
			    return oo[s] = v;
			} catch(e) {
			    if (th) throw e;
			}
		    }
		    var desc = native.Object_getOwnPropertyDescriptor(o, s);
		    if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) {
			if (desc.writable) {
			    // 15.4.5.1 [[DefineOwnProperty]](P, Desc, Throw)
			    descValue.value = v;
			    descValue.writable = true;
			    descValue.enumerable = desc.enumerable;
			    descValue.configurable = desc.configurable;
			    DefineOwnProperty(oo, s, descValue, th);
			    return v;
			} else if (th) {
			    throw new native.TypeError("Cannot assign to read only property '"+s+"' of object '"+ToStringForError(o)+"'");
			}
		    } else {
			if (desc.set !== undefined) {
			    Call(desc.set, oo, [v]);
			    return v;
			} else if (th) {
			    throw new native.TypeError("Cannot set property '"+s+"' of '"+ToStringForError(o)+"' which has only a getter");
			}
		    }
		    // 8.12.5. Step 1.b.
		    return v;
		} else {
		    // a specialized version of the virtual "getPrototypeOf".
		    var t = search(m_virt_proto, o, false);
		    if (t === null) {
			o = native.Object_getPrototypeOf(o);
			t = search(m_org_virt, o, false);
			if (t !== null) {
			    o = t;
			    insecure = true;
			}
		    } else {
			o = t;
			insecure = true;
		    }
		}
	    }
	    // Note: the following semantics are changed in the recent version of JavaScript.
	    // Thus, we ignore the semantics defined in "8.7.2 [[Put]] 7.a".
	    // if (prim)
	    // 	throw new native.TypeError("Cannot create property '"+s+"' on primitive value");

	    // 8.12.5. Step 6.
	    // not extensible can throw an exception, but it should be ignored.
	    if (th || native.Object_isExtensible(oo)) {
		descTTT.value = v;
		DefineOwnProperty(oo, s, descTTT, th);
	    }
	    return v;
	};
    
    function Put(o, s, v, th) {
	if (isObject(o)) {
	    return store_i(o, s, v, o, th, false);
	}
	o = ToObject(o);
	return store_i(o, s, v, o, th, true);
    };
    function Puto(o, s, v, th) {
	return store_i(o, s, v, o, th, true);
    }
    var CheckObjectCoercible = function(o, name) {
	if (o == null) {
	    throw new native.TypeError(name + " called on null or undefined")
	}
    };
    function store(o, s, v, th) {
	// CheckObjectCoercible
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot set property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    o = ToObject(o);
	}
	s = ToString(s);
	return store_i(o, s, v, o, th, true);
    };
    function storec(o, s, v, th) {
	// return o[s] = v;
	// CheckObjectCoercible
	if (o == null) {
	    s = ToStringForError(s);
	    throw new native.TypeError("Cannot set property '"+s+"' of "+o);
	} else if (typeof o !== "object" && typeof o !== "function") {
	    o = ToObject(o);
	}
	return store_i(o, s, v, o, th, true);
    };
    function loadvar(x, r) { 
	var rtn;
	if (native.Object_prototype_hasOwnProperty[icall](virtual, x)) {
	    rtn = virtual[x];
	}
	else if (r) throw new native.ReferenceError(x+" is not defined.");
	else rtn = undefined;
	return rtn;
    };
    function storevar(x, v, th) {
	if (th) {
	    if (!native.Object_prototype_hasOwnProperty[icall](virtual, x))
		throw native.ReferenceError(ToStringForError(x)+" is not defined");
	    virtual[x] = v;
	} else {
	    try { virtual[x] = v; } catch(_) {};
	}
	return v;
    };
    function deletevar(x, th) {
	try {
	    return delete virtual[x];
	} catch(e) {
	    if (th) throw e;
	    return false;
	}
    };
    function initIterator(o, a, v) {
	if (a === undefined) a = [];
	if (o == null) return a;

	o = ToObject(o);
	if (v === undefined) v = native.Object_create(null);
	var m, n = native.Object_keys(o);
	var vv = native.Object_getOwnPropertyNames(o);
	var i;
	for (i=0; i<n.length; i++) {
	    m = n[i];
	    if (!(m in v)) {
		descFFF.value = m;
		native.Object_defineProperty(a, a.length, descFFF);
	    }
	}
	for (i=0; i<vv.length; i++) v[vv[i]] = true;
	return initIterator(virtual_Object_getPrototypeOf(o), a, v);
    };
    
    function unary(o, op) {
	if (op === "+")
	    return +ToNumber(o);
	else if (op === "-")
	    return -ToNumber(o);
	else if (op === "~")
	    return ~ToNumber(o);
	throw new native.TypeError("unknown unary operator: "+op);
    };
    function uplusv(o) {
	return +ToNumber(o);
    };
    var uminusv = function(o) {
	return -ToNumber(o);
    };
    var unotv = function(o) {
	return ~ToNumber(o);
    };
    function ToPrimitive_plus(o) {
	var to = typeof o;
	if (isNotObject(o)) return o;
	if (virtual_Object_getPrototypeOf(o) === virtual.Date.prototype) return ToPrimitive_S(o);

	return ToPrimitive_N(o);
    };
    // 11.6. Additive Operators
    function plusvv(lhs, rhs) {
	lhs = ToPrimitive_plus(lhs);
	rhs = ToPrimitive_plus(rhs);
	if (typeof lhs === "string" || typeof rhs === "string")
	    return ToString(lhs) + ToString(rhs);
	return lhs + rhs;
    }
    var plusvn = function(lhs, rhs) {
	lhs = ToPrimitive_plus(lhs);
	if (typeof lhs === "string")
	    return ToString(lhs) + ("" + rhs);
	return lhs + rhs;
    }
    var plusnv = function(lhs, rhs) {
	rhs = ToPrimitive_plus(rhs);
	if (rhs === "string")
	    return ("" + lhs) + ToString(rhs);
	return lhs + rhs;
    }
    var minusvv = function(lhs, rhs) {
	return ToNumber(lhs) - ToNumber(rhs);
    }
    var minusvn = function(lhs, rhs) {
	return ToNumber(lhs) - rhs;
    }
    var minusnv = function(lhs, rhs) {
	return lhs - ToNumber(rhs);
    }
    // 11.5. Multiplicative Operators
    var multvv = function(lhs, rhs) {
	return ToNumber(lhs) * ToNumber(rhs);
    }
    var multvn = function(lhs, rhs) {
	return ToNumber(lhs) * rhs;
    }
    var multnv = function(lhs, rhs) {
	return lhs * ToNumber(rhs);
    }
    var divvv = function(lhs, rhs) {
	return ToNumber(lhs) / ToNumber(rhs);
    }
    var divvn = function(lhs, rhs) {
	return ToNumber(lhs) / rhs;
    }
    var divnv = function(lhs, rhs) {
	return lhs / ToNumber(rhs);
    }
    var modvv = function(lhs, rhs) {
	return ToNumber(lhs) % ToNumber(rhs);
    }
    var modvn = function(lhs, rhs) {
	return ToNumber(lhs) % rhs;
    }
    var modnv = function(lhs, rhs) {
	return lhs % ToNumber(rhs);
    }
    // 11.10. Binary Bitwise Operators
    var bandvv = function(lhs, rhs) {
	return ToNumber(lhs) & ToNumber(rhs);
    }
    var bandvn = function(lhs, rhs) {
	return ToNumber(lhs) & rhs;
    }
    var bandnv = function(lhs, rhs) {
	return lhs & ToNumber(rhs);
    }
    var borvv = function(lhs, rhs) {
	return ToNumber(lhs) | ToNumber(rhs);
    }
    var borvn = function(lhs, rhs) {
	return ToNumber(lhs) | rhs;
    }
    var bornv = function(lhs, rhs) {
	return lhs | ToNumber(rhs);
    }
    var bpowvv = function(lhs, rhs) {
	return ToNumber(lhs) ^ ToNumber(rhs);
    }
    var bpowvn = function(lhs, rhs) {
	return ToNumber(lhs) ^ rhs;
    }
    var bpownv = function(lhs, rhs) {
	return lhs ^ ToNumber(rhs);
    }
    var ltvv = function(lhs, rhs) {
	return ToPrimitive_N(lhs) < ToPrimitive_N(rhs);
    }
    var ltvn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) < rhs;
    }
    var ltnv = function(lhs, rhs) {
	return lhs < ToPrimitive_N(rhs);
    }
    var gtvv = function(lhs, rhs) {
	return ToPrimitive_N(lhs) > ToPrimitive_N(rhs);
    }
    var gtvn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) > rhs;
    }
    var gtnv = function(lhs, rhs) {
	return lhs > ToPrimitive_N(rhs);
    }
    var ltevv = function(lhs, rhs) {
	if (typeof lhs === 'number' && typeof rhs === 'number')
	    return lhs <= rhs;
	return ToPrimitive_N(lhs) <= ToPrimitive_N(rhs);
    }
    var ltevn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) <= rhs;
    }
    var ltenv = function(lhs, rhs) {
	return lhs <= ToPrimitive_N(rhs);
    }
    var gtevv = function(lhs, rhs) {
	return ToPrimitive_N(lhs) >= ToPrimitive_N(rhs);
    }
    var gtevn = function(lhs, rhs) {
	return ToPrimitive_N(lhs) >= rhs;
    }
    var gtenv = function(lhs, rhs) {
	return lhs >= ToPrimitive_N(rhs);
    }
    // 11.7. Bitwise Shift Operators
    var rshiftvv = function(lhs, rhs) {
	return ToNumber(lhs) >> ToNumber(rhs);
    }
    var rshiftvn = function(lhs, rhs) {
	return ToNumber(lhs) >> rhs;
    }
    var rshiftnv = function(lhs, rhs) {
	return lhs >> ToNumber(rhs);
    }
    var rushiftvv = function(lhs, rhs) {
	return ToNumber(lhs) >>> ToNumber(rhs);
    }
    var rushiftvn = function(lhs, rhs) {
	return ToNumber(lhs) >>> rhs;
    }
    var rushiftnv = function(lhs, rhs) {
	return lhs >>> ToNumber(rhs);
    }
    var lshiftvv = function(lhs, rhs) {
	return ToNumber(lhs) << ToNumber(rhs);
    }
    var lshiftvn = function(lhs, rhs) {
	return ToNumber(lhs) << rhs;
    }
    var lshiftnv = function(lhs, rhs) {
	return lhs << ToNumber(rhs);
    }
    // 11.9.1 The Equals Operator
    var equalvv = function(lhs, rhs) {
	var t1, t2;
	t1 = typeof lhs;
	t2 = typeof rhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs == rhs
    }
    var equalvn = function(lhs, rhs) {
	var t2;
	t2 = typeof rhs;
	if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs == rhs
    }
    var equalnv = function(lhs, rhs) {
	var t1;
	t1 = typeof lhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	return lhs == rhs
    }
    // 11.9.2. The Does-not-equals Operator
    var nequalvv = function(lhs, rhs) {
	var t1, t2;
	t1 = typeof lhs;
	t2 = typeof rhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs != rhs
    }
    var nequalvn = function(lhs, rhs) {
	var t2;
	t2 = typeof rhs;
	if ((t2 === "string" || t2 === "number" ) && isObject(lhs))
	    lhs = ToPrimitive_N(lhs);
	else if (t2 === "boolean")
	    lhs = ToNumber(lhs);
	return lhs != rhs
    }
    var nequalnv = function(lhs, rhs) {
	var t1;
	t1 = typeof lhs;
	if ((t1 === "string" || t1 === "number" ) && isObject(rhs))
	    rhs = ToPrimitive_N(rhs);
	else if (t1 === "boolean")
	    rhs = ToNumber(rhs);
	return lhs != rhs;
    }
    var instanceofvv = function (lhs, rhs) {
	if (isNotObject(rhs))
	    throw new native.TypeError("Right-hand side of 'instanceof' is not an object");
	if (typeof rhs !== "function")
	    throw new native.TypeError("Right-hand side of 'instanceof' is not callable");
	// [[HasInstance]]
	var F = rhs;
	var V = lhs;
	if (isNotObject(V))
	    return false;
	var O = Geto(F, "prototype")
	if (isNotObject(O))
	    throw new native.TypeError();
	do {
	    V = virtual_Object_getPrototypeOf(V);
	    if (O === V) return true;
	} while (V !== null)
	return false;
    };
    var invv = function (lhs, rhs) {
	if (isNotObject(rhs)) throw new native.TypeError("Cannot use 'in' operator to search for "+ToStringForError(lhs)+" in "+ToStringForError(rhs));
	lhs = ToString(lhs);
	return HasProperty(lhs, rhs)
    };
    var innv = function (lhs, rhs) {
	if (isNotObject(rhs)) throw new native.TypeError("Cannot use 'in' operator to search for "+ToStringForError(lhs)+" in "+ToStringForError(rhs));
	return HasProperty(lhs, rhs)
    };

    var newcall;
    if (map_mode) {
	newcall = function (fn, a) {
	    // only for built-in functions: Boolean, Number, Date. No other exceptions.
	    var i, nfn;
	    nfn = native_mapget[icall](m_virt_org, fn);
	    if (nfn === undefined) nfn = fn;

	    // Exceptional cases that does not have [[Construct]] internal field.
	    if (map_contains(set_no_construct, nfn) === true) {
		throw new native.TypeError(ToStringForError(nfn)+" is not a constructor");
	    }
	    return nfn;
	};
    } else {
	newcall = function (fn, a) {
	    // only for built-in functions: Boolean, Number, Date. No other exceptions.
	    var i, nfn;
	    nfn = search(m_virt_org, fn, true);

	    // Exceptional cases that does not have [[Construct]] internal field.
	    if (map_contains(set_no_construct, nfn) === true) {
		throw new native.TypeError(ToStringForError(nfn)+" is not a constructor");
	    }
	    return nfn;
	};
    }
    var preop = function (x, op) {
	if (op === "++") {
	    return storevar(x, ToNumber(loadvar(x, true)) + 1)
	} else if (op === "--") {
	    return storevar(x, ToNumber(loadvar(x, true)) - 1)
	}
    };
    var postop = function (x, op) {
	var t;
	if (op === "++") {
	    t = ToNumber(loadvar(x, true));
	    storevar(x, t + 1);
	    return t;
	} else if (op === "--") {
	    t = ToNumber(loadvar(x, true));
	    storevar(x, t - 1);
	    return t;
	}
    };
    var mpre_plus = function (o, x, th) {
	return Put(o, x, ToNumber(GetValue(o, x)) + 1, th);
    };
    var mpre_minus = function (o, x, th) {
	return Put(o, x, ToNumber(GetValue(o, x)) - 1, th);
    };
    var mpost_plus = function (o, x, th) {
	var t;
	t = ToNumber(GetValue(o, x));
	Put(o, x, t + 1, th);
	return t;
    };
    var mpost_minus = function (o, x, th) {
	var t;
	t = ToNumber(GetValue(o, x));
	Put(o, x, t - 1, th);
	return t;
    };
    function ToBoolean(o) {
	return Boolean(o);
    };
    function ToNumber(o) {
	if (typeof o === 'object' || typeof o === 'function')
	    o = ToPrimitive_N(o);
	return +o;
    };
    function ToString(o) {
	if (typeof o === 'object' || typeof o === 'function')
	    o = ToPrimitive_S(o);
	return ""+o;
    };
    var Export = function (o) {
	var v, i, names;
	o = o.exports;
	if (node_mode) {
	    module.exports = H2L(o);
	} else if (typeof _global_.define === 'function' && _global_.define.amd) {
	    _global_.define(function () { return H2L(o); });
	} else {
	    if (isNotObject(o)) return;
	    names = native.Object_keys(o)
	    for (i=0; i<names.length; i++) {
		v = names[i];
		if (isObject(o[v])) {
		    native.Object_defineProperty(native.global, v, sDesc({value: H2L(o[v]), configurable: true, enumerable: true, writable: true}));
		}
	    }
	}
    };

    var getLocalPropertyNames = function pp(o, s, p) {
	if (o == null) return p;
	// - requirement: all the objects on its prototype chain must be sealed.
	var desc, n, i, names = native.Object_getOwnPropertyNames(o);
	for (i=0; i<names.length; i++) {
	    n = names[i];
	    desc = native.Object_getOwnPropertyDescriptor(o, n);
	    try { native.Object_defineProperty(o, n, sDesc(desc)); } catch(_) { }
	}

	if (s === undefined) {
	    // TODO could be better by object with an empty proto chain
	    s = [];
	    p = [];
	}
	var n, i, names = native.Object_getOwnPropertyNames(o);
	for (i=0; i<names.length; i++) {
	    n = names[i];
	    if (native.call[icall](native.indexOf, s, n) < 0) {
		DefineOwnProperty(s, ToString(s.length), sDesc({value: n, enumerable: false, configurable: true}));
		DefineOwnProperty(p, ToString(p.length), sDesc({value: {obj: o, name: n}, enumerable: false, configurable: true}));
	    }
	}
	return p;
    };
    var safeReadExc = function(e) {
	var desc = native.Object_getOwnPropertyDescriptor(e, "message");
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) return desc.value;
	else return "";
    }
    var safeException = function(e) {
	if (isNotObject(e)) return e;
	else if (test) return e;
	else {
	    var ne;
	    var msg;
	    if (e instanceof native.TypeError) ne = new native.TypeError(safeReadExc(e));
	    else if (e instanceof native.RangeError) ne = new native.RangeError(safeReadExc(e));
	    else if (e instanceof native.SyntaxError) ne = new native.SyntaxError(safeReadExc(e));
	    else if (e instanceof native.ReferenceError) ne = new native.ReferenceError(safeReadExc(e));
	    else if (e instanceof native.URIError) ne = new native.URIError(safeReadExc(e));
	    else if (e instanceof native.EvalError) ne = new native.EvalError(safeReadExc(e));
	    else if (e instanceof native.Error) ne = new native.Error(safeReadExc(e));
	    else if (e instanceof virtual_TypeError) ne = new native.TypeError(safeReadExc(e));
	    else if (e instanceof virtual_RangeError) ne = new native.RangeError(safeReadExc(e));
	    else if (e instanceof virtual_SyntaxError) ne = new native.SyntaxError(safeReadExc(e));
	    else if (e instanceof virtual_ReferenceError) ne = new native.ReferenceError(safeReadExc(e));
	    else if (e instanceof virtual_URIError) ne = new native.URIError(safeReadExc(e));
	    else if (e instanceof virtual_EvalError) ne = new native.EvalError(safeReadExc(e));
	    else if (e instanceof virtual_Error) ne = new native.Error(safeReadExc(e));
	    else ne = new native.Error("unknown exception object");
	    var desc = native.Object_getOwnPropertyDescriptor(ne, "stack");
	    if (desc !== undefined) {
		try { delete ne.stack; }
		catch(_) { return safeReadExc(ne); }
	    }
	    return ne;
	}
    };
    var safeload_ = function(o, n, w, w_r) {
	var desc = native.Object_getOwnPropertyDescriptor(o, n);
	if (desc === undefined)
	    return undefined;
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) {
	    desc.value = w(desc.value);
	    return desc;
	}
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'get') && typeof desc.get === 'function') {
	    var get = desc.get;
	    desc.get = function() { "use strict"; try { return w(native.apply[icall](desc.get, w_r(this), w_r(arguments))); } catch (e) { throw safeException(e); } };
	}
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'set') && typeof desc.set === 'function') {
	    var set = desc.set;
	    desc.set = function() { "use strict"; try { return w(native.apply[icall](desc.set, w_r(this), w_r(arguments))); } catch (e) { throw safeException(e); } };
	}
	
	return desc;
    }
    var safeload = function(o, n, w, w_r) {
	var desc = native.Object_getOwnPropertyDescriptor(o, n);
	if (desc === undefined)
	    return undefined;
	if (native.Object_prototype_hasOwnProperty[icall](desc, 'value')) return w(desc.value);
	else if (native.Object_prototype_hasOwnProperty[icall](desc, 'get') && typeof desc.get === 'function') {
	    var get = desc.get;
	    get = function() { "use strict"; try { return native.apply[icall](desc.get, w_r(this), w_r(arguments)); } catch (e) { throw safeException(e); } };
	    return w(native.call[icall](get, o));
	}
	return undefined;
    };
    var isDate = function(o) {
	try {
	    o && typeof o === 'object' && native.apply[icall](native.Date_prototype_getTime, o);
	    return true;
	} catch(_) { return false; }
    };
    function isRegExp(argument) { // eslint-disable-line no-unused-vars
	// 1. If Type(argument) is not Object, return false.
	if (typeof argument !== 'object') {
	    return false;
	}
	// 2. Let matcher be ? Get(argument, @@match).
	var matcher = use_symbol && 'match' in Symbol ? Get(argument, native.s_symbol_match) : undefined;
	// 3. If matcher is not undefined, return ToBoolean(matcher).
	if (matcher !== undefined) {
	    return ToBoolean(matcher);
	}
	// 4. If argument does not have lastIndex, return false.
	if (!native.Object_prototype_hasOwnProperty[icall](argument, 'lastIndex'))
	    return false;
	// 5. If argument has a [[RegExpMatcher]] internal slot, return true.
	try {
	    var lastIndex = argument.lastIndex;
	    argument.lastIndex = 0;
	    RegExp.prototype.exec.call(argument);
	    return true;
	    // eslint-disable-next-line no-empty
	} catch (e) {} finally {
	    argument.lastIndex = lastIndex;
	}
	// 6. Return false.
	return false;
    }
    var H2L_proto = function(o) {
	if (typeof o === "function") {
	    var cache = L2H_map(o);
	    if (cache !== null) return cache;

	    return (function() {
		var f = o;
		var nf = function () {
		    "use strict";
		    var _this_ = L2H(this);
		    var args = L2H(arguments);
		    try { 
			return H2L(native.apply[icall](f, _this_, args));
		    } catch (e) {
			throw safeException(e);
		    }
		};
		H2L_reg(o, nf);
		var new_proto = H2L(o.prototype);
		nf.prototype = new_proto;
		return nf;
	    })();
	} else if (native.Array_isArray(o)) return [];
	else if (isDate(o)) return new native.Date(o);
	else if (isRegExp(o)) return new native.RegExp(o);
	var t = virtual_Object_getPrototypeOf(o);
	var h = search(m_H2L, t, false);
	if (h === null) {
	    if (map_mode) {
		h = native_mapget[icall](m_virt_org, t);
		if (h === undefined) h = null;
	    } else {
		h = search(m_virt_org, t, false);
	    }
	}
	// TODO
	if (t === virtual.Object.prototype) h = native.Object_prototype;
	return native.Object_create(h);
    }
    var L2H_proto = function(o) {
	if (typeof o === 'function') {
	    return (function() {
		var f = o;
		var nf = function() {
		    "use strict";
		    var _this_ = H2L(this);
		    var args = H2L(arguments);
		    try { 
			return L2H(native.apply[icall](f, _this_, args));
		    } catch (e) {
			throw safeException(e);
		    }
		};
		L2H_reg(o, nf);
		var new_proto = L2H(o.prototype);
		nf.prototype = new_proto;
		return nf;
	    })();
	} else if (native.Array_isArray(o)) return [];
	else if (isDate(o)) return new native.Date(o);
	else if (isRegExp(o)) return new native.RegExp(o);

	var t = native.Object_getPrototypeOf(o);
	var h = search(m_L2H, t, false);
	if (h === null) {
	    if (map_mode) {
		h = native_mapget[icall](m_org_virt, t);
		if (h === undefined) h = null;
	    }
	    else h = search(m_org_virt, t, false);
	}
	return native.Object_create(h);
    }
    var H2L_map = function(o) {
	return search(m_H2L, o, false);
    }
    var L2H_map = function(o) {
	return search(m_L2H, o, false);
    }
    var deepcopy = function(o, wrap, proto, map, reg, wrap_r, proto_r, map_r, reg_r) {
	if (isNotObject(o)) return o;
	var cache = map(o);
	if (cache !== null) return cache;
	var proxy = proto(o);
	reg(o, proxy);
	var g, s, i; // , names = getLocalPropertyNames(o);
	var names = native.Object_getOwnPropertyNames(o)
	var original_ = o;
	for (i=0; i<names.length; i++) {
	    // var original_ = names[i].obj;
	    var name_ = names[i];
	    var desc_ = native.Object_getOwnPropertyDescriptor(original_, name_);
	    // TODO
	    if (false && typeof desc_.value === 'function') {
		try {
		    desc_.value = wrap(desc_.value);
		    native.Object_defineProperty(proxy, names[i], desc_);
		} catch(_) {}
	    } else {
		if (native.Object_prototype_hasOwnProperty[icall](desc_, 'value')) {
		    desc_.value = wrap(desc_.value);
		} else {
		    if (native.Object_prototype_hasOwnProperty[icall](desc_, 'get') && typeof desc_.get === 'function') {
			desc_.get = wrap(desc_.get);
		    }
		    if (native.Object_prototype_hasOwnProperty[icall](desc_, 'set') && typeof desc_.set === 'function') {
			desc_.set = wrap(desc_.set);
		    }
		}
		try {
		    native.Object_defineProperty(proxy, name_, desc_);
		} catch(_) {}

		// * lazy deep copy algorithm.
		// g = (function() {
		//     "use strict";
	    	//     var original = original_;
	    	//     var name = name_;
	    	//     var desc = desc_;
	    	//     return function() {
	    	// 	// 1.1. if the field value is primitive, just copies it.
	    	// 	// 1.2. if the field value is an object, apply the lazy deep copy to the object.
	    	// 	var v = safeload_(original, name, wrap, wrap_r);
		// 	if (v === undefined) {
		// 	    delete this[name];
		// 	    return undefined;
		// 	}
		// 	try {
	    	// 	    native.Object_defineProperty(this, name, v);
		// 	} catch(_) {}
		// 	if (native.Object_prototype_hasOwnProperty[icall](v, 'get') && typeof v.get === 'function') {
		// 	    return Call(v.get, this);
		// 	} else {
		// 	    return v.value;
		// 	}
	    	// 	var e = desc.enumerable;
		// 	try {
	    	// 	    native.Object_defineProperty(this, name, {value: v, enumerable: e, configurable: true, writable: true});
		// 	} catch(_) {}
	    	// 	return v;
	    	//     };
		// })();
		// s = (function() {
		//     "use strict";
	    	//     var original = original_;
	    	//     var name = name_;
	    	//     var desc = desc_;
		//     // safe wrapping
		//     if (native.Object_prototype_hasOwnProperty[icall](desc, 'set')) {
		// 	var o = desc.set;
		// 	desc.set = function() { "use strict"; try { return wrap(native.apply[icall](o, wrap_r(this), wrap_r(arguments))); } catch (e) { throw safeException(e); } };
		//     }
	    	//     return function(v) {
	    	// 	var e = desc.enumerable;
		// 	if (native.Object_prototype_hasOwnProperty[icall](desc, 'set')) {
		// 	    Call(desc.set, this, [v]);
		// 	} else {
		// 	    // TODO: defineOwnProperty of Array object.
		// 	    try {
	    	// 		native.Object_defineProperty(this, name, {value: v, enumerable: e, configurable: true, writable: true});
		// 	    } catch(_) {}
		// 	}
	    	// 	return v;
	    	//     };
		// })();
		// registerOPT(trusted_fn, g, "");
		// registerOPT(trusted_fn, s, "");
		// try {
		//     native.Object_defineProperty(proxy, names[i], {get: g, set: s, enumerable: desc_.enumerable, configurable: true});
		// } catch(_) {}
	    }
	}
	// 2. returns the proxy object.
	return proxy;
    }
    var L2H = function(o) {
	if (isNotObject(o)) return o;
	if (o === native.global) {
	    var e = new Error("global location is passed.");
	    log(e.stack);
	    return null;
	} else if (typeof o === "object" && Class(o) === "Arguments") {
	    var i, arg = {length: o.length};
	    for (i=0; i<o.length; i++) {
		arg[i] = L2H(o[i]);
	    }
	    return arg;
	}

	return deepcopy(o, L2H, L2H_proto, L2H_map, L2H_reg, H2L, H2L_proto, H2L_map, H2L_reg);
    };
    var assert = function(c, msg) {
	if (!c)
	    console.log(msg);
    }
    var H2L_reg = function(o1, o2) {
	register(m_H2L, o1, o2);
	register(m_L2H, o2, o1);
	assert(m_H2L.value[o1['@H2L']] === o2, "what?");
	assert(m_L2H.value[o2['@L2H']] === o1, "3");
    }
    var L2H_reg = function(o1, o2) {
	register(m_L2H, o1, o2);
	register(m_H2L, o2, o1);
	assert(m_H2L.value[o2['@H2L']] === o1, "3");
	assert(m_L2H.value[o1['@L2H']] === o2, "4");
    }
    var H2L = function H2L(o) {
	if (isNotObject(o)) return o;
	if (o === virtual) {
	    var e = new Error("global location is passed.");
	    log(e.stack);
	    return null;
	} else if (typeof o === "object" && Class(o) === "Arguments") {
	    var i, arg = {length: o.length};
	    for (i=0; i<o.length; i++) {
		arg[i] = H2L(o[i]);
	    }
	    return arg;
	}

	return deepcopy(o, H2L, H2L_proto, H2L_map, H2L_reg, L2H, L2H_proto, L2H_map, L2H_reg);
    }
    function isNotObject(o) {
	return o === null || (typeof o !== 'object' && typeof o !== 'function');
    }
    function isObject(o) {
	return o && (typeof o === 'object' || typeof o === 'function');
    }

    function HasProperty(x, o) {
	while (o !== null) {
	    if (native.Object_prototype_hasOwnProperty[icall](o, x)) {
		return true;
	    }
	    o = virtual_Object_getPrototypeOf(o);
	}
	return false;
    }
    var ToInteger = function(number) {
	var sign, abs;
	number = ToNumber(number);
	if (native.isNaN(number)) return 0;
	if (!native.isFinite(number)) return number;
	// sign(number) x floor(abs(number))
	abs = native.Math_abs(number);
	sign = (number === abs)? sign = 1 : -1;
	return sign * native.Math_floor(abs);
    }

    // The original semantics
    var ToUint32 = function(o) {
	return ToNumber(o) >>> 0;
    }
    var ToInt32 = function(o) {
	return ToNumber(o) >> 0;
    }

    function IsCallable(f) { return typeof f === "function"; };
    function ToObject(v, name) {
	if (v == null) {
	    var exc;
	    if (typeof name === 'string') exc = name + " called on null or undefined"
	    else exc = 'Cannot convert undefined or null to object';
	    throw new native.TypeError(exc);
	}
	if (typeof v === "boolean") return new native.Boolean(v);
	else if (typeof v === "number") return new native.Number(v);
	else if (typeof v === "string") return new native.String(v);
	else {
	    return v;
	}
    };
    // ToPrimitive with hint String
    function ToPrimitive_S(o) {
	var f, r;
	if (isNotObject(o)) return o;
	f = Geto(o, "toString");
	if (IsCallable(f)) {
            r = Call(f, o, []);
	    if (isNotObject(r)) return r;
	}
	f = Geto(o, "valueOf");
	if (IsCallable(f)) {
            r = Call(f, o, []);
	    if (isNotObject(r)) return r;
	}
	throw new native.TypeError ("Cannot convert object to primitive value");
    };
    // ToPrimitive with hint Number
    var ToPrimitive_N = function (o) {
	var f, r;
	if (isNotObject(o)) return o;
	f = Geto(o, "valueOf");
	if (IsCallable(f)) {
            r = Call(f, o, []);
	    if (isNotObject(r)) return r;
	}
	f = Geto(o, "toString");
	if (IsCallable(f)) {
            r = Call(f, o, []);
	    if (isNotObject(r)) return r;
	}
	throw new native.TypeError ("Cannot convert object to primitive value");
    };
    var iindexOf = makeI('indexOf');

    var register = function(a, n, t, t2) {
	var searchKey = a.name;
	var i;
	i = a.key[iindexOf](n)

	if (i < 0) {
	    i = a.key.length;
	    descTTT.value = n;
	    native.Object_defineProperty(a.key, i, descTTT);
	    if (native.Object_prototype_hasOwnProperty[icall](a, 'value')) {
		descTTT.value = t;
		native.Object_defineProperty(a.value, i, descTTT);
	    }
	    // just for optimization
	    if (typeof searchKey === "string") {
	    	try {
		    descFFF.value = i;
	    	    native.Object_defineProperty(n, searchKey, descFFF);
	    	} catch (_) {}
	    }
	}
    };
    var registerOPT = function(a, n, t, t2) {
	var searchKey = a.name;
	var i = a.key.length;
	descTTT.value = n;
	native.Object_defineProperty(a.key, i, descTTT);
	if (native.Object_prototype_hasOwnProperty[icall](a, 'value')) {
	    descTTT.value = t;
	    native.Object_defineProperty(a.value, i, descTTT);
	}
	// just for optimization
	if (typeof searchKey === "string") {
	    try {
		descFFF.value = i;
	    	native.Object_defineProperty(n, searchKey, descFFF);
	    } catch (_) {}
	}
    };
    var registerOPT2 = function(a, n, t) {
	var searchKey = a.name;
	var i = a.key.length;
	descTTT.value = n;
	native.Object_defineProperty(a.key, i, descTTT);
	descTTT.value = t;
	native.Object_defineProperty(a.value, i, descTTT);

	// just for optimization
	try {
	    descFFF.value = i;
	    native.Object_defineProperty(n, searchKey, descFFF);
	} catch (_) {}
    };
					  
    function search_org(n) {
	if (n == null) return null;
	var t = native_mapget[icall](m_org_virt, n);
	if (t === undefined) return n;
	else return t;
    }

    function search(a, n, b) {
	if (n == null) return null;
	var to = typeof n;
	if (to !== "object" && to !== "function") {
	    if (b) return n;
	    else return null;
	}
	var j, i, searchKey = a.name;
	j = native.Object_getOwnPropertyDescriptor(n, searchKey);
	if (j !== undefined && native.Object_prototype_hasOwnProperty[icall](j, 'value') &&
	    typeof j.value === 'number' && j.value < a.key.length) {
	    i = j.value;
	    if (a.key[i] !== n) i = -1; // i = a.key[iindexOf](n);
	} else i = -1;// else i = a.key[iindexOf](n);

	if (i >= 0) return a['value'][i];
	else if (b) return n;
	else return null;
    };
    function map_contains(a, n) {
	if (n == null) return null;
	var to = typeof n;
	if (to !== "object" && to !== "function") return false;
	var j, i, searchKey = a.name;
	j = native.Object_getOwnPropertyDescriptor(n, searchKey);
	if (j !== undefined && native.Object_prototype_hasOwnProperty[icall](j, 'value') &&
	    typeof j.value === 'number' && j.value < a.key.length) {
	    i = j.value;
	    if (a.key[i] !== n) i = -1;
	} else return false;

	return i >= 0;
    };
    function register_function(F, name, noConstruct, i, user, danger) {
	var n;
	if (i === undefined) i = 0;

	if (name === undefined) name = " "; else name = " "+name;
	newfn(F, name);
	try { 
	    descFFF.value = i;
	    native.Object.defineProperty(F, "length", descFFF);
	} catch (_) { }
	if (noConstruct) {
	    register(set_no_construct, F);
	    // should be removed. Can we exploit 'Function.prototype.bind' to create a function without prototype field?
	    F.prototype = undefined;
	}

	return F;
    };
    var copy_desc = function(dest, org, name) {
	var desc = native.Object.getOwnPropertyDescriptor(org, name);
	if (desc === undefined) {
	    warn("* Warning: the field '"+name+"' is missing.");
	    return false;
	}
	if (native.Object_prototype_hasOwnProperty.call(desc, "set") && desc.set !== undefined)
	    desc.set = register_function(desc.set, name+" set", true, 0);
	if (native.Object_prototype_hasOwnProperty.call(desc, "get") && desc.get !== undefined)
	    desc.get = register_function(desc.get, name+" get", true, 0);
	try {
	    native.Object.defineProperty(dest, name, sDesc(desc));
	    return true;
	} catch(_) { warn("fails to update "+name); }
	return false;
    };
    
    var simpleFn = function(lhs, rhs, name, alen, noConstruct) {
	var fn = (function() {
	    var o = rhs[name];
	    var f;
	    if (alen === 0 || alen === undefined) {
		f = function() {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 1) {
		f = function(v1) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 2) {
		f = function(v1, v2) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 3) {
		f = function(v1, v2, v3) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 4) {
		f = function(v1, v2, v3, v4) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 5) {
		f = function(v1, v2, v3, v4, v5) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 6) {
		f = function(v1, v2, v3, v4, v5, v6) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 7) {
		f = function(v1, v2, v3, v4, v5, v6, v7) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 8) {
		f = function(v1, v2, v3, v4, v5, v6, v7, v8) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    }
	    return register_function(f, name, noConstruct, alen);
	})();
	var w, e, c;
	var t = native.Object.getOwnPropertyDescriptor(rhs, name);
	if (t === undefined) {
	    warn(name+' is missing.');
	} else {
	    w = t.writable;
	    e = t.enumberable;
	    c = t.configurable;
	    native.Object.defineProperty(lhs, name, sDesc({value: fn, writable: w, enumerable: e, configurable: c}));
	}
    };
    var nsimpleFn = function(lhs, rhs, name, alen, noConstruct) {
	var fn = (function() {
	    var o = rhs[name];
	    var f;
	    if (alen === 0 || alen === undefined) {
		f = function() {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 1) {
		f = function(x) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else if (alen === 2) {
		f = function(x,y) {
		    var r = native.apply[icall](o, this, arguments);
		    if (isNotObject(r)) return r;
		    return search_org(r, true);
		};
	    } else {
		throw "an error of our bootstrapping code";
	    }
	    return register_function(f, name, noConstruct, alen);
	})();
	var w, e, c;
	var t = native.Object.getOwnPropertyDescriptor(rhs, name);
	if (t === undefined) {
	    warn(name+' is missing.');
	} else {
	    w = t.writable;
	    e = t.enumberable;
	    c = t.configurable;
	    native.Object.defineProperty(lhs, name, sDesc({value: fn, writable: w, enumerable: e, configurable: c}));
	}
    };
    var eFn = function(o, name, w, e, c, fn) {
	native.Object.defineProperty(o, name, sDesc({value: fn, writable: w, enumerable: e, configurable: c}));
    };

    // Overriding the semantics of native built-in functions.
    var native_Function_prototype_toString = native.Function.prototype.toString;
    (function() {
	Function.prototype.toString = (function() {
	    var F = function() {
		if (typeof this !== 'function')
		    throw new native.TypeError("Function.prototype.toString requires that 'this' be a Function");
		return "function () { [protected code] }";
		// // var name = search(trusted_fn, this, false);
		// // if (name !== null) return "function"+name+"() { [defensive code] }"; else
		// if (this === F) return "function toString() { [native code] }";
		// else
		//     return native.apply[icall](native_Function_prototype_toString, this, arguments);
	    };
	    return F;
	})();
    })();
    if (DOM_mode) {
	// 
	(function() {
	    var n, i, lst = ["open", "showModalDialog", "showModelessDialog", "openDialog"];
	    for (i=0; i<lst.length; i++) {
		n = lst[i];
		if (_global_.window[n] !== undefined) {
		    (function() {
			var o = _global_.window[n];
			var f = function() {
			    native.apply[icall](o, this, arguments);
			    return null;
			};
			_global_.window[n] = register_function(f);
			if (_global_.window[n] !== f) {
			    warn("fails to restrict the function 'window."+n+"'");
			} else {
			    info("'window."+n+"' has been restricted.");
			}
		    })();
		}
	    }
	})();
    }
    
    // non-standard object 'console'
    virtual.console = {};
    virtual.console.log = register_function(log);
    virtual.console.warn = register_function(warn);
    
    // Virtual environment.
    // Auxiliary functions
    var ToStringWrapper1 = function(o, name, fn) {
	var f = function (s) {
	    s = ToString(s);
	    return native.call[icall](fn, this, s);
	};
	f = register_function(f, name, true, 1);
	eFn(o, name, true, false, true, f);
    };
    var ToNumberWrapper1 = function(o, name, fn) {
	var f = function (s) {
	    s = ToNumber(s);
	    return native.call[icall](fn, this, s);
	};
	f = register_function(f, name, true, 1);
	eFn(o, name, true, false, true, f);
    };
    var ToNumberWrapperN = function(o, name, fn, n1, n2) {
	var f;
	if (n2 === 0 || n2 === undefined) {
	    f  = function () {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native.apply[icall](fn, this, arguments);
	    };
	} else if (n2 === 1) {
	    f  = function (v1) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native.apply[icall](fn, this, arguments);
	    };
	} else if (n2 === 2) {
	    f  = function (v1, v2) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native.apply[icall](fn, this, arguments);
	    };
	} else if (n2 === 3) {
	    f  = function (v1, v2, v3) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native.apply[icall](fn, this, arguments);
	    };
	} else if (n2 === 4) {
	    f  = function (v1, v2, v3, v4) {
		var i;
		for (i=0; i<n1; i++) {
		    arguments[i] = ToNumber(arguments[i]);
	        }
	        for (i=n1; i<n2; i++) {
		    if (arguments.length > i)
		        arguments[i] = ToNumber(arguments[i]);
	        }
	        return native.apply[icall](fn, this, arguments);
	    };
	} else throw "TODO"
	f = register_function(f, name, true, n2);
	eFn(o, name, true, false, true, f);
    };
    var MathWrapper = function(o, name, fn) {
	var f = function (v1) {
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToNumber(arguments[i]);
	    }
	    return native.apply[icall](fn, this, arguments);
	};
	f = register_function(f, name, true, fn.length);
	eFn(o, name, true, false, true, f);
    };
    var MathWrapper2 = function(o, name, fn) {
	var f = function (v1, v2) {
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToNumber(arguments[i]);
	    }
	    return native.apply[icall](fn, this, arguments);
	};
	f = register_function(f, name, true, fn.length);
	eFn(o, name, true, false, true, f);
    };
    var ToObjectWrapper0 = function(o, name, fn) {
	var f = function () {
	    var this_ = ToObject(this)
	    return native.call[icall](fn, this_);
	};
	f = register_function(f, name, true, 0);
	eFn(o, name, true, false, true, f);
    };
    var ToPropertyDescriptor = function(desc) {
	var ndesc;
	if (isNotObject(desc)) ndesc = desc;
	else {
    	    var i, name, arr = ["value", "get", "set", "writable", "enumerable", "configurable"];
	    ndesc = native.Object_create(null);
    	    for (i=0; i<arr.length; i++) {
    		name = arr[i];
    		if (HasProperty(name, desc)) {
    		    ndesc[name] = Geto(desc, name);
    		}
    	    }
	}
	return ndesc;
    };
    var GetCompatibleRegExp = function(o, name) {
	if (Class(o) !==  'RegExp') throw new native.TypeError("Method "+name+" called on incompatible receiver "+ToStringForError(o));
	return o;
    }
    var GetPropertyDescriptor = function GetPropertyDescriptor(o, p) {
	if (o === null) return undefined;
	var desc = native.Object_getOwnPropertyDescriptor(o, p);
	if (desc !== undefined)
	    return desc;
	return GetPropertyDescriptor(virtual_Object_getPrototypeOf(o), p);
    };
    var CreateRegExp = function (o, name, gets, bReplace, bSearch, bSplit) {
	o = GetCompatibleRegExp(o, name);
	var desc, i, P, n = new native.RegExp(o);
	Put(n, 'lastIndex', Geto(o, 'lastIndex'));
	for (i=0; i<gets.length; i++) {
	    P = gets[i];
	    desc = GetPropertyDescriptor(o, P);
	    if (desc !== undefined) {
		native.Object_defineProperty(n, P, desc);
	    }
	}
	if (use_symbol) {
	    if (bReplace) {
		descTTT.value = native.symbol_replace;
		DefineOwnProperty(n, Symbol.replace, descTTT, false);
	    }
	    if (bSearch) {
		descTTT.value = native.symbol_search;
		DefineOwnProperty(n, Symbol.search, descTTT, false);
	    }
	    if (bSplit) {
		descTTT.value = native.symbol_split;
		DefineOwnProperty(n, Symbol.split, descTTT, false);
	    }
	}
	return n;
    };
    var extend = function (A, B, fields) {
	var nat = native.global[A];
    	var fn = register_function(function() { return native.apply[icall](nat, this, arguments); }, A, false, 1);
    	native.Object_defineProperty(fn, 'prototype', {value: Object.create(virtual[B].prototype), enumerable: false, writable: false, configurable: false});
    	native.Object_defineProperty(fn.prototype, 'constructor', {value: fn, enumerable: false, writable: true, configurable: true});
	var bnat = native.Object.getPrototypeOf(nat.prototype);
	(function() {
	    var i, n
	    for (i in fields) {
		n = fields[i];
		// for hopjs
		if (!copy_desc(fn.prototype, nat.prototype, n)) {
		    copy_desc(fn.prototype, bnat, n);
		}
	    }
	})();
    	native.Object_defineProperty(virtual, A, {value: fn, writable: true, configurable: true, enumerable: false});
    };

    // 15.1 The Global Object
    // 15.1.1.1 NaN
    copy_desc(virtual, native.global, "NaN");
    // 15.1.1.2 Infinity
    copy_desc(virtual, native.global, "Infinity");
    // 15.1.1.3 undefined
    copy_desc(virtual, native.global, "undefined");
    // Object.defineProperty(virtual, "undefined", sDesc({value: undefined, writable: false, enumerable: false, configurable: false}));

    // 15.1.2.1 eval(x)
    // Note: the defensive mode does not provide eval.
    virtual.eval = (function() {
    	var f = function(x) { throw new native.Error("'eval' is not allowed in defensive mode.") };
    	return register_function(f, "eval", true, 1);
    })();

    // 15.1.2.2 parseInt(string, radix)
    eFn(virtual, "parseInt", true, false, true, (function () {
	var o = parseInt;
	var f = function parseInt(string, radix) {
	    string = ToString(string);
	    radix = ToInt32(radix);
	    return native.call[icall](o, this, string, radix);
	};
	return register_function(f, "parseInt", true, 2);
    })());
    // 15.1.2.3 parseFloat(string)
    ToStringWrapper1(virtual, "parseFloat", parseFloat);
    // 15.1.2.4 isNaN(number)
    ToNumberWrapper1(virtual, "isNaN", isNaN);
    // 15.1.2.5 isFinite(number)
    ToNumberWrapper1(virtual, "isFinite", isFinite);
    // 15.1.3.1 decodeURI(encodedURI)
    ToStringWrapper1(virtual, "decodeURI", decodeURI);
    // 15.1.3.2 decodeURIComponent(encodedURIComponent)
    ToStringWrapper1(virtual, "decodeURIComponent", decodeURIComponent);
    // 15.1.3.3 encodeURI(uri)
    ToStringWrapper1(virtual, "encodeURI", encodeURI);
    // 15.1.3.4 encodeURIComponent(uriComponent)
    ToStringWrapper1(virtual, "encodeURIComponent", encodeURIComponent);
    // B.2.1 escape(string)
    ToStringWrapper1(virtual, "escape", escape);
    // B.2.2 unescape(string)
    ToStringWrapper1(virtual, "unescape", unescape);

    // Browser WebCryptoAPI (https://www.w3.org/TR/WebCryptoAPI/)
    if ("crypto" in _global_) {
	native.SubtleCrypto = _global_.SubtleCrypto;
	native.Crypto = _global_.Crypto;
	native.crypto = _global_.crypto;
	eFn(virtual, "SubtleCrypto", true, false, true, register_function(function Crypto() { throw new native.TypeError("Illegal constructor"); }, "SubtleCrypto", false, 0));
	(function() {
	    var new_proto = {};
    	    native.Object.defineProperty(virtual.SubtleCrypto, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
    	    native.Object.defineProperty(new_proto, "constructor", {value: virtual.SubtleCrypto, writable: true, enumerable: false, configurable: true});
	})();
	// TODO: Its return value has native prototype chain. We have to provide shadow objects.
	// These functions require a specific typed argument and does not carry implicit field accesses.
	// encrypt (https://www.w3.org/TR/WebCryptoAPI/#crypto-interface)
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "encrypt", 3, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "decrypt", 3, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "sign", 3, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "verify", 4, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "digest", 2, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "generateKey", 3, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "deriveKey", 5, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "deriveBits", 3, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "importKey", 5, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "exportKey", 2, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "wrapKey", 4, true);
	simpleFn(virtual.SubtleCrypto.prototype, native.SubtleCrypto.prototype, "unwrapKey", 7, true);
	
	eFn(virtual, "Crypto", true, false, true, register_function(function Crypto() { throw new native.TypeError("Illegal constructor"); }, "Crypto", false, 0));
	(function() {
	    var new_proto = {};
    	    native.Object.defineProperty(virtual.Crypto, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
    	    native.Object.defineProperty(new_proto, "constructor", {value: virtual.Crypto, writable: true, enumerable: false, configurable: true});
	})();
	// getRandomValues (https://www.w3.org/TR/WebCryptoAPI/#crypto-interface)
	eFn(virtual.Crypto.prototype, "getRandomValues", true, false, true, register_function(function (v) {
	    return native.crypto.getRandomValues(v);
	}, "getRandomValues", false, 0));

	eFn(virtual, "crypto", true, false, true, (function () {
	    return native.Object_create(virtual.Crypto.prototype);
	})());
	
	(function() {
	    var subtle = native.Object_create(virtual.SubtleCrypto.prototype);;
	    var fn = function subtle() {
		return subtle;
	    };
	    var desc = native.Object.getOwnPropertyDescriptor(native.Crypto.prototype, "subtle");
	    if (desc === undefined) {
		warn("* Warning: the field 'crypto.subtle' is missing.");
		return false;
	    }
	    desc.get = register_function(fn, "subtle", true, 0);
	    try {
		native.Object.defineProperty(virtual.Crypto.prototype, "subtle", sDesc(desc));
		return true;
	    } catch(_) { warn("fails to update "+name); }
	    return false;
	})();
    }
    
    // for testcases
    virtual.assert = register_function(function(o, msg) { if (!o) throw new native.Error(msg)}, "assert", true, 1);

    // 15.2 Object Objects
    eFn(virtual, "Object", true, false, true, register_function(virtual_Object, "Object", false, 1));
    // 15.2.3.1
    (function() {
    	var new_proto = native.Object_create(null);
    	native.Object.defineProperty(virtual.Object, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.Object, writable: true, enumerable: false, configurable: true});
    })();
    var securePrototypeOf = (function() {
    	var o = native.Object.getPrototypeOf;
	if (setproto_mode && map_mode) 
            return function(a) {
		var t = o(a);
		if (native_maphas[icall](m_org_virt, t)) return false;
		// if (map_contains(m_org_virt, t) === true) return false;
		return t;
	    };
	else
	    return function(a) {
		var t = search(m_virt_proto, a, false);
		if (t !== null) return false;
		t = o(a);
		if (map_contains(m_org_virt, t) === true) return false;
		return t;
	    };
    })();
    // 15.2.3.2
    eFn(virtual.Object, "getPrototypeOf", true, false, true, (function() {
    	var o = native.Object.getPrototypeOf;
	var f;
	if (setproto_mode)
	    f = function(a) {
    		return search_org(o(a), true);
    	    };
	else
	    f = function(a) {
		var t = search(m_virt_proto, a, false);
		if (t !== null) return t;
    		return search_org(o(a), true);
    	    };

    	return register_function(f, "getPrototypeOf", true, 1);
    })());
    // 15.2.3.3 Object.getOwnPropertyDescriptor(O, P)
    eFn(virtual.Object, "getOwnPropertyDescriptor", true, false, true, (function () {
	var o = native.Object.getOwnPropertyDescriptor;
	var f = function (O, P) {
	    if (isNotObject(O)) throw new native.TypeError("Object.getOwnPropertyDescriptor called on non-object");
	    P = ToString(P);
	    return native.call[icall](o, this, O, P);
	};
	return register_function(f, "getOwnPropertyDescriptor", true, 2);
    })());
    // 15.2.3.4 Object.getOwnPropertyNames(O)
    simpleFn(virtual.Object, native.Object, "getOwnPropertyNames", 1, true);
    // 15.2.3.5 Object.create(O[, Properties])
    eFn(virtual.Object, "create", true, false, true, (function() {
    	var nat = native.Object.create;
    	var f = function (O, Properties) {
	    // 1. If Type(O) is not Object or Null throw a TypeError exception.
	    if (typeof O !== 'object' && typeof O !== 'function' && typeof O !== null)
		throw new native.TypeError("Object.create called on non-object");
	    // 2. Let obj be the result of creating a new object as if by the expression new Object() where Object is the standard built-in constructor with that name
	    // 3. Set the [[Prototype]] internal property of obj to O.
	    var obj = native.call[icall](nat, this, O);
	    // 4. If the argument Properties is present and not undefined, add own properties to obj as if by calling the standard built-in function Object.defineProperties with arguments obj and Properties.
	    if (Properties !== undefined) {
		virtual_Object_defineProperties(obj, Properties);
	    }
	    // 5. Return obj.
	    return obj;
    	};
    	return register_function(f, "create", true, 2);
    })());
    // 15.2.3.6 Object.defineProperty(O, P, Attributes)
    eFn(virtual.Object, "defineProperty", true, false, true, (function() {
    	var f = function (O, P, Attributes) {
	    // 1. If Type(O) is not Object throw a TypeError exception.
	    if (isNotObject(O))
		throw new native.TypeError("Object.defineProperty called on non-object");
	    // 2. Let name be ToString(P).
	    var name = ToString(P);
	    // 3. Let desc be the result of calling ToPropertyDescriptor with Attributes as the argument.
	    var desc = ToPropertyDescriptor(Attributes);
	    // 4. Call the [[DefineOwnProperty]] internal method of O with arguments name, desc, and true.
	    DefineOwnProperty(O, name, desc, true);
	    // 5. Return O.
	    return O;
    	};
    	return register_function(f, "defineProperty", true, 3);
    })());
    // 15.2.3.7 Object.defineProperties(O,Properties)
    eFn(virtual.Object, "defineProperties", true, false, true, (function() {
	// Original semantics
    	var f = function (O,Properties) {
	    // 1. If Type(O) is not Object throw a TypeError exception.
	    if (isNotObject(O)) throw new native.TypeError("Object.defineProperties called on non-object");
	    // 2. Let props be ToObject(Properties).
	    var props = ToObject(Properties);
	    // 3. Let names be an internal list containing the names of each enumerable own property of props.
	    var names = native.Object_keys(props);
	    // 4. Let descriptors be an empty internal List.
	    var descriptors = [];
	    // 5. For each element P of names in list order,
	    var i, P, descObj, desc;
	    for (i=0; i<names.length; i++) {
		P = names[i];
		// a. Let descObj be the result of calling the [[Get]] internal method of props with P as the argument.
		descObj = Geto(props, P);
		// b. Let desc be the result of calling ToPropertyDescriptor with descObj as the argument.
		desc = ToPropertyDescriptor(descObj);
		// c. Append the pair (a two element List) consisting of P and desc to the end of descriptors.
    		native.Object_defineProperty(descriptors, i, sDesc({value: {P:P, desc:desc}, writable: true, enumerable: true, configurable: true}));
	    }
	    // 6. For each pair from descriptors in list order,
	    for (i=0; i < descriptors.length; i++) {
		// a. Let P be the first element of pair.
		P = descriptors[i].P;
		// b. Let desc be the second element of pair.
		desc = descriptors[i].desc;
		// c. Call the [[DefineOwnProperty]] internal method of O with arguments P, desc, and true.
		DefineOwnProperty(O, P, desc, true);
	    }
	    // 7. Return O.
	    return O;
    	};
    	return register_function(f, "defineProperties", true, 2);
    })());
    // 15.2.3.8 Object.seal(O)
    nsimpleFn(virtual.Object, native.Object, "seal", 1, true);
    // 15.2.3.9 Object.freeze(O)
    nsimpleFn(virtual.Object, native.Object, "freeze", 1, true);
    // 15.2.3.10 Object.preventExtensions(O)
    nsimpleFn(virtual.Object, native.Object, "preventExtensions", 1, true);
    // 15.2.3.11 Object.isSealed(O)
    nsimpleFn(virtual.Object, native.Object, "isSealed", 1, true);
    // 15.2.3.12 Object.isFrozen(O)
    nsimpleFn(virtual.Object, native.Object, "isFrozen", 1, true);
    // 15.2.3.13 Object.isExtensible(O)
    nsimpleFn(virtual.Object, native.Object, "isExtensible", 1, true);
    // 15.2.3.14 Object.keys(O)
    nsimpleFn(virtual.Object, native.Object, "keys", 1, true);
    
    // 15.2.4 Properties of the Object Prototype Object
    // 15.2.4.1 Object.prototype.constructor
    native.Object.defineProperty(virtual.Object.prototype, "constructor", sDesc({value: virtual.Object, writable: true, enumerable: false, configurable: true}));
    // 15.2.4.2 Object.prototype.toString()
    eFn(virtual.Object.prototype, "toString", true, false, true, (function() {
    	var f = function () {
    	    var cls = search(m_tos, this, false); 
    	    if (cls !== null) return "[object "+cls+"]";
	    else {
		// implicit accesses to "Symbol.toStringTag". secure.
		return native.call[icall](native.Object_prototype_toString, this);
	    }
    	};
    	return register_function(f, "toString", true, 0);
    })());
    // 15.2.4.3 Object.prototype.toLocaleString
    eFn(virtual.Object.prototype, "toLocaleString", true, false, true, (function() {
    	var o = native.Object.prototype.toLocaleString;
    	var f = function () {
	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
	    var O = ToObject(this);
	    // 2. Let toString be the result of calling the [[Get]] internal method of O passing "toString" as the argument.
	    var toString = Geto(O, "toString");
	    // 3. If IsCallable(toString) is false, throw a TypeError exception.
	    if (IsCallable(toString) === false)
		throw new native.TypeError(ToStringForError(toString)+" is not a function");
	    // 4. Return the result of calling the [[Call]] internal method of toString passing O as the this value and no arguments.
	    return Call(toString, O, []);
    	};
    	return register_function(f, "toLocaleString", true, 0);
    })());
    // 15.2.4.4 Object.prototype.valueOf()
    ToObjectWrapper0(virtual.Object.prototype, "valueOf", native.Object.prototype.valueOf);
    // 15.2.4.5 Object.prototype.hasOwnProperty(V)
    eFn(virtual.Object.prototype, "hasOwnProperty", true, false, true, (function() {
    	var o = native.Object.prototype.hasOwnProperty;
    	var f = function (V) {
	    V = ToString(V);
	    var O = ToObject(this);
	    return native.call[icall](o, O, V);
    	};
    	return register_function(f, "hasOwnProperty", true, 1);
    })());
    // 15.2.4.6 Object.prototype.isPrototypeOf(V)
    eFn(virtual.Object.prototype, "isPrototypeOf", true, false, true, (function() {
    	var f = function (V) {
	    // 1. If V is not an object, return false.
    	    if (isNotObject(V)) return false;
	    // 2. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = search_org(ToObject(this), true);
	    // 3. Repeat
    	    while (true) {
		// a. Let V be the value of the [[Prototype]] internal property of V.
    		V = virtual_Object_getPrototypeOf(V);
		// b. if V is null, return false
    		if (V === null) return false;
		// c. If O and V refer to the same object, return true.
    		if (O === V) return true;
    	    }
    	};
    	return register_function(f, "isPrototypeOf", true, 1);
    })());
    // 15.2.4.7 Object.prototype.propertyIsEnumerable(V)
    eFn(virtual.Object.prototype, "propertyIsEnumerable", true, false, true, (function() {
	var o = native.Object.prototype.propertyIsEnumerable;
    	var f = function (V) {
	    // 1. Let P be ToString(V)
	    var P = ToString(V);
	    var O = ToObject(this);
	    return native.call[icall](o, O, P);
    	};
    	return register_function(f, "propertyIsEnumerable", true, 1);
    })());
    // 15.3 Function Objects
    eFn(virtual, "Function", true, false, true, register_function(function(v) {
    	throw new native.Error("'eval' is not allowed in defensive mode.")
    }, "Function", false, 1, false, true));
    (function() {
	// 15.3.3.1 Function.prototype
    	var new_proto = register_function(native.call[icall](native.Function_prototype_bind, new Function()), "", true, 0);
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
	var notAllowed = function() {
	    throw new native.TypeError("'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them");
	}
	var desc = {get: notAllowed, set: notAllowed, configurable: true, enumerable: false};
	try {
	native.Object.defineProperty(new_proto, 'caller', desc);
	native.Object.defineProperty(new_proto, 'callee', desc);
	native.Object.defineProperty(new_proto, 'arguments', desc);
	} catch(_) {
	    caller_check = true;
	    throw "TODO";
	}

    	native.Object.defineProperty(virtual.Function, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.3.4 Properties of the Function Prototype Object
	// 15.3.4.1. Function.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.Function, writable: true, enumerable: false, configurable: true});
    })();
    // 15.3.4.2 Function.prototype.toString()
    eFn(virtual.Function.prototype, "toString", true, false, true, (function () {
	var f = function() {
	    if (typeof this !== 'function')
		throw new native.TypeError("Function.prototype.toString requires that 'this' be a Function");
	    return "function () { [protected code] }";
	    // var name = search(trusted_fn, this, false);
	    // // if (name === null) return "function() { [untrusted code] }";
	    // // else
	    // if (this === f) return "function toString() { [native code] }";
	    // else return native.apply[icall](native_Function_prototype_toString, this, arguments);
	};
	return register_function(f, "toString", true, 0);
    })());
    // 15.3.4.3 Function.prototype.apply(thisArg, argArray)
    eFn(virtual.Function.prototype, "apply", true, false, true, (function () {
    	var f = function(thisArg, argArray) {
	    var func = this;
	    // 1. If IsCallable(func) is false, then throw a TypeError exception.
    	    if (IsCallable(func) === false) {
    		throw new native.TypeError(typeof func + " is not a function")
	    }
	    // 2. If argArray is null or undefined, then
	    if (argArray == undefined) {
		// a. Return the result of calling the [[Call]] internal method of func, providing thisArg as the this value and an empty list of arguments.
    	    	return native.call[icall](func, thisArg);
	    }
	    // 3. If Type(argArray) is not Object, then throw a TypeError exception.
	    if (isNotObject(argArray))
    		throw new native.TypeError()
	    // 4. Let len be the result of calling the [[Get]] internal method of argArray with argument "length".
	    var len = Geto(argArray, "length");
	    // 5. Let n be ToUint32(len).
	    var n = ToUint32(len);

	    var index;

	    // optimized version. Checks whether 'argArray' is sparse array or not.
	    // TODO need to confirm.
	    var opt = true;
	    for (index = 0; index < n; index++) {
		if (!native.Object_prototype_hasOwnProperty.call(argArray, index)) {
		    opt = false;
		    break;
		}
	    }
	    if (opt)
		return native.apply[icall](func, thisArg, argArray);

	    // 6. Let argList be an empty List.
	    var argList = [];
	    // 7. Let index be 0.
	    index = 0;
	    // 8. Repeat while index < n
	    while (index < n) {
		// a. Let indexName be ToString(index).
		var indexName = ToString(index);
		// b. Let nextArg be the result of calling the [[Get]] internal method of argArray with indexName as the argument.
		var nextArg = Geto(argArray, indexName);
		// c. Append nextArg as the last element of argList.
		descTTT.value = nextArg;
    		native.Object_defineProperty(argList, indexName, descTTT);
		// d. Set index to index + 1.
		index += 1;
	    }
	    return native.apply[icall](func, thisArg, argList);
    	};
    	return register_function(f, "apply", true, 2);
    })());
    // 15.3.4.4 Function.prototype.call(thisArg [, arg1  [, arg2, ...]])
    eFn(virtual.Function.prototype, "call", true, false, true, (function () {
    	var nat = native.Function.prototype.call;
	var f = function(x) {
	    return native.apply[icall](nat, this, arguments);
	};
	return register_function(f, "call", true, 1);
    })());
    // 15.3.4.5 Function.prototype.bind(thisArg [, arg1 [, arg2, ...]])
    eFn(virtual.Function.prototype, "bind", true, false, true, (function () {
    	var nat = native.Function.prototype.bind;
    	var f = function(i) {
    	    var fn = native.apply[icall](nat, this, arguments);
	    return fn;
    	    // return registerOPT(trusted_fn, fn, ""), fn;
    	};
    	return register_function(f, "bind", true, 1);
    })());

    // TODO Uint32Array
    if ("Uint32Array" in _global_) {
	native.Uint32Array = Uint32Array;
	eFn(virtual, "Uint32Array", true, false, true, register_function(function uint32(v) {
	    if (this instanceof uint32) {
		if (arguments.length === 0)
		    return new native.Uint32Array();
		else if (arguments.length === 1)
		    return new native.Uint32Array(arguments[0]);
		else throw new native.TypeError("TODO");
	    } else {
		return native.apply[icall](native.Uint32Array, this, arguments);
	    }
	}, "Uint32Array", false, 1));
	(function() {
    	    var new_proto = new native.Uint32Array();
    	    native.Object.defineProperty(virtual.Uint32Array, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	    // 15.4.4.1 Array.prototype.constructor
    	    native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Uint32Array, writable: true, enumerable: false, configurable: true}));
	})();
    }

    // 15.4 Array Objects
    eFn(virtual, "Array", true, false, true, register_function(function(v) { return native.apply[icall](native.Array, this, arguments); }, "Array", false, 1));
    (function() {
	// 15.4.3.1 Array.prototype
    	var new_proto = new native.Array();
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Array, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.4.4.1 Array.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Array, writable: true, enumerable: false, configurable: true}));
    })();
    // 15.4.3.2 Array.isArray(arg)
    nsimpleFn(virtual.Array, native.Array, "isArray", 1, true);
    // ES6 Array.from()
    nsimpleFn(virtual.Array, native.Array, "from", 1, true);

    // 15.4.4 Properties of the Array Prototype Object
    copy_desc(virtual.Array.prototype, native.Array.prototype, "length");
    // 15.4.4.2 Array.prototype.toString()
    eFn(virtual.Array.prototype, "toString", true, false, true, (function() {
    	var f = function () {
    	    // 1. Let array be the result of calling ToObject on the this value.
    	    var array = ToObject(this);
    	    // 2. Let func be the result of calling the [[Get]] internal method of array with argument "join".
    	    var func = Geto(array, "join");
    	    // 3. If IsCallable(func) is false, then let func be the standard built-in method Object.prototype.toString (15.2.4.2).
    	    if (!IsCallable(func)) func = virtual.Object.prototype.toString;
    	    // 4. Return the result of calling the [[Call]] internal method of func providing array as the this value and an empty arguments list.
    	    return Call(func, array, []);
    	};
    	return register_function(f, "toString", true, 0);
    })());
    // 15.4.4.3 Array.prototype.toLocaleString()
    eFn(virtual.Array.prototype, "toLocaleString", true, false, true, (function() {
    	var f = function () {
    	    // 1. Let array be the result of calling ToObject passing the this value as the argument.
    	    var array = ToObject(this);
    	    // 2. Let arrayLen be the result of calling the [[Get]] internal method of array with argument "length".
    	    var arrayLen = Geto(array, "length");
    	    // 3. Let len be ToUint32(arrayLen).
    	    var len = ToUint32(arrayLen)
    	    // 4. Let separator be the String value for the list-separator String appropriate for the host environment’s current locale (this is derived in an implementation-defined way).
    	    var separator = ","
    	    // 5. If len is zero, return the empty String.
    	    if (len === 0) return "";
    	    // 6. Let firstElement be the result of calling the [[Get]] internal method of array with argument "0".
    	    var firstElement = Geto(array, "0")
    	    // 7. If firstElement is undefined or null, then
    	    var R, elementObj;
    	    if (firstElement == null) {
    		// a. Let R be the empty String.
    		R = "";
    	    } // 8. Else
    	    else {
    		// a. Let elementObj be ToObject(firstElement).
    		elementObj = ToObject(firstElement);
    		// b. Let func be the result of calling the [[Get]] internal method of elementObj with argument "toLocaleString".
    		var func = Geto(elementObj, "toLocaleString")
    		// c. If IsCallable(func) is false, throw a TypeError exception.
    		if (IsCallable(func) === false) {
    		    throw new native.TypeError(typeof func + " is not a function")
    		}
    		// d. Let R be the result of calling the [[Call]] internal method of func providing elementObj as the this value and an empty arguments list.
    		R = Call(func, elementObj, []);
    	    }
    	    // 9. Let k be 1.
    	    var k = 1;
    	    // 10. Repeat, while k < len
    	    while (k < len) {
    		// a. Let S be a String value produced by concatenating R and separator.
    		var S = R + separator;
    		// b. Let nextElement be the result of calling the [[Get]] internal method of array with argument ToString(k).
    		var nextElement = Geto(array, ToString(k));
    		// c. If nextElement is undefined or null, then
    		if (nextElement == null) {
    		    // i. Let R be the empty String.
    		    R = "";
    		} // d. Else
    		else {
    		    // i. Let elementObj be ToObject(nextElement).
    		    elementObj = ToObject(nextElement);
    		    // ii. Let func be the result of calling the [[Get]] internal method of elementObj with argument "toLocaleString".
    		    var func = Geto(elementObj, "toLocaleString");
    		    // iii. If IsCallable(func) is false, throw a TypeError exception.
    		    if (IsCallable(func) === false) {
    			throw new native.TypeError(typeof func + " is not a function");
    		    }
    		    // iv. Let R be the result of calling the [[Call]] internal method of func providing elementObj as the this value and an empty arguments list.
    		    R = Call(func, elementObj, []);
    		}
    		// e. Let R be a String value produced by concatenating S and R.
    		R = S + R;
    		// f. Increase k by 1.
    		k++;
    	    }
    	    // 11. Return R.
    	    return R;
    	};

    	return register_function(f, "toLocaleString", true, 0);
    })());
    // 15.4.4.4 Array.prototype.concat( [item1 [, item2 [, ...]]] )
    eFn(virtual.Array.prototype, "concat", true, false, true, (function() {
    	var f =  function (item) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.concat');
    	    // 2. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
    	    var A = new native.Array();
    	    // 3. Let n be 0.
    	    var n = 0;
    	    // 4. Let items be an internal List whose first element is O and whose subsequent elements are, in left to right order, the arguments that were passed to this function invocation.
    	    var items = [O];
    	    for (var i = 0; i< arguments.length; i++) {
		descTTT.value = arguments[i];
    		native.Object_defineProperty(items, ToString(i + 1), descTTT);
    	    }
	    
    	    // 5. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of the element.
    		var E = items[i];
    		// b. If the value of the [[Class]] internal property of E is "Array", then
    		if(typeof E === 'object' && Class(E) === "Array") {
    		    // i. Let k be 0.
    		    var k = 0;
    		    // ii. Let len be the result of calling the [[Get]] internal method of E with argument "length".
    		    var len = Geto(E, "length");
    		    // iii. Repeat, while k < len
    		    while(k < len){
    			// 1. Let P be ToString(k).
    			var P = ToString(k);
    			// 2. Let exists be the result of calling the [[HasProperty]] internal method of E with P.
    			var exists = HasProperty(P, E);
    			// 3. If exists is true, then
    			if (exists) {
    			    // a Let subElement be the result of calling the [[Get]] internal method of E with argument P.
    			    var subElement = Geto(E, P);
    			    // b Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), Property Descriptor {[[Value]]: subElement, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
			    descTTT.value = subElement;
    			    DefineOwnProperty(A, ToString(n), descTTT, false);
    			}
    			// 4. Increase n by 1.
    			n++;
    			// 5. Increase k by 1.
    			k++;
    		    }
    		}
    		// c. Else, E is not an Array
    		else {
    		    // i. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), Property Descriptor {[[Value]]: E, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
		    descTTT.value = E;
    		    DefineOwnProperty(A, ToString(n), descTTT, false);
    		    // ii. Increase n by 1.
    		    n++;
    		}
    	    }
    	    A.length = n;
    	    // 6. Return A.
    	    return A;
    	};
    	return register_function(f, "concat", true, 1);
    })());
    // 15.4.4.5 Array.prototype.join (separator)
    eFn(virtual.Array.prototype, "join", true, false, true, (function() {
	var nat = native.Array.prototype.join;
    	var f = function (separator) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);

	    // optimized path.
	    // TODO need to confirm.
	    var desc = native.Object_getOwnPropertyDescriptor(O, "length");
	    if (desc !== undefined) {
		var opt = true;
		if (native.Object_prototype_hasOwnProperty[icall](desc, "value") &&
		    typeof desc.value === "number") {
		    for (var i=0; i<O.length; i++) {
			if (!native.Object_prototype_hasOwnProperty[icall](O, i)) {
			    opt = false;
			    break;
			}
		    }
		}
		if (opt) return native.call[icall](nat, O, separator);
	    }

    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. If separator is undefined, let separator be the single-character String ",".
    	    if (separator === undefined){
    		separator = ",";
    	    }
    	    // 5. Let sep be ToString(separator).
    	    var sep = ToString(separator);
    	    // 6. If len is zero, return the empty String.
    	    if (len === 0){
    		return "";
    	    }
    	    // 7. Let element0 be the result of calling the [[Get]] internal method of O with argument "0".
    	    var element0 = Geto(O, "0");
    	    // 8. If element0 is undefined or null, let R be the empty String; otherwise, Let R be ToString(element0).
    	    if (element0 == null) {
    		var R = "";
    	    }else {
    		var R = ToString(element0);
    	    }
    	    // 9. Let k be 1.
    	    var k = 1;
    	    // 10. Repeat, while k < len
    	    while (k < len) {
    		// a. Let S be the String value produced by concatenating R and sep.
    		var S = R + sep;
    		// b. Let element be the result of calling the [[Get]] internal method of O with argument ToString(k).
    		var element = Geto(O, ToString(k));
    		// c. If element is undefined or null, Let next be the empty String; otherwise, let next be ToString(element).
    		if (element == null) {
    		    var next = "";
    		}else {
    		    var next = ToString(element);
    		}
    		// d. Let R be a String value produced by concatenating S and next.
    		var R = S + next;
    		// e. Increase k by 1.
    		k++;
    	    }
    	    //11. Return R.
    	    return R;
    	};
    	return register_function(f, "join", true, 1);
    })());
    // 15.4.4.6 Array.prototype.pop()
    eFn(virtual.Array.prototype, "pop", true, false, true, (function() {
	var nat = native.Array.prototype.pop;
    	var f = function () {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. If len is zero,
    	    if (len === 0) {
    		// a. Call the [[Put]] internal method of O with arguments "length", 0, and true.
    		Put(O, "length",  0);
    		// b. Return undefined.
    		return undefined;
    	    } else { // 5. Else, len > 0
    		// a. Let indx be ToString(len–1).
    		var oindx = len - 1;
		// optimization
		if (native.Object_prototype_hasOwnProperty[icall](O, oindx) &&
		    native.Object_prototype_hasOwnProperty[icall](O, "length"))
		    return native.call[icall](nat, O);
		var indx = "" + oindx;
    		// b. Let element be the result of calling the [[Get]] internal method of O with argument indx.
    		var element = Geto(O, indx);
    		// c. Call the [[Delete]] internal method of O with arguments indx and true.
    		delete O[indx];
		// Note: Since ECMA262 2015, it assigns oindx instead of assigning indx.
		// Thus, we do not follow the semantics defined in "15.4.4.6. The step 5.d" as it is.
    		// d. Call the [[Put]] internal method of O with arguments "length", indx, and true.
    		Put(O, "length",  oindx);
    		// e. Return element.
    		return element;
    	    }
    	};
    	return register_function(f, "pop", true, 0);
    })());
    // 15.4.4.7 Array.prototype.push ( [item1 [, item2 [, ... ]]] )
    eFn(virtual.Array.prototype, "push", true, false, true, (function() {
	var nat = native.Array.prototype.push;
    	var f = function (v) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);

	    // optimized path.
	    if (arguments.length === 1) {
		var desc = native.Object_getOwnPropertyDescriptor(O, "length");
		if (desc !== undefined) {
		    if (native.Object_prototype_hasOwnProperty[icall](desc, "value") &&
			typeof desc.value === "number") {
			// TODO need to confirm.
			if (!(desc.value in O)) {
			    return native.call[icall](nat, O, v);
			}
		    }
		}
	    }

    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let n be ToUint32(lenVal).
    	    var n = ToUint32(lenVal);
    	    // 4. Let items be an internal List whose elements are, in left to right order, the arguments that were passed to this function invocation.
    	    var items = arguments;
    	    // 5. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of the element.
    		var E = items[i];
    		// b. Call the [[Put]] internal method of O with arguments ToString(n), E, and true.
    		Put(O, ToString(n), E, true);
    		// c. Increase n by 1.
    		n++;
    	    }
    	    // 6. Call the [[Put]] internal method of O with arguments "length", n, and true.
    	    Put(O, "length", n, true);
    	    // 7. Return n.
    	    return n;
    	}
    	return register_function(f, "push", true, 1);
    })());
    // 15.4.4.8 Array.prototype.reverse()
    eFn(virtual.Array.prototype, "reverse", true, false, true, (function() {
    	var f = function () {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. Let middle be floor(len/2).
    	    var middle = native.Math_floor(len/2);
    	    // 5. Let lower be 0.
    	    var lower = 0;
    	    // 6. Repeat, while lower !=  middle
    	    while (lower != middle){
    		// a. Let upper be len-lower-1.
    		var upper = len-lower-1;
    		// b. Let upperP be ToString(upper).
    		var upperP = ToString(upper);
    		// c. Let lowerP be ToString(lower).
    		var lowerP = ToString(lower);
    		// d. Let lowerValue be the result of calling the [[Get]] internal method of O with argument lowerP.
    		var lowerValue = Geto(O, lowerP);
    		// e. Let upperValue be the result of calling the [[Get]] internal method of O with argument upperP .
    		var upperValue = Geto(O, upperP);
    		// f. Let lowerExists be the result of calling the [[HasProperty]] internal method of O with argument lowerP.
    		var lowerExists = HasProperty(lowerP, O);
    		// g. Let upperExists be the result of calling the [[HasProperty]] internal method of O with argument upperP.
    		var upperExists = HasProperty(upperP, O);
    		// h. If lowerExists is true and upperExists is true, then
    		if(lowerExists && upperExists){
    		    // i. Call the [[Put]] internal method of O with arguments lowerP, upperValue, and true .
    		    Put(O, lowerP, upperValue);
    		    // ii. Call the [[Put]] internal method of O with arguments upperP, lowerValue, and true .
    		    Put(O, upperP, lowerValue);
    		}
    		// i. Else if lowerExists is false and upperExists is true, then
    		else if(!lowerExists && upperExists){
    		    // i. Call the [[Put]] internal method of O with arguments lowerP, upperValue, and true .
    		    Put(O, lowerP, upperValue);
    		    // ii. Call the [[Delete]] internal method of O, with arguments upperP and true.
    		    delete O[upperP];
    		}
    		// j. Else if lowerExists is true and upperExists is false, then
    		else if (lowerExists && !upperExists){
    		    // i. Call the [[Delete]] internal method of O, with arguments lowerP and true .
    		    delete O[lowerP];
    		    // ii. Call the [[Put]] internal method of O with arguments upperP, lowerValue, and true .
    		    Put(O, upperP, lowerValue);
    		}
    		// k. Else both lowerExists and upperExists are false
		else {
    		    // i. No action is required.
		}
    		// l. Increase lower by 1.
    		lower++;
    	    }
    	    // 7. Return O .
    	    return O;
    	};
    	return register_function(f, "reverse", true, 0);
    })());
    // 15.4.4.9 Array.prototype.shift()
    eFn(virtual.Array.prototype, "shift", true, false, true, (function() {
    	var f = function() {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. If len is zero, then
    	    if(len === 0){
    		// a. Call the [[Put]] internal method of O with arguments "length", 0, and true.
    		Put(O, "length", 0);
    		// b. Return undefined.
    		return undefined;
    	    }
    	    // 5. Let first be the result of calling the [[Get]] internal method of O with argument "0".
    	    var first = Geto(O, "0");
    	    // 6. Let k be 1.
    	    var k = 1;
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let from be ToString(k).
    		var from = ToString(k);
    		// b. Let to be ToString(k–1).
    		var to = ToString(k-1);
    		// c. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		var fromPresent = HasProperty(from, O);
    		// d. If fromPresent is true, then
    		if(fromPresent){
    		    // i. Let fromVal be the result of calling the [[Get]] internal method of O with argument from.
    		    var fromVal = Geto(O, from);
    		    // ii. Call the [[Put]] internal method of O with arguments to, fromVal, and true.
    		    Put(O, to, fromVal);
    		}
    		// e. Else, fromPresent is false
    		else{
    		    // i. Call the [[Delete]] internal method of O with arguments to and true.
    		    delete O[to];
    		}
    		// f. Increase k by 1.
    		k++;
    	    }
    	    // 8. Call the [[Delete]] internal method of O with arguments ToString(len–1) and true.
    	    delete O[ToString(len-1)];
    	    // 9. Call the [[Put]] internal method of O with arguments "length", (len–1) , and true.
    	    Put(O, "length", len-1);
    	    // 10. Return first.
    	    return first;
    	};
    	return register_function(f, "shift", true, 0);
    })());
    // 15.4.4.10 Array.prototype.slice(start, end)
    eFn(virtual.Array.prototype, "slice", true, false, true, (function() {
    	var f = function (start, end) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
    	    var A = new native.Array();
    	    // 3. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 4. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 5. Let relativeStart be ToInteger(start).
    	    var relativeStart = ToInteger(start);
    	    // 6. If relativeStart is negative, let k be max((len + relativeStart),0); else let k be min(relativeStart, len).
    	    if (relativeStart < 0){
    		var k = native.Math_max(len+relativeStart,0);
    	    } else {
    		var k = native.Math_min(relativeStart,len);
    	    }
    	    // 7. If end is undefined, let relativeEnd be len; else let relativeEnd be ToInteger(end).
    	    if (end === undefined){
    		var relativeEnd = len;
    	    } else {
    		var relativeEnd = ToInteger(end);
    	    }
    	    // 8. If relativeEnd is negative, let final be max((len + relativeEnd),0); else let final be min(relativeEnd, len).
    	    if (relativeEnd < 0){
    		var final = native.Math_max(len+relativeEnd,0);
    	    } else {
    		var final = native.Math_min(relativeEnd,len);
    	    }
    	    // 9. Let n be 0.
    	    var n = 0;
    	    // 10. Repeat, while k < final
    	    while (k < final){
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), Property Descriptor {[[Value]]: kValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]:    true}, and false.
		    descTTT.value = kValue;
    		    DefineOwnProperty(A, ToString(n), descTTT, false);
    		}
    		// d. Increase k by 1.
    		k++;
    		// e. Increase n by 1.
    		n++;
    	    }
    	    A.length = n;
    	    // 11. Return A.
    	    return A;
    	};
    	return register_function(f, "slice", true, 2);
    })());
    // 15.4.4.11 Array.prototype.sort(comparefn)
    simpleFn(virtual.Array.prototype, native.Array.prototype, "sort", 1, true);
    eFn(virtual.Array.prototype, "sort", true, false, true, (function() {
    	var f = function (comparefn) {
	    // Let obj be the result of calling ToObject passing the this value as the argument.
    	    var obj = ToObject(this);
	    // Let len be the result of applying Uint32 to the result of calling the [[Get]] internal method of obj with argument "length".
    	    var len = ToUint32(Geto(obj, "length"));
	    // SortCompare abstract operation with two arguments j and k:
    	    var SortCompare = function(j, k) {
    		// 1. Let jString be ToString(j).
    		var jString = ToString(j);
    		// 2. Let kString be ToString(k).
    		var kString = ToString(k);
    		// 3. Let hasj be the result of calling the [[HasProperty]] internal method of obj with argument jString.
    		var hasj = HasProperty(jString, obj);
    		// 4. Let hask be the result of calling the [[HasProperty]] internal method of obj with argument kString.
    		var hask = HasProperty(kString, obj);
    		// 5. If hasj and hask are both false, then return +0.
    		if (hasj === false && hask === false) return +0;
    		// 6. If hasj is false, then return 1.
    		if (hasj === false) return 1;
    		// 7. If hask is false, then return –1.
    		if (hask === false) return -1;
    		// 8. Let x be the result of calling the [[Get]] internal method of obj with argument jString.
    		var x = Geto(obj, jString);
    		// 9. Let y be the result of calling the [[Get]] internal method of obj with argument kString.
    		var y = Geto(obj, kString);
    		// 10. If x and y are both undefined, return +0.
    		if (x === undefined && y === undefined) return +0;
    		// 11. If x is undefined, return 1.
    		if (x === undefined) return 1;
    		// 12. If y is undefined, return −1.
    		if (y === undefined) return -1;
    		// 13. If the argument comparefn is not undefined, then
    		if (comparefn !== undefined) {
    		    // a. If IsCallable(comparefn) is false, throw a TypeError exception.
    		    if (IsCallable(comparefn) === false) {
    			throw new native.TypeError(typeof comparefn + " is not a function");
    		    }
    		    // b. Return the result of calling the [[Call]] internal method of comparefn passing undefined as the this value and with arguments x and y.
    		    return Call(comparefn, undefined, [x, y]);
    		}
    		// 14. Let xString be ToString(x).
    		var xString = ToString(x);
    		// 15. Let yString be ToString(y).
    		var yString = ToString(y);
    		// 16. If xString < yString, return −1.
    		if (xString < yString) return -1;
    		// 17. If xString > yString, return 1.
    		if (xString > yString) return 1;
    		// 18. Return +0.
    		return +0;
    	    };

	    // an implementation dependent sorting algorithm.
    	    var b, tmp;
    	    do {
    		b = false;
    		for (var i = 1; i < len; i++) {
		    var ii = i-1, iis = "" + ii, is = "" + i;
    		    var k = SortCompare(ii, i);
    		    if (SortCompare(ii, i) > 0) {
			// tmp = obj[i - 1];
			tmp = Geto(obj, iis);
    			// obj[i-1] = obj[i];
			Put(obj, iis, Geto(obj, is), false);
    			// obj[i] = tmp;
			Put(obj, is, tmp, false);
    			b = true;
    		    }
    		}
    	    } while (b);
    	    return obj;
    	};
    	return register_function(f, "sort", true, 1);
    })());
    // 15.4.4.12 Array.prototype.splice(start, deleteCount [, item1 [, item2 [, ...]]] )
    eFn(virtual.Array.prototype, "splice", true, false, true, (function() {
    	var f = function splice (start, deleteCount) {
	    var actualStart;
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let A be a new array created as if by the expression new Array() where Array is the standard  built-in constructor with that name.
    	    var A = new native.Array();
    	    // 3. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 4. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 5. Let relativeStart be ToInteger(start).
    	    var relativeStart = ToInteger(start);
    	    // 6. If relativeStart is negative, let actualStart be max((len + relativeStart),0); else let actualStart be min(relativeStart, len).
    	    if (relativeStart < 0) {
    		actualStart = native.Math_max(len + relativeStart, 0);
    	    } else {
    		actualStart = native.Math_min(relativeStart, len);
    	    }
    	    // 7. Let actualDeleteCount be min(max(ToInteger(deleteCount),0), len – actualStart).
    	    var actualDeleteCount = native.Math_min(native.Math_max(ToInteger(deleteCount), 0), len - actualStart);
    	    // 8. Let k be 0.
    	    var k = 0;
    	    // 9. Repeat, while k < actualDeleteCount
    	    while (k < actualDeleteCount) {
    		// a. Let from be ToString(actualStart+k).
    		var from = ToString(actualStart + k);
    		// b. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		var fromPresent = HasProperty(from, O);
    		// c. If fromPresent is true, then
    		if (fromPresent) {
    		    // i. Let fromValue be the result of calling the [[Get]] internal method of O with argument from.
    		    var fromValue = Geto(O, from);
    		    // ii. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(k), Property Descriptor {[[Value]]: fromValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
		    descTTT.value = fromValue;
    		    DefineOwnProperty(A, ToString(k), descTTT, false);
    		}
    		// d. Increment k by 1.
    		k += 1;
    	    }
    	    // 10. Let items be an internal List whose elements are, in left to right order, the portion of the actual argument list starting with item1. The list will be empty if no such items are present.
    	    var items = new native.Array();
    	    for (var i = 0;i < arguments.length - 2;i++) {
		descTTT.value = arguments[i+2];
    		native.Object_defineProperty(items, ToString(i), descTTT);
    	    }
    	    // 11. Let itemCount be the number of elements in items.
    	    var itemCount = items.length;
    	    // 12. If itemCount < actualDeleteCount, then
    	    if (itemCount < actualDeleteCount) {
    		// a. Let k be actualStart.
    		var k = actualStart;
    		// b. Repeat, while k < (len – actualDeleteCount)
    		while ( k < (len - actualDeleteCount)) {
    		    // i. Let from be ToString(k+actualDeleteCount).
    		    var from = ToString(k + actualDeleteCount);
    		    // ii. Let to be ToString(k+itemCount).
    		    var to = ToString(k + itemCount);
    		    // iii. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		    var fromPresent = HasProperty(from, O);
    		    // iv If fromPresent is true, then
    		    if (fromPresent) {
    			// 1. Let fromValue be the result of calling the [[Get]] internal method of O with argument  from.
    			var fromValue = Geto(O, from);
    			// 2. Call the [[Put]] internal method of O with arguments to, fromValue, and true.
    			Put(O, to, fromValue);
    		    }
    		    // v. Else, fromPresent is false
    		    else {
    			// 1. Call the [[Delete]] internal method of O with arguments to and true.
    			delete O[to];
    		    }
    		    // vi. Increase k by 1.
    		    k++;
    		}
    		//c. Let k be len.
    		var k = len;
    		// d. Repeat, while k > (len – actualDeleteCount + itemCount)
    		while (k > (len - actualDeleteCount + itemCount)) {
    		    // i. Call the [[Delete]] internal method of O with arguments ToString(k–1) and true.
    		    delete O[ToString(k-1)];
    		    // ii. Decrease k by 1.
    		    k -= 1;
    		}
    	    }
    	    // 13. Else if itemCount > actualDeleteCount, then
    	    else {
    		// a. Let k be (len – actualDeleteCount).
    		var k = len - actualDeleteCount;
    		// b. Repeat, while k > actualStart
    		while (k > actualStart) {
    		    // i. Let from be ToString(k + actualDeleteCount – 1).
    		    var from = ToString(k + actualDeleteCount - 1);
    		    // ii. Let to be ToString(k + itemCount – 1)
    		    var to = ToString(k + itemCount - 1);
    		    // iii. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		    var fromPresent = HasProperty(from, O);
    		    // iv. If fromPresent is true, then
    		    if (fromPresent) {
    			// 1. Let fromValue be the result of calling the [[Get]] internal method of O with argument  from.
    			var fromValue = Geto(O, from);
    			// 2. Call the [[Put]] internal method of O with arguments to, fromValue, and true.
    			Put(O, to, fromValue);
    		    } else { // v. Else, fromPresent is false
    			// 1. Call the [[Delete]] internal method of O with argument to and true.
    			delete O[to];
    		    }
    		    // vi. Decrease k by 1.
    		    k -= 1;
    		}
    	    }
    	    // 14. Let k be actualStart.
    	    var k = actualStart;
    	    // 15. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of that element.
    		var E = items[i];
    		// b. Call the [[Put]] internal method of O with arguments ToString(k), E, and true.
    		Put(O, ToString(k), E);
    		// c. Increase k by 1.
    		k++;
    	    }
    	    // 16. Call the [[Put]] internal method of O with arguments "length", (len – actualDeleteCount +   itemCount), and true.
    	    Puto(O, "length", len - actualDeleteCount + itemCount);
    	    A.length = actualDeleteCount;
    	    // 17. Return A.
    	    return A;
    	};
    	return register_function(f, "splice", true, 2);
    })());
    // 15.4.4.13 Array.prototype.unshift([item1 [, item2 [, ...]]])
    eFn(virtual.Array.prototype, "unshift", true, false, true, (function() {
    	var f = function (V) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenVal be the result of calling the [[Get]] internal method of O with argument "length".
    	    var lenVal = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenVal).
    	    var len = ToUint32(lenVal);
    	    // 4. Let argCount be the number of actual arguments.
    	    var argCount = arguments.length;
    	    // 5. Let k be len.
    	    var k = len;
    	    // 6. Repeat, while k > 0,
    	    while (k > 0) {
    		// a. Let from be ToString(k–1).
    		var from = ToString(k-1);
    		// b. Let to be ToString(k+argCount –1).
    		var to = ToString(k + argCount - 1);
    		// c. Let fromPresent be the result of calling the [[HasProperty]] internal method of O with argument from.
    		var fromPresent = HasProperty(from, O);
    		// d. If fromPresent is true, then
    		if (fromPresent) {
    		    // i. Let fromValue be the result of calling the [[Get]] internal method of O with argument from.
    		    var fromValue = Geto(O, from);
    		    // ii. Call the [[Put]] internal method of O with arguments to, fromValue, and true.
    		    Put(O, to, fromValue);
    		}
    		// e. Else, fromPresent is false
    		else {
    		    // i. Call the [[Delete]] internal method of O with arguments to, and true.
    		    delete O[to];
    		}
    		// f. Decrease k by 1.
    		k -= 1;
    	    }
    	    // 7. Let j be 0.
    	    var j = 0;
    	    // 8. Let items be an internal List whose elements are, in left to right order, the arguments that were passed to this function invocation.
    	    var items = arguments;
    	    // 9. Repeat, while items is not empty
    	    for (var i = 0; i < items.length; i++) {
    		// a. Remove the first element from items and let E be the value of that element.
    		var E = items[i];
    		// b. Call the [[Put]] internal method of O with arguments ToString(j), E, and true.
    		Put(O, ToString(j), E);
    		// c. Increase j by 1.
    		j++;
    	    }
    	    // 10. Call the [[Put]] internal method of O with arguments "length", len+argCount, and true.
    	    Put(O, "length", len + argCount);
    	    // 11. Return len+argCount.
    	    return len + argCount;
    	};
    	return register_function(f, "unshift", true, 1);
    })());
    // 15.4.4.14 Array.prototype.indexOf(searchElement [, fromIndex])
    eFn(virtual.Array.prototype, "indexOf", true, false, true, (function() {
    	var f = function indexOf (searchElement /* ,fromIndex */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.indexOf');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If len is 0, return -1.
    	    if (len === 0){
    		return -1;
    	    }
    	    // 5. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be 0.
    	    if (arguments.length > 1){
    		var n = ToInteger(arguments["1"]);
    	    } else {
    		var n = 0;
    	    }
    	    // 6. If n ≥ len, return -1.
    	    if (n>=len){
    		return -1;
    	    }
    	    // 7. If n ≥ 0, then
    	    if (n>=0){
    		// a. Let k be n.
    		var k = n;
    	    }
    	    // 8. Else, n<0
    	    else {
    		// a. Let k be len - abs(n).
    		var k = len - native.Math_abs(n);
    		// b. If k is less than 0,then let k be 0.
    		if (k<0) {
    		    var k = 0;
    		}
    	    }
    	    // 9. Repeat, while k<len
    	    while (k < len) {
    		// a. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument ToString(k).
    		var kPresent = HasProperty(ToString(k), O);
    		// b. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let elementK be the result of calling the [[Get]] internal method of O with the argument ToString(k).
    		    var elementK = Geto(O, ToString(k));
    		    // ii. Let same be the result of applying the Strict Equality Comparison Algorithm to searchElement and elementK.
    		    var same = searchElement === elementK;
    		    // iii. If same is true, return k.
    		    if (same) {
    			return k;
    		    }
    		}
    		// c. Increase k by 1.
    		k++;
    	    }
    	    // 10. Return -1.
    	    return -1;
    	};
    	return register_function(f, "indexOf", true, 1);
    })());
    // 15.4.4.15 Array.prototype.lastIndexOf(searchElement [, fromIndex])
    eFn(virtual.Array.prototype, "lastIndexOf", true, false, true, (function() {
    	var f = function lastIndexOf (searchElement /* ,fromIndex */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.lastIndexOf');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If len is 0, return -1.
    	    if (len === 0) {
    		return -1;
    	    }
    	    // 5. If argument fromIndex was passed let n be ToInteger(fromIndex); else let n be len-1.
    	    if(arguments.length > 1){
    		var n = ToInteger(arguments["1"]);
    	    }else {
    		var n = len-1;
    	    }
    	    // 6. If n ≥ 0, then let k be min(n,len–1).
    	    if (n>=0){
    		var k = native.Math_min(n,len-1);
    	    }
    	    // 7. Else, n < 0
    	    else {
    		// a. Let k be len - abs(n).
    		var k = len - native.Math_abs(n);
    	    }
    	    // 8. Repeat, while k≥ 0
    	    while (k>=0) {
    		// a. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument ToString(k).
    		var kPresent = HasProperty(ToString(k), O);
    		// b. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let elementK be the result of calling the [[Get]] internal method of O with the argument ToString(k).
    		    var elementK = Geto(O, ToString(k));
    		    // ii. Let same be the result of applying the Strict Equality Comparison Algorithm to searchElement and elementK.
    		    var same = searchElement === elementK;
    		    // iii. If same is true, return k.
    		    if (same) {
    			return k;
    		    }
    		}
    		// c. Decrease k by 1.
    		k -= 1;
    	    }
    	    // 9. Return -1.
    	    return -1;
    	};
    	return register_function(f, "lastIndexOf", true, 1);
    })());
    // 15.4.4.16 Array.prototype.every(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "every", true, false, true, (function() {
    	var f = function every (callbackfn /*, thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length > 1) {
    		var T = arguments["1"];
    	    }else {
    		var T = undefined;
    	    }
    	    // 6. Let k be 0.
    	    var k = 0;
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let testResult be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var testResult = Call(callbackfn,T,[kValue,k,O]);
    		    // iii. If ToBoolean(testResult) is false, return false.
    		    if (ToBoolean(testResult) === false) {
    			return false;
    		    }
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    //8. Return true.
    	    return true;
    	};
    	return register_function(f, "every", true, 1);
    })());
    // 15.4.4.17 Array.prototype.some(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "some", true, false, true, (function() {
    	var f = function some (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.some');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length > 1) {
    		var T = arguments["1"];
    	    } else {
    		var T = undefined;
    	    }
    	    // 6. Let k be 0.
    	    var k = 0; 
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let testResult be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var testResult = Call(callbackfn,T,[kValue,k,O]);
    		    // iii. If ToBoolean(testResult) is true, return true.
    		    if (ToBoolean(testResult) === true) {
    			return true;
    		    }
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 8. Return false.
    	    return false;
    	};
    	return register_function(f, "some", true, 1);
    })());
    // 15.4.4.18 Array.prototype.forEach(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "forEach", true, false, true, (function() {
    	var f = function (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError();
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length > 1) {
    		var T = arguments["1"];
    	    } else {
    		var T = undefined;
    	    }
    	    // 6. Let k be 0.
    	    var k = 0;
    	    // 7. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Call the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    Call(callbackfn, T, [kValue, k, O]);
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 8. Return undefined.
    	    return undefined;
    	};
    	return register_function(f, "forEach", true, 1);
    })());
    // 15.4.4.19 Array.prototype.map(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "map", true, false, true, (function() {
    	var f = function (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    var T;
    	    if (arguments.length > 1){
    		T = arguments["1"];
    	    } else {
    		T = undefined;
    	    }
    	    // 6. Let A be a new array created as if by the expression new Array(len) where Array is the standard built-in constructor with that name and len is the value of len.
    	    var A = new native.Array(len);
    	    // 7. Let k be 0.
    	    var k = 0;
    	    // 8. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O)
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let mappedValue be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var m = Call(callbackfn, T, [kValue, k, O]);
    		    // iii. Call the [[DefineOwnProperty]] internal method of A with arguments Pk, Property Descriptor {[[Value]]: mappedValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
		    descTTT.value = m;
    		    DefineOwnProperty(A, Pk, descTTT, false);
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    //9. Return A.
    	    return A;
    	};
    	return register_function(f, "map", true, 1);
    })());
    // 15.4.4.20 Array.prototype.filter(callbackfn [, thisArg])
    eFn(virtual.Array.prototype, "filter", true, false, true, (function() {
    	var f = function filter (callbackfn /* , thisArg */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    	    if (arguments.length >1) {
    		var T = arguments["1"];
    	    }else {
    		var T = undefined;
    	    }
    	    // 6. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
    	    var A = new native.Array();
    	    // 7. Letk be 0.
    	    var k = 0;
    	    // 8. Let to be 0.
    	    var to = 0;
    	    // 9. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let selected be the result of calling the [[Call]] internal method of callbackfn with T as the this value and argument list containing kValue, k, and O.
    		    var selected = Call(callbackfn,T,[kValue,k,O]);
    		    // iii. If ToBoolean(selected) is true, then
    		    if (ToBoolean(selected) === true) {
    			// 1. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(to), Property Descriptor {[[Value]]: kValue, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
			descTTT.value = kValue;
    			DefineOwnProperty(A, ToString(to), descTTT, false);
    			// 2. Increase to by 1.
    			to += 1;
    		    }
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 10. Return A.
    	    return A;
    	};
    	return register_function(f, "filter", true, 1);
    })());
    // 15.4.4.21 Array.prototype.reduce(callbackfn [, initialVAlue])
    eFn(virtual.Array.prototype, "reduce", true, false, true, (function() {
    	var f = function (callbackfn /*, initialValue */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this, 'Array.prototype.reduce');
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function")
    	    }
    	    // 5. If len is 0 and initialValue is not present, throw a TypeError exception.
    	    if (len === 0 && arguments.length < 2) {
    		throw new native.TypeError();
    	    }
    	    // 6. Let k be 0.
    	    var k = 0;
    	    // 7. If initialValue is present, then
    	    if (arguments.length > 1) {
    		// a. Set accumulator to initialValue.
    		var accumulator = arguments["1"];
    	    }
    	    // 8. Else, initialValue is not present
    	    else {
    		// a. Let kPresent be false.
    		var kPresent = false;
    		// b. Repeat, while kPresent is false and k < len
    		while (kPresent === false && k < len) {
    		    // i. Let Pk be ToString(k).
    		    var Pk = ToString(k);
    		    //ii. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		    var kPresent = HasProperty(Pk, O);
    		    // iii. If kPresent is true, then
    		    if (kPresent) {
    			// 1. Let accumulator be the result of calling the [[Get]] internal method of O with argument Pk.
    			var accumulator = Geto(O, Pk);
    		    }
    		    // iv. Increase k by 1.
    		    k++;
    		}
    		// c. If kPresent is false, throw a TypeError exception.
    		if (!kPresent) {
    		    throw new TypeError();
    		}
    	    }
    	    // 9. Repeat, while k < len
    	    while (k < len) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    // i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let accumulator be the result of calling the [[Call]] internal method of callbackfn with undefined as the this value and argument list containing accumulator, kValue, k, and O.
    		    var accumulator = Call(callbackfn, undefined, [accumulator, kValue, k, O]);
    		}
    		// d. Increase k by 1.
    		k++;
    	    }
    	    // 10. Return accumulator.
    	    return accumulator;
    	};
    	return register_function(f, "reduce", true, 1);
    })());
    // 15.4.4.22 Array.prototype.reduceRight(callbackfn [, initialValue])
    eFn(virtual.Array.prototype, "reduceRight", true, false, true, (function() {
    	var f = function (callbackfn /*, initialValue */) {
    	    // 1. Let O be the result of calling ToObject passing the this value as the argument.
    	    var O = ToObject(this);
    	    // 2. Let lenValue be the result of calling the [[Get]] internal method of O with the argument "length".
    	    var lenValue = Geto(O, "length");
    	    // 3. Let len be ToUint32(lenValue).
    	    var len = ToUint32(lenValue);
    	    // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
    	    if (IsCallable(callbackfn) === false) {
    		throw new native.TypeError(typeof callbackfn + " is not a function");
    	    }
    	    // 5. If len is 0 and initialValue is not present, throw a TypeError exception.
    	    if (len === 0 && arguments.length < 2) {
    		throw new native.TypeError("Reduce of empty array with no initial value");
    	    }
    	    // 6. Let k be len-1.
    	    var k = len - 1;
    	    // 7. If initialValue is present, then
    	    if (arguments.length > 1) {
    		// a. Set accumulator to initialValue.
    		var accumulator = arguments["1"];
    	    }
    	    // 8. Else, initialValue is not present
    	    else {
    		// a. Let kPresent be false.
    		var kPresent = false;
    		// b. Repeat, while kPresent is false and k ≥ 0
    		while (kPresent === false && k >= 0) {
    		    // i. Let Pk be ToString(k).
    		    var Pk = ToString(k);
    		    // ii. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		    var kPresent = HasProperty(Pk, O);
    		    // iii. If kPresent is true, then
    		    if (kPresent) {
    			// 1. Let accumulator be the result of calling the [[Get]] internal method of O with argument Pk.
    			accumulator = Geto(O, Pk)
    		    }
    		    // iv. Decrease k by 1.
    		    k -= 1;
    		}
    		// c. If kPresent is false, throw a TypeError exception.
    		if (!kPresent) {
    		    throw new native.TypeError("Reduce of empty array with no initial value");
    		}
    	    }
    	    // 9. Repeat, while k ≥ 0
    	    while (k >= 0) {
    		// a. Let Pk be ToString(k).
    		var Pk = ToString(k);
    		// b. Let kPresent be the result of calling the [[HasProperty]] internal method of O with argument Pk.
    		var kPresent = HasProperty(Pk, O);
    		// c. If kPresent is true, then
    		if (kPresent) {
    		    //i. Let kValue be the result of calling the [[Get]] internal method of O with argument Pk.
    		    var kValue = Geto(O, Pk);
    		    // ii. Let accumulator be the result of calling the [[Call]] internal method of callbackfn with undefined as the this value and argument list containing accumulator, kValue, k, and O.
    		    var accumulator = Call(callbackfn, undefined, [accumulator, kValue, k, O]);
    		}
    		// d. Decrease k by 1.
    		k -= 1;
    	    }
    	    // 10. Return accumulator.
    	    return accumulator;
    	}
    	return register_function(f, "reduceRight", true, 1);
    })());

    // TODO need to confirm the semantics.
    eFn(virtual.Array.prototype, "fill", true, false, true, (function() {
    	var f = function (value) {
	    var start = arguments[1];
	    var end = arguments[2];
	    // 1. Let O be ? ToObject(this value).
	    var O = ToObject(this);
	    // 2. Let len be ? ToLength(? Get(O, "length")).
    	    var lenValue = Geto(O, "length");
    	    var len = ToUint32(lenValue);
	    // 3. Let relativeStart be ? ToInteger(start).
	    var relativeStart = ToInteger(start);
	    // 4. If relativeStart < 0, let k be max((len + relativeStart), 0); else let k be min(relativeStart, len)
	    var k = relativeStart < 0 ? native.Math_max((len + relativeStart), 0) : native.Math_min(relativeStart, len);
	    // 5. If end is undefined, let relativeEnd be len; else let relativeEnd be ? ToInteger(end).
	    var relativeEnd = end === undefined ? len : ToInteger(end);
	    // 6. If relativeEnd < 0, let final be max((len + relativeEnd), 0); else let final be min(relativeEnd, len).
	    var final = relativeEnd < 0 ? native.Math_max((len + relativeEnd), 0) : native.Math_min(relativeEnd, len);
	    // 7. Repeat, while k < final
	    while (k < final) {
		// a. Let Pk be ! ToString(k).
		var Pk = ToString(k);
		// b. Perform ? Set(O, Pk, value, true).
    		Puto(O, Pk, value, true);
		// c. Increase k by 1.
		k = k + 1;
	    }
	    // 8. Return O.
	    return O;
    	};
    	return register_function(f, "fill", true, 1);
    })());

    // 15.5 String Objects
    eFn(virtual, "String", true, false, true, (function () {
    	var o = String;
    	var f = function String(value) {
    	    // TODO: the following cannot distinguish "new String" and "String.apply(Object.create(String.prototype))"
    	    if (this instanceof String) {
    	    	if (arguments.length === 0) return new o();
    	    	else return new o(ToString(value));
    	    } else {
    		if (arguments.length === 0) return "";
    		else return ToString(value);
    	    }
    	};
    	return register_function(f, "String", false, 1);
    })());
    // Related to:
    // - /test262/test/suite/ch15/15.5/15.5.2/S15.5.2.1_A1_T8.js
    // - /test262/test/suite/ch15/15.5/15.5.2/S15.5.2.1_A1_T7.js
    // eFn(virtual, "String", true, false, true, (function() {
    // 	var f = function() {
    // 	    if (arguments.length > 0)
    // 		arguments[0] = ToString(arguments[0]);
    // 	    return native.String["@apply"](this, arguments);
    // 	};
    // 	return register_function(f, "String", false, 1)
    // })());
    (function() {
	// 15.5.3.1 String.prototype
    	var new_proto = new native.String();
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.String, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.5.4.1 String.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.String, writable: true, enumerable: false, configurable: true}));
    })();

    // 15.5.3.2 String.fromCharCode([char0 [, char1 [, ...]]])
    eFn(virtual.String, "fromCharCode", true, false, true, (function () {
	var o = native.String.fromCharCode;
	var f = function (v) {
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToNumber(arguments[i]);
	    }
	    return native.apply[icall](o, this, arguments);
	};
	return register_function(f, "fromCharCode", true, 1);
    })());
    // 15.5.4. Properties of the String Prototype Object
    // 15.5.4.2 String.prototype.toString()
    simpleFn(virtual.String.prototype, native.String.prototype, "toString", 0, true);
    // 15.5.4.3 String.prototype.valueOf()
    simpleFn(virtual.String.prototype, native.String.prototype, "valueOf", 0, true);
    // 15.5.4.4 String.prototype.charAt(pos)
    eFn(virtual.String.prototype, "charAt", true, false, true, (function() {
	var nat = String.prototype.charAt;
	var f = function(pos) {
	    // 1. Call CheckObjectCoercible passing the this value as its argument.
	    CheckObjectCoercible(this, 'String.prototype.charAt');
	    return native.call[icall](nat, ToString(this), ToInteger(pos));
	};
	return register_function(f, "charAt", true, 1);
    })());
    // 15.5.4.5 String.prototype.charCodeAt(pos)
    eFn(virtual.String.prototype, "charCodeAt", true, false, true, (function() {
	var nat = String.prototype.charCodeAt;
	var f = function(pos) {
	    CheckObjectCoercible(this, 'String.prototype.charCodeAt');
	    return native.call[icall](nat, ToString(this), ToInteger(pos));
	};
	return register_function(f, "charCodeAt", true, 1);
    })());
    // 15.5.4.6 String.prototype.concat([string1 [, string2 [, ...]]])
    eFn(virtual.String.prototype, "concat", true, false, true, (function() {
	var nat = String.prototype.concat;
	var f = function(v) {
	    CheckObjectCoercible(this, 'String.prototype.concat');
	    var S = ToString(this);
	    var i;
	    for (i=0; i<arguments.length; i++) {
		arguments[i] = ToString(arguments[i]);
	    }
	    return native.apply[icall](nat, S, arguments);
	};
	return register_function(f, "concat", true, 1);
    })());
    // 15.5.4.7 String.prototype.indexOf(searchString, position)
    eFn(virtual.String.prototype, "indexOf", true, false, true, (function() {
	var nat = String.prototype.indexOf;
	var f = function(searchString) {
	    var position;
	    if (arguments.length > 1)
		position = arguments[1];
	    CheckObjectCoercible(this, 'String.prototype.indexOf');
	    var S = ToString(this);
	    var searchStr = ToString(searchString);
	    if (position === undefined) position = 0;
	    var pos = ToInteger(position);
	    return native.call[icall](nat, S, searchStr, pos);
	};
	return register_function(f, "indexOf", true, 1);
    })());
    // 15.5.4.8 String.prototype.lastIndexOf(searchString, position)
    eFn(virtual.String.prototype, "lastIndexOf", true, false, true, (function() {
	var nat = String.prototype.lastIndexOf;
	var f = function(searchString) {
	    var position;
	    if (arguments.length > 1)
		position = arguments[1];
	    CheckObjectCoercible(this, 'String.prototype.lastIndexOf');
	    var S = ToString(this);
	    var searchStr = ToString(searchString);
	    var pos = ToNumber(position);
	    return native.call[icall](nat, S, searchStr, pos);
	};
	return register_function(f, "lastIndexOf", true, 1);
    })());
    // 15.5.4.9 String.prototype.localeCompare(that)
    eFn(virtual.String.prototype, "localeCompare", true, false, true, (function() {
	var nat = String.prototype.localeCompare;
	var f = function(that) {
	    CheckObjectCoercible(this, 'String.prototype.localeCompare');
	    var S = ToString(this);
	    that = ToString(that);
	    return native.call[icall](nat, S, that);
	};
	return register_function(f, "localeCompare", true, 1);
    })());
    // 15.5.4.10 String.prototype.match(regexp)
    eFn(virtual.String.prototype, "match", true, false, true, (function() {
	// original semantics
	var f = function(regexp) {
	    // 1. Call CheckObjectCoercible passing the this value as its argument.
	    CheckObjectCoercible(this, 'String.prototype.match');
	    // 2. Let S be the result of calling ToString, giving it the this value as its argument.
	    var S = ToString(this);
	    // 3. If Type(regexp) is Object and the value of the [[Class]] internal property of regexp is "RegExp", then let rx be regexp;
	    if (Class(regexp) === 'RegExp') {
		var rx = regexp;
	    } else {
		// 4. Else, let rx be a new RegExp object created as if by the expression new RegExp(regexp) where RegExp is the standard built-in constructor with that name.
		rx = new virtual_RegExp(regexp);
	    }
	    // 5. Let global be the result of calling the [[Get]] internal method of rx with argument "global".
	    var global = Geto(rx, "global");
	    // 6. Let exec be the standard built-in function RegExp.prototype.exec (see 15.10.6.2)
	    var exec = virtual_RegExp_prototype_exec;
	    // 7. If global is not true, then
	    if (global !== true) {
		// a. Return the result of calling the [[Call]] internal method of exec with rx as the this value and argument list containing S.
		return Call(exec, rx, [S]);
	    }
	    // Else, global is true
	    else if (global === true) {
		// a. Call the [[Put]] internal method of rx with arguments "lastIndex" and 0.
		Put(rx, "lastIndex", 0);
		// b. Let A be a new array created as if by the expression new Array() where Array is the standard built-in constructor with that name.
		var A = new native.Array();
		// c. Let previousLastIndex be 0.
		var previousLastIndex = 0;
		// d. Let n be 0.
		var n = 0;
		// e. Let lastMatch be true.
		var lastMatch = true;
		// f. Repeat, while lastMatch is true
		while (lastMatch === true) {
		    // i. Let result be the result of calling the [[Call]] internal method of exec with rx as the this value and argument list containing S.
		    var result = Call(exec, rx, [S]);
		    // ii. If result is null, then set lastMatch to false.
		    if (result === null) lastMatch = false;
		    // iii. Else, result is not null
		    else if (result !== null) {
			// 1. Let thisIndex be the result of calling the [[Get]] internal method of rx with argument "lastIndex".
			var thisIndex = Geto(rx, "lastIndex");
			// 2. If thisIndex = previousLastIndex then
			if (thisIndex === previousLastIndex) {
			    // a. Call the [[Put]] internal method of rx with arguments "lastIndex" and thisIndex+1.
			    Put(rx, "lastIndex", thisIndex + 1);
			    // b. Set previousLastIndex to thisIndex+1.
			    previousLastIndex = thisIndex + 1;
			}
			// 3. Else, set previousLastIndex to thisIndex.
			else {
			    previousLastIndex = thisIndex;
			}
			// 4. Let matchStr be the result of calling the [[Get]] internal method of result with argument "0".
			var matchStr = Geto(result, "0");
			// 5. Call the [[DefineOwnProperty]] internal method of A with arguments ToString(n), the Property Descriptor {[[Value]]: matchStr, [[Writable]]: true, [[Enumerable]]: true, [[configurable]]: true}, and false.
			descTTT.value = matchStr;
			DefineOwnProperty(A, ToString(n), descTTT, false);
			// 6. Increment n.
			n++;
		    }
		}
		// g. If n = 0, then return null.
		if (n === 0) return null;
		// h. Return A.
		return A;
	    }
	    // This is an undefined behavior. V8 engine does not terminate the execution in this case.
	    throw new native.Error("undefined behavior");
	};
	return register_function(f, "match", true, 1);
    })());
    // 15.5.4.11 String.prototype.replace(searchValue, replaceValue)
    eFn(virtual.String.prototype, "replace", true, false, true, (function() {
	var nat = String.prototype.replace;
	var f = function(searchValue, replaceValue) {
	    CheckObjectCoercible(this, 'String.prototype.replace');
	    var string = ToString(this);

	    if (Class(searchValue) === 'RegExp') {
		var nsearchValue = CreateRegExp(searchValue, 'String.prototype.replace', ['source', 'global', 'ignoreCase', 'multiline'], true);
		if (typeof replaceValue !== 'function' && typeof replaceValue === 'object') {
		    replaceValue = ToString(replaceValue);
		}
		var rtn = native.call[icall](nat, string, nsearchValue, replaceValue);
		searchValue.lastIndex = nsearchValue.lastIndex;
		return rtn;
	    }

	    searchValue = ToString(searchValue);
	    if (typeof replaceValue !== 'function') {
		replaceValue = ToString(replaceValue);
	    }
	    return native.call[icall](nat, string, searchValue, replaceValue);
	};
	return register_function(f, "replace", true, 2);
    })());
    // 15.5.4.12 String.prototype.search(regexp)
    eFn(virtual.String.prototype, "search", true, false, true, (function() {
	var nat = String.prototype.search;
	var f = function(regexp) {
	    var rx;
	    CheckObjectCoercible(this, 'String.prototype.search');
	    var string = ToString(this);
	    if (Class(regexp) === 'RegExp')
		rx = regexp;
	    else
		rx = new virtual_RegExp(regexp);
	    rx = CreateRegExp(rx, 'String.prototype.search', ['source', 'global', 'ignoreCase', 'multiline']);
	    return native.call[icall](nat, string, rx);
	};
	return register_function(f, "search", true, 1);
    })());
    // 15.5.4.13 String.prototype.slice(start, end)
    eFn(virtual.String.prototype, "slice", true, false, true, (function() {
	var nat = String.prototype.slice;
	var f = function(start, end) {
	    CheckObjectCoercible(this, 'String.prototype.slice');
	    var S = ToString(this);
	    start = ToInteger(start);
	    if (end !== undefined)
		end = ToInteger(end);
	    return native.call[icall](nat, S, start, end);
	};
	return register_function(f, "slice", true, 2);
    })());
    // 15.5.4.14 String.prototype.split(separator, limit)
    eFn(virtual.String.prototype, "split", true, false, true, (function() {
    	var nat = String.prototype.split;
    	var f = function(separator,limit) {
	    CheckObjectCoercible(this, 'String.prototype.split');
	    var R;
    	    var S = ToString(this);
	    if (limit !== undefined)
		limit = ToUint32(limit);
	    if (Class(separator) !== 'RegExp' && separator !== undefined) separator = ToString(separator);
	    if (Class(separator) === 'RegExp')
	    	R = CreateRegExp(separator, 'RegExp.prototype.split', ['source', 'global', 'ignoreCase', 'multiline']);
	     else
		R = separator;

	    return native.call[icall](nat, S, R, limit);
    	    // return nat[icall](S, separator, limit);
    	};
    	return register_function(f, "split", true, 2);
    })());
    // 15.5.4.15 String.prototype.substring(start, end)
    eFn(virtual.String.prototype, "substring", true, false, true, (function() {
    	var nat = String.prototype.substring;
    	var f = function(start, end) {
	    CheckObjectCoercible(this, 'String.prototype.substring');
    	    var S = ToString(this);
	    start = ToInteger(start);
	    if (end !== undefined) end = ToInteger(end);
	    return native.call[icall](nat, S, start, end);
    	    // return nat[icall](S, start, end);
    	};
    	return register_function(f, "substring", true, 2);
    })());
    // 15.5.4.16 String.prototype.toLowerCase() 
    eFn(virtual.String.prototype, "toLowerCase", true, false, true, (function() {
    	var nat = String.prototype.toLowerCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toLowerCase');
    	    var S = ToString(this);
	    return native.call[icall](nat, S);
    	    // return nat[icall](S);
    	};
    	return register_function(f, "toLowerCase", true, 0);
    })());
    // 15.5.4.17 String.prototype.toLocaleLowerCase()
    eFn(virtual.String.prototype, "toLocaleLowerCase", true, false, true, (function() {
    	var nat = String.prototype.toLocaleLowerCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toLocaleLowerCase');
    	    var S = ToString(this);
	    return native.call[icall](nat, S);
    	    // return nat[icall](S);
    	};
    	return register_function(f, "toLocaleLowerCase", true, 0);
    })());
    // 15.5.4.18 String.prototype.toUpperCase()
    eFn(virtual.String.prototype, "toUpperCase", true, false, true, (function() {
    	var nat = String.prototype.toUpperCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toUpperCase');
    	    var S = ToString(this);
	    return native.call[icall](nat, S);
    	    // return nat[icall](S);
    	};
    	return register_function(f, "toUpperCase", true, 0);
    })());
    // 15.5.4.19 String.prototype.toLocaleUpperCase()
    eFn(virtual.String.prototype, "toLocaleUpperCase", true, false, true, (function() {
    	var nat = String.prototype.toLocaleUpperCase;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.toLocaleUpperCase');
    	    var S = ToString(this);
	    return native.call[icall](nat, S);
    	    // return nat[icall](S);
    	};
    	return register_function(f, "toLocaleUpperCase", true, 0);
    })());
    // 15.5.4.20 String.prototype.trim()
    eFn(virtual.String.prototype, "trim", true, false, true, (function() {
    	var nat = String.prototype.trim;
    	var f = function() {
	    CheckObjectCoercible(this, 'String.prototype.trim');
    	    var S = ToString(this);
	    return native.call[icall](nat, S);
    	};
    	return register_function(f, "trim", true, 0);
    })());
    
    // B.2.3 String.prototype.substr(start, length)
    eFn(virtual.String.prototype, "substr", true, false, true, (function() {
    	var nat = String.prototype.substr;
    	var f = function(start, length) {
    	    var S = ToString(this);
	    start = ToInteger(start);
	    if (length !== undefined) length = ToInteger(length);
	    return native.call[icall](nat, S, start, length);
    	};
    	return register_function(f, "substr", true, 2);
    })());
    simpleFn(virtual.String.prototype, String.prototype, "substr", 2, true);
    // copy_desc(virtual.String.prototype, String.prototype, "length");


    // ES6 String.endsWith(search, this_len)
    simpleFn(virtual.String.prototype, String.prototype, "endsWith", 2, true);
    simpleFn(virtual.String.prototype, String.prototype, "includes", 2, true);
    simpleFn(virtual.String.prototype, String.prototype, "startsWith", 2, true);

    // 15.6 Boolean Objects
    eFn(virtual, "Boolean", true, false, true, register_function(function(v) { return native.apply[icall](native.Boolean, this, arguments); }, "Boolean", false, 1));
    (function() {
	// 15.6.3.1 Boolean.prototype
    	var new_proto = new Boolean(false);
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	Object.defineProperty(virtual.Boolean, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.6.4.1 Boolean.prototype.constructor
    	Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Boolean, writable: true, enumerable: false, configurable: true}));
    })();
    // 15.6.4 Properties of the Boolean Prototype Object
    // 15.6.4.2 Boolean.prototype.toString()
    simpleFn(virtual.Boolean.prototype, native.Boolean.prototype, "toString");
    // 15.6.4.3 Boolean.prototype.valueOf()
    simpleFn(virtual.Boolean.prototype, native.Boolean.prototype, "valueOf");

    // 15.7 Number Objects
    eFn(virtual, "Number", true, false, true, register_function(function(v) {
	if (arguments.length > 0) arguments[0] = ToNumber(arguments[0]);
	return native.apply[icall](native.Number, this, arguments);
    }, "Number", false, 1));
    // 15.7.3 Properties of the Number Constructor
    (function() {
	// 15.7.3.1 Number.prototype
    	var new_proto = new native.Number();
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Number, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	// 15.7.4.1 Number.prototype.constructor
	native.Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Number, writable: true, enumerable: false, configurable: true}));
    })();
    // 15.7.3.2 Number.MAX_VALUE
    copy_desc(virtual.Number, native.Number, "MAX_VALUE");
    // 15.7.3.3 Number.MIN_VALUE
    copy_desc(virtual.Number, native.Number, "MIN_VALUE");
    // 15.7.3.4 Number.NaN
    copy_desc(virtual.Number, native.Number, "NaN");
    // 15.7.3.5 Number.NEGATIVE_INFINITY
    copy_desc(virtual.Number, native.Number, "NEGATIVE_INFINITY");
    // 15.7.3.6 Number.POSITIVE_INFINITY
    copy_desc(virtual.Number, native.Number, "POSITIVE_INFINITY");
    // 15.7.4 Properties of the Number Prototype Object
    // 15.7.4.2 Number.prototype.toString([radix])
    eFn(virtual.Number.prototype, "toString", true, false, true, (function() {
    	var nat = native.Number.prototype.toString;
    	var f = function(radix) {
	    if (radix !== undefined) radix = ToInteger(radix);
	    return native.call[icall](nat, this, radix);
    	};
    	return register_function(f, "toString", true, 1);
    })());
    // 15.7.4.3 Number.prototype.toLocaleString()
    simpleFn(virtual.Number.prototype, native.Number.prototype, "toLocaleString", 0, true);
    // 15.7.4.4 Number.prototype.valueOf()
    simpleFn(virtual.Number.prototype, native.Number.prototype, "valueOf", 0, true);
    // 15.7.4.5 Number.prototype.toFixed(fractionDigits)
    eFn(virtual.Number.prototype, "toFixed", true, false, true, (function() {
    	var nat = native.Number.prototype.toFixed;
    	var f = function(fractionDigits) {
	    if (fractionDigits !== undefined)
		fractionDigits = ToInteger(fractionDigits);
	    return native.call[icall](nat, this, fractionDigits);
    	    // return nat[icall](this, fractionDigits);
    	};
    	return register_function(f, "toFixed", true, 1);
    })());
    // 15.7.4.6 Number.prototype.toExponential(fractionDigits)
    eFn(virtual.Number.prototype, "toExponential", true, false, true, (function() {
    	var nat = native.Number.prototype.toExponential;
    	var f = function(fractionDigits) {
	    if (fractionDigits !== undefined)
		fractionDigits = ToInteger(fractionDigits);
	    return native.call[icall](nat, this, fractionDigits);
    	    // return nat[icall](this, fractionDigits);
    	};
    	return register_function(f, "toExponential", true, 1);
    })());
    // 15.7.4.7 Number.prototype.toPrecision(precision)
    eFn(virtual.Number.prototype, "toPrecision", true, false, true, (function() {
    	var nat = native.Number.prototype.toPrecision;
    	var f = function(precision) {
	    if (precision !== undefined)
		precision = ToInteger(precision);
	    return native.call[icall](nat, this, precision);
    	    // return nat[icall](this, precision);
    	};
    	return register_function(f, "toPrecision", true, 1);
    })());
    // 15.8 The Math Object
    eFn(virtual, "Math", true, false, true, {});
    var virtual_Math = virtual.Math;
    if (use_symbol) {
	native.Object_defineProperty(virtual.Math, native.s_symbol_toStringTag, sDesc({value: "Math", configurable: true, writable: false, enumerable: false}));
    } else {
	registerOPT(m_tos, virtual.Math, "Math");
    }
    // 15.8.1 Value Properties of the Math Object
    // 15.8.1.1 E
    copy_desc(virtual.Math, native.Math, "E");
    // 15.8.1.2 LN10
    copy_desc(virtual.Math, native.Math, "LN10");
    // 15.8.1.3 LN2
    copy_desc(virtual.Math, native.Math, "LN2");
    // 15.8.1.4 LOG2E
    copy_desc(virtual.Math, native.Math, "LOG2E");
    // 15.8.1.5 LOG10E
    copy_desc(virtual.Math, native.Math, "LOG10E");
    // 15.8.1.6 PI
    copy_desc(virtual.Math, native.Math, "PI");
    // 15.8.1.7 SQRT1_2
    copy_desc(virtual.Math, native.Math, "SQRT1_2");
    // 15.8.1.8 SQRT2
    copy_desc(virtual.Math, native.Math, "SQRT2");
    // 15.8.2 Function Properties of the Math Object
    // 15.8.2.1 abs(x)
    MathWrapper(virtual.Math, 'abs', native.Math.abs);
    // 15.8.2.2 acos(x)
    MathWrapper(virtual.Math, 'acos', native.Math.acos);
    // 15.8.2.3 asin(x)
    MathWrapper(virtual.Math, 'asin', native.Math.asin);
    // 15.8.2.4 atan(x)
    MathWrapper(virtual.Math, 'atan', native.Math.atan);
    // 15.8.2.5 atan2(y,x)
    MathWrapper(virtual.Math, 'atan2', native.Math.atan2);
    // 15.8.2.6 ceil(x)
    MathWrapper(virtual.Math, 'ceil', native.Math.ceil);
    // 15.8.2.7 cos(x)
    MathWrapper(virtual.Math, 'cos', native.Math.cos);
    // 15.8.2.8 exp(x)
    MathWrapper(virtual.Math, 'exp', native.Math.exp);
    // 15.8.2.9 floor(x)
    MathWrapper(virtual.Math, 'floor', native.Math.floor);
    // 15.8.2.10 log(x)
    MathWrapper(virtual.Math, 'log', native.Math.log);
    // 15.8.2.11 max([value1 [, value2 [, ...]]])
    MathWrapper2(virtual.Math, 'max', native.Math.max);
    // 15.8.2.12 min([value1 [, value2 [, ...]]])
    MathWrapper2(virtual.Math, 'min', native.Math.min);
    // 15.8.2.13 pow(x,y)
    MathWrapper(virtual.Math, 'pow', native.Math.pow);
    // 15.8.2.14 random()
    simpleFn(virtual.Math, native.Math, "random");
    // 15.8.2.15 round(x)
    MathWrapper(virtual.Math, 'round', native.Math.round);
    // 15.8.2.16 sin(x)
    MathWrapper(virtual.Math, 'sin', native.Math.sin);
    // 15.8.2.17 sqrt(x)
    MathWrapper(virtual.Math, 'sqrt', native.Math.sqrt);
    // 15.8.2.18 tan(x)
    MathWrapper(virtual.Math, 'tan', native.Math.tan);
    // 15.9 Date Objects
    eFn(virtual, "Date", true, false, true, (function () {
    	var o = Date;
    	var f = function Date(a, b, c, d, e, f, g) {
    	    // TODO: the following cannot distinguish "new Date" and "Date.apply(Object.create(Date.prototype))"
    	    // In this version, Date.bind() will return a function that does not provide standard semantics of JavaScript.
    	    if (this instanceof Date) {
    	    	if (arguments.length === 0) return new o();
    	    	else if (arguments.length === 1) return new o(ToPrimitive_N(a));
    	    	else if (arguments.length === 2) return new o(ToNumber(a), ToNumber(b));
    	    	else if (arguments.length === 3) return new o(ToNumber(a), ToNumber(b), ToNumber(c));
    	    	else if (arguments.length === 4) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d));
    	    	else if (arguments.length === 5) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d), ToNumber(e));
    	    	else if (arguments.length === 6) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d), ToNumber(e), ToNumber(f));
    	    	else if (arguments.length === 7) return new o(ToNumber(a), ToNumber(b), ToNumber(c), ToNumber(d), ToNumber(e), ToNumber(f), ToNumber(g));
    	    } else {
    		return native.apply[icall](o, this, arguments);
    	    }
    	};
    	return register_function(f, "Date", false, 7);
    })());

    // 15.9.4 Properties of the Date Constructor
    (function() {
	// 15.9.4.1 Date.prototype
    	var new_proto = new native.Date(NaN);
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Date, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.9.5.1 Date.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.Date, writable: true, enumerable: false, configurable: true});
    })();
    // 15.9.4.2 Date.parse(string)
    ToStringWrapper1(virtual.Date, "parse", native.Date.parse);
    // 15.9.4.3 Date.UTC(year, month [, hours [, minutes [, seconds [, ms]]]])
    eFn(virtual.Date, "UTC", true, false, true, (function () {
    	var o = native.Date.UTC;
    	var f = function UTC(a, b, c, d, e, f, g) {
	    var arg = [];
	    descTTT.value = ToNumber(a);
    	    native.Object_defineProperty(arg, 0, descTTT);
	    descTTT.value = ToNumber(b);
    	    native.Object_defineProperty(arg, 1, descTTT);
    	    if (arguments.length >= 3)
		native.Object_defineProperty(arg, 2, sDesc({value: ToNumber(c), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 4)
		native.Object_defineProperty(arg, 3, sDesc({value: ToNumber(d), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 5)
		native.Object_defineProperty(arg, 4, sDesc({value: ToNumber(e), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 6)
		native.Object_defineProperty(arg, 5, sDesc({value: ToNumber(f), writable: true, enumerable: true, configurable: true}));
    	    if (arguments.length >= 7)
		native.Object_defineProperty(arg, 6, sDesc({value: ToNumber(g), writable: true, enumerable: true, configurable: true}));

    	    return native.apply[icall](o, this, arg);
    	};
    	return register_function(f, "UTC", false, 7);
    })());
    // 15.9.4.4 Date.now()
    simpleFn(virtual.Date, native.Date, "now", 0, true);
    // 15.9.5 Properties of the Date Prototype Object
    // 15.9.5.2 Date.prototype.toString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toString", 0, true);
    // 15.9.5.3 Date.prototype.toDateString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toDateString", 0, true);
    // 15.9.5.4 Date.prototype.toTimeString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toTimeString", 0, true);
    // 15.9.5.5 Date.prototype.toLocaleString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toLocaleString", 0, true);
    // 15.9.5.6 Date.prototype.toLocaleDateString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toLocaleDateString", 0, true);
    // 15.9.5.7 Date.prototype.toLocaleTimeString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toLocaleTimeString", 0, true);
    // 15.9.5.8 Date.prototype.valueOf()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "valueOf", 0, true);
    // 15.9.5.9 Date.prototype.getTime()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getTime", 0, true);
    // 15.9.5.10 Date.prototype.getFullYear()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getFullYear", 0, true);
    // 15.9.5.11 Date.prototype.getUTCFullYear()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCFullYear", 0, true);
    // 15.9.5.12 Date.prototype.getMonth()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getMonth", 0, true);
    // 15.9.5.13 Date.prototype.getUTCMonth()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCMonth", 0, true);
    // 15.9.5.14 Date.prototype.getDate()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getDate", 0, true);
    // 15.9.5.15 Date.prototype.getUTCDate()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCDate", 0, true);
    // 15.9.5.16 Date.prototype.getDay()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getDay", 0, true);
    // 15.9.5.17 Date.prototype.getUTCDay()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCDay", 0, true);
    // 15.9.5.18 Date.prototype.getHours()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getHours", 0, true);
    // 15.9.5.19 Date.prototype.getUTCHours()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCHours", 0, true);
    // 15.9.5.20 Date.prototype.getMinutes()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getMinutes", 0, true);
    // 15.9.5.21 Date.prototype.getUTCMinutes()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCMinutes", 0, true);
    // 15.9.5.22 Date.prototype.getSeconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getSeconds", 0, true);
    // 15.9.5.23 Date.prototype.getUTCSeconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCSeconds", 0, true);
    // 15.9.5.24 Date.prototype.getMilliseconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getMilliseconds", 0, true);
    // 15.9.5.25 Date.prototype.getUTCMilliseconds()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getUTCMilliseconds", 0, true);
    // 15.9.5.26 Date.prototype.getTimezoneOffset()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getTimezoneOffset", 0, true);
    // 15.9.5.27 Date.prototype.setTime(time)
    ToNumberWrapper1(virtual.Date.prototype, "setTime", native.Date.prototype.setTime);
    // 15.9.5.28 Date.prototype.setMilliseconds(ms)
    simpleFn(virtual.Date.prototype, native.Date.prototype, "setMilliseconds", 1, true);
    // 15.9.5.29 Date.prototype.setUTCMilliseconds(ms)
    simpleFn(virtual.Date.prototype, native.Date.prototype, "setUTCMilliseconds", 1, true);
    // 15.9.5.30 Date.prototype.setSeconds(sec [, ms])
    ToNumberWrapperN(virtual.Date.prototype, "setSeconds", native.Date.prototype.setSeconds, 1, 2, 2);
    // 15.9.5.31 Date.prototype.setUTCSeconds(sec [, ms])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCSeconds", native.Date.prototype.setUTCSeconds, 1, 2);
    // 15.9.5.32 Date.prototype.setMinutes(min [, sec [, ms]])
    ToNumberWrapperN(virtual.Date.prototype, "setMinutes", native.Date.prototype.setMinutes, 1, 3);
    // 15.9.5.33 Date.prototype.setUTCMinutes(min [, sec [, ms]])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCMinutes", native.Date.prototype.setUTCMinutes, 1, 3);
    // 15.9.5.34 Date.prototype.setHours(hour [, min [, sec [, ms]]])
    ToNumberWrapperN(virtual.Date.prototype, "setHours", native.Date.prototype.setHours, 1, 4);
    // 15.9.5.35 Date.prototype.setUTCHours(hour [, min [, sec [, ms]]])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCHours", native.Date.prototype.setUTCHours, 1, 4);
    // 15.9.5.36 Date.prototype.setDate(date)
    ToNumberWrapper1(virtual.Date.prototype, "setDate", native.Date.prototype.setDate);
    // 15.9.5.37 Date.prototype.setUTCDate(date)
    ToNumberWrapper1(virtual.Date.prototype, "setUTCDate", native.Date.prototype.setUTCDate);
    // 15.9.5.38 Date.prototype.setMonth(month [, date])
    ToNumberWrapperN(virtual.Date.prototype, "setMonth", native.Date.prototype.setMonth, 1, 2);
    // 15.9.5.39 Date.prototype.setUTCMonth(month [, date])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCMonth", native.Date.prototype.setUTCMonth, 1, 2);
    // 15.9.5.40 Date.prototype.setFullYear(year [, month [, date]])
    ToNumberWrapperN(virtual.Date.prototype, "setFullYear", native.Date.prototype.setFullYear, 1, 3);
    // 15.9.5.41 Date.prototype.setUTCFullYear(year [, month [, date]])
    ToNumberWrapperN(virtual.Date.prototype, "setUTCFullYear", native.Date.prototype.setUTCFullYear, 1, 3);
    // 15.9.5.42 Date.prototype.toUTCString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toUTCString", 0, true);
    // 15.9.5.43 Date.prototype.toISOString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toISOString", 0, true);
    // 15.9.5.44 Date.prototype.toJSON(key)
    eFn(virtual.Date.prototype, "toJSON", true, false, true, (function() {
    	var f = function(key) {
	    // 1. Let O be the result of calling ToObject, giving it the this value as its argument.
	    var O = ToObject(this);
	    // 2. Let tv be ToPrimitive(O, hint Number).
	    var tv = ToPrimitive_N(O);
	    // 3. If tv is a Number and is not finite, return null.
	    if (typeof tv === 'number' && !native.isFinite(tv))	return null;
	    // 4. Let toISO be the result of calling the [[Get]] internal method of O with argument "toISOString".
	    var toISO = Geto(O, 'toISOString');
	    // 5. If IsCallable(toISO) is false, throw a TypeError exception.
	    if (IsCallable(toISO) === false) throw new native.TypeError("toISOString is not a function");
	    // 6. Return the result of calling the [[Call]] internal method of toISO with O as the this value and an empty argument list.
	    return Call(toISO, O, []);
    	};
    	return register_function(f, "toJSON", true, 1);
    })());
    // B.2.4 Date.prototype.getYear()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "getYear", 0, true);
    // B.2.5 Date.prototype.setYear(year)
    ToNumberWrapper1(virtual.Date.prototype, "setYear", native.Date.prototype.setYear);
    // B.2.6 Date.prototype.toGMTString()
    simpleFn(virtual.Date.prototype, native.Date.prototype, "toGMTString", 0, true);

    // 15.10 RegExp (Regular Expression) Objects
    eFn(virtual, "RegExp", true, false, true, register_function(function(pattern, flags) {
	// TODO: the following cannot distinguish "new RegExp" and "RegExp.apply(Object.create(RegExp.prototype))"
	var obj;
	var cls = Class(pattern);
	if (cls === 'RegExp') {
            if (!(this instanceof native.RegExp)) {
		// [[Call]]
		if (flags === undefined) return pattern;
		else return new native.RegExp(pattern, ToString(flags));
	    }
	    // [[Construct]]
	    else if (flags !== undefined) return new native.RegExp(pattern, flags);
            else return new native.RegExp(pattern);
	} else if (pattern === undefined) pattern = "";
	else pattern = ToString(pattern);
	if (flags === undefined) flags = "";
	else flags = ToString(flags);
	return new native.RegExp(pattern, flags);
    }, "RegExp", false, 2));
    // 15.10.5 Properties of the RegExp Constructor
    (function() {
	// 15.10.5.1 RegExp.prototype
    	var new_proto = new native.RegExp();
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.RegExp, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.10.6 Properties of the RegExp Prototype Object
	// 15.10.6.1 RegExp.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.RegExp, writable: true, enumerable: false, configurable: true});
    })();
    // 15.10.6.2 RegExp.prototype.exec(string)
    eFn(virtual.RegExp.prototype, "exec", true, false, true, (function() {
    	var nat = native.RegExp.prototype.exec;
    	var f = function(string) {
	    var R = CreateRegExp(this, 'RegExp.prototype.exec', ['global']);
	    var S = ToString(string);
	    var result = native.call[icall](nat, R, S);
	    // var result = nat[icall](R, S);
	    Put(this, "lastIndex", R.lastIndex);
	    return result;
    	};
    	return register_function(f, "exec", true, 1);
    })());
    // 15.10.6.3 RegExp.prototype.test(string)
    eFn(virtual.RegExp.prototype, "test", true, false, true, (function() {
    	var f = function(string) {
	    var R = CreateRegExp(this, 'RegExp.prototype.test', ['global']);
	    var S = ToString(string);
	    // 1. Let match be the result of evaluating the RegExp.prototype.exec (15.10.6.2) algorithm upon this RegExp object using string as the argument.
	    var match = virtual_RegExp_prototype_exec[icall](R, S);
	    Put(this, "lastIndex", R.lastIndex);
	    // If match is not null, then return true; else return false.
	    if (match !== null) return true;
	    else return false;
    	};
    	return register_function(f, "test", true, 1);
    })());
    // 15.10.6.4 RegExp.prototype.toString()
    eFn(virtual.RegExp.prototype, "toString", true, false, true, (function() {
	var nat = native.RegExp.prototype.toString;
    	var f = function() {
	    var R = CreateRegExp(this, 'RegExp.prototype.toString', ['source', 'global', 'ignoreCase', 'multiline']);
	    // TODO ToString(source)
	    return native.call[icall](nat, R);
	    // return nat[icall](R);
    	};
    	return register_function(f, "toString", true, 0);
    })());

    // B RegExp.prototype.compile Non-standard semantics. 
    simpleFn(virtual.RegExp.prototype, native.RegExp.prototype, "compile", 2, true);

    // 15.10.7 Properties of RegExp Instances
    // 15.10.7.1 source
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "source");
    // 15.10.7.2 global
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "global");
    // 15.10.7.3 ignoreCase
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "ignoreCase");
    // 15.10.7.4 multiline
    copy_desc(virtual.RegExp.prototype, native.RegExp.prototype, "multiline");
    // 15.10.7.5 lastIndex: automatically created.

    // 15.11 Error Objects
    eFn(virtual, "Error", true, false, true, register_function(function(e) { return native.apply[icall](native.Error, this, arguments); }, "Error", false, 1));
    // 15.11.3 Properties of the Error Constructor
    (function() {
	// 15.11.3.1 Error.prototype
    	var new_proto = new native.Error();
	if (setproto_mode)
	    native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	native.Object.defineProperty(virtual.Error, "prototype", {value: new_proto, writable: false, enumerable: false, configurable: false});
	// 15.11.4 Properties of the Error Prototype Object
	// 15.11.4.1 Error.prototype.constructor
    	native.Object.defineProperty(new_proto, "constructor", {value: virtual.Error, writable: true, enumerable: false, configurable: true});
    })();

    // 15.11.4 Properties of the Error Prototype Object
    // 15.11.4.2 Error.prototype.name
    copy_desc(virtual.Error.prototype, native.Error.prototype, "name");
    // 15.11.4.3 Error.prototype.message
    copy_desc(virtual.Error.prototype, native.Error.prototype, "message");
    // 15.11.4.4 Error.prototype.toString()
    simpleFn(virtual.Error.prototype, native.Error.prototype, "toString");
    eFn(virtual.Error.prototype, "toString", true, false, true, (function() {
    	var f = function() {
	    // 1. Let O be the this value.
	    var O = this;
	    // 2. If Type(O) is not Object, throw a TypeError exception.
	    if (isNotObject(O))
		throw new native.TypeError("Method Error.prototype.toString called on incompatible receiver "+ToStringForError(O));
	    // 3. Let name be the result of calling the [[Get]] internal method of O with argument "name".
	    var name = Geto(O, 'name');
	    // 4. If name is undefined, then let name be "Error"; else let name be ToString(name).
	    if (name === undefined) name = "Error";
	    else name = ToString(name);
	    // 5. Let msg be the result of calling the [[Get]] internal method of O with argument "message".
	    var msg = Geto(O, 'message');
	    // 6. If msg is undefined, then let msg be the empty String; else let msg be ToString(msg).
	    if (msg === undefined) msg = "";
	    else msg = ToString(msg);
	    // 7. [mistake] If msg is undefined, then let msg be the empty String; else let msg be ToString(msg).
	    // 8. If name is the empty String, return msg.
	    if (name === "") return msg;
	    // 9. If msg is the empty String, return name.
	    if (msg === "") return name;
	    // 10. Return the result of concatenating name, ":", a single space character, and msg.
	    return name + ": " + msg;
    	};
    	return register_function(f, "toString", true, 1);
    })());
    
    // 15.11.6.1 EvalError
    extend("EvalError", "Error", ["name", "message"]);
    // 15.11.6.2 RangeError
    extend("RangeError", "Error", ["name", "message"]);
    // 15.11.6.3 ReferenceError
    extend("ReferenceError", "Error", ["name", "message"]);
    // 15.11.6.4 SyntaxError
    extend("SyntaxError", "Error", ["name", "message"]);
    // 15.11.6.5 TypeError
    extend("TypeError", "Error", ["name", "message"]);
    // 15.11.6.6 URIError
    extend("URIError", "Error", ["name", "message"]);

    // 15.12 The JSON Object
    eFn(virtual, "JSON", true, false, true, {});
    // The value of [[Class]] internal property of the JSON object is "JSON".
    registerOPT(m_tos, virtual.JSON, "JSON");
    // 15.12.2 parse(text [, reviver])
    eFn(virtual.JSON, "parse", true, false, true, (function() {
	var nat = native.JSON.parse;
    	var f = function(text, reviver) {
	    if (arguments.length > 0)
		arguments[0] = ToString(text);
	    return native.apply[icall](nat, this, arguments);
    	};
    	return register_function(f, "parse", true, 2);
    })());
    // 15.12.3 stringify(value [, replacer [, space]])
    eFn(virtual.JSON, "stringify", true, false, true, (function() {
	var nat = native.JSON.stringify;
	var Quote = nat;
	var JO = function JO(value, env) {
	    // 1. If stack contains value then throw a TypeError exception because the structure is cyclical.
	    if (elementOf(env.stack, value))
		throw new native.TypeError("Converting circular structure to JSON");
	    // 2. Append value to stack.
	    Append(env.stack, value);
	    // 3. Let stepback be indent.
	    var stepback = env.indent;
	    // 4. Let indent be the concatenation of indent and gap.
	    env.indent += env.gap;
	    // 5. If PropertyList is not undefined, then
	    var K;
	    if (env.PropertyList !== undefined) {
		// a. Let K be PropertyList.
		K = env.PropertyList;
	    }
	    // 6. Else
	    else {
		// a. Let K be an internal List of Strings consisting of the names of all the own properties of value whose [[Enumerable]] attribute is true. The ordering of the Strings should be the same as that used by the Object.keys standard built-in function.
		K = native.Object_keys(value);
	    }
	    // 7. Let partial be an empty List.
	    var partial = [];
	    var member;
	    // 8. For each element P of K.
	    for (var i=0; i<K.length; i++) {
		var P = K[i];
		// a. Let strP be the result of calling the abstract operation Str with arguments P and value.
		var strP = Str(P, value, env);
		// b. If strP is not undefined
		if (strP !== undefined) {
		    // i. Let member be the result of calling the abstract operation Quote with argument P.
		    member = Quote(P);
		    // ii. Let member be the concatenation of member and the colon character.
		    member += ":";
		    // iii. If gap is not the empty String
		    if (env.gap !== "") {
			// 1. Let member be the concatenation of member and the space character.
			member += " ";
		    }
		    // iv. Let member be the concatenation of member and strP.
		    member += strP;
		    // v. Append member to partial.
		    Append(partial, member);
		}
	    }
	    // 9. If partial is empty, then
	    var finale, separator, properties;
	    if (partial.length === 0) {
		// a. Let final be "{}".
		finale = "{}";
	    }
	    // 10. Else
	    else {
		// a. If gap is the empty String
		if (env.gap === "") {
		    // i. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with the comma character. A comma is not inserted either before the first String or after the last String.
		    properties = native.call[icall](native.Array_prototype_join, partial, ",");
		    // ii. Let final be the result of concatenating "{", properties, and "}".
		    finale = "{" + properties + "}";
		}
		// b. Else gap is not the empty String
		else if (env.gap !== "") {
		    // i. Let separator be the result of concatenating the comma character, the line feed character, and indent.
		    separator = ",\n" + env.indent;
		    // ii. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with separator. The separator String is not inserted either before the first String or after the last String.
		    properties = native.call[icall](native.Array_prototype_join, partial, separator);
		    // iii. Let final be the result of concatenating "{", the line feed character, indent, properties, the line feed character, stepback, and "}".
		    finale = "{\n" + env.indent + properties + "\n" + stepback + "}";
		}
	    }
	    // 11. Remove the last element of stack.
	    env.stack.length = env.stack.length - 1;
	    // 12. Let indent be stepback.
	    env.indent = stepback;
	    // 13. Return final.
	    return finale;
	};
	var JA = function(value, env) {
	    // 1. If stack contains value then throw a TypeError exception because the structure is cyclical.
	    if (elementOf(env.stack, value))
		throw new native.TypeError("Converting circular structure to JSON");
	    // 2. Append value to stack.
	    Append(env.stack, value);
	    // 3. Let stepback be indent.
	    var stepback = env.indent;
	    // 4. Let indent be the concatenation of indent and gap.
	    env.indent += env.gap;
	    // 5. Let partial be an empty List.
	    var partial = [];
	    // 6. Let len be the result of calling the [[Get]] internal method of value with argument "length".
	    // value.[[Class]] === "Array"
	    var len = value.length;
	    // 7. Let index be 0.
	    var index = 0;
	    // 8. Repeat while index < len
	    while (index < len) {
		// a. Let strP be the result of calling the abstract operation Str with arguments ToString(index) and value.
		var strP = Str(ToString(index), value, env);
		// b. If strP is undefined
		if (strP === undefined) {
		    // i. Append "null" to partial.
		    Append(partial, "null");
		}
		// c. Else
		else {
		    // i. Append strP to partial.
		    Append(partial, strP);
		}
		// d. Increment index by 1.
		index += 1;
	    }
	    // 9. If partial is empty ,then
	    var finale, properties;
	    if (partial.length === 0) {
		// a. Let final be "[]".
		finale = "[]";
	    }
	    // 10. Else
	    else {
		// a. If gap is the empty String
		if (env.gap === "") {
		    // i. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with the comma character. A comma is not inserted either before the first String or after the last String.
		    properties = native.call[icall](native.Array_prototype_join, partial, ",");
		    // ii. Let final be the result of concatenating "[", properties, and "]".
		    finale = "[" + properties + "]";
		}
		// b. Else
		else if (env.gap !== "") {
		    // i. Let separator be the result of concatenating the comma character, the line feed character, and indent.
		    var separator = ",\n" + env.indent;
		    // ii. Let properties be a String formed by concatenating all the element Strings of partial with each adjacent pair of Strings separated with separator. The separator String is not inserted either before the first String or after the last String.
		    properties = native.call[icall](native.Array_prototype_join, partial, separator);
		    // iii. Let final be the result of concatenating "[", the line feed character, indent, properties, the line feed character, stepback, and "]".		    
		    finale = "[\n" + env.indent + properties + "\n" + stepback + "]";
		}
	    }
	    // 11. Remove the last element of stack.
	    env.stack.length = env.stack.length - 1;
	    // 12. Let indent be stepback.
	    env.indent = stepback;
	    // 13. Return final.
	    return finale;
	};
	var Str = function Str(key, holder, env) {
	    // 1. Let value be the result of calling the [[Get]] internal method of holder with argument key.
	    var value = Get(holder, key);
	    // 2. If Type(value) is Object, then
	    if (isObject(value)) {
		// a. Let toJSON be the result of calling the [[Get]] internal method of value with argument "toJSON".
		var toJSON = Geto(value, "toJSON");
		// b. If IsCallable(toJSON) is true
		if (IsCallable(toJSON)) {
		    // i. Let value be the result of calling the [[Call]] internal method of toJSON passing value as the this value and with an argument list consisting of key.
		    value = Call(toJSON, value, [key]);
		}
	    }
	    // 3. If ReplacerFunction is not undefined, then
	    if (env.ReplacerFunction !== undefined) {
		// a. Let value be the result of calling the [[Call]] internal method of ReplacerFunction passing holder as the this value and with an argument list consisting of key and value.
		value = Call(env.ReplacerFunction, holder, [key, value]);
	    }
	    // 4. If Type(value) is Object then,
	    if (isObject(value)) {
		var cls = Class(value);
		// a. If the [[Class]] internal property of value is "Number" then,
		if (cls === 'Number') {
		    // i. Let value be ToNumber(value).
		    value = ToNumber(value);
		}
		// b. Else if the [[Class]] internal property of value is "String" then,
		else if (cls === 'String') {
		    // i. Let value be ToString(value).
		    value = ToString(value);
		}
		// c. Else if the [[Class]] internal property of value is "Boolean" then,
		else if (cls === 'Boolean') {
		    // i. Let value be the value of the [[PrimitiveValue]] internal property of value.
		    value = native.call[icall](native.Boolean_prototype_valueOf, value);
		}
	    }
	    // 5. If value is null then return "null".
	    if (value === null) return "null";
	    // 6. If value is true then return "true".
	    if (value === true) return "true";
	    // 7. If value is false then return "false".
	    if (value === false) return "false";
	    // 8. If Type(value) is String, then return the result of calling the abstract operation Quote with argument value.
	    if (typeof value === 'string') {
		return Quote(value);
	    }
	    // 9. If Type(value) is Number
	    if (typeof value === 'number') {
		// a. If value is finite then return ToString(value).
		if (native.isFinite(value)) return ToString(value);
		// b. Else, return "null".
		else return "null";
	    }
	    // 10. If Type(value) is Object, and IsCallable(value) is false
	    if (isObject(value) && !IsCallable(value)) {
		// a. If the [[Class]] internal property of value is "Array" then
		if (Class(value) === 'Array') {
		    // i. Return the result of calling the abstract operation JA with argument value.
		    return JA(value, env);
		}
		// b. Else, return the result of calling the abstract operation JO with argument value.
		else {
		    return JO(value, env);
		}
	    }
	    // 11. Return undefined.
	    return undefined;
	};
	var elementOf = function(list, item) {
	    return native.call[icall](native.indexOf, list, item) >= 0;
	};
	var Append = function(list, item) {
	    descTTT.value = item;
    	    native.Object_defineProperty(list, list.length, descTTT);
	};
    	var f = function(value, replacer, space) {
	    var i;
	    // 1. Let stack be an empty List.
	    // 2. Let indent be the empty String.
	    // 3. Let PropertyList and ReplacerFunction be undefined.
	    var env = {stack: [], indent: "", gap: undefined, PropertyList: undefined, ReplacerFunction: undefined};
	    // 4. If Type(replacer) is Object, then
	    if (isObject(replacer)) {
		// a. If IsCallable(replacer) is true, then
		if (IsCallable(replacer)) {
		    // i. Let ReplacerFunction be replacer.
		    env.ReplacerFunction = replacer;
		}
		// b. Else if the [[Class]] internal property of replacer is "Array", then
		else if (Class(replacer) === 'Array') {
		    // i. Let PropertyList be an empty internal List
		    env.PropertyList = [];
		    // ii. For each value v of a property of replacer that has an array index property name. The properties are enumerated in the ascending array index order of their names.
		    var v;
		    for (i=0; i<replacer.length; i++) {
			v = Geto(replacer, "" + i);
			// 1. Let item be undefined.
			var item = undefined;
			// 2. If Type(v) is String then let item be v.
			if (typeof v === 'string') item = v;
			// 3. Else if Type(v) is Number then let item be ToString(v).
			else if (typeof v === 'number') item = ToString(v);
			// 4. Else if Type(v) is Object then,
			else if (isObject(v)) {
			    // a. If the [[Class]] internal property of v is "String" or "Number" then let item be ToString(v).
			    var cls = Class(v);
			    if (cls === 'String' || cls === 'Number') item = ToString(v);
			}
			// 5. If item is not undefined and item is not currently an element of PropertyList then,
			if (item !== undefined && !elementOf(env.PropertyList, item)) {
			    // a. Append item to the end of PropertyList.
			    Append(env.PropertyList, item);
			}
		    }
		}
	    }
	    // 5. If Type(space) is Object then,
	    if (isObject(space)) {
		var cls = Class(space);
		// a. If the [[Class]] internal property of space is "Number" then,
		//   i. Let space be ToNumber(space).
		if (cls === 'Number') space = ToNumber(space);
		// b. Else if the [[Class]] internal property of space is "String" then,
		//   i. Let space be ToString(space).
		else if (cls === 'String') space = ToString(space);
	    }
	    // 6. If Type(space) is Number
	    var gap = "";
	    if (typeof space === 'number') {
		// a. Let space be min(10, ToInteger(space)).
		space = native.Math_min(10, ToInteger(space));
		// b. Set gap to a String containing space space characters. This will be the empty String if space is less than 1.
		if (space >= 1)
		    for (i=0; i<space; i++) gap += " ";
	    }
	    // 7. Else if Type(space) is String
	    else if (typeof space === 'string') {
		// a. If the number of characters in space is 10 or less, set gap to space otherwise set gap to a String consisting of the first 10 characters of space.
		if (space.length <= 10) gap = space;
		else gap = native.call[icall](native.String_prototype_substring, space, 0, 10);
	    }
	    // 8. Else
	    else {
		// a. Set gap to the empty String.
		gap = "";
	    }
	    env.gap = gap;
	    // 9. Let wrapper be a new object created as if by the expression new Object(), where Object is the standard built-in constructor with that name.
	    var wrapper = {};
	    // 10. Call the [[DefineOwnProperty]] internal method of wrapper with arguments the empty String, the Property Descriptor {[[Value]]: value, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true}, and false.
	    DefineOwnProperty(wrapper, "", sDesc({value: value, writable: true, enumerable: true, configurable: true}), false);
	    return Str("", wrapper, env);
    	};
    	return register_function(f, "stringify", true, 3);
    })());

    // TODO Node.js
   if (node_mode) {
       // nodejs modules.
   }

    // DOM
    if (DOM_mode) {
	// window
	native.Object_defineProperty(virtual, 'window', sDesc({value: virtual, writable: false, enumerable: true, configurable: false}));
	// document and location
	(function() {
	    var proxy_getter = function(o, name) {
		return function() {
		    var desc = native.Object_getOwnPropertyDescriptor(o, name);
		    if (native.Object_prototype_hasOwnProperty[icall](desc, "value"))
			return desc.value;
		    if (native.Object_prototype_hasOwnProperty[icall](desc, "get")) {
			var f = desc.get;
			return native.call[icall](f, o);
		    }
		    return undefined;
		};
	    };
	    var proxy_setter = function(o, name) {
		return function(v) {
		    var desc = native.Object_getOwnPropertyDescriptor(o, name);
		    if (native.Object_prototype_hasOwnProperty[icall](desc, "value")) {
			return o = v;
		    }
		    if (native.Object_prototype_hasOwnProperty[icall](desc, "set")) {
			var f = desc.set;
			return native.call[icall](f, o, v);
		    }
		    return v;
		};
	    };
	    var proxy_caller = function(o, name) {
		var f = o[name];
		return function () {
		    return native.apply[icall](f, o, arguments);
		};
	    }
	    var read_proxy = function(o_proxy, o, name) {
		var desc = Object.getOwnPropertyDescriptor(o, name);
		Object.defineProperty(o_proxy, name, {get: proxy_getter(o_loc, name), enumerable: desc.enumerable, configurable: desc.configurable});
	    };
	    var call_proxy = function(o_proxy, o, name) {
		var desc = Object.getOwnPropertyDescriptor(o, name);
		Object.defineProperty(o_proxy, name, {value: proxy_caller(o_loc, name), writable: desc.writable, enumerable: desc.enumerable, configurable: desc.configurable});
	    };
	    var o_doc = document;
	    var o_loc = document.location;
	    var location = {};
	    // TODO: 'location' is not an instance of Location in the defensive mode.
	    registerOPT(m_tos, location, "Location");
	    call_proxy(location, o_loc, "assign");
	    call_proxy(location, o_loc, "reload");
	    call_proxy(location, o_loc, "replace");
	    call_proxy(location, o_loc, "toString");
	    call_proxy(location, o_loc, "valueOf");
	    // the following fields are 'readonly'.
	    // TODO: read_proxy(location, o_loc, "ancestorOrigins"); // DOMStringList
	    read_proxy(location, o_loc, "host");
	    read_proxy(location, o_loc, "hostname");
	    read_proxy(location, o_loc, "origin");
	    read_proxy(location, o_loc, "pathname");
	    read_proxy(location, o_loc, "port");
	    read_proxy(location, o_loc, "protocol");
	    read_proxy(location, o_loc, "search");
	    // location.hash is exclusive in defensive mode.
	    var hash = document.location.hash;
	    Object.defineProperty(location, "hash", {get: function() { return hash; }, set: function(v) { o_loc.hash = v; }, enumerable: true, configurable: false});
	    document.location.hash = "";
	    
	    var doc = {};
	    // TODO: 'document' is not an instance of HTMLDocument in the defensive mode.
	    registerOPT(m_tos, doc, "HTMLDocument");
	    Object.defineProperty(virtual, "document", {get: function() { return doc; }, enumerable: true, configurable: false});
	    Object.defineProperty(doc, "location", {get: function() { return location; }, enumerable: true, configurable: false});
	    Object.defineProperty(virtual, "location", {get: function() { return location; }, enumerable: true, configurable: false});
	})();

	// XMLHttpRequest
	// https://xhr.spec.whatwg.org/#interface-xmlhttprequest
	// EventTarget
	eFn(virtual, "EventTarget", true, false, true, register_function(function() { throw new native.TypeError("Failed to construct 'EventTarget': Please use the 'new' operator, this DOM object constructor cannot be called as a function"); }, "EventTarget", false, 0));
	(function() {
    	    var new_proto = {};
	    registerOPT(m_tos, new_proto, "EventTarget");
	    if (setproto_mode)
		native_setPrototypeOf(new_proto, virtual.Object.prototype);
    	    Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.EventTarget, writable: true, enumerable: false, configurable: true}));
    	    Object.defineProperty(virtual.EventTarget, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	})();
	simpleFn(virtual.EventTarget.prototype, EventTarget.prototype, "addEventListener", 2, true);
	simpleFn(virtual.EventTarget.prototype, EventTarget.prototype, "dispatchEvent", 1, true);
	simpleFn(virtual.EventTarget.prototype, EventTarget.prototype, "removeEventListener", 2, true);

	// XMLHttpRequestEventTarget
	eFn(virtual, "XMLHttpRequestEventTarget", true, false, true, register_function(function() { throw new native.TypeError("Illegal constructor"); }, "XMLHttpRequestEventTarget", false, 0));
	if (native.global.XMLHttpRequestEventTarget) {
	    (function() {
    		var new_proto = Object.create(virtual.EventTarget.prototype);

		if (setproto_mode)
		    native_setPrototypeOf(new_proto, virtual.EventTarget.prototype);

		registerOPT(m_tos, new_proto, "XMLHttpRequestEventTarget");
    		Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.XMLHttpRequestEventTarget, writable: true, enumerable: false, configurable: true}));
    		Object.defineProperty(virtual.XMLHttpRequestEventTarget, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	    })();

	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "onabort");
	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "onerror");
	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "onload");
	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "onloadend");
	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "onloadstart");
	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "onprogress");
	    copy_desc(virtual.XMLHttpRequestEventTarget.prototype, _global_.XMLHttpRequestEventTarget.prototype, "ontimeout");
	}
	
	eFn(virtual, "XMLHttpRequest", true, false, true, register_function(function() { return native.apply[icall](native.XMLHttpRequest, this, arguments); }, "XMLHttpRequest", false, 0));
	(function() {
    	    var new_proto = Object.create(virtual.XMLHttpRequest);
	    if (setproto_mode)
		native_setPrototypeOf(new_proto, virtual.XMLHttpRequestEventTarget.prototype);

	    registerOPT(m_tos, new_proto, "XMLHttpRequest");
    	    Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.XMLHttpRequest, writable: true, enumerable: false, configurable: true}));
    	    Object.defineProperty(virtual.XMLHttpRequest, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	})();

	// Request [https://xhr.spec.whatwg.org/#request]
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "open", 2, true);
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "setRequestHeader", 2, true);
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "send", 0, true);
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "abort", 0, true);

	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "timeout"); // number
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "withCredentials"); // boolean
	// TODO: copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "upload"); // XMLHttpRequestUpload, XMLHttpRequestEventTarget
	
	// Response [https://xhr.spec.whatwg.org/#xmlhttprequest-response]
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "responseURL"); // null or string
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "status"); // number
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "statusText"); // string
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "getResponseHeader", 1, true); // null or string
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "getAllResponseHeaders", 0, true); // string
	simpleFn(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "overrideMimeType", 1, true);
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "responseType"); // string
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "response"); // string
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "responseText"); // string
	// TODO: copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "responseXML"); // 'document' element.
	
	copy_desc(virtual.XMLHttpRequest.prototype, XMLHttpRequest.prototype, "onreadystatechange");
	
	// TODO This is not secure. a function and its arguments must be correctly wrapped.
	eFn(virtual, "setInterval", true, true, true, (function() {
	    var o = native.global.setInterval;
	    var f = function (s) {
		if (typeof s === 'function') {
		    if (arguments.length > 1)
			arguments[1] = ToNumber(arguments[1])
		    var i;
		    for (i=2; i<arguments.length; i++) {
			arguments[i] = L2H(arguments[i]);
		    }
		    return native.apply[icall](o, this, arguments);
		}
		return -1;
	    };
	    return register_function(f, "setInterval", true, 1);
	})());

	// DOMException
	eFn(virtual, "DOMException", true, false, true, register_function(function() { throw new native.TypeError("Failed to construct 'DOMException': Please use the 'new' operator, this DOM object constructor cannot be called as a function"); }, "DOMException", false, 0));
	(function() {
    	    var new_proto = {};
	    registerOPT(m_tos, new_proto, "DOMException");
    	    Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.DOMException, writable: true, enumerable: false, configurable: true}));
    	    Object.defineProperty(virtual.DOMException, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	})();
	// TODO: is this working? (they are getters)
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "name");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "message");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "code");

	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INDEX_SIZE_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "DOMSTRING_SIZE_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "HIERARCHY_REQUEST_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "WRONG_DOCUMENT_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INVALID_CHARACTER_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "NO_DATA_ALLOWED_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "NO_MODIFICATION_ALLOWED_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "NOT_FOUND_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "NOT_SUPPORTED_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INUSE_ATTRIBUTE_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INVALID_STATE_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "SYNTAX_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INVALID_MODIFICATION_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "NAMESPACE_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INVALID_ACCESS_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "VALIDATION_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "TYPE_MISMATCH_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "SECURITY_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "NETWORK_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "ABORT_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "URL_MISMATCH_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "QUOTA_EXCEEDED_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "TIMEOUT_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "INVALID_NODE_TYPE_ERR");
	copy_desc(virtual.DOMException.prototype, DOMException.prototype, "DATA_CLONE_ERR");
	
	// Event
	eFn(virtual, "Event", true, false, true, register_function(function() { throw new native.TypeError("Failed to construct 'Event': Please use the 'new' operator, this DOM object constructor cannot be called as a function"); }, "Event", false, 0));
	(function() {
    	    var new_proto = {};
	    registerOPT(m_tos, new_proto, "Event");
    	    Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.Event, writable: true, enumerable: false, configurable: true}));
    	    Object.defineProperty(virtual.Event, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	})();
	copy_desc(virtual.Event.prototype, Event.prototype, "type"); // string
	copy_desc(virtual.Event.prototype, Event.prototype, "target");
	copy_desc(virtual.Event.prototype, Event.prototype, "srcElement");
	copy_desc(virtual.Event.prototype, Event.prototype, "currentTarget");
	simpleFn(virtual.Event.prototype, Event.prototype, "composedPath", 0, true);
	
	copy_desc(virtual.Event.prototype, Event.prototype, "NONE");
	copy_desc(virtual.Event.prototype, Event.prototype, "CAPTURING_PHASE");
	copy_desc(virtual.Event.prototype, Event.prototype, "AT_TARGET");
	copy_desc(virtual.Event.prototype, Event.prototype, "BUBBLING_PHASE");
	copy_desc(virtual.Event.prototype, Event.prototype, "eventPhase");

	simpleFn(virtual.Event.prototype, Event.prototype, "stopPropagation", 0, true);
	copy_desc(virtual.Event.prototype, Event.prototype, "cancelBubble");
	simpleFn(virtual.Event.prototype, Event.prototype, "stopImmediatePropagation", 0, true);

	copy_desc(virtual.Event.prototype, Event.prototype, "bubbles");
	copy_desc(virtual.Event.prototype, Event.prototype, "cancelable");
	copy_desc(virtual.Event.prototype, Event.prototype, "returnValue");
	simpleFn(virtual.Event.prototype, Event.prototype, "preventDefault", 0, true);
	copy_desc(virtual.Event.prototype, Event.prototype, "defaultPrevented");
	copy_desc(virtual.Event.prototype, Event.prototype, "composed");

	copy_desc(virtual.Event.prototype, Event.prototype, "timeStamp");

	simpleFn(virtual.Event.prototype, Event.prototype, "initEvent", 1, true);
	
	// ProgressEvent
	// https://xhr.spec.whatwg.org/#interface-progressevent
	eFn(virtual, "ProgressEvent", true, false, true, register_function(function() { throw new native.TypeError("Failed to construct 'ProgressEvent': Please use the 'new' operator, this DOM object constructor cannot be called as a function"); }, "ProgressEvent", false, 0));
	(function() {
    	    var new_proto = Object.create(virtual.Event.prototype);
	    registerOPT(m_tos, new_proto, "ProgressEvent");
    	    Object.defineProperty(new_proto, "constructor", sDesc({value: virtual.ProgressEvent, writable: true, enumerable: false, configurable: true}));
    	    Object.defineProperty(virtual.ProgressEvent, "prototype", sDesc({value: new_proto, writable: false, enumerable: false, configurable: false}));
	})();
	
	copy_desc(virtual.ProgressEvent.prototype, ProgressEvent.prototype, "lengthComputable");
	copy_desc(virtual.ProgressEvent.prototype, ProgressEvent.prototype, "loaded");
	copy_desc(virtual.ProgressEvent.prototype, ProgressEvent.prototype, "total");
    }

    // [[Construct]] !== [[Function]]
    (function() {
    	var o, n, a;
    	var list= ["Boolean", "Number", "Date"];
	if (DOM_mode) {
	    list.push("XMLHttpRequest");
	    list.push("EventTarget");
	    list.push("DOMException");
	}
    	for (v in list) {
    	    v = list[v];
	    if (map_mode) native_mapset[icall](m_virt_org, virtual[v], native.global[v]);
    	    else register(m_virt_org, virtual[v], native.global[v]);
    	}
    })();
    (function() {
    	var o, n, a;
    	var list= ["Object", "Array", "String", "Boolean", "RegExp", "Number", "Math", "Function", "TypeError", "Date", "Error", "RangeError", "SyntaxError", "ReferenceError", "URIError", "EvalError", "JSON"];
	if (DOM_mode) {
	    list.push("XMLHttpRequest");
	    list.push("EventTarget");
	    list.push("DOMException");
	}
    	for (v in list) {
    	    v = list[v];
	    if (map_mode) native_mapset[icall](m_org_virt, native.global[v], virtual[v]);
    	    else register(m_org_virt, native.global[v], virtual[v]);
    	}
    })();
    // During the executions of JavaScript primitive constructs, the following native objects can be created.
    if (map_mode) {
	native_mapset[icall](m_org_virt, Object.prototype, virtual.Object.prototype);
	native_mapset[icall](m_org_virt, Date.prototype, virtual.Date.prototype);
	native_mapset[icall](m_org_virt, Array.prototype, virtual.Array.prototype);
	native_mapset[icall](m_org_virt, String.prototype, virtual.String.prototype);
	native_mapset[icall](m_org_virt, Boolean.prototype, virtual.Boolean.prototype);
	native_mapset[icall](m_org_virt, Number.prototype, virtual.Number.prototype);
	native_mapset[icall](m_org_virt, RegExp.prototype, virtual.RegExp.prototype);
	native_mapset[icall](m_org_virt, Function.prototype, virtual.Function.prototype);
	native_mapset[icall](m_org_virt, Error.prototype, virtual.Error.prototype);
	native_mapset[icall](m_org_virt, TypeError.prototype, virtual.TypeError.prototype);
	native_mapset[icall](m_org_virt, RangeError.prototype, virtual.RangeError.prototype);
	native_mapset[icall](m_org_virt, SyntaxError.prototype, virtual.SyntaxError.prototype);
	native_mapset[icall](m_org_virt, ReferenceError.prototype, virtual.ReferenceError.prototype);
	native_mapset[icall](m_org_virt, URIError.prototype, virtual.URIError.prototype);
	native_mapset[icall](m_org_virt, EvalError.prototype, virtual.EvalError.prototype);
	if (DOM_mode) {
	    native_mapset[icall](m_org_virt, XMLHttpRequest.prototype, virtual.XMLHttpRequest.prototype);
	    native_mapset[icall](m_org_virt, EventTarget.prototype, virtual.EventTarget.prototype);
	    native_mapset[icall](m_org_virt, DOMException.prototype, virtual.DOMException.prototype);
	}
    } else {
	register(m_org_virt, Object.prototype, virtual.Object.prototype);
	register(m_org_virt, Date.prototype, virtual.Date.prototype);
	register(m_org_virt, Array.prototype, virtual.Array.prototype);
	register(m_org_virt, String.prototype, virtual.String.prototype);
	register(m_org_virt, Boolean.prototype, virtual.Boolean.prototype);
	register(m_org_virt, Number.prototype, virtual.Number.prototype);
	register(m_org_virt, RegExp.prototype, virtual.RegExp.prototype);
	register(m_org_virt, Function.prototype, virtual.Function.prototype);
	register(m_org_virt, Error.prototype, virtual.Error.prototype);
	register(m_org_virt, TypeError.prototype, virtual.TypeError.prototype);
	register(m_org_virt, RangeError.prototype, virtual.RangeError.prototype);
	register(m_org_virt, SyntaxError.prototype, virtual.SyntaxError.prototype);
	register(m_org_virt, ReferenceError.prototype, virtual.ReferenceError.prototype);
	register(m_org_virt, URIError.prototype, virtual.URIError.prototype);
	register(m_org_virt, EvalError.prototype, virtual.EvalError.prototype);
	if (DOM_mode) {
	    register(m_org_virt, XMLHttpRequest.prototype, virtual.XMLHttpRequest.prototype);
	    register(m_org_virt, EventTarget.prototype, virtual.EventTarget.prototype);
	    register(m_org_virt, DOMException.prototype, virtual.DOMException.prototype);
	}
    }

    var e = {};
    virtual.exports = e;
    virtual.module = {id: '.',
		      path: '.',
		      parent: null,
		      filename: '',
		      loaded: false,
		      children: [],
		      paths: [],
		      exports: e};

    // auxiliary function for public fields.
    var declare_public = function(o, name) {
	var iname = '_@'+name;
	native.Object_defineProperty(o, name, {
	    get: function() { return this[iname]; },
	    set: function(e) { return this[iname] = e; }
	});
    };
    // registerOPT(trusted_fn, declare_public, "");
    var declare_shared = function(o, name) {
	var iname = '_@'+name;
	if (proxy_mode) {
	    native.Object_defineProperty(o, name, {
		get: function() { return this[iname]; },
		set: function(e) {
		    var handler = {
			get: function(obj, prop) {
			    return obj[prop];
			},
			set: function(obj, prop, v) {
			    return obj[prop] = v;
			}
		    };
		    e = new native.Proxy(e, handler);
		    return this[iname] = e;
		}});
	} else {
	    native.Object_defineProperty(o, name, {
		get: function() { return this[iname]; },
		set: function(e) {
		    if (e && typeof e === 'object') {
			if (native.Array_isArray(e)) {
			    warn("Array object cannot be mutable without a Proxy object");
			} else {
			    var o = e, e = {};
			    var names = native.Object_getOwnPropertyNames(o);
			    for (var i=0; i<names.length; i++) {
				(function() {
				    var name = names[i];
				    var g = function() {
					return H2L(o[name]);
				    };
				    var s = function(v) {
					o[name] = L2H(v);
				    };
				    native.Object_defineProperty(e, name, {get: g, set: s});
				})();
			    }
			}
		    }
		    return this[iname] = e;
		}
	    });
	}
    };
    // registerOPT(trusted_fn, declare_shared, "");
    descFFF.value = declare_public;native.Object_defineProperty(virtual, 'declare_public', descFFF);
    descFFF.value = declare_shared;native.Object_defineProperty(virtual, 'declare_shared', descFFF);

    var securePropertyc = function(o,s) {
	var t = typeof o;
	if (o == null) return true;
	else if (t !== "object" && t !== "function") return false;
	while (o !== null) {
	    if (native.Object_prototype_hasOwnProperty[icall](o, s))
		return true;
	    o = securePrototypeOf(o);
	    if (o === false) return false;
	}
	return true;
    };
    // creates an object of auxiliary functions to access a secure environment.
    var g = {
	newfn: newfn,
	load_check: load_check,
	store_check: store_check,
	mload_check: mload_check,
	load: GetValue,
	loadc: GetValuec,
	oloadc: Get,
	nloadc: nGet,
	mload: mGetValue,
	mloadc: mGetValuec,
	store: store,
	storec: storec,
	Put: Put,
	loadvar: loadvar,
	storevar: storevar,
	deletevar: deletevar,
	apply: native.apply,
	call: native.call,
	newcall: newcall,
	preop: preop,
	postop: postop,
	mpre_plus: mpre_plus,
	mpre_minus: mpre_minus,
	mpost_plus: mpost_plus,
	mpost_minus: mpost_minus,

	unary: unary,

	plusvv: plusvv,
	plusvn: plusvn,
	plusnv: plusnv,
	minusvv: minusvv,
	minusvn: minusvn,
	minusnv: minusnv,
	multvv: multvv,
	multvn: multvn,
	multnv: multnv,
	divvv: divvv,
	divvn: divvn,
	divnv: divnv,

	equalvv: equalvv,
	equalvn: equalvn,
	equalnv: equalnv,
	nequalvv: nequalvv,
	nequalvn: nequalvn,
	nequalnv: nequalnv,

	modvv: modvv,
	modvn: modvn,
	modnv: modnv,
	bandvv: bandvv,
	bandvn: bandvn,
	bandnv: bandnv,
	borvv: borvv,
	borvn: borvn,
	bornv: bornv,
	bpowvv: bpowvv,
	bpowvn: bpowvn,
	bpownv: bpownv,
	ltvv: ltvv,
	ltvn: ltvn,
	ltnv: ltnv,
	gtvv: gtvv,
	gtvn: gtvn,
	gtnv: gtnv,
	ltevv: ltevv,
	ltevn: ltevn,
	ltenv: ltenv,
	gtevv: gtevv,
	gtevn: gtevn,
	gtenv: gtenv,
	equalvv: equalvv,
	equalvn: equalvn,
	equalnv: equalnv,
	rshiftvv: rshiftvv,
	rshiftvn: rshiftvn,
	rshiftnv: rshiftnv,
	rushiftvv: rushiftvv,
	rushiftvn: rushiftvn,
	rushiftnv: rushiftnv,
	lshiftvv: lshiftvv,
	lshiftvn: lshiftvn,
	lshiftnv: lshiftnv,
	uplusv: uplusv,
	uminusv: uminusv,
	unotv: unotv,
	instanceofvv: instanceofvv,
	invv: invv,
	innv: innv,
	this: thisCheck,
	this_opt: this_opt,

	hasSecurePropertyc: securePropertyc,
	hasOwnProperty: native.Object_prototype_hasOwnProperty,
	Export: Export,
	ToNumber: ToNumber,
	ToString: ToString,
	initIterator: initIterator,
	accessors: initAccessors,
	safeException: safeException,
	getPrototypeOf: virtual.Object.getPrototypeOf
    };

    var profiling = "${bProfile}";
    var profile = {cnt: {}, time: {}};
    if (profiling == "true") {
//	for (var v in g) {
//	    var t = (function() {
//		var name = v;
//		var o = g[v];
//		return function() {
//		    if (profile.cnt[name] === undefined) {
//			profile.cnt[name] = 1n;
//			profile.time[name] = 0n;
//		    } else profile.cnt[name] += 1n;
//
//		    var hrstart = process.hrtime.bigint();
//		    var rtn = o.apply(this, arguments);
//		    var hrduration = process.hrtime.bigint() - hrstart;
//		    if (name !== "call" && name !== "apply") profile.time[name] += hrduration;
//
//		    return rtn;
//		};
//	    })();
//	    g[v] = t;
//	    install_call(t);
//	}
//	var show = function() {
//	    for (var v in profile.cnt) {
//       		console.info('%s %d ms called %d times %d', v, Number(profile.time[v]) / 1000, Number(profile.cnt[v]), Number(profile.time[v]) / Number(profile.cnt[v]));
//	    }
//	};
//	g.show = show;
    } else {
	native.Object_freeze(g);
    }

    // for internal use.
    var virtual_RegExp = virtual.RegExp;
    var virtual_RegExp_prototype_exec = virtual.RegExp.prototype.exec;
    install_call(virtual_RegExp_prototype_exec);
    var virtual_Object_getPrototypeOf = virtual.Object.getPrototypeOf;
    var virtual_Array_prototype = virtual.Array.prototype;
    var virtual_Boolean_prototype = virtual.Boolean.prototype;
//    var virtual_Date = virtual.Date;
    var virtual_Date_prototype = virtual.Date.prototype;
    var virtual_Error = virtual.Error;
    var virtual_Error_prototype = virtual.Error.prototype;
    var virtual_EvalError = virtual.EvalError;
    var virtual_Function_prototype = virtual.Function.prototype;
    var virtual_Number_prototype = virtual.Number.prototype;
    var virtual_Object_prototype = virtual.Object.prototype;
    var virtual_Object_defineProperties = virtual.Object.defineProperties;
    var virtual_RangeError = virtual.RangeError;
    var virtual_ReferenceError = virtual.ReferenceError;
    var virtual_RegExp_prototype = virtual.RegExp.prototype;
    var virtual_String_prototype = virtual.String.prototype;
    var virtual_SyntaxError = virtual.SyntaxError;
    var virtual_TypeError = virtual.TypeError;
    var virtual_URIError = virtual.URIError;
    
    if (!setproto_mode) {
	register(m_virt_proto, virtual_Function_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_Array_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_Boolean_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_String_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_Number_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_RegExp_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_Date_prototype, virtual_Object_prototype);
	register(m_virt_proto, virtual_Error_prototype, virtual_Object_prototype);

	if (DOM_mode) {
    	    var virtual_EventTarget_prototype = virtual.EventTarget.prototype;
    	    var virtual_XMLHttpRequestEventTarget_prototype = virtual.XMLHttpRequestEventTarget.prototype;
    	    var virtual_XMLHttpRequest_prototype = virtual.XMLHttpRequest.prototype;
    	    register(m_virt_proto, virtual_XMLHttpRequest_prototype, virtual_XMLHttpRequestEventTarget_prototype);
    	    register(m_virt_proto, virtual_XMLHttpRequestEventTarget_prototype, virtual_EventTarget_prototype);
    	    register(m_virt_proto, virtual_EventTarget_prototype, virtual_Object_prototype);
	}
    }

    // Node.js
    if (node_mode) {
	ToStringWrapper1(virtual, "require", L2H(require));
    }

    var exposed = function (s) { return g; };
    // registerOPT(trusted_fn, exposed, "");
    descFFF.value = exposed;
    native.Object.defineProperty(native.global, "_$_secret_env_$_", descFFF);

    // for debug
    // g.virtual = virtual;
    // Object.defineProperty(native.global, "_", {value: g, enumerable: false, configurable: false, writable: false});
    // Test mode.
    if (test) {
    	var testFns = ["ERROR", "$ERROR", "$FAIL", "$PRINT", "Test262Error", "fnGlobalObject", "runTestCase", "fnExists", "arrayContains", "dataPropertyAttributesAreCorrect", "accessorPropertyAttributesAreCorrect", "quit", "isEqual", "getPrecision", "$INCLUDE", "compareArray"];
    	var n;
    	for (var v in testFns) {
    	    n = testFns[v];
	    if (native.global[n] === undefined) log("* Warning: function "+n+" is missing");
	    else virtual[n] = register_function(native.global[n], n, false, 0);
    	}
    	virtual.fnGlobalObject = newfn(function() { return virtual; }, " ");
	if (native.global["print"] !== undefined) {
		virtual["print"] = (function() {
			var org = native.global["print"];
			return register_function(function () {
				org("secure start");
				org.apply(this, arguments);
				org("secure end");
			}, " "); })();
	}
    }
})(this);
} catch (e) { if (typeof e === 'object') warn(e.message); else warn(e); }
})();
