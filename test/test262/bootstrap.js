this.compareArray = function compareArray(aExpected, aActual) {
    if (aActual.length != aExpected.length) {
        return false;
    }

    aExpected.sort();
    aActual.sort();

    var s;
    for (var i = 0; i < aExpected.length; i++) {
        if (aActual[i] !== aExpected[i]) {
            return false;
        }
    }
    return true;
}
this.ToInteger = function ToInteger(p) {
  x = Number(p);

  if(isNaN(x)){
    return +0;
  }

  if((x === +0)
  || (x === -0)
  || (x === Number.POSITIVE_INFINITY)
  || (x === Number.NEGATIVE_INFINITY)){
     return x;
  }

  var sign = ( x < 0 ) ? -1 : 1;

  return (sign*Math.floor(Math.abs(x)));
}
this["$INCLUDE"] = function $INCLUDE() {}
var prec;
this["isEqual"] = function isEqual(num1, num2)
{
        if ((num1 === Infinity)&&(num2 === Infinity))
        {
                return(true);
        }
        if ((num1 === -Infinity)&&(num2 === -Infinity))
        {
                return(true);
        }
        prec = this.getPrecision(Math.min(Math.abs(num1), Math.abs(num2)));
        return(Math.abs(num1 - num2) <= prec);
        //return(num1 === num2);
}
this["getPrecision"] = function getPrecision(num)
{
	//TODO: Create a table of prec's,
	//      because using Math for testing Math isn't that correct.

	log2num = Math.log(Math.abs(num))/Math.LN2;
	pernum = Math.ceil(log2num);
	return(2 * Math.pow(2, -52 + pernum));
	//return(0);
}
this["ERROR"] = function ERROR( s ) { console.log( "Error: ", s ); this.quit( 1 ) }
this["$ERROR"] = function $ERROR( s ) { console.log( "Error: ", s ); this.quit( 1 ) }
this["$FAIL"] = function $FAIL( s ) { console.log( "Fail: ", s ); this.quit( 1 ) }
this["$PRINT"] = function $PRINT( s ) { /* console.log( s ); */ }
this["Test262Error"] = function Test262Error() { return false; }
this["fnGlobalObject"] = function fnGlobalObject() { return this; }
this["runTestCase"] = function runTestCase( proc ) { if( !proc() ) this.$ERROR( 'Test failed..' ); }
this["fnExists"] = function fnExists( proc ) { return typeof( proc ) === 'function'; }
this["compareArray"] = function compareArray( a1, a2 ) {
if( a1.length != a2.length ) return false;
for( var i = 0; i < a1.length; i++ ) {
  if( a1[ i ] != a2[ i ] ) return false;
}
return true; };
this["arrayContains"] = function arrayContains( a2, a1 ) {  for( var p in a1) {if (a2.indexOf( a1[ p ] ) < 0) return false;} return true;}
this["dataPropertyAttributesAreCorrect"] = function dataPropertyAttributesAreCorrect( obj, prop, val, writable, enumerable, configurable ) {
  var o = Object.getOwnPropertyDescriptor( obj, prop );
  var r = (((obj[prop] === val) || (isNaN(val) && isNaN(obj[prop]))) && o.writable === writable && o.enumerable === enumerable && o.configurable === configurable);
  if( !r ) {
    console.log( "prop=", obj[prop], "/", val );
    console.log( "write=", o.writable, "/", writable );
    console.log( "enum=", o.enumerable, "/", enumerable );
    console.log( "conf=", o.configurable, "/", configurable );
    return false;
  } else {
    return true;
  }}
this["accessorPropertyAttributesAreCorrect"] = function accessorPropertyAttributesAreCorrect( obj, prop, get, set, _, enumerable, configurable ) {
  var o = Object.getOwnPropertyDescriptor( obj, prop );
  var r = (get === o.get && o.set === set && o.enumerable === enumerable && o.configurable === configurable);
  if( !r ) {
    console.log( "get=", o.get, "/", get );
    console.log( "set=", o.set, "/", set );
    console.log( "enum=", o.enumerable, "/", enumerable );
    console.log( "conf=", o.configurable, "/", configurable );
    return false;
  } else {
  return true;
  }}; 
this["quit"] = function quit( code ) { throw code; };

