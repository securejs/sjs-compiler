#!/bin/bash

for v in `cat /tmp/fails.log`;do
	filename=$(basename -- "$v")
	b=$(grep -o "\"$filename\"" TODOLIST BLACKLIST)
	if [[ "$b" == "" ]]; then
		n=`cat $v bootstrap.js | node 2>&1 > /dev/null`
		err=$?
		n=$(djscompile -o $v 2>&1)
		n2=$((cat bootstrap.js;djscompile -a $v) | node 2>&1)
		nerr=$?
		n=$(printf '%s' "$n" | grep -o "ERROR:djscompiler does not support '.*'")
		n2=$(printf '%s' "$n2" | egrep -o "(Error: 'eval' is not allowed|Error: Accessing 'this'|TypeError: 'caller', 'callee', and 'arguments')")
		if [[ " $n" != " " ]];then 
			djsadd bl $v "$n"
		elif [[ " $n2" != " " ]];then
			djsadd bl $v "$n2"
		else
			echo skip $v - node[$err] djs[$nerr]
		fi;
	fi
done

#for v in `cat /tmp/fails.log`; do n=`(cat bootstrap.js;djscompile -o $v) 2>&1 | node 2>&1 > /dev/null`;nerr=$?;echo $v - $nerr;done
