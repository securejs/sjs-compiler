function bar() {

}

bar.prototype.show = function() {
	console.log("show!");
};

function foo() {
	this.x = 10;
	this.y = 20;
}

foo.prototype.play = function() {
	console.log("trusted code access: ", this.x, this.y);
};

module.exports = foo;

