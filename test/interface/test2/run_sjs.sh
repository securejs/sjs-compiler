if [ ! -f m.sjs.js ];then
	echo "* SecureJS compilation"
	djscompile -a -d m.js > m.sjs.js
fi

echo "* Original execution"
cat index.js | node

echo

echo "* SecureJS execution"
cat index.sjs.js | node
