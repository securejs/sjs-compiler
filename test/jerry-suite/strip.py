#!/usr/bin/python
import fileinput

mode=0
r=0
erc=0
for line in fileinput.input():
	c=line.strip()
	if c.endswith(":Start"):
		mode=1
		continue
	if c.endswith(":End"):
		mode=0
	if c.endswith("secure start"):
		mode=2
		r+=1
	if c.endswith("secure end"):
		r-=1
		if r==0:
			mode=1
		continue

	if c and (mode==1):
		print c
		erc+=1

if erc>0:
	raise Exception(erc)

