# SecureJS Compiler: Portable Memory Isolation in JavaScript

## Vagrant Setting
 We provide a virtual machine configuration to run SecureJS compiler.

### Prerequisites:
* Virtualbox 
* Vagrant (>= 2.2.14)

### To prepare a virtual machine:
All the configuration is already prepared in `Vagrantfile`. So, just run the following command in the root folder of this project.
```bash
$ vagrant up
```
Then, you can login to the machine:
```bash
$ vagrant ssh
```

## Manual installation
### Requirement
- hop compiler [link](https://github.com/manuel-serrano/hop.git)

### Environment variables
- `SJS_PATH`: A root path of this project.
- `HOPC_PATH`: A path for the hopc binary.

### Example:
```bash
 $ export SJS_PATH=$PWD
 $ export HOPC_PATH=$HOP_PROJECT/bin/hopc
 $ export PATH=$PATH:$PWD/bin
```

## How to run

### Compile 'test.js' file
```bash
 $ sjscompile test.js
```

### Compile 'test.js' file and present it with a bootstrap code for nodejs.
```bash
 $ sjscompile -a test.js
```
